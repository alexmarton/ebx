package nl.systemation.training.ebx.mdm.training.widget;

import com.orchestranetworks.ui.form.widget.UIWidgetFactory;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetFactorySetupContext;

public class UICheckedFactory implements UIWidgetFactory<UIChecked> {

    @Override
    public UIChecked newInstance(final WidgetFactoryContext context) {

        return new UIChecked(context);
    }

    @Override
    public void setup(WidgetFactorySetupContext widgetFactorySetupContext) {

    }
}
