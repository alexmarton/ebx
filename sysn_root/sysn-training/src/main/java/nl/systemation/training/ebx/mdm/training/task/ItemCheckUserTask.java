package nl.systemation.training.ebx.mdm.training.task;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.CreationWorkItemSpec;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.UserTaskCreationContext;
import com.orchestranetworks.workflow.UserTaskWorkItemCompletionContext;

public class ItemCheckUserTask extends UserTask {
	public static final String OWNER = "owner";

	@Override
	public void handleCreate(final UserTaskCreationContext context) throws OperationException {
		final String userIdentifier = context.getVariableString(OWNER);

		if (userIdentifier == null) {
			super.handleCreate(context);

			return;
		}

		final UserReference userReference = UserReference.forUser(userIdentifier);

		final CreationWorkItemSpec creationWorkItemSpec = CreationWorkItemSpec.forAllocation(userReference);

		context.createWorkItem(creationWorkItemSpec);
	}

	@Override
	public void handleWorkItemCompletion(final UserTaskWorkItemCompletionContext context) throws OperationException {
		final WorkItem workItem = context.getCompletedWorkItem();

		final UserReference userReference = workItem.getUserReference();

		final String userIdentifier = userReference.getUserId();

		context.setVariableString(OWNER, userIdentifier);

		super.handleWorkItemCompletion(context);
	}
}