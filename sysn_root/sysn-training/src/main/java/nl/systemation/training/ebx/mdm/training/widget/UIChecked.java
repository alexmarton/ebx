package nl.systemation.training.ebx.mdm.training.widget;

import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.ui.form.widget.UISimpleCustomWidget;
import com.orchestranetworks.ui.form.widget.WidgetDisplayContext;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetWriter;

public class UIChecked extends UISimpleCustomWidget {

    //Drawing CSS etc etc
    private static final String classFirst = "background-color: green; ";
    private static final String classSecond = "background-color: red;";

    public UIChecked(WidgetFactoryContext widgetFactoryContext) {
        super(widgetFactoryContext);
    }

    @Override
    public void write(WidgetWriter writer, WidgetDisplayContext context) throws SecurityException {
        // ADMINISTRATOR decide is available
        if (writer.getSession().isUserInRole(Profile.ADMINISTRATOR)) {
            if (context.isDisplayedInTable()) {
                // Part of html etc
                final String style = getStyle(context);
                writer.add("<span").addSafeAttribute("style", style).add(">");
                writer.addWidget(Path.SELF);
                writer.add("</span>");
            }
            if (context.isDisplayedInForm()) {
                writer.addWidget(Path.SELF);
            }
        }
    }

    // What shlould do TO DO
    private String getStyle(final WidgetDisplayContext context) {
        final ValueContext valueContext = context.getValueContext();
        final Boolean value = (Boolean) valueContext.getValue(Path.SELF);

        // Here you do stuff and stuff with the value returning a style
        if (value) return classFirst;

        return classSecond;
    }

}
