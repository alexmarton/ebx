package nl.systemation.training.ebx.mdm.training.trigger;

import com.onwbp.adaptation.*;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.addon.dama.ext.bean.DigitalAsset;
import com.orchestranetworks.addon.dama.ext.bean.DigitalAssetComponentContext;
import com.orchestranetworks.addon.dama.ext.bean.DigitalAssetSpec;
import com.orchestranetworks.addon.dama.ext.bean.GeneralDigitalAssetSpec;
import com.orchestranetworks.addon.dama.ext.drivemanager.DriveManager;
import com.orchestranetworks.addon.dama.ext.exception.DAMException;
import com.orchestranetworks.addon.dama.ext.resource.FileResource;
import com.orchestranetworks.addon.dama.externalmanagement.request.ExternalUploadAssetRequest;
import com.orchestranetworks.addon.dama.models.MediaType;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.*;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;
import nl.systemation.training.ebx.mdm.training.Paths;
import nl.systemation.training.ebx.mdm.training.service.DigitalAssetFileCreator;
import nl.systemation.training.ebx.mdm.training.service.Messages;
import nl.systemation.training.ebx.mdm.training.service.AssetFileDetail;

import java.io.File;
import java.math.BigDecimal;

public class ItemTrigger extends TableTrigger {

    DriveManager driveManager;

    @Override
    public void setup(final TriggerSetupContext context) {
        // Nothing to do
    }

    @Override
    public void handleBeforeCreate(final BeforeCreateOccurrenceContext occurrenceContext) throws OperationException {
        final ValueContextForUpdate valueContext = occurrenceContext.getOccurrenceContextForUpdate();


        final BigDecimal itemPrice = new BigDecimal("888");
        valueContext.setValue(itemPrice, Paths._Item._DefaultPrice);
    }

    @Override
    public void handleBeforeModify(final BeforeModifyOccurrenceContext occurrenceContext) throws OperationException {
        String driveId = "B4705E74-2EA3-42FD-B098-45C7EC55C190";
        String digitalAssetComponentId = "DAC_Icon_Drive_Item_pic";
        final AdaptationHome adaptationHome = occurrenceContext.getAdaptationHome();

        final ValueContextForUpdate valueContextForUpdate = occurrenceContext.getOccurrenceContextForUpdate();
        final Adaptation adaptation = occurrenceContext.getAdaptationOccurrence();
        final Session session = occurrenceContext.getSession();
        final Path pathMediaCreate = Path.parse("./pictureItem");

        final DigitalAssetComponentContext digitalAssetComponentContext
                = new DigitalAssetComponentContext(adaptation, pathMediaCreate, session);

        String uriToBeAttached = "nothingfk";
        final AssetFileDetail assetFileDetail = new AssetFileDetail();
        final DigitalAssetFileCreator digitalAssetFileCreator = new DigitalAssetFileCreator();

//        BufferedImage bImage = null;
        try {
            String pathLocal = "C://Users/alexm/uploads/img.jpg";
//            String uri = "https://www.w3schools.com/html/img_girl.jpg";
//            URL url = new URL(uri);
//            File file = new File(url.getFile());


//                File initialImage = new File("C://Users/Rou/Desktop/image.jpg");
            File file = new File("C://Users/alexm/uploads/img.jpg");
            final UserMessage message = Messages.get(Severity.ERROR, "TheStartDateAndTheEndDateMustBeOrdered");

            DigitalAsset digitalAsset = new DigitalAsset();

//            file = ImageIO.read(file);


            if (file.isFile()) {
                FileResource fileResource = new FileResource(file);
                ExternalUploadAssetRequest externalUploadAssetRequest =
                        new ExternalUploadAssetRequest(adaptationHome, session, driveId, digitalAssetComponentId);
                externalUploadAssetRequest.setFileResource(fileResource);

                GeneralDigitalAssetSpec digitalAssetSpec = new DigitalAssetSpec(fileResource);
                digitalAsset.setLabel(message);
                digitalAsset.setDescription(message);


                digitalAsset = assetFileDetail.create(digitalAssetSpec);

                System.out.println("File size create: "+digitalAsset.getFileSize());

                digitalAsset = digitalAssetFileCreator.upload(externalUploadAssetRequest.getFileResource());


                System.out.println("File size upload: "+digitalAsset.getFileSize());
                System.out.println("File: "+ file.getName());
                System.out.println("File resource: "+fileResource.getFile().getName());
                System.out.println("File external resource: "+externalUploadAssetRequest.getFileResource().getFile().getName());


//                uriToBeAttached = uploadImage.upload(externalUploadAssetRequest.getFileResource()).getKey().getPrimaryKey().toString();
                System.out.println("Is file");
            } else {
                System.out.println("Something is wrong");
            }


        } catch (DAMException e) {
            e.printStackTrace();
        }


        MediaType mediaType = new MediaType();
        mediaType.setAttachment(uriToBeAttached);


//        valueContextForUpdate.setValue(new BigDecimal("999"), Paths._Item._DefaultPrice);
        valueContextForUpdate.setValue(mediaType, digitalAssetComponentContext.getMediaPath());

    }

    @Override
    public void handleAfterDelete(final AfterDeleteOccurrenceContext occurrenceContext) throws OperationException {
        final RequestResult collectedInventories = collectInventories(occurrenceContext);

        final ProcedureContext procedureContext = occurrenceContext.getProcedureContext();

        deleteInventories(procedureContext, collectedInventories);
    }

    protected static RequestResult collectInventories(final AfterDeleteOccurrenceContext occurrenceContext) {
        final ValueContext valueContext = occurrenceContext.getOccurrenceContext();

        final int itemIdentifier = (Integer) valueContext.getValue(Paths._Item._Identifier);

        final String condition = Paths._Inventory._Item.format() + " = '" + itemIdentifier + "'";

        final Adaptation dataSet = valueContext.getAdaptationInstance();

        final AdaptationTable inventories = dataSet.getTable(Paths._Inventory.getPathInSchema());

        final RequestResult collectedInventories = inventories.createRequestResult(condition);

        return collectedInventories;
    }

    protected static void deleteInventories(final ProcedureContext procedureContext,
                                            final RequestResult collectedInventories) throws OperationException {
        try {
            for (Adaptation inventory; (inventory = collectedInventories.nextAdaptation()) != null; ) {
                final AdaptationName inventoryName = inventory.getAdaptationName();

                procedureContext.doDelete(inventoryName, false);
            }
        } finally {
            collectedInventories.close();
        }
    }


}
