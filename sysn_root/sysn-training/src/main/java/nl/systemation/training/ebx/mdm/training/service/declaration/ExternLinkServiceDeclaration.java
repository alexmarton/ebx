package nl.systemation.training.ebx.mdm.training.service.declaration;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.declaration.ActivationContextOnTableView;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import com.orchestranetworks.userservice.declaration.UserServicePropertiesDefinitionContext;
import com.orchestranetworks.userservice.declaration.WebComponentDeclarationContext;
import nl.systemation.training.ebx.mdm.training.service.ExternLinkService;
import nl.systemation.training.ebx.mdm.training.service.Messages;

public class ExternLinkServiceDeclaration implements UserServiceDeclaration.OnTableView {

    public static final ServiceKey SERVICE_KEY = ServiceKey.forName("ExternLink");

    @Override
    public ServiceKey getServiceKey() {
        return SERVICE_KEY;
    }

    @Override
    public UserService<TableViewEntitySelection> createUserService() {
        return new ExternLinkService();
    }

    @Override
    public void defineActivation(ActivationContextOnTableView context) {
    }

    @Override
    public void defineProperties(UserServicePropertiesDefinitionContext context) {
        final UserMessage message = Messages.get("ExternLink");
        context.setLabel(message);
    }

    @Override
    public void declareWebComponent(WebComponentDeclarationContext context) {
    }
}
