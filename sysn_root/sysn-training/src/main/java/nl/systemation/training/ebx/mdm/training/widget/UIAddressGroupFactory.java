package nl.systemation.training.ebx.mdm.training.widget;

import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.ui.form.widget.UIWidgetFactory;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetFactorySetupContext;

public class UIAddressGroupFactory implements UIWidgetFactory<UIAddressGroup> {
	public static final String ADDRESS_TYPE = "Address";

	@Override
	public void setup(final WidgetFactorySetupContext context) {
		final SchemaNode schemaNode = context.getSchemaNode();

		final SchemaTypeName schemaTypeName = schemaNode.getXsTypeName();

		if (schemaTypeName.getNameWithoutPrefix().equals(ADDRESS_TYPE)) {
			return;
		}

		final UserMessage message = Messages.get(Severity.ERROR, "TheAttributeMustHaveTheAddressType");

		context.addMessage(message);
	}

	@Override
	public UIAddressGroup newInstance(final WidgetFactoryContext context) {
		return new UIAddressGroup(context);
	}
}
