package nl.systemation.training.ebx.mdm.training.service;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.UIButtonSpec;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserServiceDisplayConfigurator;
import com.orchestranetworks.userservice.UserServiceEventContext;
import com.orchestranetworks.userservice.UserServiceEventOutcome;
import com.orchestranetworks.userservice.UserServicePaneContext;
import com.orchestranetworks.userservice.UserServicePaneWriter;
import com.orchestranetworks.userservice.UserServiceSetupDisplayContext;

public class DisplayFilterStep implements DisplayStep {

	@Override
	public void setupDisplay(final UserServiceSetupDisplayContext<TableViewEntitySelection> context,
			final UserServiceDisplayConfigurator config) {
		config.setContent(this::writeContent);

		{
			final UIButtonSpec cancelButtonSpec = config.newCancelButton();

			config.setLeftButtons(cancelButtonSpec);
		}

		{
			final UIButtonSpecNavigation nextButtonSpec = config.newNextButton(this::onNextPressed);

			nextButtonSpec.setDefaultButton(true);
			config.setRightButtons(nextButtonSpec);
		}
	}

	private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
		writer.setCurrentObject(FilterPaths._objectKey);

		{
			final UserMessage message = Messages.get("TheEmptyInventoriesMatchingTheseCriteriaWillBePurged");
			final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

			writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
			writer.addUILabel(labelSpec);
			writer.add("</div>");
		}

		writer.startTableFormRow();
		writer.addFormRow(FilterPaths._startDate);
		writer.addFormRow(FilterPaths._endDate);
		writer.endTableFormRow();
	}

	private UserServiceEventOutcome onNextPressed(final UserServiceEventContext context) {
		return EventOutcome.DISPLAY_CONFIRMATION;
	}
}
