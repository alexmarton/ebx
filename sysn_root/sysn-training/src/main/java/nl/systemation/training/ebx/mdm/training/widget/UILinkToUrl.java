package nl.systemation.training.ebx.mdm.training.widget;

import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.form.widget.UISimpleCustomWidget;
import com.orchestranetworks.ui.form.widget.WidgetDisplayContext;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetWriter;

public class UILinkToUrl extends UISimpleCustomWidget {


    public UILinkToUrl(WidgetFactoryContext widgetFactoryContext) {
        super(widgetFactoryContext);
    }

    @Override
    public void write(WidgetWriter widgetWriter, WidgetDisplayContext widgetDisplayContext) {
        if (widgetDisplayContext.isDisplayedInTable()) {

            final ValueContext valueContext = widgetDisplayContext.getValueContext();
            final Object value = valueContext.getValue(Path.SELF);
            widgetWriter.add("<a target='_blank' ");
            widgetWriter.addSafeAttribute("href", value.toString());
            widgetWriter.add(">Website</a");
        }
        if (widgetDisplayContext.isDisplayedInForm()) {
            widgetWriter.addWidget(Path.SELF);
        }
    }
}