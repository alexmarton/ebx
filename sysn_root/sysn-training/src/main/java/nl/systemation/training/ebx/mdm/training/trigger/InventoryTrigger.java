package nl.systemation.training.ebx.mdm.training.trigger;

import java.util.Date;

import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;
import nl.systemation.training.ebx.mdm.training.Paths;

public class InventoryTrigger extends TableTrigger {

	@Override
	public void setup(final TriggerSetupContext context) {
		// Nothing to do
	}

	@Override
	public void handleBeforeCreate(final BeforeCreateOccurrenceContext occurrenceContext) throws OperationException {
		final ValueContextForUpdate valueContext = occurrenceContext.getOccurrenceContextForUpdate();

		valueContext.setValue(new Date(), Paths._Inventory._Modified);
	}

	@Override
	public void handleBeforeModify(final BeforeModifyOccurrenceContext occurrenceContext) throws OperationException {
		final ValueContextForUpdate valueContext = occurrenceContext.getOccurrenceContextForUpdate();

		valueContext.setValue(new Date(), Paths._Inventory._Modified);
	}
}