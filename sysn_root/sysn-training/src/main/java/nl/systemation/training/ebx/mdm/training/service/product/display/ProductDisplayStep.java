package nl.systemation.training.ebx.mdm.training.service.product.display;

import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserServiceDisplayConfigurator;
import com.orchestranetworks.userservice.UserServiceEventOutcome;
import com.orchestranetworks.userservice.UserServiceSetupDisplayContext;

public interface ProductDisplayStep {

	public enum EventOutcome implements UserServiceEventOutcome {
		DISPLAY_FILTER, DISPLAY_CONFIRMATION, DISPLAY_RESULT
	};

	public void setupDisplay(final UserServiceSetupDisplayContext<TableViewEntitySelection> context,
                             final UserServiceDisplayConfigurator config);
}
