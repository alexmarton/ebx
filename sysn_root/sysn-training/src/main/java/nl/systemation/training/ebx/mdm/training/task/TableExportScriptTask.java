package nl.systemation.training.ebx.mdm.training.task;

import java.io.File;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ExportSpec;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class TableExportScriptTask extends ScriptTaskBean {
	private String dataSpaceName;

	private String dataSetName;

	private String tablePath;

	private String filePath;

	public final void setDataSpaceName(final String dataSpaceName) {
		this.dataSpaceName = dataSpaceName;
	}

	public final void setDataSetName(final String dataSetName) {
		this.dataSetName = dataSetName;
	}

	public final void setTablePath(final String tablePath) {
		this.tablePath = tablePath;
	}

	public final void setFilePath(final String filePath) {
		this.filePath = filePath;
	}

	@Override
	public void executeScript(final ScriptTaskBeanContext context) throws OperationException {
		final Repository repository = context.getRepository();

		final AdaptationHome dataSpace = toDataSpace(repository, this.dataSpaceName);

		final Adaptation dataSet = toDataSet(dataSpace, this.dataSetName);

		final AdaptationTable table = toTable(dataSet, this.tablePath);

		final File file = toFile(this.filePath);

		final ExportSpec exportSpec = createExportSpec(table, file);

		executeScript(context, dataSpace, exportSpec);
	}

	private static AdaptationHome toDataSpace(final Repository repository, final String dataSpaceName)
			throws OperationException {
		try {
			final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpaceName);

			final AdaptationHome dataSpace = repository.lookupHome(dataSpaceKey);

			if (dataSpace == null) {
				throw new IllegalArgumentException();
			}

			return dataSpace;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedDataSpaceNameIsIncorrect{0}",
					dataSpaceName);

			throw OperationException.createError(message);
		}
	}

	private static Adaptation toDataSet(final AdaptationHome dataSpace, final String dataSetName)
			throws OperationException {
		try {
			final AdaptationName dataSetKey = AdaptationName.forName(dataSetName);

			final Adaptation dataSet = dataSpace.findAdaptationOrNull(dataSetKey);

			if (dataSet == null) {
				throw new IllegalArgumentException();
			}

			return dataSet;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedDataSetNameIsIncorrect{0}",
					dataSetName);

			throw OperationException.createError(message);
		}
	}

	private static AdaptationTable toTable(final Adaptation dataSet, final String tablePath) throws OperationException {
		try {
			final AdaptationTable table = dataSet.getTable(Path.parse(tablePath));

			return table;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedTablePathIsIncorrect{0}", tablePath);

			throw OperationException.createError(message);
		}
	}

	private static File toFile(final String filePath) throws OperationException {
		try {
			final File file = new File(filePath);

			file.getCanonicalPath();

			return file;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedFilePathIsIncorrect{0}", filePath);

			throw OperationException.createError(message);
		}
	}

	protected ExportSpec createExportSpec(final AdaptationTable table, final File file) {
		final ExportSpec exportSpec = new ExportSpec();

		final Request request = table.createRequest();

		exportSpec.setRequest(request);
		exportSpec.setCheckAccessRules(false);
		exportSpec.setContentIndented(true);
		exportSpec.setIncludesTechnicalData(false);
		exportSpec.setDestinationFile(file);

		return exportSpec;
	}

	private static void executeScript(final ScriptTaskBeanContext taskContext, final AdaptationHome dataSpace,
			final ExportSpec exportSpec) throws OperationException {
		final Session session = taskContext.getSession();

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataSpace);

		final ProcedureResult result = service.execute(new Procedure() {
			public void execute(final ProcedureContext procedureContext) throws Exception {
				procedureContext.doExport(exportSpec);
			}
		});

		if (result.hasFailed()) {
			final OperationException exception = result.getException();

			throw exception;
		}
	}
}