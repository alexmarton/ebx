package nl.systemation.training.ebx.mdm.training.service.product;

import com.ebx5.frameworkrt.procedure.CreateRecordProcedure;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.Nomenclature;
import com.onwbp.base.text.UserMessage;
import com.onwbp.com.google.gson.JsonArray;
import com.onwbp.com.google.gson.JsonParser;
import com.onwbp.org.jdom.Content;
import com.onwbp.org.jdom.Text;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.schema.dynamic.BeanDefinition;
import com.orchestranetworks.schema.dynamic.BeanElement;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;
import nl.systemation.training.ebx.mdm.commons.datamodel.Item;
import nl.systemation.training.ebx.mdm.commons.datamodel.Store;
import nl.systemation.training.ebx.mdm.training.Paths;
import nl.systemation.training.ebx.mdm.training.service.FilterConstraint;
import nl.systemation.training.ebx.mdm.training.service.FilterPaths;
import nl.systemation.training.ebx.mdm.training.service.Messages;
import nl.systemation.training.ebx.mdm.training.service.product.display.ProductDisplayConfirmationStep;
import nl.systemation.training.ebx.mdm.training.service.product.display.ProductDisplayFilterStep;
import nl.systemation.training.ebx.mdm.training.service.product.display.ProductDisplayResultStep;
import nl.systemation.training.ebx.mdm.training.service.product.display.ProductDisplayStep;
import nl.systemation.training.ebx.mdm.training.service.product.documentation.DocumentationConstraint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProductService implements UserService<TableViewEntitySelection> {

    private static final Format DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private ProductDisplayStep currentStep;

    public ProductService() {
        this.currentStep = new ProductDisplayFilterStep();
    }

    @Override
    public void setupObjectContext(UserServiceSetupObjectContext<TableViewEntitySelection> context, UserServiceObjectContextBuilder builder) {
        System.out.println("We are on setupObjectContext");

        if (!context.isInitialDisplay()) {
            return;
        }

        final BeanDefinition definition = builder.createBeanDefinition();

        {
            final BeanElement element = definition.createElement(FilterPaths._type, SchemaTypeName.XS_STRING);
            final UserMessage message = Messages.get("type");


            Nomenclature<Object> nomenclatureItem = new Nomenclature<>();
            nomenclatureItem.addItemValue("H", "Hypermarket");
            nomenclatureItem.addItemValue("S", "Supermarket");
            nomenclatureItem.addItemValue("L", "Localmarket");

            element.setLabel(message);
            element.setMinOccurs(1);
            element.setMaxOccurs(1);
            element.addFacetEnumeration(nomenclatureItem);
            element.addFacetConstraint(DocumentationConstraint.class);
        }

        {
            final BeanElement element = definition.createElement(FilterPaths._startDate, SchemaTypeName.XS_DATETIME);
            final UserMessage message = Messages.get("StartDate");

            element.setLabel(message);
            element.setMinOccurs(1);
            element.setMaxOccurs(1);
            element.setDefaultValue(new Date());
            element.addFacetConstraint(FilterConstraint.class);
        }

        {
            final BeanElement element = definition.createElement(FilterPaths._endDate, SchemaTypeName.XS_DATETIME);
            final UserMessage message = Messages.get("EndDate");

            element.setLabel(message);
            element.setMinOccurs(1);
            element.setMaxOccurs(1);
            element.setDefaultValue(new Date());
            element.addFacetConstraint(FilterConstraint.class);
        }

        builder.registerBean(FilterPaths._objectKey, definition);

    }

    @Override
    public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> context, UserServiceDisplayConfigurator config) {
        System.out.println("We are on setupDisplay");
        this.currentStep.setupDisplay(context, config);
    }

    @Override
    public void validate(UserServiceValidateContext<TableViewEntitySelection> userServiceValidateContext) {
        // Nothing to validate
    }

    @Override
    public UserServiceEventOutcome processEventOutcome(UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context,
                                                       UserServiceEventOutcome eventOutcome) {
        System.out.println("We are on UserServiceEventOutcome processEventOutcome");
        if (!(eventOutcome instanceof ProductDisplayStep.EventOutcome)) {
            return eventOutcome;
        }

        switch ((ProductDisplayStep.EventOutcome) eventOutcome) {

            case DISPLAY_FILTER:
                this.currentStep = new ProductDisplayFilterStep();
                return null;

            case DISPLAY_CONFIRMATION:
                final int inventoriesCount = requestService().size();
                this.currentStep = new ProductDisplayConfirmationStep(inventoriesCount);
                return null;

            case DISPLAY_RESULT:
                final ProcedureResult result = updateData(context);
                this.currentStep = new ProductDisplayResultStep(result);
                return null;

            default:
                return null;
        }


    }

    private static RequestResult createRequestResult(
            final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {

        final TableViewEntitySelection selection = context.getEntitySelection();
        final AdaptationTable products = selection.getTable();

        // Conditions Interogations
        final String typePath = Paths._Store._Type.format();
        final String modifiedDate = Paths._Store._Modified.format();
        final ValueContextForValidation valueContext = context.getValueContext(FilterPaths._objectKey);

        // Create Interogation paramter
        final String type = valueContext.getValue(FilterPaths._type).toString();
        final String startDate = DATE_FORMAT.format(valueContext.getValue(FilterPaths._startDate));
        final String endDate = DATE_FORMAT.format(valueContext.getValue(FilterPaths._endDate));

        // Create condition
        System.out.println("TypePath: " + typePath);
        final String condition = typePath + "=\"" + type + "\" and date-greater-than(" + modifiedDate + ",'" + startDate
                + "') and date-less-than(" + modifiedDate + ",'" + endDate + "')";
        System.out.println("condition: " + condition);

        /*
         *
         * */


        final RequestResult result = products.createRequestResult(condition);
//        final RequestResult result = requestService();
        return result;
    }


    private static void updateData(final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {

        final AdaptationTable adaptationTable = context.getEntitySelection().getTable();
        final Adaptation adaptation = adaptationTable.getContainerAdaptation();
        final AdaptationHome adaptationHome = adaptation.getHome();

        final Store storeCreate = new Store(context.getEntitySelection().getDataset());
        storeCreate.set_Name("Testing Backend");

        System.out.println("String Value Store: "+storeCreate.get_Name());


        try {
            final Adaptation storeAdaptation = storeCreate.create(context.getSession(),adaptationHome);
//            final ValueContext valueContext = storeAdaptation.createValueContext();
//            final SchemaNode schemaNode = valueContext.getNode(Path.SELF);
//            final SessionPermissions sessionPermissions = context.getSession().getPermissions();
//            sessionPermissions.getNodeAccessPermission(schemaNode, adaptation);
//            schemaNode.createNewOccurrence();
        }catch (OperationException ep){
            ep.printStackTrace();
        }
    }


    public static JsonArray requestService() {

        //Pre config
        String staticUrl = "http://localhost:8091/api/stores";
        JsonArray jsonArray = new JsonArray();

        try {
            URL url = new URL(staticUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoInput(true);
            connection.setDoOutput(true);


            int status = connection.getResponseCode();
            if (status == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                jsonArray = new JsonParser().parse(String.valueOf(content)).getAsJsonArray();
                in.close();
                return jsonArray;
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
        return jsonArray;
    }


//    private static ProcedureResult purgeInventories(
//            final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {
//        final RequestResult result = createRequestResult(context);
//
//        final Procedure procedure = new Procedure() {
//            public void execute(final ProcedureContext procedureContext) throws Exception {
//                try {
//                    for (Adaptation inventory; (inventory = result.nextAdaptation()) != null; ) {
//                        final AdaptationName inventoryName = inventory.getAdaptationName();
//
//                        procedureContext.doDelete(inventoryName, false);
//                    }
//                } finally {
//                    result.close();
//                }
//            }
//        };
//
//        final UserServiceTransaction transaction = context.createTransaction();
//        transaction.add(procedure);
//        return transaction.execute();
//    }

}
