package nl.systemation.training.ebx.mdm.training.task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;
import com.orchestranetworks.workflow.ProcessInstance;
import com.orchestranetworks.workflow.ProcessInstanceHistory;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.ProcessInstanceStep;
import com.orchestranetworks.workflow.UserTask.WorkItem;
import com.orchestranetworks.workflow.WorkflowEngine;

/**
 * Indicates whether the last user tasks preceding a condition have all been
 * accepted. <br>
 * Considers recursively the latest of these possible steps of the current
 * process instance:
 * <ul>
 * <li>The last user task or</li>
 * <li>The last invocation of sub-processes containing user tasks.</li>
 * </ul>
 */
public class LastUserTasksAcceptedCondition extends ConditionBean {

	/**
	 * Indicates whether the last user tasks preceding this condition have all
	 * been accepted. <br>
	 * Calls <code>lastUserTasksAccepted</code> providing the current process
	 * instance.
	 * 
	 * @param context
	 *            the context
	 * @return <code>true</code> if all accepted; <code>false</code> otherwise
	 * 
	 * @throws OperationException
	 *             if the history of a process instance cannot be accessed
	 */
	@Override
	public boolean evaluateCondition(final ConditionBeanContext context) throws OperationException {
		final Repository repository = context.getRepository();

		final WorkflowEngine workflowEngine = WorkflowEngine.getFromRepository(repository, null);

		final ProcessInstance processInstance = context.getProcessInstance();

		final Boolean lastUserTasksAccepted = lastUserTasksAccepted(workflowEngine, processInstance);

		return Boolean.TRUE.equals(lastUserTasksAccepted);
	}

	/**
	 * Indicates whether the last user tasks of the provided process instance
	 * have all been accepted. <br>
	 * Calls itself recursively for each sub-process instance found.
	 * 
	 * @param workflowEngine
	 *            the workflow engine
	 * @param processInstance
	 *            the process instance
	 * @return <code>true</code> if all accepted; <code>false</code> if any
	 *         rejected; <code>null</code> otherwise
	 * @throws OperationException
	 *             if the history of a process instance cannot be accessed
	 */
	@SuppressWarnings("unchecked")
	private Boolean lastUserTasksAccepted(final WorkflowEngine workflowEngine, final ProcessInstance processInstance)
			throws OperationException {
		final ProcessInstanceKey processInstanceKey = processInstance.getProcessInstanceKey();

		final ProcessInstanceHistory processInstanceHistory = workflowEngine
				.getProcessInstanceHistory(processInstanceKey);
		final List<ProcessInstanceStep> processInstanceSteps = new ArrayList<ProcessInstanceStep>(
				processInstanceHistory.getSteps());

		Collections.reverse(processInstanceSteps);

		for (final ProcessInstanceStep processInstanceStep : processInstanceSteps) {
			if (processInstanceStep.isUserTask()) {
				final List<WorkItem> workItems = processInstanceStep.getUserTaskWorkItems();

				for (final WorkItem workItem : workItems) {
					if (workItem.isAccepted() == false) {
						return false;
					}
				}

				return true;
			}

			if (processInstanceStep.isSubWorkflowsInvocation()) {
				final List<ProcessInstanceKey> subProcessInstanceKeys = processInstanceStep.getSubWorkflowsKeys();

				boolean constainsUserTask = false;

				for (final ProcessInstanceKey subProcessInstanceKey : subProcessInstanceKeys) {
					final ProcessInstance subProcessInstance = workflowEngine.getProcessInstance(subProcessInstanceKey);

					final Boolean lastUserTasksAccepted = lastUserTasksAccepted(workflowEngine, subProcessInstance);

					if (Boolean.FALSE.equals(lastUserTasksAccepted)) {
						return false;
					}

					if (lastUserTasksAccepted != null) {
						constainsUserTask = true;
					}
				}

				if (constainsUserTask == true) {
					return true;
				}
			}
		}

		return null;
	}
}
