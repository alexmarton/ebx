package nl.systemation.training.ebx.mdm.training.widget;

import java.util.HashMap;
import java.util.Map;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.base.Size;
import com.orchestranetworks.ui.form.widget.UISimpleCustomWidget;
import com.orchestranetworks.ui.form.widget.UITextBox;
import com.orchestranetworks.ui.form.widget.WidgetDisplayContext;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetWriter;

public class UIAddressGroup extends UISimpleCustomWidget {

	private static final Map<String, String> FIELD_TO_STYLE = new HashMap<String, String>() {
		private static final long serialVersionUID = -529169199186746031L;

		{
			put("street", "");
			put("city", "margin-left:10px");
			put("state", "margin-left:10px");
			put("postcode", "margin-left:10px");
			put("country", "margin-left:10px");
		}
	};

	private static final Map<String, String> FIELD_TO_LABEL = new HashMap<String, String>() {
		private static final long serialVersionUID = -2092652323906522589L;

		{
			put("street", "Street");
			put("city", "City");
			put("state", "State");
			put("postcode", "Postcode");
			put("country", "Country");
		}
	};

	private static final Map<String, Size> FIELD_TO_WIDTH = new HashMap<String, Size>() {
		private static final long serialVersionUID = -2092652323906522589L;

		{
			put("street", new Size(200, Size.Unit.PIXEL));
			put("city", new Size(100, Size.Unit.PIXEL));
			put("state", new Size(50, Size.Unit.PIXEL));
			put("postcode", new Size(50, Size.Unit.PIXEL));
			put("country", new Size(100, Size.Unit.PIXEL));
		}
	};

	public UIAddressGroup(final WidgetFactoryContext context) {
		super(context);
	}

	@Override
	public void write(final WidgetWriter writer, final WidgetDisplayContext context) {
		if (isEditorDisabled()) {
			{
				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("street")).add(">");
				writer.addWidget(Path.parse("street"));
				writer.add("</span>");
				writer.add(",");
			}

			{
				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("city")).add(">");
				writer.addWidget(Path.parse("city"));
				writer.add("</span>");
				writer.add(",");
			}

			{
				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("state")).add(">");
				writer.addWidget(Path.parse("state"));
				writer.add("</span>");
				writer.add(",");
			}

			{
				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("postcode")).add(">");
				writer.addWidget(Path.parse("postcode"));
				writer.add("</span>");
				writer.add(",");
			}

			{
				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("country")).add(">");
				writer.addWidget(Path.parse("country"));
				writer.add("</span>");
			}
		} else {
			{
				final UITextBox textBox = writer.newTextBox(Path.parse("street"));

				textBox.setWidth(FIELD_TO_WIDTH.get("street"));

				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("street")).add(">");
				writer.add(Messages.get(FIELD_TO_LABEL.get("street")));
				writer.addWidget(textBox);
				writer.add("</span>");
			}

			{
				final UITextBox textBox = writer.newTextBox(Path.parse("city"));

				textBox.setWidth(FIELD_TO_WIDTH.get("city"));

				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("city")).add(">");
				writer.add(Messages.get(FIELD_TO_LABEL.get("city")));
				writer.addWidget(textBox);
				writer.add("</span>");
			}

			{
				final UITextBox textBox = writer.newTextBox(Path.parse("state"));

				textBox.setWidth(FIELD_TO_WIDTH.get("state"));

				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("state")).add(">");
				writer.add(Messages.get(FIELD_TO_LABEL.get("state")));
				writer.addWidget(textBox);
				writer.add("</span>");
			}

			{
				final UITextBox textBox = writer.newTextBox(Path.parse("postcode"));

				textBox.setWidth(FIELD_TO_WIDTH.get("postcode"));

				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("postcode")).add(">");
				writer.add(Messages.get(FIELD_TO_LABEL.get("postcode")));
				writer.addWidget(textBox);
				writer.add("</span>");
			}

			{
				final UITextBox textBox = writer.newTextBox(Path.parse("country"));

				textBox.setWidth(FIELD_TO_WIDTH.get("country"));

				writer.add("<span").addSafeAttribute("style", FIELD_TO_STYLE.get("country")).add(">");
				writer.add(Messages.get(FIELD_TO_LABEL.get("country")));
				writer.addWidget(textBox);
				writer.add("</span>");
			}
		}
	}
}
