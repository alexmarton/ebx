package nl.systemation.training.ebx.mdm.training.task;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.CreationWorkItemSpec;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.UserTaskCreationContext;

public class ItemCorrectionUserTask extends UserTask {
	public static final String STEWARD = "steward";

	@Override
	public void handleCreate(final UserTaskCreationContext context) throws OperationException {
		final String userIdentifier = context.getVariableString(STEWARD);

		final UserReference userReference = UserReference.forUser(userIdentifier);

		final CreationWorkItemSpec creationWorkItemSpec = CreationWorkItemSpec.forAllocation(userReference);

		context.createWorkItem(creationWorkItemSpec);
	}
}
