package nl.systemation.training.ebx.mdm.training.filter;

import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.TableRefFilter;
import com.orchestranetworks.schema.TableRefFilterContext;
import nl.systemation.training.ebx.mdm.training.Paths;

public class ItemCategoryFilter implements TableRefFilter {

	@Override
	public void setup(final TableRefFilterContext context) {
		final SchemaNode itemCategoryNode = context.getSchemaNode();

		final SchemaNode categoriesNode = itemCategoryNode.getNode(Paths._Category.getPathInSchema());

		context.addDependencyToInsertDeleteAndModify(categoriesNode);
	}

	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext context) throws InvalidSchemaException {
		final String message = Messages.get(locale, "TheCategoryMustBeALeafCategory");

		return message;
	}

	@Override
	public boolean accept(final Adaptation adaptation, final ValueContext context) {
		final int categoryIdentifier = adaptation.get_int(Paths._Category._Identifier);

		final Adaptation dataSet = context.getAdaptationInstance();

		final AdaptationTable categories = dataSet.getTable(Paths._Category.getPathInSchema());

		final String condition = Paths._Category._Parent.format() + " = '" + categoryIdentifier + "'";

		final RequestResult result = categories.createRequestResult(condition);

		return result.isEmpty();
	}
}