package nl.systemation.training.ebx.mdm.training.directory;

import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.service.directory.Directory;
import com.orchestranetworks.service.directory.DirectoryFactory;

public class GoogleDirectoryFactory extends DirectoryFactory {

    @Override
    public Directory createDirectory(AdaptationHome adaptationHome) throws Exception {
        return GoogleDirectory.getInstance(adaptationHome);
    }
}
