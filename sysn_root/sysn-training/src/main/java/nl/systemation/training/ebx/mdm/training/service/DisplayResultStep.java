package nl.systemation.training.ebx.mdm.training.service;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserServiceDisplayConfigurator;
import com.orchestranetworks.userservice.UserServicePaneContext;
import com.orchestranetworks.userservice.UserServicePaneWriter;
import com.orchestranetworks.userservice.UserServiceSetupDisplayContext;

public class DisplayResultStep implements DisplayStep {
	private final ProcedureResult result;

	public DisplayResultStep(final ProcedureResult result) {
		this.result = result;
	}

	@Override
	public void setupDisplay(final UserServiceSetupDisplayContext<TableViewEntitySelection> context,
			final UserServiceDisplayConfigurator config) {
		config.setContent(this::writeContent);

		{
			final UIButtonSpecNavigation closeButtonSpec = config.newCloseButton();

			closeButtonSpec.setDefaultButton(true);
			config.setRightButtons(closeButtonSpec);
		}
	}

	private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
		writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");

		if (result.hasFailed()) {
			final UserMessage message = Messages.get("ThePurgeOfTheEmptyInventoriesHasFailed");
			final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

			writer.addUILabel(labelSpec);
		} else {
			final UserMessage message = Messages.get("ThePurgeOfTheEmptyInventoriesHasSucceeded");
			final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

			writer.addUILabel(labelSpec);
		}

		writer.add("</div>");
	}
}
