package nl.systemation.training.ebx.mdm.training.directory;

import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryDefault;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class GoogleDirectory extends DirectoryDefault {

    private static GoogleDirectory googleDirectory;
    private String clientId = "123456";
    private String clientSecret = "secret";
    private String urlRequest = "http://localhost:8090/api/inventories";

    protected GoogleDirectory(AdaptationHome adaptationHome) {
        super(adaptationHome);
    }

    public static GoogleDirectory getInstance(AdaptationHome adaptationHome) {
        if (googleDirectory == null) googleDirectory = new GoogleDirectory(adaptationHome);
        return googleDirectory;
    }


    @Override
    public UserReference authenticateUserFromLoginPassword(String user, String password) {
      UserReference userReference = UserReference.forUser("admin");

        URL url = null;
        try {
            url = new URL(this.urlRequest);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            connection.setRequestProperty("client_id", clientId);
            connection.setRequestProperty("client_secret", clientSecret);
            connection.setRequestProperty("Content-Type", "application/json");
            int status = connection.getResponseCode();
            if(status == 200){
                System.out.println("Good Test");
            }else{
                System.out.println("Something is wrong");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return userReference;

    }
}

