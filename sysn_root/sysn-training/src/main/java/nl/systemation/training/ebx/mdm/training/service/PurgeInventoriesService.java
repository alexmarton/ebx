package nl.systemation.training.ebx.mdm.training.service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.schema.dynamic.BeanDefinition;
import com.orchestranetworks.schema.dynamic.BeanElement;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import nl.systemation.training.ebx.mdm.training.Paths;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.UserServiceDisplayConfigurator;
import com.orchestranetworks.userservice.UserServiceEventOutcome;
import com.orchestranetworks.userservice.UserServiceObjectContextBuilder;
import com.orchestranetworks.userservice.UserServiceProcessEventOutcomeContext;
import com.orchestranetworks.userservice.UserServiceSetupDisplayContext;
import com.orchestranetworks.userservice.UserServiceSetupObjectContext;
import com.orchestranetworks.userservice.UserServiceTransaction;
import com.orchestranetworks.userservice.UserServiceValidateContext;

public class PurgeInventoriesService implements UserService<TableViewEntitySelection> {
	private static final Format DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

	private DisplayStep currentStep;

	public PurgeInventoriesService() {
		this.currentStep = new DisplayFilterStep();
	}

	@Override
	public void setupObjectContext(final UserServiceSetupObjectContext<TableViewEntitySelection> context,
			final UserServiceObjectContextBuilder builder) {
		if (!context.isInitialDisplay()) {
			return;
		}

		final BeanDefinition definition = builder.createBeanDefinition();

		{
			final BeanElement element = definition.createElement(FilterPaths._startDate, SchemaTypeName.XS_DATETIME);
			final UserMessage message = Messages.get("StartDate");

			element.setLabel(message);
			element.setMinOccurs(1);
			element.setMaxOccurs(1);
			element.setDefaultValue(new Date());
			element.addFacetConstraint(FilterConstraint.class);
		}

		{
			final BeanElement element = definition.createElement(FilterPaths._endDate, SchemaTypeName.XS_DATETIME);
			final UserMessage message = Messages.get("EndDate");

			element.setLabel(message);
			element.setMinOccurs(1);
			element.setMaxOccurs(1);
			element.setDefaultValue(new Date());
			element.addFacetConstraint(FilterConstraint.class);
		}

		builder.registerBean(FilterPaths._objectKey, definition);
	}

	@Override
	public void setupDisplay(final UserServiceSetupDisplayContext<TableViewEntitySelection> context,
			final UserServiceDisplayConfigurator config) {
		this.currentStep.setupDisplay(context, config);
	}

	@Override
	public void validate(final UserServiceValidateContext<TableViewEntitySelection> context) {
		// Nothing to do
	}

	@Override
	public UserServiceEventOutcome processEventOutcome(
			final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context,
			final UserServiceEventOutcome eventOutcome) {
		if (!(eventOutcome instanceof DisplayStep.EventOutcome)) {
			return eventOutcome;
		}

		switch ((DisplayStep.EventOutcome) eventOutcome) {

		case DISPLAY_FILTER:
			this.currentStep = new DisplayFilterStep();
			return null;

		case DISPLAY_CONFIRMATION:
			final int inventoriesCount = createRequestResult(context).getSize();
			this.currentStep = new DisplayConfirmationStep(inventoriesCount);
			return null;

		case DISPLAY_RESULT:
			final ProcedureResult result = purgeInventories(context);
			this.currentStep = new DisplayResultStep(result);
			return null;

		default:
			return null;
		}
	}

	private static RequestResult createRequestResult(
			final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {
		final TableViewEntitySelection selection = context.getEntitySelection();

		final AdaptationTable inventories = selection.getTable();

		final String stockPath = Paths._Inventory._Stock.format();

		final String modifiedPath = Paths._Inventory._Modified.format();

		final ValueContextForValidation valueContext = context.getValueContext(FilterPaths._objectKey);

		final String startDate = DATE_FORMAT.format(valueContext.getValue(FilterPaths._startDate));

		final String endDate = DATE_FORMAT.format(valueContext.getValue(FilterPaths._endDate));

		final String condition = stockPath + "='0' and date-greater-than(" + modifiedPath + ",'" + startDate
				+ "') and date-less-than(" + modifiedPath + ",'" + endDate + "')";
		final RequestResult result = inventories.createRequestResult(condition);

		return result;
	}

	private static ProcedureResult purgeInventories(
			final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {
		final RequestResult result = createRequestResult(context);

		final Procedure procedure = new Procedure() {
			public void execute(final ProcedureContext procedureContext) throws Exception {
				try {
					for (Adaptation inventory; (inventory = result.nextAdaptation()) != null;) {
						final AdaptationName inventoryName = inventory.getAdaptationName();

						procedureContext.doDelete(inventoryName, false);
					}
				} finally {
					result.close();
				}
			}
		};

		final UserServiceTransaction transaction = context.createTransaction();

		transaction.add(procedure);

		return transaction.execute();
	}
}
