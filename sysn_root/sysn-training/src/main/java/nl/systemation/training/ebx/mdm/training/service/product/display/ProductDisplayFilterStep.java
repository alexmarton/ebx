package nl.systemation.training.ebx.mdm.training.service.product.display;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.UIButtonSpec;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;
import nl.systemation.training.ebx.mdm.training.service.FilterPaths;
import nl.systemation.training.ebx.mdm.training.service.Messages;

public class ProductDisplayFilterStep implements ProductDisplayStep {

    @Override
    public void setupDisplay(final UserServiceSetupDisplayContext<TableViewEntitySelection> context,
                             final UserServiceDisplayConfigurator config) {
        config.setContent(this::writeContent);

        {
            final UIButtonSpec cancelButtonSpec = config.newCancelButton();

            config.setLeftButtons(cancelButtonSpec);
        }

        {
            final UIButtonSpecNavigation nextButtonSpec = config.newNextButton(this::onNextPressed);

            nextButtonSpec.setDefaultButton(true);
            config.setRightButtons(nextButtonSpec);
        }
    }

    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
        writer.setCurrentObject(FilterPaths._objectKey);

        {
            final UserMessage message = Messages.get("TheEmptyStoresMatchingTheseCriteriaWillBeUpdate");
            final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

            writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
            writer.addUILabel(labelSpec);
            writer.add("</div>");
        }

        writer.startTableFormRow();
        writer.addFormRow(FilterPaths._type);
        writer.addFormRow(FilterPaths._startDate);
        writer.addFormRow(FilterPaths._endDate);
        writer.endTableFormRow();
    }

    private UserServiceEventOutcome onNextPressed(final UserServiceEventContext context) {
        return EventOutcome.DISPLAY_CONFIRMATION;
    }
}
