package nl.systemation.training.ebx.mdm.training.constraint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;
import nl.systemation.training.ebx.mdm.training.Paths;

public class CategoryConstraint implements ConstraintOnTable {

	private static class CategoryClusterKey {
		private final String categoryParent;

		private final String categoryName;

		public CategoryClusterKey(final Adaptation category) {
			this.categoryParent = (String) category.get(Paths._Category._Parent);

			this.categoryName = (String) category.get(Paths._Category._Name);
		}

		@Override
		public boolean equals(final Object object) {
			if (object instanceof CategoryClusterKey == false) {
				return false;
			}

			final CategoryClusterKey key = (CategoryClusterKey) object;

			return Arrays.equals(new Object[] { this.categoryParent, this.categoryName },
					new Object[] { key.categoryParent, key.categoryName });
		}

		@Override
		public int hashCode() {
			return Arrays.hashCode(new Object[] { this.categoryParent, this.categoryName });
		}
	}

	@Override
	public void setup(final ConstraintContextOnTable context) {
		final SchemaNode categoriesNode = context.getSchemaNode();

		context.addDependencyToInsertDeleteAndModify(categoriesNode);
	}

	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext context) throws InvalidSchemaException {
		final String message = Messages.get(locale, "CategoriesCannotHaveTheSameNameAndTheSameParent");

		return message;
	}

	@Override
	public void checkTable(final ValueContextForValidationOnTable context) {
		final AdaptationTable categories = context.getTable();

		final Collection<List<Adaptation>> categoryClusters = collectCategoryClusters(categories);

		final Collection<List<Adaptation>> duplicateCategoryClusters = filterDuplicateCategoryClusters(
				categoryClusters);

		addDuplicateErrorMessages(context, duplicateCategoryClusters);
	}

	private static Collection<List<Adaptation>> collectCategoryClusters(final AdaptationTable categories) {
		final RequestResult result = categories.createRequestResult(null);

		final Map<CategoryClusterKey, List<Adaptation>> categoryClusters = new HashMap<CategoryClusterKey, List<Adaptation>>();

		try {
			for (Adaptation category; (category = result.nextAdaptation()) != null;) {
				final CategoryClusterKey clusterKey = new CategoryClusterKey(category);

				List<Adaptation> categoryCluster = categoryClusters.get(clusterKey);

				if (categoryCluster == null) {
					categoryCluster = new ArrayList<Adaptation>();
					categoryClusters.put(clusterKey, categoryCluster);
				}

				categoryCluster.add(category);
			}

			return categoryClusters.values();
		} finally {
			result.close();
		}
	}

	private static Collection<List<Adaptation>> filterDuplicateCategoryClusters(
			Collection<List<Adaptation>> categoryClusters) {
		final Collection<List<Adaptation>> duplicateCategoryClusters = new ArrayList<List<Adaptation>>();

		for (final List<Adaptation> categoryCluster : categoryClusters) {
			if (categoryCluster.size() > 1) {
				duplicateCategoryClusters.add(categoryCluster);
			}
		}

		return duplicateCategoryClusters;
	}

	private static void addDuplicateErrorMessages(final ValueContextForValidationOnTable context,
			Collection<List<Adaptation>> duplicateCategoryClusters) {
		for (final List<Adaptation> duplicateCategoryCluster : duplicateCategoryClusters) {
			final UserMessage message = Messages.get(Severity.ERROR, "CategoriesCannotHaveTheSameNameAndTheSameParent");

			context.addMessage(duplicateCategoryCluster, message);
		}
	}
}