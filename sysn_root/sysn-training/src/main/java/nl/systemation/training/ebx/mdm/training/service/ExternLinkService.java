package nl.systemation.training.ebx.mdm.training.service;

import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;

public class ExternLinkService implements UserService<TableViewEntitySelection> {

    private final static ObjectKey _RecordObjectKey = ObjectKey.forName("record");

    public ExternLinkService() {
        new DisplayFilterStep();
    }

    @Override
    public void setupObjectContext(UserServiceSetupObjectContext<TableViewEntitySelection> userServiceSetupObjectContext, UserServiceObjectContextBuilder userServiceObjectContextBuilder) {
        System.out.println("We are on setupObjectContext");
        if (userServiceSetupObjectContext.isInitialDisplay()) {
//            System.out.println("We are on setupObjectContext");
        }

    }

    @Override
    public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> userServiceSetupDisplayContext, UserServiceDisplayConfigurator userServiceDisplayConfigurator) {
        System.out.println("We are on setupDisplay");

        userServiceDisplayConfigurator.setContent(this::writePane);

        userServiceDisplayConfigurator.setLeftButtons(
                userServiceDisplayConfigurator.newCloseButton());
    }

    @Override
    public void validate(UserServiceValidateContext<TableViewEntitySelection> userServiceValidateContext) {

    }

    @Override
    public UserServiceEventOutcome processEventOutcome(UserServiceProcessEventOutcomeContext<TableViewEntitySelection> userServiceProcessEventOutcomeContext,
                                                       UserServiceEventOutcome userServiceEventOutcome) {
        System.out.println("We are on UserServiceEventOutcome processEventOutcome");
        return userServiceEventOutcome;
    }

    private UserServiceEventOutcome gotoLink(UserServiceEventContext anEventContext) {
        System.out.println("We are on UserServiceEventOutcome onSave");
        return null;
    }

    private void writePane(UserServicePaneContext aPaneContext, UserServicePaneWriter aWriter) {
        // Displays an ULR that redirect current user.
//        String url = aWriter.getURLForAction(this::goElsewhere);
        String url = "https://www.ristoranteroma.at/";
        aWriter.add("<iframe src="+url +" style=\"height:450px;width:300px;\"></iframe>\n");
//        aWriter.addSafeAttribute("href", url);
//        aWriter.add(">Go external</a");
    }


}
