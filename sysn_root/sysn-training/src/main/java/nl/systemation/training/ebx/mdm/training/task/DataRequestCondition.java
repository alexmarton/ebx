package nl.systemation.training.ebx.mdm.training.task;


import com.onwbp.adaptation.*;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;
import nl.systemation.training.ebx.mdm.training.Paths;

import java.math.BigDecimal;


public class DataRequestCondition extends ConditionBean  {

    private String dataSpaceName;

    private String dataSetName;

    private String tablePath;

    public final void setDataSpaceName(String dataSpaceName) {
        this.dataSpaceName = dataSpaceName;
    }

    public final void setDataSetName(String dataSetName) {
        this.dataSetName = dataSetName;
    }

    public final void setTablePath(String tablePath) {
        this.tablePath = tablePath;
    }

    @Override
    public boolean evaluateCondition(ConditionBeanContext aContext) throws OperationException {

        final Repository repository = aContext.getRepository();
        final AdaptationHome dataSpace = toDataSpace(repository, dataSpaceName);
        final Adaptation dataSet = toDataSet(dataSpace, dataSetName);
        final Adaptation item = toItem(dataSet, tablePath);



        final BigDecimal itemPrice = (BigDecimal) item.get(Paths._Item._DefaultPrice);

        //Create a condition where default price is no more then a static number.
        return itemPrice.compareTo(new BigDecimal("50")) > 0;
    }

    //	Adaptation to DataSpace
    private static AdaptationHome toDataSpace(final Repository repository, final String dataSpaceName)
            throws OperationException {
        try {
            final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpaceName);

            final AdaptationHome dataSpace = repository.lookupHome(dataSpaceKey);

            if (dataSpace == null) {
                throw new IllegalArgumentException();
            }

            return dataSpace;
        } catch (final Exception exception) {
            final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedDataSpaceNameIsIncorrect{0}",
                    dataSpaceName);

            throw OperationException.createError(message);
        }
    }

    // Adaptation to DataSet
    private static Adaptation toDataSet(final AdaptationHome dataSpace, final String dataSetName)
            throws OperationException {
        try {
            final AdaptationName dataSetKey = AdaptationName.forName(dataSetName);

            final Adaptation dataSet = dataSpace.findAdaptationOrNull(dataSetKey);

            if (dataSet == null) {
                throw new IllegalArgumentException();
            }

            return dataSet;
        } catch (final Exception exception) {
            final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedDataSetNameIsIncorrect{0}",
                    dataSetName);

            throw OperationException.createError(message);
        }
    }

    // Adaptation to Item
    private static Adaptation toItem(final Adaptation dataSet, final String itemPath) throws OperationException {
        try {
            final Adaptation item = XPathExpressionHelper.lookupFirstRecordMatchingXPath(dataSet, itemPath);

            if (item == null) {
                throw new IllegalArgumentException();
            }

            return item;
        } catch (final Exception exception) {
            final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedItemPathIsIncorrect{0}", itemPath);

            throw OperationException.createError(message);
        }
    }





}
 