package nl.systemation.training.ebx.mdm.training.service;

import com.orchestranetworks.addon.dama.DigitalAssetMediaContentImpl;
import com.orchestranetworks.addon.dama.ext.bean.*;
import com.orchestranetworks.addon.dama.ext.componentcontroller.DigitalAssetComponentServices;
import com.orchestranetworks.addon.dama.ext.drivemanager.DriveManager;
import com.orchestranetworks.addon.dama.ext.exception.DAMException;
import com.orchestranetworks.addon.dama.ext.exception.OperationExecutionStatus;
import com.orchestranetworks.addon.dama.ext.resource.MediaContent;

import java.util.List;

public class AssetFileDetail implements DriveManager {


    @Override
    public DigitalAsset create(GeneralDigitalAssetSpec generalDigitalAssetSpec) throws DAMException {

        DigitalAsset digitalAsset = new DigitalAsset();
        digitalAsset.setDescription(generalDigitalAssetSpec.getDescription());
        digitalAsset.setLabel(generalDigitalAssetSpec.getLabel());
        return digitalAsset;
    }

    @Override
    public DigitalAssetVersion create(GeneralDigitalAssetVersionSpec generalDigitalAssetVersionSpec) throws DAMException {
        return null;
    }

    @Override
    public OperationExecutionStatus delete(DigitalAssetKey digitalAssetKey) {
        return null;
    }

    @Override
    public OperationExecutionStatus delete(DigitalAssetVersionKey digitalAssetVersionKey) {
        return null;
    }

    @Override
    public OperationExecutionStatus update(DigitalAsset digitalAsset) {
        return null;
    }

    @Override
    public OperationExecutionStatus update(DigitalAssetVersion digitalAssetVersion) {
        return null;
    }

    @Override
    public List<DigitalAsset> search(SearchFilter searchFilter) throws DAMException {
        return null;
    }

    @Override
    public List<DigitalAsset> getDigitalAssets() throws DAMException {
        return null;
    }

    @Override
    public DigitalAsset getDigitalAsset(DigitalAssetKey digitalAssetKey) throws DAMException {
        return null;
    }

    @Override
    public MediaContent getMediaContent(DigitalAsset digitalAsset) throws DAMException {
        return null;
    }

    @Override
    public MediaContent getMediaContent(DigitalAssetVersion digitalAssetVersion) throws DAMException {
        return null;
    }
}
