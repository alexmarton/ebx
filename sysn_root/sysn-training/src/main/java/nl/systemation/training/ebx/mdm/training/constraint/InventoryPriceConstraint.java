package nl.systemation.training.ebx.mdm.training.constraint;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import nl.systemation.training.ebx.mdm.training.Paths;

public class InventoryPriceConstraint implements Constraint {
	private BigDecimal tolerance;

	public void setTolerance(final BigDecimal tolerance) {
		this.tolerance = tolerance;
	}

	@Override
	public void setup(final ConstraintContext context) {
		// Constraint dependencies must not be declared,
		// in case it is an inherited attribute.

		if ((this.tolerance == null) || (this.tolerance.doubleValue() < 0)) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheToleranceMustBeAPositiveDecimal");

			context.addMessage(message);
		}
	}

	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext context) throws InvalidSchemaException {
		final NumberFormat format = NumberFormat.getInstance(locale);

		final String message = Messages.get(locale, "ThePriceMustNotDifferFromTheDefaultPriceMoreThan{0}",
				format.format(this.tolerance.doubleValue() * 100));

		return message;
	}

	@Override
	public void checkOccurrence(final Object value, final ValueContextForValidation context)
			throws InvalidSchemaException {
		final Double price = getInventoryPrice(context);
		final Double defaultPrice = getItemDefaultPrice(context);

		if ((price == null) || (defaultPrice == null)) {
			return;
		}

		if (Math.abs(price - defaultPrice) > (this.tolerance.doubleValue() * defaultPrice)) {
			final NumberFormat format = NumberFormat.getInstance();
			final UserMessage message = Messages.get(Severity.ERROR,
					"ThePriceMustNotDifferFromTheDefaultPriceMoreThan{0}",
					format.format(this.tolerance.doubleValue() * 100));

			context.addMessage(message);
		}
	}

	protected Double getInventoryPrice(final ValueContextForValidation context) {
		final BigDecimal price = (BigDecimal) context.getValue(Path.SELF);

		if (price == null) {
			return null;
		}

		return price.doubleValue();
	}

	protected Double getItemDefaultPrice(final ValueContextForValidation context) {
		final String itemIdentifier = (String) context.getValue(Path.PARENT.add(Paths._Inventory._Item));

		final Adaptation dataSet = context.getAdaptationInstance();

		final AdaptationTable items = dataSet.getTable(Paths._Item.getPathInSchema());

		final Adaptation item = items.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(itemIdentifier));

		if (item == null) {
			return null;
		}

		final BigDecimal defaultPrice = (BigDecimal) item.get(Paths._Item._DefaultPrice);

		if (defaultPrice == null) {
			return null;
		}

		return defaultPrice.doubleValue();
	}
}
