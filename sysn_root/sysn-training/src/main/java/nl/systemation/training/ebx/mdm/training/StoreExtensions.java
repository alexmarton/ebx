package nl.systemation.training.ebx.mdm.training;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;
import com.orchestranetworks.service.AccessRule;
import nl.systemation.training.ebx.mdm.training.permissions.InventoryPriceAccessRule;
import nl.systemation.training.ebx.mdm.training.service.PurgeInventoriesServiceDeclaration;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import nl.systemation.training.ebx.mdm.training.service.declaration.ExternLinkServiceDeclaration;
import nl.systemation.training.ebx.mdm.training.service.declaration.ProductServiceDeclaration;

public class StoreExtensions implements SchemaExtensions {

	@Override
	public void defineExtensions(final SchemaExtensionsContext context) {
		{
			final UserServiceDeclaration.OnTableView userServiceDeclaration = new PurgeInventoriesServiceDeclaration();
			final ExternLinkServiceDeclaration.OnTableView externLinkServiceDeclaration = new ExternLinkServiceDeclaration();
			final ProductServiceDeclaration.OnTableView productServiceDeclaration = new ProductServiceDeclaration();

			context.registerUserService(userServiceDeclaration);
			context.registerUserService(externLinkServiceDeclaration);
			context.registerUserService(productServiceDeclaration);
		}

		{
			final Path path = Paths._Inventory.getPathInSchema().add(Paths._Inventory._Price);

			final AccessRule accessRule = new InventoryPriceAccessRule();

			context.setAccessRuleOnNode(path, accessRule);
		}
	}
}