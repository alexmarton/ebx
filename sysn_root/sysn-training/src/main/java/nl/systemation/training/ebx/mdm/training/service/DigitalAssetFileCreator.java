package nl.systemation.training.ebx.mdm.training.service;

import com.orchestranetworks.addon.dama.ext.bean.DigitalAsset;
import com.orchestranetworks.addon.dama.ext.bean.DigitalAssetKey;
import com.orchestranetworks.addon.dama.ext.bean.DigitalAssetSpec;
import com.orchestranetworks.addon.dama.ext.bean.DigitalAssetVersion;
import com.orchestranetworks.addon.dama.ext.componentcontroller.DigitalAssetComponentServices;
import com.orchestranetworks.addon.dama.ext.enumeration.Action;
import com.orchestranetworks.addon.dama.ext.exception.DAMException;
import com.orchestranetworks.addon.dama.ext.exception.OperationExecutionStatus;
import com.orchestranetworks.addon.dama.ext.resource.Resource;

import java.util.List;

public class DigitalAssetFileCreator implements DigitalAssetComponentServices {
    @Override
    public List<DigitalAssetKey> getDigitalAssetKeys() throws DAMException {
        return null;
    }

    @Override
    public OperationExecutionStatus attach(List<DigitalAssetKey> list) {
        return null;
    }

    @Override
    public DigitalAsset upload(Resource resource) throws DAMException {


        System.out.println(resource.getFile());
        DigitalAssetSpec digitalAssetSpec = new DigitalAssetSpec(resource);
        DigitalAssetVersion digitalAssetVersion = new DigitalAssetVersion();

        DigitalAsset digitalAsset = new DigitalAsset();
        digitalAsset.setLabel(digitalAssetSpec.getLabel());
        digitalAsset.setDescription(digitalAssetSpec.getDescription());
        System.out.println("=>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Until here");
        return digitalAsset;

    }

    @Override
    public OperationExecutionStatus upload(List<? extends Resource> list, Action action) {
        return null;
    }

    @Override
    public OperationExecutionStatus detach(List<DigitalAssetKey> list) {
        return null;
    }
}
