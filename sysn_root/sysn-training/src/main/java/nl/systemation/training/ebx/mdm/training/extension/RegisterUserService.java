package nl.systemation.training.ebx.mdm.training.extension;

import com.orchestranetworks.schema.*;
import nl.systemation.training.ebx.mdm.training.service.declaration.ExternLinkServiceDeclaration;

public class RegisterUserService
{
	public static void registerUserService(final SchemaExtensionsContext context)
	{
		context.registerUserService(new ExternLinkServiceDeclaration());
	}
}

