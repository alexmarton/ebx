package nl.systemation.training.ebx.mdm.training.widget;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.onwbp.base.text.UserMessageRef;

public class Messages {
	private static final String BUNDLE_NAME = Messages.class.getPackage().getName() + ".Messages";

	private Messages() {
	}

	public static UserMessage get(final String key, final Object... values) {
		return get(Severity.INFO, key, values);
	}

	public static UserMessage get(final Severity severity, final String key, final Object... values) {
		final ClassLoader classLoader = Messages.class.getClassLoader();

		return new UserMessageRef(severity, key, BUNDLE_NAME, values, classLoader);
	}

	public static String get(final Locale locale, final String key, final Object... values) {
		try {
			final ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);

			return MessageFormat.format(bundle.getString(key), values);
		} catch (final MissingResourceException exception) {
			return key;
		}
	}
}
