package nl.systemation.training.ebx.mdm.training.widget;

import com.orchestranetworks.ui.form.widget.UIWidgetFactory;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetFactorySetupContext;

public class UILinkToUrlFactory implements UIWidgetFactory<UILinkToUrl> {
    @Override
    public UILinkToUrl newInstance(final WidgetFactoryContext context) {
        return new UILinkToUrl(context);
    }

    @Override
    public void setup(WidgetFactorySetupContext widgetFactorySetupContext) {

    }
}
