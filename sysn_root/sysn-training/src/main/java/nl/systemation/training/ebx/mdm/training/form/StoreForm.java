package nl.systemation.training.ebx.mdm.training.form;

import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.form.UIForm;
import com.orchestranetworks.ui.form.UIFormBody;
import com.orchestranetworks.ui.form.UIFormBottomBar;
import com.orchestranetworks.ui.form.UIFormContext;
import com.orchestranetworks.ui.form.UIFormHeader;
import com.orchestranetworks.ui.form.UIFormPaneWithTabs;

public class StoreForm extends UIForm {

	@Override
	public void defineHeader(final UIFormHeader header, final UIFormContext context) {
		final Session session = context.getSession();

		if (session.isUserInRole(Role.ADMINISTRATOR)) {
			super.defineHeader(header, context);

			return;
		}

		if (context.isCreatingRecord()) {
			final String title = Messages.get(context.getLocale(), "NewStore");

			header.setTitle(new UIFormLabelSpec(title));
		} else {
			final Adaptation store = context.getCurrentRecord();

			final Locale locale = context.getLocale();

			final String storeLabel = store.getLabel(locale);

			final String title = Messages.get(locale, "Store{0}", storeLabel);

			header.setTitle(new UIFormLabelSpec(title));
		}

		header.removePagingPane();
	}

	@Override
	public void defineBody(final UIFormBody body, final UIFormContext context) {
		final Session session = context.getSession();

		if (session.isUserInRole(Role.ADMINISTRATOR)) {
			super.defineBody(body, context);

			return;
		}

		final UIFormPaneWithTabs tabs = new UIFormPaneWithTabs();

		tabs.addTab(Messages.get("Main"), new StoreMainPane());
		tabs.addTab(Messages.get("Location"), new StoreLocationPane());
		tabs.addTab(Messages.get("Inventories"), new StoreInventoriesPane());

		body.setContent(tabs);
	}

	@Override
	public void defineBottomBar(final UIFormBottomBar bottomBar, final UIFormContext context) {
		final Session session = context.getSession();

		if (session.isUserInRole(Role.ADMINISTRATOR)) {
			super.defineBottomBar(bottomBar, context);

			return;
		}

		if (context.isCreatingRecord()) {
			bottomBar.setCloseButtonLabel(Messages.get("Close"));
			bottomBar.setSubmitButtonLabel(Messages.get("Create"));
			bottomBar.setSubmitAndCloseButtonLabel(Messages.get("CreateAndClose"));
		} else {
			bottomBar.setCloseButtonLabel(Messages.get("Close"));
			bottomBar.setSubmitButtonLabel(Messages.get("Save"));
			bottomBar.setSubmitAndCloseButtonLabel(Messages.get("SaveAndClose"));
		}

		bottomBar.setRevertButtonDisplayable(false);
	}
}
