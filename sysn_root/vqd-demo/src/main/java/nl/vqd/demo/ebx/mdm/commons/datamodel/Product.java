package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Product" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class Product extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/product";

	public Product(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Product(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Product(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Product(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Product(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Product(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Product(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Product(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * Name (Standard)
    *
    */

	public String get_Name()
	{
		return this.getString("./name");
	}

	public String get_Name_After()
	{
		return (String)this.getValueAfter("./name");
	}

	public String get_Name_Before()
	{
		return (String)this.getValueBefore("./name");
	}

	public void set_Name(final String value)
	{
		this.setObject(value, "./name", String.class);
	}

	public boolean is_Name_Changed()
	{
	    return (((String)this.getValueAfter("./name")) != null && 
	        !((String)this.getValueAfter("./name")).equals((this.getValueBefore("./name"))));
	}

	private static final com.orchestranetworks.schema.Path path_Name = com.orchestranetworks.schema.Path.parse("./name");
	public static final com.orchestranetworks.schema.Path getPath_Name()
	{
		return path_Name;
	}

   /* Get and Set methods for attribute:  
    * whitelabelCode (Standard)
    *
    */

	public Integer get_WhitelabelCode()
	{
		return this.getInteger("./whitelabelCode");
	}

	public Integer get_WhitelabelCode_After()
	{
		return (Integer)this.getValueAfter("./whitelabelCode");
	}

	public Integer get_WhitelabelCode_Before()
	{
		return (Integer)this.getValueBefore("./whitelabelCode");
	}

	public void set_WhitelabelCode(final Integer value)
	{
		this.setObject(value, "./whitelabelCode", Integer.class);
	}

	public boolean is_WhitelabelCode_Changed()
	{
	    return (((Integer)this.getValueAfter("./whitelabelCode")) != null && 
	        !((Integer)this.getValueAfter("./whitelabelCode")).equals((this.getValueBefore("./whitelabelCode"))));
	}

	private static final com.orchestranetworks.schema.Path path_WhitelabelCode = com.orchestranetworks.schema.Path.parse("./whitelabelCode");
	public static final com.orchestranetworks.schema.Path getPath_WhitelabelCode()
	{
		return path_WhitelabelCode;
	}

   /* Get and Set methods for attribute:  
    * Loan (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_FK()
	{
		String ret = this.getString("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Loan_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./loan", String.class);
	}

	public void set_Loan(final nl.vqd.demo.ebx.mdm.commons.datamodel.Loan value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./loan", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Loan_Record()
	{
		return super.getFK("./loan");
	}

	public boolean is_Loan_Changed()
	{
	    return (((String)this.getValueAfter("./loan")) != null && 
	        !((String)this.getValueAfter("./loan")).equals((this.getValueBefore("./loan"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Loan get_Loan()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./loan");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Loan(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Loan = com.orchestranetworks.schema.Path.parse("./loan");
	public static final com.orchestranetworks.schema.Path getPath_Loan()
	{
		return path_Loan;
	}

   /* Get and Set methods for attribute:  
    * Deellening (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_LoanComponent_FK()
	{
		String ret = this.getString("./loanComponent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_LoanComponent_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./loanComponent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_LoanComponent_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./loanComponent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_LoanComponent_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./loanComponent", String.class);
	}

	public void set_LoanComponent(final nl.vqd.demo.ebx.mdm.commons.datamodel.LoanComponent value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./loanComponent", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_LoanComponent_Record()
	{
		return super.getFK("./loanComponent");
	}

	public boolean is_LoanComponent_Changed()
	{
	    return (((String)this.getValueAfter("./loanComponent")) != null && 
	        !((String)this.getValueAfter("./loanComponent")).equals((this.getValueBefore("./loanComponent"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.LoanComponent get_LoanComponent()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./loanComponent");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.LoanComponent(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_LoanComponent = com.orchestranetworks.schema.Path.parse("./loanComponent");
	public static final com.orchestranetworks.schema.Path getPath_LoanComponent()
	{
		return path_LoanComponent;
	}

}

