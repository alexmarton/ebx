package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Transaction" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class Transaction extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/transaction";

	public Transaction(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Transaction(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Transaction(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Transaction(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Transaction(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Transaction(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Transaction(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Transaction(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * Account (Standard)
    *
    */

	public String get_Account()
	{
		return this.getString("./account");
	}

	public String get_Account_After()
	{
		return (String)this.getValueAfter("./account");
	}

	public String get_Account_Before()
	{
		return (String)this.getValueBefore("./account");
	}

	public void set_Account(final String value)
	{
		this.setObject(value, "./account", String.class);
	}

	public boolean is_Account_Changed()
	{
	    return (((String)this.getValueAfter("./account")) != null && 
	        !((String)this.getValueAfter("./account")).equals((this.getValueBefore("./account"))));
	}

	private static final com.orchestranetworks.schema.Path path_Account = com.orchestranetworks.schema.Path.parse("./account");
	public static final com.orchestranetworks.schema.Path getPath_Account()
	{
		return path_Account;
	}

   /* Get and Set methods for attribute:  
    * Transaction Type (Standard)
    *
    */

	public String get_TransactionType()
	{
		return this.getString("./transactionType");
	}

	public String get_TransactionType_After()
	{
		return (String)this.getValueAfter("./transactionType");
	}

	public String get_TransactionType_Before()
	{
		return (String)this.getValueBefore("./transactionType");
	}

	public void set_TransactionType(final String value)
	{
		this.setObject(value, "./transactionType", String.class);
	}

	public boolean is_TransactionType_Changed()
	{
	    return (((String)this.getValueAfter("./transactionType")) != null && 
	        !((String)this.getValueAfter("./transactionType")).equals((this.getValueBefore("./transactionType"))));
	}

	private static final com.orchestranetworks.schema.Path path_TransactionType = com.orchestranetworks.schema.Path.parse("./transactionType");
	public static final com.orchestranetworks.schema.Path getPath_TransactionType()
	{
		return path_TransactionType;
	}

   /* Get and Set methods for attribute:  
    * Service ID (Standard)
    *
    */

	public String get_TransactionServiceID()
	{
		return this.getString("./transactionServiceID");
	}

	public String get_TransactionServiceID_After()
	{
		return (String)this.getValueAfter("./transactionServiceID");
	}

	public String get_TransactionServiceID_Before()
	{
		return (String)this.getValueBefore("./transactionServiceID");
	}

	public void set_TransactionServiceID(final String value)
	{
		this.setObject(value, "./transactionServiceID", String.class);
	}

	public boolean is_TransactionServiceID_Changed()
	{
	    return (((String)this.getValueAfter("./transactionServiceID")) != null && 
	        !((String)this.getValueAfter("./transactionServiceID")).equals((this.getValueBefore("./transactionServiceID"))));
	}

	private static final com.orchestranetworks.schema.Path path_TransactionServiceID = com.orchestranetworks.schema.Path.parse("./transactionServiceID");
	public static final com.orchestranetworks.schema.Path getPath_TransactionServiceID()
	{
		return path_TransactionServiceID;
	}

   /* Get and Set methods for attribute:  
    * Transaction Status (Standard)
    *
    */

	public String get_TransactionStatus()
	{
		return this.getString("./transactionStatus");
	}

	public String get_TransactionStatus_After()
	{
		return (String)this.getValueAfter("./transactionStatus");
	}

	public String get_TransactionStatus_Before()
	{
		return (String)this.getValueBefore("./transactionStatus");
	}

	public void set_TransactionStatus(final String value)
	{
		this.setObject(value, "./transactionStatus", String.class);
	}

	public boolean is_TransactionStatus_Changed()
	{
	    return (((String)this.getValueAfter("./transactionStatus")) != null && 
	        !((String)this.getValueAfter("./transactionStatus")).equals((this.getValueBefore("./transactionStatus"))));
	}

	private static final com.orchestranetworks.schema.Path path_TransactionStatus = com.orchestranetworks.schema.Path.parse("./transactionStatus");
	public static final com.orchestranetworks.schema.Path getPath_TransactionStatus()
	{
		return path_TransactionStatus;
	}

}

