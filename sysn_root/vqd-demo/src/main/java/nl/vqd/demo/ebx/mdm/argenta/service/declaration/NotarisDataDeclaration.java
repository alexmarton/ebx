package nl.vqd.demo.ebx.mdm.argenta.service.declaration;

import com.orchestranetworks.schema.types.dataspace.DataspaceSet;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.selection.RecordEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.declaration.ActivationContextOnRecord;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import com.orchestranetworks.userservice.declaration.UserServicePropertiesDefinitionContext;
import com.orchestranetworks.userservice.declaration.WebComponentDeclarationContext;
import nl.vqd.demo.ebx.mdm.argenta.service.NotarisDataService;
import nl.vqd.demo.ebx.mdm.commons.datamodel.Contract;

public class NotarisDataDeclaration implements UserServiceDeclaration.OnRecord {

    public static final ServiceKey SERVICE_KEY = ServiceKey.forName("NotarisData");

    @Override
    public UserService<RecordEntitySelection> createUserService() {
        return new NotarisDataService();
    }

    @Override
    public ServiceKey getServiceKey() {
        return NotarisDataDeclaration.SERVICE_KEY;
    }

    @Override
    public void defineActivation(ActivationContextOnRecord context) {
        context.includeAllDatasets();
        context.includeAllDataspaces(DataspaceSet.DataspaceType.BRANCH);
        context.includeSchemaNodesMatching(Contract.getTablePath());
    }

    @Override
    public void defineProperties(UserServicePropertiesDefinitionContext userServicePropertiesDefinitionContext) {
        userServicePropertiesDefinitionContext.setLabel("Notaris Check");
    }

    @Override
    public void declareWebComponent(WebComponentDeclarationContext webComponentDeclarationContext) {

    }
}
