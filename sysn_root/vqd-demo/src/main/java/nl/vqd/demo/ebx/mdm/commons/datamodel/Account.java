package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Account" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class Account extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/account";

	public Account(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Account(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Account(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Account(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Account(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Account(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Account(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Account(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	

	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * Account Name (Standard)
    *
    */

	public String get_AccountName()
	{
		return this.getString("./accountName");
	}

	public String get_AccountName_After()
	{
		return (String)this.getValueAfter("./accountName");
	}

	public String get_AccountName_Before()
	{
		return (String)this.getValueBefore("./accountName");
	}

	public void set_AccountName(final String value)
	{
		this.setObject(value, "./accountName", String.class);
	}

	public boolean is_AccountName_Changed()
	{
	    return (((String)this.getValueAfter("./accountName")) != null && 
	        !((String)this.getValueAfter("./accountName")).equals((this.getValueBefore("./accountName"))));
	}

	private static final com.orchestranetworks.schema.Path path_AccountName = com.orchestranetworks.schema.Path.parse("./accountName");
	public static final com.orchestranetworks.schema.Path getPath_AccountName()
	{
		return path_AccountName;
	}

   /* Get and Set methods for attribute:  
    * Current Account Balance (Standard)
    *
    */

	public String get_AccountBalance()
	{
		return this.getString("./accountBalance");
	}

	public String get_AccountBalance_After()
	{
		return (String)this.getValueAfter("./accountBalance");
	}

	public String get_AccountBalance_Before()
	{
		return (String)this.getValueBefore("./accountBalance");
	}

	public void set_AccountBalance(final String value)
	{
		this.setObject(value, "./accountBalance", String.class);
	}

	public boolean is_AccountBalance_Changed()
	{
	    return (((String)this.getValueAfter("./accountBalance")) != null && 
	        !((String)this.getValueAfter("./accountBalance")).equals((this.getValueBefore("./accountBalance"))));
	}

	private static final com.orchestranetworks.schema.Path path_AccountBalance = com.orchestranetworks.schema.Path.parse("./accountBalance");
	public static final com.orchestranetworks.schema.Path getPath_AccountBalance()
	{
		return path_AccountBalance;
	}

   /* Get and Set methods for attribute:  
    * Last Event (Standard)
    *
    */

	public java.util.Date get_AccountLastEvent()
	{
		return this.getDate("./accountLastEvent");
	}

	public java.util.Date get_AccountLastEvent_After()
	{
		return (java.util.Date)this.getValueAfter("./accountLastEvent");
	}

	public java.util.Date get_AccountLastEvent_Before()
	{
		return (java.util.Date)this.getValueBefore("./accountLastEvent");
	}

	public void set_AccountLastEvent(final java.util.Date value)
	{
		this.setObject(value, "./accountLastEvent", java.util.Date.class);
	}

	public boolean is_AccountLastEvent_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./accountLastEvent")) != null && 
	        !((java.util.Date)this.getValueAfter("./accountLastEvent")).equals((this.getValueBefore("./accountLastEvent"))));
	}

	private static final com.orchestranetworks.schema.Path path_AccountLastEvent = com.orchestranetworks.schema.Path.parse("./accountLastEvent");
	public static final com.orchestranetworks.schema.Path getPath_AccountLastEvent()
	{
		return path_AccountLastEvent;
	}

   /* Get and Set methods for attribute:  
    * Used by Financial Instrument (Standard boolean)
    *
    */
    
	/**
	 * 
	 * @Deprecated use the is_AccountFinancialInstrument() method instead
	 */
	@Deprecated 
	public Boolean get_AccountFinancialInstrument()
	{
		return this.getBoolean("./accountFinancialInstrument");
	}

	public Boolean get_AccountFinancialInstrument_After()
	{
		return (Boolean)this.getValueAfter("./accountFinancialInstrument");
	}

	public Boolean get_AccountFinancialInstrument_Before()
	{
		return (Boolean)this.getValueBefore("./accountFinancialInstrument");
	}

	public void set_AccountFinancialInstrument(final Boolean value)
	{
		this.setObject(value, "./accountFinancialInstrument", Boolean.class);
	}

	public boolean is_AccountFinancialInstrument() {
		return this.getBoolean("./accountFinancialInstrument");
	}

	public boolean is_AccountFinancialInstrument_Changed()
	{
	    return (((Boolean)this.getValueAfter("./accountFinancialInstrument")) != null && 
	        !((Boolean)this.getValueAfter("./accountFinancialInstrument")).equals((this.getValueBefore("./accountFinancialInstrument"))));
	}

	private static final com.orchestranetworks.schema.Path path_AccountFinancialInstrument = com.orchestranetworks.schema.Path.parse("./accountFinancialInstrument");
	public static final com.orchestranetworks.schema.Path getPath_AccountFinancialInstrument()
	{
		return path_AccountFinancialInstrument;
	}

   /* Get and Set methods for attribute:  
    * Other Details (Standard)
    *
    */

	public String get_AccountDetails()
	{
		return this.getString("./accountDetails");
	}

	public String get_AccountDetails_After()
	{
		return (String)this.getValueAfter("./accountDetails");
	}

	public String get_AccountDetails_Before()
	{
		return (String)this.getValueBefore("./accountDetails");
	}

	public void set_AccountDetails(final String value)
	{
		this.setObject(value, "./accountDetails", String.class);
	}

	public boolean is_AccountDetails_Changed()
	{
	    return (((String)this.getValueAfter("./accountDetails")) != null && 
	        !((String)this.getValueAfter("./accountDetails")).equals((this.getValueBefore("./accountDetails"))));
	}

	private static final com.orchestranetworks.schema.Path path_AccountDetails = com.orchestranetworks.schema.Path.parse("./accountDetails");
	public static final com.orchestranetworks.schema.Path getPath_AccountDetails()
	{
		return path_AccountDetails;
	}

}

