package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Financial Instrument" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class FinancialInstrument extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/financialInstrument";

	public FinancialInstrument(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public FinancialInstrument(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public FinancialInstrument(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public FinancialInstrument(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public FinancialInstrument(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public FinancialInstrument(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public FinancialInstrument(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public FinancialInstrument(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * Name (Standard)
    *
    */

	public String get_InstrumentName()
	{
		return this.getString("./instrumentName");
	}

	public String get_InstrumentName_After()
	{
		return (String)this.getValueAfter("./instrumentName");
	}

	public String get_InstrumentName_Before()
	{
		return (String)this.getValueBefore("./instrumentName");
	}

	public void set_InstrumentName(final String value)
	{
		this.setObject(value, "./instrumentName", String.class);
	}

	public boolean is_InstrumentName_Changed()
	{
	    return (((String)this.getValueAfter("./instrumentName")) != null && 
	        !((String)this.getValueAfter("./instrumentName")).equals((this.getValueBefore("./instrumentName"))));
	}

	private static final com.orchestranetworks.schema.Path path_InstrumentName = com.orchestranetworks.schema.Path.parse("./instrumentName");
	public static final com.orchestranetworks.schema.Path getPath_InstrumentName()
	{
		return path_InstrumentName;
	}

   /* Get and Set methods for attribute:  
    * Type (Standard)
    *
    */

	public String get_InstrumentType()
	{
		return this.getString("./instrumentType");
	}

	public String get_InstrumentType_After()
	{
		return (String)this.getValueAfter("./instrumentType");
	}

	public String get_InstrumentType_Before()
	{
		return (String)this.getValueBefore("./instrumentType");
	}

	public void set_InstrumentType(final String value)
	{
		this.setObject(value, "./instrumentType", String.class);
	}

	public boolean is_InstrumentType_Changed()
	{
	    return (((String)this.getValueAfter("./instrumentType")) != null && 
	        !((String)this.getValueAfter("./instrumentType")).equals((this.getValueBefore("./instrumentType"))));
	}

	private static final com.orchestranetworks.schema.Path path_InstrumentType = com.orchestranetworks.schema.Path.parse("./instrumentType");
	public static final com.orchestranetworks.schema.Path getPath_InstrumentType()
	{
		return path_InstrumentType;
	}

   /* Get and Set methods for attribute:  
    * Subtype (Standard)
    *
    */

	public String get_InstrumentSubType()
	{
		return this.getString("./instrumentSubType");
	}

	public String get_InstrumentSubType_After()
	{
		return (String)this.getValueAfter("./instrumentSubType");
	}

	public String get_InstrumentSubType_Before()
	{
		return (String)this.getValueBefore("./instrumentSubType");
	}

	public void set_InstrumentSubType(final String value)
	{
		this.setObject(value, "./instrumentSubType", String.class);
	}

	public boolean is_InstrumentSubType_Changed()
	{
	    return (((String)this.getValueAfter("./instrumentSubType")) != null && 
	        !((String)this.getValueAfter("./instrumentSubType")).equals((this.getValueBefore("./instrumentSubType"))));
	}

	private static final com.orchestranetworks.schema.Path path_InstrumentSubType = com.orchestranetworks.schema.Path.parse("./instrumentSubType");
	public static final com.orchestranetworks.schema.Path getPath_InstrumentSubType()
	{
		return path_InstrumentSubType;
	}

   /* Get and Set methods for attribute:  
    * Risk Exposure Class (Standard)
    *
    */

	public String get_InstrumentRiskExposureClass()
	{
		return this.getString("./instrumentRiskExposureClass");
	}

	public String get_InstrumentRiskExposureClass_After()
	{
		return (String)this.getValueAfter("./instrumentRiskExposureClass");
	}

	public String get_InstrumentRiskExposureClass_Before()
	{
		return (String)this.getValueBefore("./instrumentRiskExposureClass");
	}

	public void set_InstrumentRiskExposureClass(final String value)
	{
		this.setObject(value, "./instrumentRiskExposureClass", String.class);
	}

	public boolean is_InstrumentRiskExposureClass_Changed()
	{
	    return (((String)this.getValueAfter("./instrumentRiskExposureClass")) != null && 
	        !((String)this.getValueAfter("./instrumentRiskExposureClass")).equals((this.getValueBefore("./instrumentRiskExposureClass"))));
	}

	private static final com.orchestranetworks.schema.Path path_InstrumentRiskExposureClass = com.orchestranetworks.schema.Path.parse("./instrumentRiskExposureClass");
	public static final com.orchestranetworks.schema.Path getPath_InstrumentRiskExposureClass()
	{
		return path_InstrumentRiskExposureClass;
	}

   /* Get and Set methods for attribute:  
    * Account (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Account_FK()
	{
		String ret = this.getString("./account");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Account_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./account");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Account_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./account");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Account_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./account", String.class);
	}

	public void set_Account(final nl.vqd.demo.ebx.mdm.commons.datamodel.Account value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./account", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Account_Record()
	{
		return super.getFK("./account");
	}

	public boolean is_Account_Changed()
	{
	    return (((String)this.getValueAfter("./account")) != null && 
	        !((String)this.getValueAfter("./account")).equals((this.getValueBefore("./account"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Account get_Account()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./account");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Account(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Account = com.orchestranetworks.schema.Path.parse("./account");
	public static final com.orchestranetworks.schema.Path getPath_Account()
	{
		return path_Account;
	}

}

