package nl.vqd.demo.ebx.mdm.argenta.service.product.filter;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.userservice.ObjectKey;

public interface FilterPaths {
	public final static ObjectKey _objectKey = ObjectKey.forName("filter");

	public final static Path _startDate = Path.parse("startDate");
	public final static Path _endDate = Path.parse("endDate");
}
