package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Fund" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class Fund extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/fund";

	public Fund(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Fund(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Fund(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Fund(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Fund(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Fund(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Fund(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Fund(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * Status (Standard)
    *
    */

	public String get_Status()
	{
		return this.getString("./status");
	}

	public String get_Status_After()
	{
		return (String)this.getValueAfter("./status");
	}

	public String get_Status_Before()
	{
		return (String)this.getValueBefore("./status");
	}

	public void set_Status(final String value)
	{
		this.setObject(value, "./status", String.class);
	}

	public boolean is_Status_Changed()
	{
	    return (((String)this.getValueAfter("./status")) != null && 
	        !((String)this.getValueAfter("./status")).equals((this.getValueBefore("./status"))));
	}

	private static final com.orchestranetworks.schema.Path path_Status = com.orchestranetworks.schema.Path.parse("./status");
	public static final com.orchestranetworks.schema.Path getPath_Status()
	{
		return path_Status;
	}

   /* Get and Set methods for attribute:  
    * Object (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Object_FK()
	{
		String ret = this.getString("./object");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Object_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./object");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Object_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./object");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Object_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./object", String.class);
	}

	public void set_Object(final nl.vqd.demo.ebx.mdm.commons.datamodel.Object value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./object", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Object_Record()
	{
		return super.getFK("./object");
	}

	public boolean is_Object_Changed()
	{
	    return (((String)this.getValueAfter("./object")) != null && 
	        !((String)this.getValueAfter("./object")).equals((this.getValueBefore("./object"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Object get_Object()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./object");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Object(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Object = com.orchestranetworks.schema.Path.parse("./object");
	public static final com.orchestranetworks.schema.Path getPath_Object()
	{
		return path_Object;
	}

}

