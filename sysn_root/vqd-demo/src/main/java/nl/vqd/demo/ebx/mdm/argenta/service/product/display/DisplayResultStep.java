package nl.vqd.demo.ebx.mdm.argenta.service.product.display;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserServiceDisplayConfigurator;
import com.orchestranetworks.userservice.UserServicePaneContext;
import com.orchestranetworks.userservice.UserServicePaneWriter;
import com.orchestranetworks.userservice.UserServiceSetupDisplayContext;
import nl.vqd.demo.ebx.mdm.argenta.service.product.message.Messages;


public class DisplayResultStep implements DisplayStep {

	private final Boolean result;

	public DisplayResultStep(final Boolean result) {
		this.result = result;
	}


//	private final ProcedureResult result;
//
//	public DisplayResultStep(final ProcedureResult result) {
//		this.result = result;
//	}

	@Override
	public void setupDisplay(final UserServiceSetupDisplayContext<TableViewEntitySelection> context,
			final UserServiceDisplayConfigurator config) {
		config.setContent(this::writeContent);

		{
			final UserMessage closeMessage = Messages.get("Close");
			final UIButtonSpecNavigation closeButtonSpec = config.newCloseButton();

			closeButtonSpec.setLabel(closeMessage);
			closeButtonSpec.setDefaultButton(true);
			config.setRightButtons(closeButtonSpec);
		}
	}

	private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
		writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");

		if (!result) {
			final UserMessage message = Messages.get("TheUpdateOfTheRowsHasFailed");
			final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

			writer.addUILabel(labelSpec);
		} else {
			final UserMessage message = Messages.get("TheUpdateOfTheRowsHasSucceeded");
			final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

			writer.addUILabel(labelSpec);
		}

		writer.add("</div>");
	}
}
