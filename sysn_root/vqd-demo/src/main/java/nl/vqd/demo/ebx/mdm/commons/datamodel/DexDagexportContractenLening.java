package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "DexDagexportContractenLening" record from the data model "Argenta Demo"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Thu Nov 19 14:33:38 CET 2020
*/

public class DexDagexportContractenLening extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/DexDagexportContractenLening";

	public DexDagexportContractenLening(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public DexDagexportContractenLening(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public DexDagexportContractenLening(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public DexDagexportContractenLening(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public DexDagexportContractenLening(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public DexDagexportContractenLening(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public DexDagexportContractenLening(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public DexDagexportContractenLening(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Funder (Standard)
    *
    */

	public String get_Funder()
	{
		return this.getString("./funder");
	}

	public String get_Funder_After()
	{
		return (String)this.getValueAfter("./funder");
	}

	public String get_Funder_Before()
	{
		return (String)this.getValueBefore("./funder");
	}

	public void set_Funder(final String value)
	{
		this.setObject(value, "./funder", String.class);
	}

	public boolean is_Funder_Changed()
	{
	    return (((String)this.getValueAfter("./funder")) != null && 
	        !((String)this.getValueAfter("./funder")).equals((this.getValueBefore("./funder"))));
	}

	private static final com.orchestranetworks.schema.Path path_Funder = com.orchestranetworks.schema.Path.parse("./funder");
	public static final com.orchestranetworks.schema.Path getPath_Funder()
	{
		return path_Funder;
	}

   /* Get and Set methods for attribute:  
    * Depersonaliseer (Standard)
    *
    */

	public String get_Depersonaliseer()
	{
		return this.getString("./depersonaliseer");
	}

	public String get_Depersonaliseer_After()
	{
		return (String)this.getValueAfter("./depersonaliseer");
	}

	public String get_Depersonaliseer_Before()
	{
		return (String)this.getValueBefore("./depersonaliseer");
	}

	public void set_Depersonaliseer(final String value)
	{
		this.setObject(value, "./depersonaliseer", String.class);
	}

	public boolean is_Depersonaliseer_Changed()
	{
	    return (((String)this.getValueAfter("./depersonaliseer")) != null && 
	        !((String)this.getValueAfter("./depersonaliseer")).equals((this.getValueBefore("./depersonaliseer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Depersonaliseer = com.orchestranetworks.schema.Path.parse("./depersonaliseer");
	public static final com.orchestranetworks.schema.Path getPath_Depersonaliseer()
	{
		return path_Depersonaliseer;
	}

   /* Get and Set methods for attribute:  
    * Column Name (Standard)
    *
    */

	public String get_ColumnName()
	{
		return this.getString("./columnName");
	}

	public String get_ColumnName_After()
	{
		return (String)this.getValueAfter("./columnName");
	}

	public String get_ColumnName_Before()
	{
		return (String)this.getValueBefore("./columnName");
	}

	public void set_ColumnName(final String value)
	{
		this.setObject(value, "./columnName", String.class);
	}

	public boolean is_ColumnName_Changed()
	{
	    return (((String)this.getValueAfter("./columnName")) != null && 
	        !((String)this.getValueAfter("./columnName")).equals((this.getValueBefore("./columnName"))));
	}

	private static final com.orchestranetworks.schema.Path path_ColumnName = com.orchestranetworks.schema.Path.parse("./columnName");
	public static final com.orchestranetworks.schema.Path getPath_ColumnName()
	{
		return path_ColumnName;
	}

   /* Get and Set methods for attribute:  
    * Active (Standard)
    *
    */

	public String get_Active()
	{
		return this.getString("./active");
	}

	public String get_Active_After()
	{
		return (String)this.getValueAfter("./active");
	}

	public String get_Active_Before()
	{
		return (String)this.getValueBefore("./active");
	}

	public void set_Active(final String value)
	{
		this.setObject(value, "./active", String.class);
	}

	public boolean is_Active_Changed()
	{
	    return (((String)this.getValueAfter("./active")) != null && 
	        !((String)this.getValueAfter("./active")).equals((this.getValueBefore("./active"))));
	}

	private static final com.orchestranetworks.schema.Path path_Active = com.orchestranetworks.schema.Path.parse("./active");
	public static final com.orchestranetworks.schema.Path getPath_Active()
	{
		return path_Active;
	}

   /* Get and Set methods for attribute:  
    * Description (Dutch) (Standard)
    *
    */

	public String get_DescriptionDutch()
	{
		return this.getString("./descriptionDutch");
	}

	public String get_DescriptionDutch_After()
	{
		return (String)this.getValueAfter("./descriptionDutch");
	}

	public String get_DescriptionDutch_Before()
	{
		return (String)this.getValueBefore("./descriptionDutch");
	}

	public void set_DescriptionDutch(final String value)
	{
		this.setObject(value, "./descriptionDutch", String.class);
	}

	public boolean is_DescriptionDutch_Changed()
	{
	    return (((String)this.getValueAfter("./descriptionDutch")) != null && 
	        !((String)this.getValueAfter("./descriptionDutch")).equals((this.getValueBefore("./descriptionDutch"))));
	}

	private static final com.orchestranetworks.schema.Path path_DescriptionDutch = com.orchestranetworks.schema.Path.parse("./descriptionDutch");
	public static final com.orchestranetworks.schema.Path getPath_DescriptionDutch()
	{
		return path_DescriptionDutch;
	}

   /* Get and Set methods for attribute:  
    * Description (English) (Standard)
    *
    */

	public String get_DescriptionEnglish()
	{
		return this.getString("./descriptionEnglish");
	}

	public String get_DescriptionEnglish_After()
	{
		return (String)this.getValueAfter("./descriptionEnglish");
	}

	public String get_DescriptionEnglish_Before()
	{
		return (String)this.getValueBefore("./descriptionEnglish");
	}

	public void set_DescriptionEnglish(final String value)
	{
		this.setObject(value, "./descriptionEnglish", String.class);
	}

	public boolean is_DescriptionEnglish_Changed()
	{
	    return (((String)this.getValueAfter("./descriptionEnglish")) != null && 
	        !((String)this.getValueAfter("./descriptionEnglish")).equals((this.getValueBefore("./descriptionEnglish"))));
	}

	private static final com.orchestranetworks.schema.Path path_DescriptionEnglish = com.orchestranetworks.schema.Path.parse("./descriptionEnglish");
	public static final com.orchestranetworks.schema.Path getPath_DescriptionEnglish()
	{
		return path_DescriptionEnglish;
	}

   /* Get and Set methods for attribute:  
    * Example Values (Standard)
    *
    */

	public String get_ExampleValues()
	{
		return this.getString("./exampleValues");
	}

	public String get_ExampleValues_After()
	{
		return (String)this.getValueAfter("./exampleValues");
	}

	public String get_ExampleValues_Before()
	{
		return (String)this.getValueBefore("./exampleValues");
	}

	public void set_ExampleValues(final String value)
	{
		this.setObject(value, "./exampleValues", String.class);
	}

	public boolean is_ExampleValues_Changed()
	{
	    return (((String)this.getValueAfter("./exampleValues")) != null && 
	        !((String)this.getValueAfter("./exampleValues")).equals((this.getValueBefore("./exampleValues"))));
	}

	private static final com.orchestranetworks.schema.Path path_ExampleValues = com.orchestranetworks.schema.Path.parse("./exampleValues");
	public static final com.orchestranetworks.schema.Path getPath_ExampleValues()
	{
		return path_ExampleValues;
	}

   /* Get and Set methods for attribute:  
    * Voorbeeld data (Standard)
    *
    */

	public String get_VoorbeeldData()
	{
		return this.getString("./voorbeeldData");
	}

	public String get_VoorbeeldData_After()
	{
		return (String)this.getValueAfter("./voorbeeldData");
	}

	public String get_VoorbeeldData_Before()
	{
		return (String)this.getValueBefore("./voorbeeldData");
	}

	public void set_VoorbeeldData(final String value)
	{
		this.setObject(value, "./voorbeeldData", String.class);
	}

	public boolean is_VoorbeeldData_Changed()
	{
	    return (((String)this.getValueAfter("./voorbeeldData")) != null && 
	        !((String)this.getValueAfter("./voorbeeldData")).equals((this.getValueBefore("./voorbeeldData"))));
	}

	private static final com.orchestranetworks.schema.Path path_VoorbeeldData = com.orchestranetworks.schema.Path.parse("./voorbeeldData");
	public static final com.orchestranetworks.schema.Path getPath_VoorbeeldData()
	{
		return path_VoorbeeldData;
	}

}

