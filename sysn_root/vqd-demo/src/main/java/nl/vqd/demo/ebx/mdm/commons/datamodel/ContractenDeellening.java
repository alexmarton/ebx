package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Contracten Deellening" record from the data model "Argenta Demo"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Thu Nov 19 14:33:38 CET 2020
*/

public class ContractenDeellening extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/contractenDeellening";

	public ContractenDeellening(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public ContractenDeellening(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public ContractenDeellening(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public ContractenDeellening(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public ContractenDeellening(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public ContractenDeellening(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public ContractenDeellening(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public ContractenDeellening(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * rapportagedatum (Standard)
    *
    */

	public java.util.Date get_Rapportagedatum()
	{
		return this.getDate("./rapportagedatum");
	}

	public java.util.Date get_Rapportagedatum_After()
	{
		return (java.util.Date)this.getValueAfter("./rapportagedatum");
	}

	public java.util.Date get_Rapportagedatum_Before()
	{
		return (java.util.Date)this.getValueBefore("./rapportagedatum");
	}

	public void set_Rapportagedatum(final java.util.Date value)
	{
		this.setObject(value, "./rapportagedatum", java.util.Date.class);
	}

	public boolean is_Rapportagedatum_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./rapportagedatum")) != null && 
	        !((java.util.Date)this.getValueAfter("./rapportagedatum")).equals((this.getValueBefore("./rapportagedatum"))));
	}

	private static final com.orchestranetworks.schema.Path path_Rapportagedatum = com.orchestranetworks.schema.Path.parse("./rapportagedatum");
	public static final com.orchestranetworks.schema.Path getPath_Rapportagedatum()
	{
		return path_Rapportagedatum;
	}

   /* Get and Set methods for attribute:  
    * lening_contract_nummer (Standard)
    *
    */

	public String get_Lening_contract_nummer()
	{
		return this.getString("./lening_contract_nummer");
	}

	public String get_Lening_contract_nummer_After()
	{
		return (String)this.getValueAfter("./lening_contract_nummer");
	}

	public String get_Lening_contract_nummer_Before()
	{
		return (String)this.getValueBefore("./lening_contract_nummer");
	}

	public void set_Lening_contract_nummer(final String value)
	{
		this.setObject(value, "./lening_contract_nummer", String.class);
	}

	public boolean is_Lening_contract_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./lening_contract_nummer")) != null && 
	        !((String)this.getValueAfter("./lening_contract_nummer")).equals((this.getValueBefore("./lening_contract_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_contract_nummer = com.orchestranetworks.schema.Path.parse("./lening_contract_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Lening_contract_nummer()
	{
		return path_Lening_contract_nummer;
	}

   /* Get and Set methods for attribute:  
    * contract_procesversie (Standard)
    *
    */

	public String get_Contract_procesversie()
	{
		return this.getString("./contract_procesversie");
	}

	public String get_Contract_procesversie_After()
	{
		return (String)this.getValueAfter("./contract_procesversie");
	}

	public String get_Contract_procesversie_Before()
	{
		return (String)this.getValueBefore("./contract_procesversie");
	}

	public void set_Contract_procesversie(final String value)
	{
		this.setObject(value, "./contract_procesversie", String.class);
	}

	public boolean is_Contract_procesversie_Changed()
	{
	    return (((String)this.getValueAfter("./contract_procesversie")) != null && 
	        !((String)this.getValueAfter("./contract_procesversie")).equals((this.getValueBefore("./contract_procesversie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Contract_procesversie = com.orchestranetworks.schema.Path.parse("./contract_procesversie");
	public static final com.orchestranetworks.schema.Path getPath_Contract_procesversie()
	{
		return path_Contract_procesversie;
	}

   /* Get and Set methods for attribute:  
    * lening_nummer (Standard)
    *
    */

	public String get_Lening_nummer()
	{
		return this.getString("./lening_nummer");
	}

	public String get_Lening_nummer_After()
	{
		return (String)this.getValueAfter("./lening_nummer");
	}

	public String get_Lening_nummer_Before()
	{
		return (String)this.getValueBefore("./lening_nummer");
	}

	public void set_Lening_nummer(final String value)
	{
		this.setObject(value, "./lening_nummer", String.class);
	}

	public boolean is_Lening_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./lening_nummer")) != null && 
	        !((String)this.getValueAfter("./lening_nummer")).equals((this.getValueBefore("./lening_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_nummer = com.orchestranetworks.schema.Path.parse("./lening_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Lening_nummer()
	{
		return path_Lening_nummer;
	}

   /* Get and Set methods for attribute:  
    * deellening_nummer (Standard)
    *
    */

	public String get_Deellening_nummer()
	{
		return this.getString("./deellening_nummer");
	}

	public String get_Deellening_nummer_After()
	{
		return (String)this.getValueAfter("./deellening_nummer");
	}

	public String get_Deellening_nummer_Before()
	{
		return (String)this.getValueBefore("./deellening_nummer");
	}

	public void set_Deellening_nummer(final String value)
	{
		this.setObject(value, "./deellening_nummer", String.class);
	}

	public boolean is_Deellening_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_nummer")) != null && 
	        !((String)this.getValueAfter("./deellening_nummer")).equals((this.getValueBefore("./deellening_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_nummer = com.orchestranetworks.schema.Path.parse("./deellening_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_nummer()
	{
		return path_Deellening_nummer;
	}

   /* Get and Set methods for attribute:  
    * economische_eigenaar_nummer (Standard)
    *
    */

	public String get_Economische_eigenaar_nummer()
	{
		return this.getString("./economische_eigenaar_nummer");
	}

	public String get_Economische_eigenaar_nummer_After()
	{
		return (String)this.getValueAfter("./economische_eigenaar_nummer");
	}

	public String get_Economische_eigenaar_nummer_Before()
	{
		return (String)this.getValueBefore("./economische_eigenaar_nummer");
	}

	public void set_Economische_eigenaar_nummer(final String value)
	{
		this.setObject(value, "./economische_eigenaar_nummer", String.class);
	}

	public boolean is_Economische_eigenaar_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./economische_eigenaar_nummer")) != null && 
	        !((String)this.getValueAfter("./economische_eigenaar_nummer")).equals((this.getValueBefore("./economische_eigenaar_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Economische_eigenaar_nummer = com.orchestranetworks.schema.Path.parse("./economische_eigenaar_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Economische_eigenaar_nummer()
	{
		return path_Economische_eigenaar_nummer;
	}

   /* Get and Set methods for attribute:  
    * datum_transactie_securitisatie (Standard)
    *
    */

	public String get_Datum_transactie_securitisatie()
	{
		return this.getString("./datum_transactie_securitisatie");
	}

	public String get_Datum_transactie_securitisatie_After()
	{
		return (String)this.getValueAfter("./datum_transactie_securitisatie");
	}

	public String get_Datum_transactie_securitisatie_Before()
	{
		return (String)this.getValueBefore("./datum_transactie_securitisatie");
	}

	public void set_Datum_transactie_securitisatie(final String value)
	{
		this.setObject(value, "./datum_transactie_securitisatie", String.class);
	}

	public boolean is_Datum_transactie_securitisatie_Changed()
	{
	    return (((String)this.getValueAfter("./datum_transactie_securitisatie")) != null && 
	        !((String)this.getValueAfter("./datum_transactie_securitisatie")).equals((this.getValueBefore("./datum_transactie_securitisatie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_transactie_securitisatie = com.orchestranetworks.schema.Path.parse("./datum_transactie_securitisatie");
	public static final com.orchestranetworks.schema.Path getPath_Datum_transactie_securitisatie()
	{
		return path_Datum_transactie_securitisatie;
	}

   /* Get and Set methods for attribute:  
    * datum_toewijzing_securitisatie (Standard)
    *
    */

	public String get_Datum_toewijzing_securitisatie()
	{
		return this.getString("./datum_toewijzing_securitisatie");
	}

	public String get_Datum_toewijzing_securitisatie_After()
	{
		return (String)this.getValueAfter("./datum_toewijzing_securitisatie");
	}

	public String get_Datum_toewijzing_securitisatie_Before()
	{
		return (String)this.getValueBefore("./datum_toewijzing_securitisatie");
	}

	public void set_Datum_toewijzing_securitisatie(final String value)
	{
		this.setObject(value, "./datum_toewijzing_securitisatie", String.class);
	}

	public boolean is_Datum_toewijzing_securitisatie_Changed()
	{
	    return (((String)this.getValueAfter("./datum_toewijzing_securitisatie")) != null && 
	        !((String)this.getValueAfter("./datum_toewijzing_securitisatie")).equals((this.getValueBefore("./datum_toewijzing_securitisatie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_toewijzing_securitisatie = com.orchestranetworks.schema.Path.parse("./datum_toewijzing_securitisatie");
	public static final com.orchestranetworks.schema.Path getPath_Datum_toewijzing_securitisatie()
	{
		return path_Datum_toewijzing_securitisatie;
	}

   /* Get and Set methods for attribute:  
    * economische_eigenaar_kandidaat_nummer (Standard)
    *
    */

	public String get_Economische_eigenaar_kandidaat_nummer()
	{
		return this.getString("./economische_eigenaar_kandidaat_nummer");
	}

	public String get_Economische_eigenaar_kandidaat_nummer_After()
	{
		return (String)this.getValueAfter("./economische_eigenaar_kandidaat_nummer");
	}

	public String get_Economische_eigenaar_kandidaat_nummer_Before()
	{
		return (String)this.getValueBefore("./economische_eigenaar_kandidaat_nummer");
	}

	public void set_Economische_eigenaar_kandidaat_nummer(final String value)
	{
		this.setObject(value, "./economische_eigenaar_kandidaat_nummer", String.class);
	}

	public boolean is_Economische_eigenaar_kandidaat_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./economische_eigenaar_kandidaat_nummer")) != null && 
	        !((String)this.getValueAfter("./economische_eigenaar_kandidaat_nummer")).equals((this.getValueBefore("./economische_eigenaar_kandidaat_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Economische_eigenaar_kandidaat_nummer = com.orchestranetworks.schema.Path.parse("./economische_eigenaar_kandidaat_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Economische_eigenaar_kandidaat_nummer()
	{
		return path_Economische_eigenaar_kandidaat_nummer;
	}

   /* Get and Set methods for attribute:  
    * economische_eigenaar_concept_nummer (Standard)
    *
    */

	public String get_Economische_eigenaar_concept_nummer()
	{
		return this.getString("./economische_eigenaar_concept_nummer");
	}

	public String get_Economische_eigenaar_concept_nummer_After()
	{
		return (String)this.getValueAfter("./economische_eigenaar_concept_nummer");
	}

	public String get_Economische_eigenaar_concept_nummer_Before()
	{
		return (String)this.getValueBefore("./economische_eigenaar_concept_nummer");
	}

	public void set_Economische_eigenaar_concept_nummer(final String value)
	{
		this.setObject(value, "./economische_eigenaar_concept_nummer", String.class);
	}

	public boolean is_Economische_eigenaar_concept_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./economische_eigenaar_concept_nummer")) != null && 
	        !((String)this.getValueAfter("./economische_eigenaar_concept_nummer")).equals((this.getValueBefore("./economische_eigenaar_concept_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Economische_eigenaar_concept_nummer = com.orchestranetworks.schema.Path.parse("./economische_eigenaar_concept_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Economische_eigenaar_concept_nummer()
	{
		return path_Economische_eigenaar_concept_nummer;
	}

   /* Get and Set methods for attribute:  
    * deellening_type_rentebedenktijd (Standard)
    *
    */

	public String get_Deellening_type_rentebedenktijd()
	{
		return this.getString("./deellening_type_rentebedenktijd");
	}

	public String get_Deellening_type_rentebedenktijd_After()
	{
		return (String)this.getValueAfter("./deellening_type_rentebedenktijd");
	}

	public String get_Deellening_type_rentebedenktijd_Before()
	{
		return (String)this.getValueBefore("./deellening_type_rentebedenktijd");
	}

	public void set_Deellening_type_rentebedenktijd(final String value)
	{
		this.setObject(value, "./deellening_type_rentebedenktijd", String.class);
	}

	public boolean is_Deellening_type_rentebedenktijd_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_type_rentebedenktijd")) != null && 
	        !((String)this.getValueAfter("./deellening_type_rentebedenktijd")).equals((this.getValueBefore("./deellening_type_rentebedenktijd"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_type_rentebedenktijd = com.orchestranetworks.schema.Path.parse("./deellening_type_rentebedenktijd");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_type_rentebedenktijd()
	{
		return path_Deellening_type_rentebedenktijd;
	}

   /* Get and Set methods for attribute:  
    * bedrag_oorspronkelijke_deelsom (Standard)
    *
    */

	public String get_Bedrag_oorspronkelijke_deelsom()
	{
		return this.getString("./bedrag_oorspronkelijke_deelsom");
	}

	public String get_Bedrag_oorspronkelijke_deelsom_After()
	{
		return (String)this.getValueAfter("./bedrag_oorspronkelijke_deelsom");
	}

	public String get_Bedrag_oorspronkelijke_deelsom_Before()
	{
		return (String)this.getValueBefore("./bedrag_oorspronkelijke_deelsom");
	}

	public void set_Bedrag_oorspronkelijke_deelsom(final String value)
	{
		this.setObject(value, "./bedrag_oorspronkelijke_deelsom", String.class);
	}

	public boolean is_Bedrag_oorspronkelijke_deelsom_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_oorspronkelijke_deelsom")) != null && 
	        !((String)this.getValueAfter("./bedrag_oorspronkelijke_deelsom")).equals((this.getValueBefore("./bedrag_oorspronkelijke_deelsom"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_oorspronkelijke_deelsom = com.orchestranetworks.schema.Path.parse("./bedrag_oorspronkelijke_deelsom");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_oorspronkelijke_deelsom()
	{
		return path_Bedrag_oorspronkelijke_deelsom;
	}

   /* Get and Set methods for attribute:  
    * indicator_inkomende_contractsovername (Standard)
    *
    */

	public String get_Indicator_inkomende_contractsovername()
	{
		return this.getString("./indicator_inkomende_contractsovername");
	}

	public String get_Indicator_inkomende_contractsovername_After()
	{
		return (String)this.getValueAfter("./indicator_inkomende_contractsovername");
	}

	public String get_Indicator_inkomende_contractsovername_Before()
	{
		return (String)this.getValueBefore("./indicator_inkomende_contractsovername");
	}

	public void set_Indicator_inkomende_contractsovername(final String value)
	{
		this.setObject(value, "./indicator_inkomende_contractsovername", String.class);
	}

	public boolean is_Indicator_inkomende_contractsovername_Changed()
	{
	    return (((String)this.getValueAfter("./indicator_inkomende_contractsovername")) != null && 
	        !((String)this.getValueAfter("./indicator_inkomende_contractsovername")).equals((this.getValueBefore("./indicator_inkomende_contractsovername"))));
	}

	private static final com.orchestranetworks.schema.Path path_Indicator_inkomende_contractsovername = com.orchestranetworks.schema.Path.parse("./indicator_inkomende_contractsovername");
	public static final com.orchestranetworks.schema.Path getPath_Indicator_inkomende_contractsovername()
	{
		return path_Indicator_inkomende_contractsovername;
	}

   /* Get and Set methods for attribute:  
    * indicator_uitgaande_contractsovername (Standard)
    *
    */

	public String get_Indicator_uitgaande_contractsovername()
	{
		return this.getString("./indicator_uitgaande_contractsovername");
	}

	public String get_Indicator_uitgaande_contractsovername_After()
	{
		return (String)this.getValueAfter("./indicator_uitgaande_contractsovername");
	}

	public String get_Indicator_uitgaande_contractsovername_Before()
	{
		return (String)this.getValueBefore("./indicator_uitgaande_contractsovername");
	}

	public void set_Indicator_uitgaande_contractsovername(final String value)
	{
		this.setObject(value, "./indicator_uitgaande_contractsovername", String.class);
	}

	public boolean is_Indicator_uitgaande_contractsovername_Changed()
	{
	    return (((String)this.getValueAfter("./indicator_uitgaande_contractsovername")) != null && 
	        !((String)this.getValueAfter("./indicator_uitgaande_contractsovername")).equals((this.getValueBefore("./indicator_uitgaande_contractsovername"))));
	}

	private static final com.orchestranetworks.schema.Path path_Indicator_uitgaande_contractsovername = com.orchestranetworks.schema.Path.parse("./indicator_uitgaande_contractsovername");
	public static final com.orchestranetworks.schema.Path getPath_Indicator_uitgaande_contractsovername()
	{
		return path_Indicator_uitgaande_contractsovername;
	}

   /* Get and Set methods for attribute:  
    * datum_start_deellening (Standard)
    *
    */

	public String get_Datum_start_deellening()
	{
		return this.getString("./datum_start_deellening");
	}

	public String get_Datum_start_deellening_After()
	{
		return (String)this.getValueAfter("./datum_start_deellening");
	}

	public String get_Datum_start_deellening_Before()
	{
		return (String)this.getValueBefore("./datum_start_deellening");
	}

	public void set_Datum_start_deellening(final String value)
	{
		this.setObject(value, "./datum_start_deellening", String.class);
	}

	public boolean is_Datum_start_deellening_Changed()
	{
	    return (((String)this.getValueAfter("./datum_start_deellening")) != null && 
	        !((String)this.getValueAfter("./datum_start_deellening")).equals((this.getValueBefore("./datum_start_deellening"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_start_deellening = com.orchestranetworks.schema.Path.parse("./datum_start_deellening");
	public static final com.orchestranetworks.schema.Path getPath_Datum_start_deellening()
	{
		return path_Datum_start_deellening;
	}

   /* Get and Set methods for attribute:  
    * datum_start_rentevast_periode (Standard)
    *
    */

	public String get_Datum_start_rentevast_periode()
	{
		return this.getString("./datum_start_rentevast_periode");
	}

	public String get_Datum_start_rentevast_periode_After()
	{
		return (String)this.getValueAfter("./datum_start_rentevast_periode");
	}

	public String get_Datum_start_rentevast_periode_Before()
	{
		return (String)this.getValueBefore("./datum_start_rentevast_periode");
	}

	public void set_Datum_start_rentevast_periode(final String value)
	{
		this.setObject(value, "./datum_start_rentevast_periode", String.class);
	}

	public boolean is_Datum_start_rentevast_periode_Changed()
	{
	    return (((String)this.getValueAfter("./datum_start_rentevast_periode")) != null && 
	        !((String)this.getValueAfter("./datum_start_rentevast_periode")).equals((this.getValueBefore("./datum_start_rentevast_periode"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_start_rentevast_periode = com.orchestranetworks.schema.Path.parse("./datum_start_rentevast_periode");
	public static final com.orchestranetworks.schema.Path getPath_Datum_start_rentevast_periode()
	{
		return path_Datum_start_rentevast_periode;
	}

   /* Get and Set methods for attribute:  
    * datum_einde_deelovereenkomst (Standard)
    *
    */

	public String get_Datum_einde_deelovereenkomst()
	{
		return this.getString("./datum_einde_deelovereenkomst");
	}

	public String get_Datum_einde_deelovereenkomst_After()
	{
		return (String)this.getValueAfter("./datum_einde_deelovereenkomst");
	}

	public String get_Datum_einde_deelovereenkomst_Before()
	{
		return (String)this.getValueBefore("./datum_einde_deelovereenkomst");
	}

	public void set_Datum_einde_deelovereenkomst(final String value)
	{
		this.setObject(value, "./datum_einde_deelovereenkomst", String.class);
	}

	public boolean is_Datum_einde_deelovereenkomst_Changed()
	{
	    return (((String)this.getValueAfter("./datum_einde_deelovereenkomst")) != null && 
	        !((String)this.getValueAfter("./datum_einde_deelovereenkomst")).equals((this.getValueBefore("./datum_einde_deelovereenkomst"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_einde_deelovereenkomst = com.orchestranetworks.schema.Path.parse("./datum_einde_deelovereenkomst");
	public static final com.orchestranetworks.schema.Path getPath_Datum_einde_deelovereenkomst()
	{
		return path_Datum_einde_deelovereenkomst;
	}

   /* Get and Set methods for attribute:  
    * datum_renteherziening (Standard)
    *
    */

	public String get_Datum_renteherziening()
	{
		return this.getString("./datum_renteherziening");
	}

	public String get_Datum_renteherziening_After()
	{
		return (String)this.getValueAfter("./datum_renteherziening");
	}

	public String get_Datum_renteherziening_Before()
	{
		return (String)this.getValueBefore("./datum_renteherziening");
	}

	public void set_Datum_renteherziening(final String value)
	{
		this.setObject(value, "./datum_renteherziening", String.class);
	}

	public boolean is_Datum_renteherziening_Changed()
	{
	    return (((String)this.getValueAfter("./datum_renteherziening")) != null && 
	        !((String)this.getValueAfter("./datum_renteherziening")).equals((this.getValueBefore("./datum_renteherziening"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_renteherziening = com.orchestranetworks.schema.Path.parse("./datum_renteherziening");
	public static final com.orchestranetworks.schema.Path getPath_Datum_renteherziening()
	{
		return path_Datum_renteherziening;
	}

   /* Get and Set methods for attribute:  
    * deellening_aantal_maanden_rentebedenktijd (Standard)
    *
    */

	public String get_Deellening_aantal_maanden_rentebedenktijd()
	{
		return this.getString("./deellening_aantal_maanden_rentebedenktijd");
	}

	public String get_Deellening_aantal_maanden_rentebedenktijd_After()
	{
		return (String)this.getValueAfter("./deellening_aantal_maanden_rentebedenktijd");
	}

	public String get_Deellening_aantal_maanden_rentebedenktijd_Before()
	{
		return (String)this.getValueBefore("./deellening_aantal_maanden_rentebedenktijd");
	}

	public void set_Deellening_aantal_maanden_rentebedenktijd(final String value)
	{
		this.setObject(value, "./deellening_aantal_maanden_rentebedenktijd", String.class);
	}

	public boolean is_Deellening_aantal_maanden_rentebedenktijd_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_aantal_maanden_rentebedenktijd")) != null && 
	        !((String)this.getValueAfter("./deellening_aantal_maanden_rentebedenktijd")).equals((this.getValueBefore("./deellening_aantal_maanden_rentebedenktijd"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_aantal_maanden_rentebedenktijd = com.orchestranetworks.schema.Path.parse("./deellening_aantal_maanden_rentebedenktijd");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_aantal_maanden_rentebedenktijd()
	{
		return path_Deellening_aantal_maanden_rentebedenktijd;
	}

   /* Get and Set methods for attribute:  
    * deellening_percentage_bovenmarge_en_ondermarge (Standard)
    *
    */

	public String get_Deellening_percentage_bovenmarge_en_ondermarge()
	{
		return this.getString("./deellening_percentage_bovenmarge_en_ondermarge");
	}

	public String get_Deellening_percentage_bovenmarge_en_ondermarge_After()
	{
		return (String)this.getValueAfter("./deellening_percentage_bovenmarge_en_ondermarge");
	}

	public String get_Deellening_percentage_bovenmarge_en_ondermarge_Before()
	{
		return (String)this.getValueBefore("./deellening_percentage_bovenmarge_en_ondermarge");
	}

	public void set_Deellening_percentage_bovenmarge_en_ondermarge(final String value)
	{
		this.setObject(value, "./deellening_percentage_bovenmarge_en_ondermarge", String.class);
	}

	public boolean is_Deellening_percentage_bovenmarge_en_ondermarge_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_percentage_bovenmarge_en_ondermarge")) != null && 
	        !((String)this.getValueAfter("./deellening_percentage_bovenmarge_en_ondermarge")).equals((this.getValueBefore("./deellening_percentage_bovenmarge_en_ondermarge"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_percentage_bovenmarge_en_ondermarge = com.orchestranetworks.schema.Path.parse("./deellening_percentage_bovenmarge_en_ondermarge");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_percentage_bovenmarge_en_ondermarge()
	{
		return path_Deellening_percentage_bovenmarge_en_ondermarge;
	}

   /* Get and Set methods for attribute:  
    * aflossingsvorm_omschrijving (Standard)
    *
    */

	public String get_Aflossingsvorm_omschrijving()
	{
		return this.getString("./aflossingsvorm_omschrijving");
	}

	public String get_Aflossingsvorm_omschrijving_After()
	{
		return (String)this.getValueAfter("./aflossingsvorm_omschrijving");
	}

	public String get_Aflossingsvorm_omschrijving_Before()
	{
		return (String)this.getValueBefore("./aflossingsvorm_omschrijving");
	}

	public void set_Aflossingsvorm_omschrijving(final String value)
	{
		this.setObject(value, "./aflossingsvorm_omschrijving", String.class);
	}

	public boolean is_Aflossingsvorm_omschrijving_Changed()
	{
	    return (((String)this.getValueAfter("./aflossingsvorm_omschrijving")) != null && 
	        !((String)this.getValueAfter("./aflossingsvorm_omschrijving")).equals((this.getValueBefore("./aflossingsvorm_omschrijving"))));
	}

	private static final com.orchestranetworks.schema.Path path_Aflossingsvorm_omschrijving = com.orchestranetworks.schema.Path.parse("./aflossingsvorm_omschrijving");
	public static final com.orchestranetworks.schema.Path getPath_Aflossingsvorm_omschrijving()
	{
		return path_Aflossingsvorm_omschrijving;
	}

   /* Get and Set methods for attribute:  
    * deellening_code_betalingsfrequentie (Standard)
    *
    */

	public String get_Deellening_code_betalingsfrequentie()
	{
		return this.getString("./deellening_code_betalingsfrequentie");
	}

	public String get_Deellening_code_betalingsfrequentie_After()
	{
		return (String)this.getValueAfter("./deellening_code_betalingsfrequentie");
	}

	public String get_Deellening_code_betalingsfrequentie_Before()
	{
		return (String)this.getValueBefore("./deellening_code_betalingsfrequentie");
	}

	public void set_Deellening_code_betalingsfrequentie(final String value)
	{
		this.setObject(value, "./deellening_code_betalingsfrequentie", String.class);
	}

	public boolean is_Deellening_code_betalingsfrequentie_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_code_betalingsfrequentie")) != null && 
	        !((String)this.getValueAfter("./deellening_code_betalingsfrequentie")).equals((this.getValueBefore("./deellening_code_betalingsfrequentie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_code_betalingsfrequentie = com.orchestranetworks.schema.Path.parse("./deellening_code_betalingsfrequentie");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_code_betalingsfrequentie()
	{
		return path_Deellening_code_betalingsfrequentie;
	}

   /* Get and Set methods for attribute:  
    * deellening_datum_eerste_incasso (Standard)
    *
    */

	public String get_Deellening_datum_eerste_incasso()
	{
		return this.getString("./deellening_datum_eerste_incasso");
	}

	public String get_Deellening_datum_eerste_incasso_After()
	{
		return (String)this.getValueAfter("./deellening_datum_eerste_incasso");
	}

	public String get_Deellening_datum_eerste_incasso_Before()
	{
		return (String)this.getValueBefore("./deellening_datum_eerste_incasso");
	}

	public void set_Deellening_datum_eerste_incasso(final String value)
	{
		this.setObject(value, "./deellening_datum_eerste_incasso", String.class);
	}

	public boolean is_Deellening_datum_eerste_incasso_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_datum_eerste_incasso")) != null && 
	        !((String)this.getValueAfter("./deellening_datum_eerste_incasso")).equals((this.getValueBefore("./deellening_datum_eerste_incasso"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_datum_eerste_incasso = com.orchestranetworks.schema.Path.parse("./deellening_datum_eerste_incasso");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_datum_eerste_incasso()
	{
		return path_Deellening_datum_eerste_incasso;
	}

   /* Get and Set methods for attribute:  
    * aantal_maanden_resterende_looptijd (Standard)
    *
    */

	public String get_Aantal_maanden_resterende_looptijd()
	{
		return this.getString("./aantal_maanden_resterende_looptijd");
	}

	public String get_Aantal_maanden_resterende_looptijd_After()
	{
		return (String)this.getValueAfter("./aantal_maanden_resterende_looptijd");
	}

	public String get_Aantal_maanden_resterende_looptijd_Before()
	{
		return (String)this.getValueBefore("./aantal_maanden_resterende_looptijd");
	}

	public void set_Aantal_maanden_resterende_looptijd(final String value)
	{
		this.setObject(value, "./aantal_maanden_resterende_looptijd", String.class);
	}

	public boolean is_Aantal_maanden_resterende_looptijd_Changed()
	{
	    return (((String)this.getValueAfter("./aantal_maanden_resterende_looptijd")) != null && 
	        !((String)this.getValueAfter("./aantal_maanden_resterende_looptijd")).equals((this.getValueBefore("./aantal_maanden_resterende_looptijd"))));
	}

	private static final com.orchestranetworks.schema.Path path_Aantal_maanden_resterende_looptijd = com.orchestranetworks.schema.Path.parse("./aantal_maanden_resterende_looptijd");
	public static final com.orchestranetworks.schema.Path getPath_Aantal_maanden_resterende_looptijd()
	{
		return path_Aantal_maanden_resterende_looptijd;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_type_mutatie (Standard)
    *
    */

	public String get_Omschrijving_type_mutatie()
	{
		return this.getString("./omschrijving_type_mutatie");
	}

	public String get_Omschrijving_type_mutatie_After()
	{
		return (String)this.getValueAfter("./omschrijving_type_mutatie");
	}

	public String get_Omschrijving_type_mutatie_Before()
	{
		return (String)this.getValueBefore("./omschrijving_type_mutatie");
	}

	public void set_Omschrijving_type_mutatie(final String value)
	{
		this.setObject(value, "./omschrijving_type_mutatie", String.class);
	}

	public boolean is_Omschrijving_type_mutatie_Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_type_mutatie")) != null && 
	        !((String)this.getValueAfter("./omschrijving_type_mutatie")).equals((this.getValueBefore("./omschrijving_type_mutatie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_type_mutatie = com.orchestranetworks.schema.Path.parse("./omschrijving_type_mutatie");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_type_mutatie()
	{
		return path_Omschrijving_type_mutatie;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_risicoklasse (Standard)
    *
    */

	public String get_Omschrijving_risicoklasse()
	{
		return this.getString("./omschrijving_risicoklasse");
	}

	public String get_Omschrijving_risicoklasse_After()
	{
		return (String)this.getValueAfter("./omschrijving_risicoklasse");
	}

	public String get_Omschrijving_risicoklasse_Before()
	{
		return (String)this.getValueBefore("./omschrijving_risicoklasse");
	}

	public void set_Omschrijving_risicoklasse(final String value)
	{
		this.setObject(value, "./omschrijving_risicoklasse", String.class);
	}

	public boolean is_Omschrijving_risicoklasse_Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_risicoklasse")) != null && 
	        !((String)this.getValueAfter("./omschrijving_risicoklasse")).equals((this.getValueBefore("./omschrijving_risicoklasse"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_risicoklasse = com.orchestranetworks.schema.Path.parse("./omschrijving_risicoklasse");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_risicoklasse()
	{
		return path_Omschrijving_risicoklasse;
	}

   /* Get and Set methods for attribute:  
    * bedrag_consumptief_gedeelte (Standard)
    *
    */

	public String get_Bedrag_consumptief_gedeelte()
	{
		return this.getString("./bedrag_consumptief_gedeelte");
	}

	public String get_Bedrag_consumptief_gedeelte_After()
	{
		return (String)this.getValueAfter("./bedrag_consumptief_gedeelte");
	}

	public String get_Bedrag_consumptief_gedeelte_Before()
	{
		return (String)this.getValueBefore("./bedrag_consumptief_gedeelte");
	}

	public void set_Bedrag_consumptief_gedeelte(final String value)
	{
		this.setObject(value, "./bedrag_consumptief_gedeelte", String.class);
	}

	public boolean is_Bedrag_consumptief_gedeelte_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_consumptief_gedeelte")) != null && 
	        !((String)this.getValueAfter("./bedrag_consumptief_gedeelte")).equals((this.getValueBefore("./bedrag_consumptief_gedeelte"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_consumptief_gedeelte = com.orchestranetworks.schema.Path.parse("./bedrag_consumptief_gedeelte");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_consumptief_gedeelte()
	{
		return path_Bedrag_consumptief_gedeelte;
	}

   /* Get and Set methods for attribute:  
    * deellening_datum_einde_rentekorting (Standard)
    *
    */

	public String get_Deellening_datum_einde_rentekorting()
	{
		return this.getString("./deellening_datum_einde_rentekorting");
	}

	public String get_Deellening_datum_einde_rentekorting_After()
	{
		return (String)this.getValueAfter("./deellening_datum_einde_rentekorting");
	}

	public String get_Deellening_datum_einde_rentekorting_Before()
	{
		return (String)this.getValueBefore("./deellening_datum_einde_rentekorting");
	}

	public void set_Deellening_datum_einde_rentekorting(final String value)
	{
		this.setObject(value, "./deellening_datum_einde_rentekorting", String.class);
	}

	public boolean is_Deellening_datum_einde_rentekorting_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_datum_einde_rentekorting")) != null && 
	        !((String)this.getValueAfter("./deellening_datum_einde_rentekorting")).equals((this.getValueBefore("./deellening_datum_einde_rentekorting"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_datum_einde_rentekorting = com.orchestranetworks.schema.Path.parse("./deellening_datum_einde_rentekorting");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_datum_einde_rentekorting()
	{
		return path_Deellening_datum_einde_rentekorting;
	}

   /* Get and Set methods for attribute:  
    * deellening_looptijd (Standard)
    *
    */

	public String get_Deellening_looptijd()
	{
		return this.getString("./deellening_looptijd");
	}

	public String get_Deellening_looptijd_After()
	{
		return (String)this.getValueAfter("./deellening_looptijd");
	}

	public String get_Deellening_looptijd_Before()
	{
		return (String)this.getValueBefore("./deellening_looptijd");
	}

	public void set_Deellening_looptijd(final String value)
	{
		this.setObject(value, "./deellening_looptijd", String.class);
	}

	public boolean is_Deellening_looptijd_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_looptijd")) != null && 
	        !((String)this.getValueAfter("./deellening_looptijd")).equals((this.getValueBefore("./deellening_looptijd"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_looptijd = com.orchestranetworks.schema.Path.parse("./deellening_looptijd");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_looptijd()
	{
		return path_Deellening_looptijd;
	}

   /* Get and Set methods for attribute:  
    * product_rentetype (Standard)
    *
    */

	public String get_Product_rentetype()
	{
		return this.getString("./product_rentetype");
	}

	public String get_Product_rentetype_After()
	{
		return (String)this.getValueAfter("./product_rentetype");
	}

	public String get_Product_rentetype_Before()
	{
		return (String)this.getValueBefore("./product_rentetype");
	}

	public void set_Product_rentetype(final String value)
	{
		this.setObject(value, "./product_rentetype", String.class);
	}

	public boolean is_Product_rentetype_Changed()
	{
	    return (((String)this.getValueAfter("./product_rentetype")) != null && 
	        !((String)this.getValueAfter("./product_rentetype")).equals((this.getValueBefore("./product_rentetype"))));
	}

	private static final com.orchestranetworks.schema.Path path_Product_rentetype = com.orchestranetworks.schema.Path.parse("./product_rentetype");
	public static final com.orchestranetworks.schema.Path getPath_Product_rentetype()
	{
		return path_Product_rentetype;
	}

   /* Get and Set methods for attribute:  
    * lening_indicator_BKW (Standard)
    *
    */

	public String get_Lening_indicator_BKW()
	{
		return this.getString("./lening_indicator_BKW");
	}

	public String get_Lening_indicator_BKW_After()
	{
		return (String)this.getValueAfter("./lening_indicator_BKW");
	}

	public String get_Lening_indicator_BKW_Before()
	{
		return (String)this.getValueBefore("./lening_indicator_BKW");
	}

	public void set_Lening_indicator_BKW(final String value)
	{
		this.setObject(value, "./lening_indicator_BKW", String.class);
	}

	public boolean is_Lening_indicator_BKW_Changed()
	{
	    return (((String)this.getValueAfter("./lening_indicator_BKW")) != null && 
	        !((String)this.getValueAfter("./lening_indicator_BKW")).equals((this.getValueBefore("./lening_indicator_BKW"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_indicator_BKW = com.orchestranetworks.schema.Path.parse("./lening_indicator_BKW");
	public static final com.orchestranetworks.schema.Path getPath_Lening_indicator_BKW()
	{
		return path_Lening_indicator_BKW;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentevastperiode (Standard)
    *
    */

	public String get_Deellening_rentevastperiode()
	{
		return this.getString("./deellening_rentevastperiode");
	}

	public String get_Deellening_rentevastperiode_After()
	{
		return (String)this.getValueAfter("./deellening_rentevastperiode");
	}

	public String get_Deellening_rentevastperiode_Before()
	{
		return (String)this.getValueBefore("./deellening_rentevastperiode");
	}

	public void set_Deellening_rentevastperiode(final String value)
	{
		this.setObject(value, "./deellening_rentevastperiode", String.class);
	}

	public boolean is_Deellening_rentevastperiode_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentevastperiode")) != null && 
	        !((String)this.getValueAfter("./deellening_rentevastperiode")).equals((this.getValueBefore("./deellening_rentevastperiode"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentevastperiode = com.orchestranetworks.schema.Path.parse("./deellening_rentevastperiode");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentevastperiode()
	{
		return path_Deellening_rentevastperiode;
	}

   /* Get and Set methods for attribute:  
    * bedrag_spaardepot (Standard)
    *
    */

	public String get_Bedrag_spaardepot()
	{
		return this.getString("./bedrag_spaardepot");
	}

	public String get_Bedrag_spaardepot_After()
	{
		return (String)this.getValueAfter("./bedrag_spaardepot");
	}

	public String get_Bedrag_spaardepot_Before()
	{
		return (String)this.getValueBefore("./bedrag_spaardepot");
	}

	public void set_Bedrag_spaardepot(final String value)
	{
		this.setObject(value, "./bedrag_spaardepot", String.class);
	}

	public boolean is_Bedrag_spaardepot_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_spaardepot")) != null && 
	        !((String)this.getValueAfter("./bedrag_spaardepot")).equals((this.getValueBefore("./bedrag_spaardepot"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_spaardepot = com.orchestranetworks.schema.Path.parse("./bedrag_spaardepot");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_spaardepot()
	{
		return path_Bedrag_spaardepot;
	}

   /* Get and Set methods for attribute:  
    * bedrag_restant_deelsom (Standard)
    *
    */

	public String get_Bedrag_restant_deelsom()
	{
		return this.getString("./bedrag_restant_deelsom");
	}

	public String get_Bedrag_restant_deelsom_After()
	{
		return (String)this.getValueAfter("./bedrag_restant_deelsom");
	}

	public String get_Bedrag_restant_deelsom_Before()
	{
		return (String)this.getValueBefore("./bedrag_restant_deelsom");
	}

	public void set_Bedrag_restant_deelsom(final String value)
	{
		this.setObject(value, "./bedrag_restant_deelsom", String.class);
	}

	public boolean is_Bedrag_restant_deelsom_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_restant_deelsom")) != null && 
	        !((String)this.getValueAfter("./bedrag_restant_deelsom")).equals((this.getValueBefore("./bedrag_restant_deelsom"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_restant_deelsom = com.orchestranetworks.schema.Path.parse("./bedrag_restant_deelsom");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_restant_deelsom()
	{
		return path_Bedrag_restant_deelsom;
	}

   /* Get and Set methods for attribute:  
    * tussenpersoon_nummer (Standard)
    *
    */

	public String get_Tussenpersoon_nummer()
	{
		return this.getString("./tussenpersoon_nummer");
	}

	public String get_Tussenpersoon_nummer_After()
	{
		return (String)this.getValueAfter("./tussenpersoon_nummer");
	}

	public String get_Tussenpersoon_nummer_Before()
	{
		return (String)this.getValueBefore("./tussenpersoon_nummer");
	}

	public void set_Tussenpersoon_nummer(final String value)
	{
		this.setObject(value, "./tussenpersoon_nummer", String.class);
	}

	public boolean is_Tussenpersoon_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./tussenpersoon_nummer")) != null && 
	        !((String)this.getValueAfter("./tussenpersoon_nummer")).equals((this.getValueBefore("./tussenpersoon_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Tussenpersoon_nummer = com.orchestranetworks.schema.Path.parse("./tussenpersoon_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Tussenpersoon_nummer()
	{
		return path_Tussenpersoon_nummer;
	}

   /* Get and Set methods for attribute:  
    * tussenpersoon_naam (Standard)
    *
    */

	public String get_Tussenpersoon_naam()
	{
		return this.getString("./tussenpersoon_naam");
	}

	public String get_Tussenpersoon_naam_After()
	{
		return (String)this.getValueAfter("./tussenpersoon_naam");
	}

	public String get_Tussenpersoon_naam_Before()
	{
		return (String)this.getValueBefore("./tussenpersoon_naam");
	}

	public void set_Tussenpersoon_naam(final String value)
	{
		this.setObject(value, "./tussenpersoon_naam", String.class);
	}

	public boolean is_Tussenpersoon_naam_Changed()
	{
	    return (((String)this.getValueAfter("./tussenpersoon_naam")) != null && 
	        !((String)this.getValueAfter("./tussenpersoon_naam")).equals((this.getValueBefore("./tussenpersoon_naam"))));
	}

	private static final com.orchestranetworks.schema.Path path_Tussenpersoon_naam = com.orchestranetworks.schema.Path.parse("./tussenpersoon_naam");
	public static final com.orchestranetworks.schema.Path getPath_Tussenpersoon_naam()
	{
		return path_Tussenpersoon_naam;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_basistarief (Standard)
    *
    */

	public String get_Deellening_rentepercentage_basistarief()
	{
		return this.getString("./deellening_rentepercentage_basistarief");
	}

	public String get_Deellening_rentepercentage_basistarief_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_basistarief");
	}

	public String get_Deellening_rentepercentage_basistarief_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_basistarief");
	}

	public void set_Deellening_rentepercentage_basistarief(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_basistarief", String.class);
	}

	public boolean is_Deellening_rentepercentage_basistarief_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_basistarief")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_basistarief")).equals((this.getValueBefore("./deellening_rentepercentage_basistarief"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_basistarief = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_basistarief");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_basistarief()
	{
		return path_Deellening_rentepercentage_basistarief;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_risico_opslag (Standard)
    *
    */

	public String get_Deellening_rentepercentage_risico_opslag()
	{
		return this.getString("./deellening_rentepercentage_risico_opslag");
	}

	public String get_Deellening_rentepercentage_risico_opslag_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_risico_opslag");
	}

	public String get_Deellening_rentepercentage_risico_opslag_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_risico_opslag");
	}

	public void set_Deellening_rentepercentage_risico_opslag(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_risico_opslag", String.class);
	}

	public boolean is_Deellening_rentepercentage_risico_opslag_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_risico_opslag")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_risico_opslag")).equals((this.getValueBefore("./deellening_rentepercentage_risico_opslag"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_risico_opslag = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_risico_opslag");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_risico_opslag()
	{
		return path_Deellening_rentepercentage_risico_opslag;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_procesopslag (Standard)
    *
    */

	public String get_Deellening_rentepercentage_procesopslag()
	{
		return this.getString("./deellening_rentepercentage_procesopslag");
	}

	public String get_Deellening_rentepercentage_procesopslag_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_procesopslag");
	}

	public String get_Deellening_rentepercentage_procesopslag_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_procesopslag");
	}

	public void set_Deellening_rentepercentage_procesopslag(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_procesopslag", String.class);
	}

	public boolean is_Deellening_rentepercentage_procesopslag_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_procesopslag")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_procesopslag")).equals((this.getValueBefore("./deellening_rentepercentage_procesopslag"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_procesopslag = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_procesopslag");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_procesopslag()
	{
		return path_Deellening_rentepercentage_procesopslag;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_productlijnopslag (Standard)
    *
    */

	public String get_Deellening_rentepercentage_productlijnopslag()
	{
		return this.getString("./deellening_rentepercentage_productlijnopslag");
	}

	public String get_Deellening_rentepercentage_productlijnopslag_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_productlijnopslag");
	}

	public String get_Deellening_rentepercentage_productlijnopslag_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_productlijnopslag");
	}

	public void set_Deellening_rentepercentage_productlijnopslag(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_productlijnopslag", String.class);
	}

	public boolean is_Deellening_rentepercentage_productlijnopslag_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_productlijnopslag")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_productlijnopslag")).equals((this.getValueBefore("./deellening_rentepercentage_productlijnopslag"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_productlijnopslag = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_productlijnopslag");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_productlijnopslag()
	{
		return path_Deellening_rentepercentage_productlijnopslag;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_productopslag (Standard)
    *
    */

	public String get_Deellening_rentepercentage_productopslag()
	{
		return this.getString("./deellening_rentepercentage_productopslag");
	}

	public String get_Deellening_rentepercentage_productopslag_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_productopslag");
	}

	public String get_Deellening_rentepercentage_productopslag_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_productopslag");
	}

	public void set_Deellening_rentepercentage_productopslag(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_productopslag", String.class);
	}

	public boolean is_Deellening_rentepercentage_productopslag_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_productopslag")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_productopslag")).equals((this.getValueBefore("./deellening_rentepercentage_productopslag"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_productopslag = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_productopslag");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_productopslag()
	{
		return path_Deellening_rentepercentage_productopslag;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_afspraakopslag (Standard)
    *
    */

	public String get_Deellening_rentepercentage_afspraakopslag()
	{
		return this.getString("./deellening_rentepercentage_afspraakopslag");
	}

	public String get_Deellening_rentepercentage_afspraakopslag_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_afspraakopslag");
	}

	public String get_Deellening_rentepercentage_afspraakopslag_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_afspraakopslag");
	}

	public void set_Deellening_rentepercentage_afspraakopslag(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_afspraakopslag", String.class);
	}

	public boolean is_Deellening_rentepercentage_afspraakopslag_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_afspraakopslag")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_afspraakopslag")).equals((this.getValueBefore("./deellening_rentepercentage_afspraakopslag"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_afspraakopslag = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_afspraakopslag");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_afspraakopslag()
	{
		return path_Deellening_rentepercentage_afspraakopslag;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_nominaal (Standard)
    *
    */

	public String get_Deellening_rentepercentage_nominaal()
	{
		return this.getString("./deellening_rentepercentage_nominaal");
	}

	public String get_Deellening_rentepercentage_nominaal_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_nominaal");
	}

	public String get_Deellening_rentepercentage_nominaal_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_nominaal");
	}

	public void set_Deellening_rentepercentage_nominaal(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_nominaal", String.class);
	}

	public boolean is_Deellening_rentepercentage_nominaal_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_nominaal")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_nominaal")).equals((this.getValueBefore("./deellening_rentepercentage_nominaal"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_nominaal = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_nominaal");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_nominaal()
	{
		return path_Deellening_rentepercentage_nominaal;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_effectief (Standard)
    *
    */

	public String get_Deellening_rentepercentage_effectief()
	{
		return this.getString("./deellening_rentepercentage_effectief");
	}

	public String get_Deellening_rentepercentage_effectief_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_effectief");
	}

	public String get_Deellening_rentepercentage_effectief_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_effectief");
	}

	public void set_Deellening_rentepercentage_effectief(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_effectief", String.class);
	}

	public boolean is_Deellening_rentepercentage_effectief_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_effectief")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_effectief")).equals((this.getValueBefore("./deellening_rentepercentage_effectief"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_effectief = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_effectief");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_effectief()
	{
		return path_Deellening_rentepercentage_effectief;
	}

   /* Get and Set methods for attribute:  
    * deellening_rentepercentage_nominaal_oorspronkelijk (Standard)
    *
    */

	public String get_Deellening_rentepercentage_nominaal_oorspronkelijk()
	{
		return this.getString("./deellening_rentepercentage_nominaal_oorspronkelijk");
	}

	public String get_Deellening_rentepercentage_nominaal_oorspronkelijk_After()
	{
		return (String)this.getValueAfter("./deellening_rentepercentage_nominaal_oorspronkelijk");
	}

	public String get_Deellening_rentepercentage_nominaal_oorspronkelijk_Before()
	{
		return (String)this.getValueBefore("./deellening_rentepercentage_nominaal_oorspronkelijk");
	}

	public void set_Deellening_rentepercentage_nominaal_oorspronkelijk(final String value)
	{
		this.setObject(value, "./deellening_rentepercentage_nominaal_oorspronkelijk", String.class);
	}

	public boolean is_Deellening_rentepercentage_nominaal_oorspronkelijk_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_rentepercentage_nominaal_oorspronkelijk")) != null && 
	        !((String)this.getValueAfter("./deellening_rentepercentage_nominaal_oorspronkelijk")).equals((this.getValueBefore("./deellening_rentepercentage_nominaal_oorspronkelijk"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_rentepercentage_nominaal_oorspronkelijk = com.orchestranetworks.schema.Path.parse("./deellening_rentepercentage_nominaal_oorspronkelijk");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_rentepercentage_nominaal_oorspronkelijk()
	{
		return path_Deellening_rentepercentage_nominaal_oorspronkelijk;
	}

   /* Get and Set methods for attribute:  
    * product_rentepercentage_basistarief_actueel (Standard)
    *
    */

	public String get_Product_rentepercentage_basistarief_actueel()
	{
		return this.getString("./product_rentepercentage_basistarief_actueel");
	}

	public String get_Product_rentepercentage_basistarief_actueel_After()
	{
		return (String)this.getValueAfter("./product_rentepercentage_basistarief_actueel");
	}

	public String get_Product_rentepercentage_basistarief_actueel_Before()
	{
		return (String)this.getValueBefore("./product_rentepercentage_basistarief_actueel");
	}

	public void set_Product_rentepercentage_basistarief_actueel(final String value)
	{
		this.setObject(value, "./product_rentepercentage_basistarief_actueel", String.class);
	}

	public boolean is_Product_rentepercentage_basistarief_actueel_Changed()
	{
	    return (((String)this.getValueAfter("./product_rentepercentage_basistarief_actueel")) != null && 
	        !((String)this.getValueAfter("./product_rentepercentage_basistarief_actueel")).equals((this.getValueBefore("./product_rentepercentage_basistarief_actueel"))));
	}

	private static final com.orchestranetworks.schema.Path path_Product_rentepercentage_basistarief_actueel = com.orchestranetworks.schema.Path.parse("./product_rentepercentage_basistarief_actueel");
	public static final com.orchestranetworks.schema.Path getPath_Product_rentepercentage_basistarief_actueel()
	{
		return path_Product_rentepercentage_basistarief_actueel;
	}

   /* Get and Set methods for attribute:  
    * bedrag_rente_huidige_maand (Standard)
    *
    */

	public String get_Bedrag_rente_huidige_maand()
	{
		return this.getString("./bedrag_rente_huidige_maand");
	}

	public String get_Bedrag_rente_huidige_maand_After()
	{
		return (String)this.getValueAfter("./bedrag_rente_huidige_maand");
	}

	public String get_Bedrag_rente_huidige_maand_Before()
	{
		return (String)this.getValueBefore("./bedrag_rente_huidige_maand");
	}

	public void set_Bedrag_rente_huidige_maand(final String value)
	{
		this.setObject(value, "./bedrag_rente_huidige_maand", String.class);
	}

	public boolean is_Bedrag_rente_huidige_maand_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_rente_huidige_maand")) != null && 
	        !((String)this.getValueAfter("./bedrag_rente_huidige_maand")).equals((this.getValueBefore("./bedrag_rente_huidige_maand"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_rente_huidige_maand = com.orchestranetworks.schema.Path.parse("./bedrag_rente_huidige_maand");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_rente_huidige_maand()
	{
		return path_Bedrag_rente_huidige_maand;
	}

   /* Get and Set methods for attribute:  
    * bedrag_aflossing_huidige_maand (Standard)
    *
    */

	public String get_Bedrag_aflossing_huidige_maand()
	{
		return this.getString("./bedrag_aflossing_huidige_maand");
	}

	public String get_Bedrag_aflossing_huidige_maand_After()
	{
		return (String)this.getValueAfter("./bedrag_aflossing_huidige_maand");
	}

	public String get_Bedrag_aflossing_huidige_maand_Before()
	{
		return (String)this.getValueBefore("./bedrag_aflossing_huidige_maand");
	}

	public void set_Bedrag_aflossing_huidige_maand(final String value)
	{
		this.setObject(value, "./bedrag_aflossing_huidige_maand", String.class);
	}

	public boolean is_Bedrag_aflossing_huidige_maand_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_aflossing_huidige_maand")) != null && 
	        !((String)this.getValueAfter("./bedrag_aflossing_huidige_maand")).equals((this.getValueBefore("./bedrag_aflossing_huidige_maand"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_aflossing_huidige_maand = com.orchestranetworks.schema.Path.parse("./bedrag_aflossing_huidige_maand");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_aflossing_huidige_maand()
	{
		return path_Bedrag_aflossing_huidige_maand;
	}

   /* Get and Set methods for attribute:  
    * serviceprovider_naam (Standard)
    *
    */

	public String get_Serviceprovider_naam()
	{
		return this.getString("./serviceprovider_naam");
	}

	public String get_Serviceprovider_naam_After()
	{
		return (String)this.getValueAfter("./serviceprovider_naam");
	}

	public String get_Serviceprovider_naam_Before()
	{
		return (String)this.getValueBefore("./serviceprovider_naam");
	}

	public void set_Serviceprovider_naam(final String value)
	{
		this.setObject(value, "./serviceprovider_naam", String.class);
	}

	public boolean is_Serviceprovider_naam_Changed()
	{
	    return (((String)this.getValueAfter("./serviceprovider_naam")) != null && 
	        !((String)this.getValueAfter("./serviceprovider_naam")).equals((this.getValueBefore("./serviceprovider_naam"))));
	}

	private static final com.orchestranetworks.schema.Path path_Serviceprovider_naam = com.orchestranetworks.schema.Path.parse("./serviceprovider_naam");
	public static final com.orchestranetworks.schema.Path getPath_Serviceprovider_naam()
	{
		return path_Serviceprovider_naam;
	}

   /* Get and Set methods for attribute:  
    * product_model (Standard)
    *
    */

	public String get_Product_model()
	{
		return this.getString("./product_model");
	}

	public String get_Product_model_After()
	{
		return (String)this.getValueAfter("./product_model");
	}

	public String get_Product_model_Before()
	{
		return (String)this.getValueBefore("./product_model");
	}

	public void set_Product_model(final String value)
	{
		this.setObject(value, "./product_model", String.class);
	}

	public boolean is_Product_model_Changed()
	{
	    return (((String)this.getValueAfter("./product_model")) != null && 
	        !((String)this.getValueAfter("./product_model")).equals((this.getValueBefore("./product_model"))));
	}

	private static final com.orchestranetworks.schema.Path path_Product_model = com.orchestranetworks.schema.Path.parse("./product_model");
	public static final com.orchestranetworks.schema.Path getPath_Product_model()
	{
		return path_Product_model;
	}

   /* Get and Set methods for attribute:  
    * product_lijn (Standard)
    *
    */

	public String get_Product_lijn()
	{
		return this.getString("./product_lijn");
	}

	public String get_Product_lijn_After()
	{
		return (String)this.getValueAfter("./product_lijn");
	}

	public String get_Product_lijn_Before()
	{
		return (String)this.getValueBefore("./product_lijn");
	}

	public void set_Product_lijn(final String value)
	{
		this.setObject(value, "./product_lijn", String.class);
	}

	public boolean is_Product_lijn_Changed()
	{
	    return (((String)this.getValueAfter("./product_lijn")) != null && 
	        !((String)this.getValueAfter("./product_lijn")).equals((this.getValueBefore("./product_lijn"))));
	}

	private static final com.orchestranetworks.schema.Path path_Product_lijn = com.orchestranetworks.schema.Path.parse("./product_lijn");
	public static final com.orchestranetworks.schema.Path getPath_Product_lijn()
	{
		return path_Product_lijn;
	}

   /* Get and Set methods for attribute:  
    * datum_toetsing_variabele_rente (Standard)
    *
    */

	public String get_Datum_toetsing_variabele_rente()
	{
		return this.getString("./datum_toetsing_variabele_rente");
	}

	public String get_Datum_toetsing_variabele_rente_After()
	{
		return (String)this.getValueAfter("./datum_toetsing_variabele_rente");
	}

	public String get_Datum_toetsing_variabele_rente_Before()
	{
		return (String)this.getValueBefore("./datum_toetsing_variabele_rente");
	}

	public void set_Datum_toetsing_variabele_rente(final String value)
	{
		this.setObject(value, "./datum_toetsing_variabele_rente", String.class);
	}

	public boolean is_Datum_toetsing_variabele_rente_Changed()
	{
	    return (((String)this.getValueAfter("./datum_toetsing_variabele_rente")) != null && 
	        !((String)this.getValueAfter("./datum_toetsing_variabele_rente")).equals((this.getValueBefore("./datum_toetsing_variabele_rente"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_toetsing_variabele_rente = com.orchestranetworks.schema.Path.parse("./datum_toetsing_variabele_rente");
	public static final com.orchestranetworks.schema.Path getPath_Datum_toetsing_variabele_rente()
	{
		return path_Datum_toetsing_variabele_rente;
	}

   /* Get and Set methods for attribute:  
    * bedrag_oorspronkelijke_deeldepot (Standard)
    *
    */

	public String get_Bedrag_oorspronkelijke_deeldepot()
	{
		return this.getString("./bedrag_oorspronkelijke_deeldepot");
	}

	public String get_Bedrag_oorspronkelijke_deeldepot_After()
	{
		return (String)this.getValueAfter("./bedrag_oorspronkelijke_deeldepot");
	}

	public String get_Bedrag_oorspronkelijke_deeldepot_Before()
	{
		return (String)this.getValueBefore("./bedrag_oorspronkelijke_deeldepot");
	}

	public void set_Bedrag_oorspronkelijke_deeldepot(final String value)
	{
		this.setObject(value, "./bedrag_oorspronkelijke_deeldepot", String.class);
	}

	public boolean is_Bedrag_oorspronkelijke_deeldepot_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_oorspronkelijke_deeldepot")) != null && 
	        !((String)this.getValueAfter("./bedrag_oorspronkelijke_deeldepot")).equals((this.getValueBefore("./bedrag_oorspronkelijke_deeldepot"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_oorspronkelijke_deeldepot = com.orchestranetworks.schema.Path.parse("./bedrag_oorspronkelijke_deeldepot");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_oorspronkelijke_deeldepot()
	{
		return path_Bedrag_oorspronkelijke_deeldepot;
	}

   /* Get and Set methods for attribute:  
    * bedrag_deeldepot (Standard)
    *
    */

	public String get_Bedrag_deeldepot()
	{
		return this.getString("./bedrag_deeldepot");
	}

	public String get_Bedrag_deeldepot_After()
	{
		return (String)this.getValueAfter("./bedrag_deeldepot");
	}

	public String get_Bedrag_deeldepot_Before()
	{
		return (String)this.getValueBefore("./bedrag_deeldepot");
	}

	public void set_Bedrag_deeldepot(final String value)
	{
		this.setObject(value, "./bedrag_deeldepot", String.class);
	}

	public boolean is_Bedrag_deeldepot_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_deeldepot")) != null && 
	        !((String)this.getValueAfter("./bedrag_deeldepot")).equals((this.getValueBefore("./bedrag_deeldepot"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_deeldepot = com.orchestranetworks.schema.Path.parse("./bedrag_deeldepot");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_deeldepot()
	{
		return path_Bedrag_deeldepot;
	}

   /* Get and Set methods for attribute:  
    * deellening_donordeellening (Standard)
    *
    */

	public String get_Deellening_donordeellening()
	{
		return this.getString("./deellening_donordeellening");
	}

	public String get_Deellening_donordeellening_After()
	{
		return (String)this.getValueAfter("./deellening_donordeellening");
	}

	public String get_Deellening_donordeellening_Before()
	{
		return (String)this.getValueBefore("./deellening_donordeellening");
	}

	public void set_Deellening_donordeellening(final String value)
	{
		this.setObject(value, "./deellening_donordeellening", String.class);
	}

	public boolean is_Deellening_donordeellening_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_donordeellening")) != null && 
	        !((String)this.getValueAfter("./deellening_donordeellening")).equals((this.getValueBefore("./deellening_donordeellening"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_donordeellening = com.orchestranetworks.schema.Path.parse("./deellening_donordeellening");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_donordeellening()
	{
		return path_Deellening_donordeellening;
	}

   /* Get and Set methods for attribute:  
    * deellening_indicator_BKW (Standard)
    *
    */

	public String get_Deellening_indicator_BKW()
	{
		return this.getString("./deellening_indicator_BKW");
	}

	public String get_Deellening_indicator_BKW_After()
	{
		return (String)this.getValueAfter("./deellening_indicator_BKW");
	}

	public String get_Deellening_indicator_BKW_Before()
	{
		return (String)this.getValueBefore("./deellening_indicator_BKW");
	}

	public void set_Deellening_indicator_BKW(final String value)
	{
		this.setObject(value, "./deellening_indicator_BKW", String.class);
	}

	public boolean is_Deellening_indicator_BKW_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_indicator_BKW")) != null && 
	        !((String)this.getValueAfter("./deellening_indicator_BKW")).equals((this.getValueBefore("./deellening_indicator_BKW"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_indicator_BKW = com.orchestranetworks.schema.Path.parse("./deellening_indicator_BKW");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_indicator_BKW()
	{
		return path_Deellening_indicator_BKW;
	}

   /* Get and Set methods for attribute:  
    * type_mutatie_laatste_acceptatieproces (Standard)
    *
    */

	public String get_Type_mutatie_laatste_acceptatieproces()
	{
		return this.getString("./type_mutatie_laatste_acceptatieproces");
	}

	public String get_Type_mutatie_laatste_acceptatieproces_After()
	{
		return (String)this.getValueAfter("./type_mutatie_laatste_acceptatieproces");
	}

	public String get_Type_mutatie_laatste_acceptatieproces_Before()
	{
		return (String)this.getValueBefore("./type_mutatie_laatste_acceptatieproces");
	}

	public void set_Type_mutatie_laatste_acceptatieproces(final String value)
	{
		this.setObject(value, "./type_mutatie_laatste_acceptatieproces", String.class);
	}

	public boolean is_Type_mutatie_laatste_acceptatieproces_Changed()
	{
	    return (((String)this.getValueAfter("./type_mutatie_laatste_acceptatieproces")) != null && 
	        !((String)this.getValueAfter("./type_mutatie_laatste_acceptatieproces")).equals((this.getValueBefore("./type_mutatie_laatste_acceptatieproces"))));
	}

	private static final com.orchestranetworks.schema.Path path_Type_mutatie_laatste_acceptatieproces = com.orchestranetworks.schema.Path.parse("./type_mutatie_laatste_acceptatieproces");
	public static final com.orchestranetworks.schema.Path getPath_Type_mutatie_laatste_acceptatieproces()
	{
		return path_Type_mutatie_laatste_acceptatieproces;
	}

   /* Get and Set methods for attribute:  
    * valutasoort (Standard)
    *
    */

	public String get_Valutasoort()
	{
		return this.getString("./valutasoort");
	}

	public String get_Valutasoort_After()
	{
		return (String)this.getValueAfter("./valutasoort");
	}

	public String get_Valutasoort_Before()
	{
		return (String)this.getValueBefore("./valutasoort");
	}

	public void set_Valutasoort(final String value)
	{
		this.setObject(value, "./valutasoort", String.class);
	}

	public boolean is_Valutasoort_Changed()
	{
	    return (((String)this.getValueAfter("./valutasoort")) != null && 
	        !((String)this.getValueAfter("./valutasoort")).equals((this.getValueBefore("./valutasoort"))));
	}

	private static final com.orchestranetworks.schema.Path path_Valutasoort = com.orchestranetworks.schema.Path.parse("./valutasoort");
	public static final com.orchestranetworks.schema.Path getPath_Valutasoort()
	{
		return path_Valutasoort;
	}

   /* Get and Set methods for attribute:  
    * product_naam (Standard)
    *
    */

	public String get_Product_naam()
	{
		return this.getString("./product_naam");
	}

	public String get_Product_naam_After()
	{
		return (String)this.getValueAfter("./product_naam");
	}

	public String get_Product_naam_Before()
	{
		return (String)this.getValueBefore("./product_naam");
	}

	public void set_Product_naam(final String value)
	{
		this.setObject(value, "./product_naam", String.class);
	}

	public boolean is_Product_naam_Changed()
	{
	    return (((String)this.getValueAfter("./product_naam")) != null && 
	        !((String)this.getValueAfter("./product_naam")).equals((this.getValueBefore("./product_naam"))));
	}

	private static final com.orchestranetworks.schema.Path path_Product_naam = com.orchestranetworks.schema.Path.parse("./product_naam");
	public static final com.orchestranetworks.schema.Path getPath_Product_naam()
	{
		return path_Product_naam;
	}

   /* Get and Set methods for attribute:  
    * deellening_datum_beeindigd (Standard)
    *
    */

	public String get_Deellening_datum_beeindigd()
	{
		return this.getString("./deellening_datum_beeindigd");
	}

	public String get_Deellening_datum_beeindigd_After()
	{
		return (String)this.getValueAfter("./deellening_datum_beeindigd");
	}

	public String get_Deellening_datum_beeindigd_Before()
	{
		return (String)this.getValueBefore("./deellening_datum_beeindigd");
	}

	public void set_Deellening_datum_beeindigd(final String value)
	{
		this.setObject(value, "./deellening_datum_beeindigd", String.class);
	}

	public boolean is_Deellening_datum_beeindigd_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_datum_beeindigd")) != null && 
	        !((String)this.getValueAfter("./deellening_datum_beeindigd")).equals((this.getValueBefore("./deellening_datum_beeindigd"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_datum_beeindigd = com.orchestranetworks.schema.Path.parse("./deellening_datum_beeindigd");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_datum_beeindigd()
	{
		return path_Deellening_datum_beeindigd;
	}

   /* Get and Set methods for attribute:  
    * bedrag_banksparen (Standard)
    *
    */

	public String get_Bedrag_banksparen()
	{
		return this.getString("./bedrag_banksparen");
	}

	public String get_Bedrag_banksparen_After()
	{
		return (String)this.getValueAfter("./bedrag_banksparen");
	}

	public String get_Bedrag_banksparen_Before()
	{
		return (String)this.getValueBefore("./bedrag_banksparen");
	}

	public void set_Bedrag_banksparen(final String value)
	{
		this.setObject(value, "./bedrag_banksparen", String.class);
	}

	public boolean is_Bedrag_banksparen_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_banksparen")) != null && 
	        !((String)this.getValueAfter("./bedrag_banksparen")).equals((this.getValueBefore("./bedrag_banksparen"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_banksparen = com.orchestranetworks.schema.Path.parse("./bedrag_banksparen");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_banksparen()
	{
		return path_Bedrag_banksparen;
	}

   /* Get and Set methods for attribute:  
    * bedrag_gecedeerde_deelsom (Standard)
    *
    */

	public String get_Bedrag_gecedeerde_deelsom()
	{
		return this.getString("./bedrag_gecedeerde_deelsom");
	}

	public String get_Bedrag_gecedeerde_deelsom_After()
	{
		return (String)this.getValueAfter("./bedrag_gecedeerde_deelsom");
	}

	public String get_Bedrag_gecedeerde_deelsom_Before()
	{
		return (String)this.getValueBefore("./bedrag_gecedeerde_deelsom");
	}

	public void set_Bedrag_gecedeerde_deelsom(final String value)
	{
		this.setObject(value, "./bedrag_gecedeerde_deelsom", String.class);
	}

	public boolean is_Bedrag_gecedeerde_deelsom_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_gecedeerde_deelsom")) != null && 
	        !((String)this.getValueAfter("./bedrag_gecedeerde_deelsom")).equals((this.getValueBefore("./bedrag_gecedeerde_deelsom"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_gecedeerde_deelsom = com.orchestranetworks.schema.Path.parse("./bedrag_gecedeerde_deelsom");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_gecedeerde_deelsom()
	{
		return path_Bedrag_gecedeerde_deelsom;
	}

   /* Get and Set methods for attribute:  
    * bedrag_gecedeerd_spaardepot (Standard)
    *
    */

	public String get_Bedrag_gecedeerd_spaardepot()
	{
		return this.getString("./bedrag_gecedeerd_spaardepot");
	}

	public String get_Bedrag_gecedeerd_spaardepot_After()
	{
		return (String)this.getValueAfter("./bedrag_gecedeerd_spaardepot");
	}

	public String get_Bedrag_gecedeerd_spaardepot_Before()
	{
		return (String)this.getValueBefore("./bedrag_gecedeerd_spaardepot");
	}

	public void set_Bedrag_gecedeerd_spaardepot(final String value)
	{
		this.setObject(value, "./bedrag_gecedeerd_spaardepot", String.class);
	}

	public boolean is_Bedrag_gecedeerd_spaardepot_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_gecedeerd_spaardepot")) != null && 
	        !((String)this.getValueAfter("./bedrag_gecedeerd_spaardepot")).equals((this.getValueBefore("./bedrag_gecedeerd_spaardepot"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_gecedeerd_spaardepot = com.orchestranetworks.schema.Path.parse("./bedrag_gecedeerd_spaardepot");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_gecedeerd_spaardepot()
	{
		return path_Bedrag_gecedeerd_spaardepot;
	}

   /* Get and Set methods for attribute:  
    * bedrag_gecedeerd_bankspaar (Standard)
    *
    */

	public String get_Bedrag_gecedeerd_bankspaar()
	{
		return this.getString("./bedrag_gecedeerd_bankspaar");
	}

	public String get_Bedrag_gecedeerd_bankspaar_After()
	{
		return (String)this.getValueAfter("./bedrag_gecedeerd_bankspaar");
	}

	public String get_Bedrag_gecedeerd_bankspaar_Before()
	{
		return (String)this.getValueBefore("./bedrag_gecedeerd_bankspaar");
	}

	public void set_Bedrag_gecedeerd_bankspaar(final String value)
	{
		this.setObject(value, "./bedrag_gecedeerd_bankspaar", String.class);
	}

	public boolean is_Bedrag_gecedeerd_bankspaar_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_gecedeerd_bankspaar")) != null && 
	        !((String)this.getValueAfter("./bedrag_gecedeerd_bankspaar")).equals((this.getValueBefore("./bedrag_gecedeerd_bankspaar"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_gecedeerd_bankspaar = com.orchestranetworks.schema.Path.parse("./bedrag_gecedeerd_bankspaar");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_gecedeerd_bankspaar()
	{
		return path_Bedrag_gecedeerd_bankspaar;
	}

   /* Get and Set methods for attribute:  
    * soort_advies (Standard)
    *
    */

	public String get_Soort_advies()
	{
		return this.getString("./soort_advies");
	}

	public String get_Soort_advies_After()
	{
		return (String)this.getValueAfter("./soort_advies");
	}

	public String get_Soort_advies_Before()
	{
		return (String)this.getValueBefore("./soort_advies");
	}

	public void set_Soort_advies(final String value)
	{
		this.setObject(value, "./soort_advies", String.class);
	}

	public boolean is_Soort_advies_Changed()
	{
	    return (((String)this.getValueAfter("./soort_advies")) != null && 
	        !((String)this.getValueAfter("./soort_advies")).equals((this.getValueBefore("./soort_advies"))));
	}

	private static final com.orchestranetworks.schema.Path path_Soort_advies = com.orchestranetworks.schema.Path.parse("./soort_advies");
	public static final com.orchestranetworks.schema.Path getPath_Soort_advies()
	{
		return path_Soort_advies;
	}

   /* Get and Set methods for attribute:  
    * deellening_bouwdepot_aantal_maal_verlengd (Standard)
    *
    */

	public String get_Deellening_bouwdepot_aantal_maal_verlengd()
	{
		return this.getString("./deellening_bouwdepot_aantal_maal_verlengd");
	}

	public String get_Deellening_bouwdepot_aantal_maal_verlengd_After()
	{
		return (String)this.getValueAfter("./deellening_bouwdepot_aantal_maal_verlengd");
	}

	public String get_Deellening_bouwdepot_aantal_maal_verlengd_Before()
	{
		return (String)this.getValueBefore("./deellening_bouwdepot_aantal_maal_verlengd");
	}

	public void set_Deellening_bouwdepot_aantal_maal_verlengd(final String value)
	{
		this.setObject(value, "./deellening_bouwdepot_aantal_maal_verlengd", String.class);
	}

	public boolean is_Deellening_bouwdepot_aantal_maal_verlengd_Changed()
	{
	    return (((String)this.getValueAfter("./deellening_bouwdepot_aantal_maal_verlengd")) != null && 
	        !((String)this.getValueAfter("./deellening_bouwdepot_aantal_maal_verlengd")).equals((this.getValueBefore("./deellening_bouwdepot_aantal_maal_verlengd"))));
	}

	private static final com.orchestranetworks.schema.Path path_Deellening_bouwdepot_aantal_maal_verlengd = com.orchestranetworks.schema.Path.parse("./deellening_bouwdepot_aantal_maal_verlengd");
	public static final com.orchestranetworks.schema.Path getPath_Deellening_bouwdepot_aantal_maal_verlengd()
	{
		return path_Deellening_bouwdepot_aantal_maal_verlengd;
	}

}

