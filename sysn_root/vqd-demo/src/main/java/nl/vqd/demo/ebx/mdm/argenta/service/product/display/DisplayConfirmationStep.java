package nl.vqd.demo.ebx.mdm.argenta.service.product.display;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;
import nl.vqd.demo.ebx.mdm.argenta.service.product.message.Messages;


public class DisplayConfirmationStep implements DisplayStep {
    private final int rowCounts;

    public DisplayConfirmationStep(final int rowCounts) {
        this.rowCounts = rowCounts;
    }

    @Override
    public void setupDisplay(final UserServiceSetupDisplayContext<TableViewEntitySelection> context,
                             final UserServiceDisplayConfigurator config) {
        config.setContent(this::writeContent);

        {
            final UserMessage backMessage = Messages.get("Back");
            final UserMessage cancelMessage = Messages.get("Cancel");
            final UIButtonSpecNavigation backButtonSpec = config.newBackButton(this::onBackPressed);
            final UIButtonSpecNavigation cancelButtonSpec = config.newCancelButton();

            backButtonSpec.setLabel(backMessage);
            backButtonSpec.setDefaultButton(this.rowCounts == 0);
            cancelButtonSpec.setLabel(cancelMessage);
            config.setLeftButtons(backButtonSpec, cancelButtonSpec);
        }

        {
            final UserMessage message = Messages.get("Update");
            final UIButtonSpecNavigation updateButtonSpec = config.newActionButton(message, this::onUpdatePressed);

            updateButtonSpec.setDefaultButton(this.rowCounts > 0);
            updateButtonSpec.setDisabled(this.rowCounts == 0);
            config.setRightButtons(updateButtonSpec);
        }
    }

    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
        final UserMessage message = Messages.get("ThisNumberOfRowsWillBeUpdate{0}", this.rowCounts);
        final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

        writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
        writer.addUILabel(labelSpec);
        writer.add("</div>");
    }

    private UserServiceEventOutcome onBackPressed(final UserServiceEventContext context) {
        return EventOutcome.DISPLAY_FILTER;
    }

    private UserServiceEventOutcome onUpdatePressed(final UserServiceEventContext context) {
        return EventOutcome.DISPLAY_RESULT;
    }
}
