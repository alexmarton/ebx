package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Loan Component" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class LoanComponent extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/loanComponent";

	public LoanComponent(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public LoanComponent(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public LoanComponent(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public LoanComponent(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public LoanComponent(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public LoanComponent(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public LoanComponent(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public LoanComponent(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * processVersion (Standard)
    *
    */

	public Integer get_ProcessVersion()
	{
		return this.getInteger("./processVersion");
	}

	public Integer get_ProcessVersion_After()
	{
		return (Integer)this.getValueAfter("./processVersion");
	}

	public Integer get_ProcessVersion_Before()
	{
		return (Integer)this.getValueBefore("./processVersion");
	}

	public void set_ProcessVersion(final Integer value)
	{
		this.setObject(value, "./processVersion", Integer.class);
	}

	public boolean is_ProcessVersion_Changed()
	{
	    return (((Integer)this.getValueAfter("./processVersion")) != null && 
	        !((Integer)this.getValueAfter("./processVersion")).equals((this.getValueBefore("./processVersion"))));
	}

	private static final com.orchestranetworks.schema.Path path_ProcessVersion = com.orchestranetworks.schema.Path.parse("./processVersion");
	public static final com.orchestranetworks.schema.Path getPath_ProcessVersion()
	{
		return path_ProcessVersion;
	}

   /* Get and Set methods for attribute:  
    * loanComponentNumber (Standard)
    *
    */

	public Integer get_LoanComponentNumber()
	{
		return this.getInteger("./loanComponentNumber");
	}

	public Integer get_LoanComponentNumber_After()
	{
		return (Integer)this.getValueAfter("./loanComponentNumber");
	}

	public Integer get_LoanComponentNumber_Before()
	{
		return (Integer)this.getValueBefore("./loanComponentNumber");
	}

	public void set_LoanComponentNumber(final Integer value)
	{
		this.setObject(value, "./loanComponentNumber", Integer.class);
	}

	public boolean is_LoanComponentNumber_Changed()
	{
	    return (((Integer)this.getValueAfter("./loanComponentNumber")) != null && 
	        !((Integer)this.getValueAfter("./loanComponentNumber")).equals((this.getValueBefore("./loanComponentNumber"))));
	}

	private static final com.orchestranetworks.schema.Path path_LoanComponentNumber = com.orchestranetworks.schema.Path.parse("./loanComponentNumber");
	public static final com.orchestranetworks.schema.Path getPath_LoanComponentNumber()
	{
		return path_LoanComponentNumber;
	}

   /* Get and Set methods for attribute:  
    * loanNumber (Standard)
    *
    */

	public Integer get_LoanNumber()
	{
		return this.getInteger("./loanNumber");
	}

	public Integer get_LoanNumber_After()
	{
		return (Integer)this.getValueAfter("./loanNumber");
	}

	public Integer get_LoanNumber_Before()
	{
		return (Integer)this.getValueBefore("./loanNumber");
	}

	public void set_LoanNumber(final Integer value)
	{
		this.setObject(value, "./loanNumber", Integer.class);
	}

	public boolean is_LoanNumber_Changed()
	{
	    return (((Integer)this.getValueAfter("./loanNumber")) != null && 
	        !((Integer)this.getValueAfter("./loanNumber")).equals((this.getValueBefore("./loanNumber"))));
	}

	private static final com.orchestranetworks.schema.Path path_LoanNumber = com.orchestranetworks.schema.Path.parse("./loanNumber");
	public static final com.orchestranetworks.schema.Path getPath_LoanNumber()
	{
		return path_LoanNumber;
	}

   /* Get and Set methods for attribute:  
    * Event (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Event_FK()
	{
		String ret = this.getString("./event");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Event_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./event");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Event_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./event");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Event_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./event", String.class);
	}

	public void set_Event(final nl.vqd.demo.ebx.mdm.commons.datamodel.Event value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./event", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Event_Record()
	{
		return super.getFK("./event");
	}

	public boolean is_Event_Changed()
	{
	    return (((String)this.getValueAfter("./event")) != null && 
	        !((String)this.getValueAfter("./event")).equals((this.getValueBefore("./event"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Event get_Event()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./event");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Event(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Event = com.orchestranetworks.schema.Path.parse("./event");
	public static final com.orchestranetworks.schema.Path getPath_Event()
	{
		return path_Event;
	}

}

