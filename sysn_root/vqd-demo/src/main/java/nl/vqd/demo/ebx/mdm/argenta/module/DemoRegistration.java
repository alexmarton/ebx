package nl.vqd.demo.ebx.mdm.argenta.module;

import com.orchestranetworks.module.ModuleContextOnRepositoryStartup;
import com.orchestranetworks.module.ModuleRegistrationServlet;
import com.orchestranetworks.module.ModuleServiceRegistrationContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class DemoRegistration extends ModuleRegistrationServlet {

	private static final long serialVersionUID = -1595820418555507933L;
	public static LoggingCategory log = null;
	
	@Override
	public void handleRepositoryStartup(ModuleContextOnRepositoryStartup context) throws OperationException {

		super.handleRepositoryStartup(context);
		log = context.getLoggingCategory();
	
		//CreateDataSpaceDataSet.createDataSpaceDataSet(context);
		
		DemoRegistration.log.info("Demo Viqtor Davis module started");
	}

	@Override
	public void handleServiceRegistration(ModuleServiceRegistrationContext context) {
		
		super.handleServiceRegistration(context);

		//context.registerUserService(new SamplServiceDeclaration(Constant.Training_MODULE));
	}

}