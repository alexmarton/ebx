package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Contract" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Mon Nov 23 01:02:03 CET 2020
*/

public class Contract extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/contract";

	public Contract(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Contract(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Contract(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Contract(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Contract(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Contract(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Contract(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Contract(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * Notaris (Standard)
    *
    */

	public String get_Notaris()
	{
		return this.getString("./notaris");
	}

	public String get_Notaris_After()
	{
		return (String)this.getValueAfter("./notaris");
	}

	public String get_Notaris_Before()
	{
		return (String)this.getValueBefore("./notaris");
	}

	public void set_Notaris(final String value)
	{
		this.setObject(value, "./notaris", String.class);
	}

	public boolean is_Notaris_Changed()
	{
	    return (((String)this.getValueAfter("./notaris")) != null && 
	        !((String)this.getValueAfter("./notaris")).equals((this.getValueBefore("./notaris"))));
	}

	private static final com.orchestranetworks.schema.Path path_Notaris = com.orchestranetworks.schema.Path.parse("./notaris");
	public static final com.orchestranetworks.schema.Path getPath_Notaris()
	{
		return path_Notaris;
	}

   /* Get and Set methods for attribute:  
    * Process Version (Standard)
    *
    */

	public String get_ProcessVersion()
	{
		return this.getString("./processVersion");
	}

	public String get_ProcessVersion_After()
	{
		return (String)this.getValueAfter("./processVersion");
	}

	public String get_ProcessVersion_Before()
	{
		return (String)this.getValueBefore("./processVersion");
	}

	public void set_ProcessVersion(final String value)
	{
		this.setObject(value, "./processVersion", String.class);
	}

	public boolean is_ProcessVersion_Changed()
	{
	    return (((String)this.getValueAfter("./processVersion")) != null && 
	        !((String)this.getValueAfter("./processVersion")).equals((this.getValueBefore("./processVersion"))));
	}

	private static final com.orchestranetworks.schema.Path path_ProcessVersion = com.orchestranetworks.schema.Path.parse("./processVersion");
	public static final com.orchestranetworks.schema.Path getPath_ProcessVersion()
	{
		return path_ProcessVersion;
	}

   /* Get and Set methods for attribute:  
    * Contract Number (Standard)
    *
    */

	public String get_ContractNumber()
	{
		return this.getString("./contractNumber");
	}

	public String get_ContractNumber_After()
	{
		return (String)this.getValueAfter("./contractNumber");
	}

	public String get_ContractNumber_Before()
	{
		return (String)this.getValueBefore("./contractNumber");
	}

	public void set_ContractNumber(final String value)
	{
		this.setObject(value, "./contractNumber", String.class);
	}

	public boolean is_ContractNumber_Changed()
	{
	    return (((String)this.getValueAfter("./contractNumber")) != null && 
	        !((String)this.getValueAfter("./contractNumber")).equals((this.getValueBefore("./contractNumber"))));
	}

	private static final com.orchestranetworks.schema.Path path_ContractNumber = com.orchestranetworks.schema.Path.parse("./contractNumber");
	public static final com.orchestranetworks.schema.Path getPath_ContractNumber()
	{
		return path_ContractNumber;
	}

}

