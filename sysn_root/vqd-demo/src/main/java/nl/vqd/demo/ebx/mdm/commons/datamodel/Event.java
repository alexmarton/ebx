package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Event" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class Event extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/event";

	public Event(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Event(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Event(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Event(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Event(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Event(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Event(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Event(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * Type (Standard)
    *
    */

	public String get_EventType()
	{
		return this.getString("./eventType");
	}

	public String get_EventType_After()
	{
		return (String)this.getValueAfter("./eventType");
	}

	public String get_EventType_Before()
	{
		return (String)this.getValueBefore("./eventType");
	}

	public void set_EventType(final String value)
	{
		this.setObject(value, "./eventType", String.class);
	}

	public boolean is_EventType_Changed()
	{
	    return (((String)this.getValueAfter("./eventType")) != null && 
	        !((String)this.getValueAfter("./eventType")).equals((this.getValueBefore("./eventType"))));
	}

	private static final com.orchestranetworks.schema.Path path_EventType = com.orchestranetworks.schema.Path.parse("./eventType");
	public static final com.orchestranetworks.schema.Path getPath_EventType()
	{
		return path_EventType;
	}

   /* Get and Set methods for attribute:  
    * Processed (Y/N) (Standard boolean)
    *
    */
    
	/**
	 * 
	 * @Deprecated use the is_EventProcessed() method instead
	 */
	@Deprecated 
	public Boolean get_EventProcessed()
	{
		return this.getBoolean("./eventProcessed");
	}

	public Boolean get_EventProcessed_After()
	{
		return (Boolean)this.getValueAfter("./eventProcessed");
	}

	public Boolean get_EventProcessed_Before()
	{
		return (Boolean)this.getValueBefore("./eventProcessed");
	}

	public void set_EventProcessed(final Boolean value)
	{
		this.setObject(value, "./eventProcessed", Boolean.class);
	}

	public boolean is_EventProcessed() {
		return this.getBoolean("./eventProcessed");
	}

	public boolean is_EventProcessed_Changed()
	{
	    return (((Boolean)this.getValueAfter("./eventProcessed")) != null && 
	        !((Boolean)this.getValueAfter("./eventProcessed")).equals((this.getValueBefore("./eventProcessed"))));
	}

	private static final com.orchestranetworks.schema.Path path_EventProcessed = com.orchestranetworks.schema.Path.parse("./eventProcessed");
	public static final com.orchestranetworks.schema.Path getPath_EventProcessed()
	{
		return path_EventProcessed;
	}

   /* Get and Set methods for attribute:  
    * Processed at (Standard)
    *
    */

	public java.util.Date get_EventDate()
	{
		return this.getDate("./eventDate");
	}

	public java.util.Date get_EventDate_After()
	{
		return (java.util.Date)this.getValueAfter("./eventDate");
	}

	public java.util.Date get_EventDate_Before()
	{
		return (java.util.Date)this.getValueBefore("./eventDate");
	}

	public void set_EventDate(final java.util.Date value)
	{
		this.setObject(value, "./eventDate", java.util.Date.class);
	}

	public boolean is_EventDate_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./eventDate")) != null && 
	        !((java.util.Date)this.getValueAfter("./eventDate")).equals((this.getValueBefore("./eventDate"))));
	}

	private static final com.orchestranetworks.schema.Path path_EventDate = com.orchestranetworks.schema.Path.parse("./eventDate");
	public static final com.orchestranetworks.schema.Path getPath_EventDate()
	{
		return path_EventDate;
	}

   /* Get and Set methods for attribute:  
    * Account (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Account_FK()
	{
		String ret = this.getString("./account");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Account_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./account");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Account_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./account");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Account_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./account", String.class);
	}

	public void set_Account(final nl.vqd.demo.ebx.mdm.commons.datamodel.Account value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./account", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Account_Record()
	{
		return super.getFK("./account");
	}

	public boolean is_Account_Changed()
	{
	    return (((String)this.getValueAfter("./account")) != null && 
	        !((String)this.getValueAfter("./account")).equals((this.getValueBefore("./account"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Account get_Account()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./account");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Account(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Account = com.orchestranetworks.schema.Path.parse("./account");
	public static final com.orchestranetworks.schema.Path getPath_Account()
	{
		return path_Account;
	}

}

