package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Contracten Lening" record from the data model "Argenta Demo"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Thu Nov 19 14:33:38 CET 2020
*/

public class ContractenLening extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/contractenLening";

	public ContractenLening(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public ContractenLening(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public ContractenLening(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public ContractenLening(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public ContractenLening(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public ContractenLening(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public ContractenLening(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public ContractenLening(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * rapportagedatum (Standard)
    *
    */

	public java.util.Date get_Rapportagedatum()
	{
		return this.getDate("./rapportagedatum");
	}

	public java.util.Date get_Rapportagedatum_After()
	{
		return (java.util.Date)this.getValueAfter("./rapportagedatum");
	}

	public java.util.Date get_Rapportagedatum_Before()
	{
		return (java.util.Date)this.getValueBefore("./rapportagedatum");
	}

	public void set_Rapportagedatum(final java.util.Date value)
	{
		this.setObject(value, "./rapportagedatum", java.util.Date.class);
	}

	public boolean is_Rapportagedatum_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./rapportagedatum")) != null && 
	        !((java.util.Date)this.getValueAfter("./rapportagedatum")).equals((this.getValueBefore("./rapportagedatum"))));
	}

	private static final com.orchestranetworks.schema.Path path_Rapportagedatum = com.orchestranetworks.schema.Path.parse("./rapportagedatum");
	public static final com.orchestranetworks.schema.Path getPath_Rapportagedatum()
	{
		return path_Rapportagedatum;
	}

   /* Get and Set methods for attribute:  
    * lening_contract_nummer (Standard)
    *
    */

	public String get_Lening_contract_nummer()
	{
		return this.getString("./lening_contract_nummer");
	}

	public String get_Lening_contract_nummer_After()
	{
		return (String)this.getValueAfter("./lening_contract_nummer");
	}

	public String get_Lening_contract_nummer_Before()
	{
		return (String)this.getValueBefore("./lening_contract_nummer");
	}

	public void set_Lening_contract_nummer(final String value)
	{
		this.setObject(value, "./lening_contract_nummer", String.class);
	}

	public boolean is_Lening_contract_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./lening_contract_nummer")) != null && 
	        !((String)this.getValueAfter("./lening_contract_nummer")).equals((this.getValueBefore("./lening_contract_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_contract_nummer = com.orchestranetworks.schema.Path.parse("./lening_contract_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Lening_contract_nummer()
	{
		return path_Lening_contract_nummer;
	}

   /* Get and Set methods for attribute:  
    * contract_procesversie (Standard)
    *
    */

	public String get_Contract_procesversie()
	{
		return this.getString("./contract_procesversie");
	}

	public String get_Contract_procesversie_After()
	{
		return (String)this.getValueAfter("./contract_procesversie");
	}

	public String get_Contract_procesversie_Before()
	{
		return (String)this.getValueBefore("./contract_procesversie");
	}

	public void set_Contract_procesversie(final String value)
	{
		this.setObject(value, "./contract_procesversie", String.class);
	}

	public boolean is_Contract_procesversie_Changed()
	{
	    return (((String)this.getValueAfter("./contract_procesversie")) != null && 
	        !((String)this.getValueAfter("./contract_procesversie")).equals((this.getValueBefore("./contract_procesversie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Contract_procesversie = com.orchestranetworks.schema.Path.parse("./contract_procesversie");
	public static final com.orchestranetworks.schema.Path getPath_Contract_procesversie()
	{
		return path_Contract_procesversie;
	}

   /* Get and Set methods for attribute:  
    * lening_nummer (Standard)
    *
    */

	public String get_Lening_nummer()
	{
		return this.getString("./lening_nummer");
	}

	public String get_Lening_nummer_After()
	{
		return (String)this.getValueAfter("./lening_nummer");
	}

	public String get_Lening_nummer_Before()
	{
		return (String)this.getValueBefore("./lening_nummer");
	}

	public void set_Lening_nummer(final String value)
	{
		this.setObject(value, "./lening_nummer", String.class);
	}

	public boolean is_Lening_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./lening_nummer")) != null && 
	        !((String)this.getValueAfter("./lening_nummer")).equals((this.getValueBefore("./lening_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_nummer = com.orchestranetworks.schema.Path.parse("./lening_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Lening_nummer()
	{
		return path_Lening_nummer;
	}

   /* Get and Set methods for attribute:  
    * whitelabel_code (Standard)
    *
    */

	public String get_Whitelabel_code()
	{
		return this.getString("./whitelabel_code");
	}

	public String get_Whitelabel_code_After()
	{
		return (String)this.getValueAfter("./whitelabel_code");
	}

	public String get_Whitelabel_code_Before()
	{
		return (String)this.getValueBefore("./whitelabel_code");
	}

	public void set_Whitelabel_code(final String value)
	{
		this.setObject(value, "./whitelabel_code", String.class);
	}

	public boolean is_Whitelabel_code_Changed()
	{
	    return (((String)this.getValueAfter("./whitelabel_code")) != null && 
	        !((String)this.getValueAfter("./whitelabel_code")).equals((this.getValueBefore("./whitelabel_code"))));
	}

	private static final com.orchestranetworks.schema.Path path_Whitelabel_code = com.orchestranetworks.schema.Path.parse("./whitelabel_code");
	public static final com.orchestranetworks.schema.Path getPath_Whitelabel_code()
	{
		return path_Whitelabel_code;
	}

   /* Get and Set methods for attribute:  
    * funder_naam (Standard)
    *
    */

	public String get_Funder_naam()
	{
		return this.getString("./funder_naam");
	}

	public String get_Funder_naam_After()
	{
		return (String)this.getValueAfter("./funder_naam");
	}

	public String get_Funder_naam_Before()
	{
		return (String)this.getValueBefore("./funder_naam");
	}

	public void set_Funder_naam(final String value)
	{
		this.setObject(value, "./funder_naam", String.class);
	}

	public boolean is_Funder_naam_Changed()
	{
	    return (((String)this.getValueAfter("./funder_naam")) != null && 
	        !((String)this.getValueAfter("./funder_naam")).equals((this.getValueBefore("./funder_naam"))));
	}

	private static final com.orchestranetworks.schema.Path path_Funder_naam = com.orchestranetworks.schema.Path.parse("./funder_naam");
	public static final com.orchestranetworks.schema.Path getPath_Funder_naam()
	{
		return path_Funder_naam;
	}

   /* Get and Set methods for attribute:  
    * juridische_eigenaar_nummer (Standard)
    *
    */

	public String get_Juridische_eigenaar_nummer()
	{
		return this.getString("./juridische_eigenaar_nummer");
	}

	public String get_Juridische_eigenaar_nummer_After()
	{
		return (String)this.getValueAfter("./juridische_eigenaar_nummer");
	}

	public String get_Juridische_eigenaar_nummer_Before()
	{
		return (String)this.getValueBefore("./juridische_eigenaar_nummer");
	}

	public void set_Juridische_eigenaar_nummer(final String value)
	{
		this.setObject(value, "./juridische_eigenaar_nummer", String.class);
	}

	public boolean is_Juridische_eigenaar_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./juridische_eigenaar_nummer")) != null && 
	        !((String)this.getValueAfter("./juridische_eigenaar_nummer")).equals((this.getValueBefore("./juridische_eigenaar_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Juridische_eigenaar_nummer = com.orchestranetworks.schema.Path.parse("./juridische_eigenaar_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Juridische_eigenaar_nummer()
	{
		return path_Juridische_eigenaar_nummer;
	}

   /* Get and Set methods for attribute:  
    * datum_werkelijke_passering (Standard)
    *
    */

	public String get_Datum_werkelijke_passering()
	{
		return this.getString("./datum_werkelijke_passering");
	}

	public String get_Datum_werkelijke_passering_After()
	{
		return (String)this.getValueAfter("./datum_werkelijke_passering");
	}

	public String get_Datum_werkelijke_passering_Before()
	{
		return (String)this.getValueBefore("./datum_werkelijke_passering");
	}

	public void set_Datum_werkelijke_passering(final String value)
	{
		this.setObject(value, "./datum_werkelijke_passering", String.class);
	}

	public boolean is_Datum_werkelijke_passering_Changed()
	{
	    return (((String)this.getValueAfter("./datum_werkelijke_passering")) != null && 
	        !((String)this.getValueAfter("./datum_werkelijke_passering")).equals((this.getValueBefore("./datum_werkelijke_passering"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_werkelijke_passering = com.orchestranetworks.schema.Path.parse("./datum_werkelijke_passering");
	public static final com.orchestranetworks.schema.Path getPath_Datum_werkelijke_passering()
	{
		return path_Datum_werkelijke_passering;
	}

   /* Get and Set methods for attribute:  
    * lening_datum_proforma_aflossing_opgevraagd (Standard)
    *
    */

	public String get_Lening_datum_proforma_aflossing_opgevraagd()
	{
		return this.getString("./lening_datum_proforma_aflossing_opgevraagd");
	}

	public String get_Lening_datum_proforma_aflossing_opgevraagd_After()
	{
		return (String)this.getValueAfter("./lening_datum_proforma_aflossing_opgevraagd");
	}

	public String get_Lening_datum_proforma_aflossing_opgevraagd_Before()
	{
		return (String)this.getValueBefore("./lening_datum_proforma_aflossing_opgevraagd");
	}

	public void set_Lening_datum_proforma_aflossing_opgevraagd(final String value)
	{
		this.setObject(value, "./lening_datum_proforma_aflossing_opgevraagd", String.class);
	}

	public boolean is_Lening_datum_proforma_aflossing_opgevraagd_Changed()
	{
	    return (((String)this.getValueAfter("./lening_datum_proforma_aflossing_opgevraagd")) != null && 
	        !((String)this.getValueAfter("./lening_datum_proforma_aflossing_opgevraagd")).equals((this.getValueBefore("./lening_datum_proforma_aflossing_opgevraagd"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_datum_proforma_aflossing_opgevraagd = com.orchestranetworks.schema.Path.parse("./lening_datum_proforma_aflossing_opgevraagd");
	public static final com.orchestranetworks.schema.Path getPath_Lening_datum_proforma_aflossing_opgevraagd()
	{
		return path_Lening_datum_proforma_aflossing_opgevraagd;
	}

   /* Get and Set methods for attribute:  
    * lening_datum_algehele_aflossing_opgevraagd (Standard)
    *
    */

	public String get_Lening_datum_algehele_aflossing_opgevraagd()
	{
		return this.getString("./lening_datum_algehele_aflossing_opgevraagd");
	}

	public String get_Lening_datum_algehele_aflossing_opgevraagd_After()
	{
		return (String)this.getValueAfter("./lening_datum_algehele_aflossing_opgevraagd");
	}

	public String get_Lening_datum_algehele_aflossing_opgevraagd_Before()
	{
		return (String)this.getValueBefore("./lening_datum_algehele_aflossing_opgevraagd");
	}

	public void set_Lening_datum_algehele_aflossing_opgevraagd(final String value)
	{
		this.setObject(value, "./lening_datum_algehele_aflossing_opgevraagd", String.class);
	}

	public boolean is_Lening_datum_algehele_aflossing_opgevraagd_Changed()
	{
	    return (((String)this.getValueAfter("./lening_datum_algehele_aflossing_opgevraagd")) != null && 
	        !((String)this.getValueAfter("./lening_datum_algehele_aflossing_opgevraagd")).equals((this.getValueBefore("./lening_datum_algehele_aflossing_opgevraagd"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_datum_algehele_aflossing_opgevraagd = com.orchestranetworks.schema.Path.parse("./lening_datum_algehele_aflossing_opgevraagd");
	public static final com.orchestranetworks.schema.Path getPath_Lening_datum_algehele_aflossing_opgevraagd()
	{
		return path_Lening_datum_algehele_aflossing_opgevraagd;
	}

   /* Get and Set methods for attribute:  
    * lening_datum_algehele_aflossing_verwacht (Standard)
    *
    */

	public String get_Lening_datum_algehele_aflossing_verwacht()
	{
		return this.getString("./lening_datum_algehele_aflossing_verwacht");
	}

	public String get_Lening_datum_algehele_aflossing_verwacht_After()
	{
		return (String)this.getValueAfter("./lening_datum_algehele_aflossing_verwacht");
	}

	public String get_Lening_datum_algehele_aflossing_verwacht_Before()
	{
		return (String)this.getValueBefore("./lening_datum_algehele_aflossing_verwacht");
	}

	public void set_Lening_datum_algehele_aflossing_verwacht(final String value)
	{
		this.setObject(value, "./lening_datum_algehele_aflossing_verwacht", String.class);
	}

	public boolean is_Lening_datum_algehele_aflossing_verwacht_Changed()
	{
	    return (((String)this.getValueAfter("./lening_datum_algehele_aflossing_verwacht")) != null && 
	        !((String)this.getValueAfter("./lening_datum_algehele_aflossing_verwacht")).equals((this.getValueBefore("./lening_datum_algehele_aflossing_verwacht"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_datum_algehele_aflossing_verwacht = com.orchestranetworks.schema.Path.parse("./lening_datum_algehele_aflossing_verwacht");
	public static final com.orchestranetworks.schema.Path getPath_Lening_datum_algehele_aflossing_verwacht()
	{
		return path_Lening_datum_algehele_aflossing_verwacht;
	}

   /* Get and Set methods for attribute:  
    * datum_werkelijke_algehele_aflossing (Standard)
    *
    */

	public String get_Datum_werkelijke_algehele_aflossing()
	{
		return this.getString("./datum_werkelijke_algehele_aflossing");
	}

	public String get_Datum_werkelijke_algehele_aflossing_After()
	{
		return (String)this.getValueAfter("./datum_werkelijke_algehele_aflossing");
	}

	public String get_Datum_werkelijke_algehele_aflossing_Before()
	{
		return (String)this.getValueBefore("./datum_werkelijke_algehele_aflossing");
	}

	public void set_Datum_werkelijke_algehele_aflossing(final String value)
	{
		this.setObject(value, "./datum_werkelijke_algehele_aflossing", String.class);
	}

	public boolean is_Datum_werkelijke_algehele_aflossing_Changed()
	{
	    return (((String)this.getValueAfter("./datum_werkelijke_algehele_aflossing")) != null && 
	        !((String)this.getValueAfter("./datum_werkelijke_algehele_aflossing")).equals((this.getValueBefore("./datum_werkelijke_algehele_aflossing"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_werkelijke_algehele_aflossing = com.orchestranetworks.schema.Path.parse("./datum_werkelijke_algehele_aflossing");
	public static final com.orchestranetworks.schema.Path getPath_Datum_werkelijke_algehele_aflossing()
	{
		return path_Datum_werkelijke_algehele_aflossing;
	}

   /* Get and Set methods for attribute:  
    * lening_reden_algehele_aflossing (Standard)
    *
    */

	public String get_Lening_reden_algehele_aflossing()
	{
		return this.getString("./lening_reden_algehele_aflossing");
	}

	public String get_Lening_reden_algehele_aflossing_After()
	{
		return (String)this.getValueAfter("./lening_reden_algehele_aflossing");
	}

	public String get_Lening_reden_algehele_aflossing_Before()
	{
		return (String)this.getValueBefore("./lening_reden_algehele_aflossing");
	}

	public void set_Lening_reden_algehele_aflossing(final String value)
	{
		this.setObject(value, "./lening_reden_algehele_aflossing", String.class);
	}

	public boolean is_Lening_reden_algehele_aflossing_Changed()
	{
	    return (((String)this.getValueAfter("./lening_reden_algehele_aflossing")) != null && 
	        !((String)this.getValueAfter("./lening_reden_algehele_aflossing")).equals((this.getValueBefore("./lening_reden_algehele_aflossing"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_reden_algehele_aflossing = com.orchestranetworks.schema.Path.parse("./lening_reden_algehele_aflossing");
	public static final com.orchestranetworks.schema.Path getPath_Lening_reden_algehele_aflossing()
	{
		return path_Lening_reden_algehele_aflossing;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_soort_regeling (Standard)
    *
    */

	public String get_Omschrijving_soort_regeling()
	{
		return this.getString("./omschrijving_soort_regeling");
	}

	public String get_Omschrijving_soort_regeling_After()
	{
		return (String)this.getValueAfter("./omschrijving_soort_regeling");
	}

	public String get_Omschrijving_soort_regeling_Before()
	{
		return (String)this.getValueBefore("./omschrijving_soort_regeling");
	}

	public void set_Omschrijving_soort_regeling(final String value)
	{
		this.setObject(value, "./omschrijving_soort_regeling", String.class);
	}

	public boolean is_Omschrijving_soort_regeling_Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_soort_regeling")) != null && 
	        !((String)this.getValueAfter("./omschrijving_soort_regeling")).equals((this.getValueBefore("./omschrijving_soort_regeling"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_soort_regeling = com.orchestranetworks.schema.Path.parse("./omschrijving_soort_regeling");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_soort_regeling()
	{
		return path_Omschrijving_soort_regeling;
	}

   /* Get and Set methods for attribute:  
    * lening_rang_hypothecaire_inschrijving (Standard)
    *
    */

	public String get_Lening_rang_hypothecaire_inschrijving()
	{
		return this.getString("./lening_rang_hypothecaire_inschrijving");
	}

	public String get_Lening_rang_hypothecaire_inschrijving_After()
	{
		return (String)this.getValueAfter("./lening_rang_hypothecaire_inschrijving");
	}

	public String get_Lening_rang_hypothecaire_inschrijving_Before()
	{
		return (String)this.getValueBefore("./lening_rang_hypothecaire_inschrijving");
	}

	public void set_Lening_rang_hypothecaire_inschrijving(final String value)
	{
		this.setObject(value, "./lening_rang_hypothecaire_inschrijving", String.class);
	}

	public boolean is_Lening_rang_hypothecaire_inschrijving_Changed()
	{
	    return (((String)this.getValueAfter("./lening_rang_hypothecaire_inschrijving")) != null && 
	        !((String)this.getValueAfter("./lening_rang_hypothecaire_inschrijving")).equals((this.getValueBefore("./lening_rang_hypothecaire_inschrijving"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_rang_hypothecaire_inschrijving = com.orchestranetworks.schema.Path.parse("./lening_rang_hypothecaire_inschrijving");
	public static final com.orchestranetworks.schema.Path getPath_Lening_rang_hypothecaire_inschrijving()
	{
		return path_Lening_rang_hypothecaire_inschrijving;
	}

   /* Get and Set methods for attribute:  
    * bedrag_voorgaande_hypothecaire_inschrijving (Standard)
    *
    */

	public String get_Bedrag_voorgaande_hypothecaire_inschrijving()
	{
		return this.getString("./bedrag_voorgaande_hypothecaire_inschrijving");
	}

	public String get_Bedrag_voorgaande_hypothecaire_inschrijving_After()
	{
		return (String)this.getValueAfter("./bedrag_voorgaande_hypothecaire_inschrijving");
	}

	public String get_Bedrag_voorgaande_hypothecaire_inschrijving_Before()
	{
		return (String)this.getValueBefore("./bedrag_voorgaande_hypothecaire_inschrijving");
	}

	public void set_Bedrag_voorgaande_hypothecaire_inschrijving(final String value)
	{
		this.setObject(value, "./bedrag_voorgaande_hypothecaire_inschrijving", String.class);
	}

	public boolean is_Bedrag_voorgaande_hypothecaire_inschrijving_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_voorgaande_hypothecaire_inschrijving")) != null && 
	        !((String)this.getValueAfter("./bedrag_voorgaande_hypothecaire_inschrijving")).equals((this.getValueBefore("./bedrag_voorgaande_hypothecaire_inschrijving"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_voorgaande_hypothecaire_inschrijving = com.orchestranetworks.schema.Path.parse("./bedrag_voorgaande_hypothecaire_inschrijving");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_voorgaande_hypothecaire_inschrijving()
	{
		return path_Bedrag_voorgaande_hypothecaire_inschrijving;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_boetevrije_aflossing_ (Standard)
    *
    */

	public String get_Omschrijving_boetevrije_aflossing_()
	{
		return this.getString("./omschrijving_boetevrije_aflossing_");
	}

	public String get_Omschrijving_boetevrije_aflossing__After()
	{
		return (String)this.getValueAfter("./omschrijving_boetevrije_aflossing_");
	}

	public String get_Omschrijving_boetevrije_aflossing__Before()
	{
		return (String)this.getValueBefore("./omschrijving_boetevrije_aflossing_");
	}

	public void set_Omschrijving_boetevrije_aflossing_(final String value)
	{
		this.setObject(value, "./omschrijving_boetevrije_aflossing_", String.class);
	}

	public boolean is_Omschrijving_boetevrije_aflossing__Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_boetevrije_aflossing_")) != null && 
	        !((String)this.getValueAfter("./omschrijving_boetevrije_aflossing_")).equals((this.getValueBefore("./omschrijving_boetevrije_aflossing_"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_boetevrije_aflossing_ = com.orchestranetworks.schema.Path.parse("./omschrijving_boetevrije_aflossing_");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_boetevrije_aflossing_()
	{
		return path_Omschrijving_boetevrije_aflossing_;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_acceptatie_voorwaarde (Standard)
    *
    */

	public String get_Omschrijving_acceptatie_voorwaarde()
	{
		return this.getString("./omschrijving_acceptatie_voorwaarde");
	}

	public String get_Omschrijving_acceptatie_voorwaarde_After()
	{
		return (String)this.getValueAfter("./omschrijving_acceptatie_voorwaarde");
	}

	public String get_Omschrijving_acceptatie_voorwaarde_Before()
	{
		return (String)this.getValueBefore("./omschrijving_acceptatie_voorwaarde");
	}

	public void set_Omschrijving_acceptatie_voorwaarde(final String value)
	{
		this.setObject(value, "./omschrijving_acceptatie_voorwaarde", String.class);
	}

	public boolean is_Omschrijving_acceptatie_voorwaarde_Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_acceptatie_voorwaarde")) != null && 
	        !((String)this.getValueAfter("./omschrijving_acceptatie_voorwaarde")).equals((this.getValueBefore("./omschrijving_acceptatie_voorwaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_acceptatie_voorwaarde = com.orchestranetworks.schema.Path.parse("./omschrijving_acceptatie_voorwaarde");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_acceptatie_voorwaarde()
	{
		return path_Omschrijving_acceptatie_voorwaarde;
	}

   /* Get and Set methods for attribute:  
    * naam_notaris (Standard)
    *
    */

	public String get_Naam_notaris()
	{
		return this.getString("./naam_notaris");
	}

	public String get_Naam_notaris_After()
	{
		return (String)this.getValueAfter("./naam_notaris");
	}

	public String get_Naam_notaris_Before()
	{
		return (String)this.getValueBefore("./naam_notaris");
	}

	public void set_Naam_notaris(final String value)
	{
		this.setObject(value, "./naam_notaris", String.class);
	}

	public boolean is_Naam_notaris_Changed()
	{
	    return (((String)this.getValueAfter("./naam_notaris")) != null && 
	        !((String)this.getValueAfter("./naam_notaris")).equals((this.getValueBefore("./naam_notaris"))));
	}

	private static final com.orchestranetworks.schema.Path path_Naam_notaris = com.orchestranetworks.schema.Path.parse("./naam_notaris");
	public static final com.orchestranetworks.schema.Path getPath_Naam_notaris()
	{
		return path_Naam_notaris;
	}

   /* Get and Set methods for attribute:  
    * datum_passering_eerste_lening (Standard)
    *
    */

	public String get_Datum_passering_eerste_lening()
	{
		return this.getString("./datum_passering_eerste_lening");
	}

	public String get_Datum_passering_eerste_lening_After()
	{
		return (String)this.getValueAfter("./datum_passering_eerste_lening");
	}

	public String get_Datum_passering_eerste_lening_Before()
	{
		return (String)this.getValueBefore("./datum_passering_eerste_lening");
	}

	public void set_Datum_passering_eerste_lening(final String value)
	{
		this.setObject(value, "./datum_passering_eerste_lening", String.class);
	}

	public boolean is_Datum_passering_eerste_lening_Changed()
	{
	    return (((String)this.getValueAfter("./datum_passering_eerste_lening")) != null && 
	        !((String)this.getValueAfter("./datum_passering_eerste_lening")).equals((this.getValueBefore("./datum_passering_eerste_lening"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_passering_eerste_lening = com.orchestranetworks.schema.Path.parse("./datum_passering_eerste_lening");
	public static final com.orchestranetworks.schema.Path getPath_Datum_passering_eerste_lening()
	{
		return path_Datum_passering_eerste_lening;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_status_verwerking (Standard)
    *
    */

	public String get_Omschrijving_status_verwerking()
	{
		return this.getString("./omschrijving_status_verwerking");
	}

	public String get_Omschrijving_status_verwerking_After()
	{
		return (String)this.getValueAfter("./omschrijving_status_verwerking");
	}

	public String get_Omschrijving_status_verwerking_Before()
	{
		return (String)this.getValueBefore("./omschrijving_status_verwerking");
	}

	public void set_Omschrijving_status_verwerking(final String value)
	{
		this.setObject(value, "./omschrijving_status_verwerking", String.class);
	}

	public boolean is_Omschrijving_status_verwerking_Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_status_verwerking")) != null && 
	        !((String)this.getValueAfter("./omschrijving_status_verwerking")).equals((this.getValueBefore("./omschrijving_status_verwerking"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_status_verwerking = com.orchestranetworks.schema.Path.parse("./omschrijving_status_verwerking");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_status_verwerking()
	{
		return path_Omschrijving_status_verwerking;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_status_lening (Standard)
    *
    */

	public String get_Omschrijving_status_lening()
	{
		return this.getString("./omschrijving_status_lening");
	}

	public String get_Omschrijving_status_lening_After()
	{
		return (String)this.getValueAfter("./omschrijving_status_lening");
	}

	public String get_Omschrijving_status_lening_Before()
	{
		return (String)this.getValueBefore("./omschrijving_status_lening");
	}

	public void set_Omschrijving_status_lening(final String value)
	{
		this.setObject(value, "./omschrijving_status_lening", String.class);
	}

	public boolean is_Omschrijving_status_lening_Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_status_lening")) != null && 
	        !((String)this.getValueAfter("./omschrijving_status_lening")).equals((this.getValueBefore("./omschrijving_status_lening"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_status_lening = com.orchestranetworks.schema.Path.parse("./omschrijving_status_lening");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_status_lening()
	{
		return path_Omschrijving_status_lening;
	}

   /* Get and Set methods for attribute:  
    * lening_indicator_subsidieregeling (Standard)
    *
    */

	public String get_Lening_indicator_subsidieregeling()
	{
		return this.getString("./lening_indicator_subsidieregeling");
	}

	public String get_Lening_indicator_subsidieregeling_After()
	{
		return (String)this.getValueAfter("./lening_indicator_subsidieregeling");
	}

	public String get_Lening_indicator_subsidieregeling_Before()
	{
		return (String)this.getValueBefore("./lening_indicator_subsidieregeling");
	}

	public void set_Lening_indicator_subsidieregeling(final String value)
	{
		this.setObject(value, "./lening_indicator_subsidieregeling", String.class);
	}

	public boolean is_Lening_indicator_subsidieregeling_Changed()
	{
	    return (((String)this.getValueAfter("./lening_indicator_subsidieregeling")) != null && 
	        !((String)this.getValueAfter("./lening_indicator_subsidieregeling")).equals((this.getValueBefore("./lening_indicator_subsidieregeling"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_indicator_subsidieregeling = com.orchestranetworks.schema.Path.parse("./lening_indicator_subsidieregeling");
	public static final com.orchestranetworks.schema.Path getPath_Lening_indicator_subsidieregeling()
	{
		return path_Lening_indicator_subsidieregeling;
	}

   /* Get and Set methods for attribute:  
    * bedrag_woonlasten (Standard)
    *
    */

	public String get_Bedrag_woonlasten()
	{
		return this.getString("./bedrag_woonlasten");
	}

	public String get_Bedrag_woonlasten_After()
	{
		return (String)this.getValueAfter("./bedrag_woonlasten");
	}

	public String get_Bedrag_woonlasten_Before()
	{
		return (String)this.getValueBefore("./bedrag_woonlasten");
	}

	public void set_Bedrag_woonlasten(final String value)
	{
		this.setObject(value, "./bedrag_woonlasten", String.class);
	}

	public boolean is_Bedrag_woonlasten_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_woonlasten")) != null && 
	        !((String)this.getValueAfter("./bedrag_woonlasten")).equals((this.getValueBefore("./bedrag_woonlasten"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_woonlasten = com.orchestranetworks.schema.Path.parse("./bedrag_woonlasten");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_woonlasten()
	{
		return path_Bedrag_woonlasten;
	}

   /* Get and Set methods for attribute:  
    * indicator_CHF_toetsing (Standard)
    *
    */

	public String get_Indicator_CHF_toetsing()
	{
		return this.getString("./indicator_CHF_toetsing");
	}

	public String get_Indicator_CHF_toetsing_After()
	{
		return (String)this.getValueAfter("./indicator_CHF_toetsing");
	}

	public String get_Indicator_CHF_toetsing_Before()
	{
		return (String)this.getValueBefore("./indicator_CHF_toetsing");
	}

	public void set_Indicator_CHF_toetsing(final String value)
	{
		this.setObject(value, "./indicator_CHF_toetsing", String.class);
	}

	public boolean is_Indicator_CHF_toetsing_Changed()
	{
	    return (((String)this.getValueAfter("./indicator_CHF_toetsing")) != null && 
	        !((String)this.getValueAfter("./indicator_CHF_toetsing")).equals((this.getValueBefore("./indicator_CHF_toetsing"))));
	}

	private static final com.orchestranetworks.schema.Path path_Indicator_CHF_toetsing = com.orchestranetworks.schema.Path.parse("./indicator_CHF_toetsing");
	public static final com.orchestranetworks.schema.Path getPath_Indicator_CHF_toetsing()
	{
		return path_Indicator_CHF_toetsing;
	}

   /* Get and Set methods for attribute:  
    * lening_geldverstrekker_wijziging_generiek_model (Standard)
    *
    */

	public String get_Lening_geldverstrekker_wijziging_generiek_model()
	{
		return this.getString("./lening_geldverstrekker_wijziging_generiek_model");
	}

	public String get_Lening_geldverstrekker_wijziging_generiek_model_After()
	{
		return (String)this.getValueAfter("./lening_geldverstrekker_wijziging_generiek_model");
	}

	public String get_Lening_geldverstrekker_wijziging_generiek_model_Before()
	{
		return (String)this.getValueBefore("./lening_geldverstrekker_wijziging_generiek_model");
	}

	public void set_Lening_geldverstrekker_wijziging_generiek_model(final String value)
	{
		this.setObject(value, "./lening_geldverstrekker_wijziging_generiek_model", String.class);
	}

	public boolean is_Lening_geldverstrekker_wijziging_generiek_model_Changed()
	{
	    return (((String)this.getValueAfter("./lening_geldverstrekker_wijziging_generiek_model")) != null && 
	        !((String)this.getValueAfter("./lening_geldverstrekker_wijziging_generiek_model")).equals((this.getValueBefore("./lening_geldverstrekker_wijziging_generiek_model"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_geldverstrekker_wijziging_generiek_model = com.orchestranetworks.schema.Path.parse("./lening_geldverstrekker_wijziging_generiek_model");
	public static final com.orchestranetworks.schema.Path getPath_Lening_geldverstrekker_wijziging_generiek_model()
	{
		return path_Lening_geldverstrekker_wijziging_generiek_model;
	}

   /* Get and Set methods for attribute:  
    * lening_toetsresultaat (Standard)
    *
    */

	public String get_Lening_toetsresultaat()
	{
		return this.getString("./lening_toetsresultaat");
	}

	public String get_Lening_toetsresultaat_After()
	{
		return (String)this.getValueAfter("./lening_toetsresultaat");
	}

	public String get_Lening_toetsresultaat_Before()
	{
		return (String)this.getValueBefore("./lening_toetsresultaat");
	}

	public void set_Lening_toetsresultaat(final String value)
	{
		this.setObject(value, "./lening_toetsresultaat", String.class);
	}

	public boolean is_Lening_toetsresultaat_Changed()
	{
	    return (((String)this.getValueAfter("./lening_toetsresultaat")) != null && 
	        !((String)this.getValueAfter("./lening_toetsresultaat")).equals((this.getValueBefore("./lening_toetsresultaat"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_toetsresultaat = com.orchestranetworks.schema.Path.parse("./lening_toetsresultaat");
	public static final com.orchestranetworks.schema.Path getPath_Lening_toetsresultaat()
	{
		return path_Lening_toetsresultaat;
	}

   /* Get and Set methods for attribute:  
    * teamcode_naam (Standard)
    *
    */

	public String get_Teamcode_naam()
	{
		return this.getString("./teamcode_naam");
	}

	public String get_Teamcode_naam_After()
	{
		return (String)this.getValueAfter("./teamcode_naam");
	}

	public String get_Teamcode_naam_Before()
	{
		return (String)this.getValueBefore("./teamcode_naam");
	}

	public void set_Teamcode_naam(final String value)
	{
		this.setObject(value, "./teamcode_naam", String.class);
	}

	public boolean is_Teamcode_naam_Changed()
	{
	    return (((String)this.getValueAfter("./teamcode_naam")) != null && 
	        !((String)this.getValueAfter("./teamcode_naam")).equals((this.getValueBefore("./teamcode_naam"))));
	}

	private static final com.orchestranetworks.schema.Path path_Teamcode_naam = com.orchestranetworks.schema.Path.parse("./teamcode_naam");
	public static final com.orchestranetworks.schema.Path getPath_Teamcode_naam()
	{
		return path_Teamcode_naam;
	}

   /* Get and Set methods for attribute:  
    * regeling_laatste_acceptatieproces (Standard)
    *
    */

	public String get_Regeling_laatste_acceptatieproces()
	{
		return this.getString("./regeling_laatste_acceptatieproces");
	}

	public String get_Regeling_laatste_acceptatieproces_After()
	{
		return (String)this.getValueAfter("./regeling_laatste_acceptatieproces");
	}

	public String get_Regeling_laatste_acceptatieproces_Before()
	{
		return (String)this.getValueBefore("./regeling_laatste_acceptatieproces");
	}

	public void set_Regeling_laatste_acceptatieproces(final String value)
	{
		this.setObject(value, "./regeling_laatste_acceptatieproces", String.class);
	}

	public boolean is_Regeling_laatste_acceptatieproces_Changed()
	{
	    return (((String)this.getValueAfter("./regeling_laatste_acceptatieproces")) != null && 
	        !((String)this.getValueAfter("./regeling_laatste_acceptatieproces")).equals((this.getValueBefore("./regeling_laatste_acceptatieproces"))));
	}

	private static final com.orchestranetworks.schema.Path path_Regeling_laatste_acceptatieproces = com.orchestranetworks.schema.Path.parse("./regeling_laatste_acceptatieproces");
	public static final com.orchestranetworks.schema.Path getPath_Regeling_laatste_acceptatieproces()
	{
		return path_Regeling_laatste_acceptatieproces;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_procesversie (Standard)
    *
    */

	public String get_Omschrijving_procesversie()
	{
		return this.getString("./omschrijving_procesversie");
	}

	public String get_Omschrijving_procesversie_After()
	{
		return (String)this.getValueAfter("./omschrijving_procesversie");
	}

	public String get_Omschrijving_procesversie_Before()
	{
		return (String)this.getValueBefore("./omschrijving_procesversie");
	}

	public void set_Omschrijving_procesversie(final String value)
	{
		this.setObject(value, "./omschrijving_procesversie", String.class);
	}

	public boolean is_Omschrijving_procesversie_Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_procesversie")) != null && 
	        !((String)this.getValueAfter("./omschrijving_procesversie")).equals((this.getValueBefore("./omschrijving_procesversie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_procesversie = com.orchestranetworks.schema.Path.parse("./omschrijving_procesversie");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_procesversie()
	{
		return path_Omschrijving_procesversie;
	}

   /* Get and Set methods for attribute:  
    * valutasoort (Standard)
    *
    */

	public String get_Valutasoort()
	{
		return this.getString("./valutasoort");
	}

	public String get_Valutasoort_After()
	{
		return (String)this.getValueAfter("./valutasoort");
	}

	public String get_Valutasoort_Before()
	{
		return (String)this.getValueBefore("./valutasoort");
	}

	public void set_Valutasoort(final String value)
	{
		this.setObject(value, "./valutasoort", String.class);
	}

	public boolean is_Valutasoort_Changed()
	{
	    return (((String)this.getValueAfter("./valutasoort")) != null && 
	        !((String)this.getValueAfter("./valutasoort")).equals((this.getValueBefore("./valutasoort"))));
	}

	private static final com.orchestranetworks.schema.Path path_Valutasoort = com.orchestranetworks.schema.Path.parse("./valutasoort");
	public static final com.orchestranetworks.schema.Path getPath_Valutasoort()
	{
		return path_Valutasoort;
	}

   /* Get and Set methods for attribute:  
    * bedrag_oorspronkelijke_bouwdepot (Standard)
    *
    */

	public String get_Bedrag_oorspronkelijke_bouwdepot()
	{
		return this.getString("./bedrag_oorspronkelijke_bouwdepot");
	}

	public String get_Bedrag_oorspronkelijke_bouwdepot_After()
	{
		return (String)this.getValueAfter("./bedrag_oorspronkelijke_bouwdepot");
	}

	public String get_Bedrag_oorspronkelijke_bouwdepot_Before()
	{
		return (String)this.getValueBefore("./bedrag_oorspronkelijke_bouwdepot");
	}

	public void set_Bedrag_oorspronkelijke_bouwdepot(final String value)
	{
		this.setObject(value, "./bedrag_oorspronkelijke_bouwdepot", String.class);
	}

	public boolean is_Bedrag_oorspronkelijke_bouwdepot_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_oorspronkelijke_bouwdepot")) != null && 
	        !((String)this.getValueAfter("./bedrag_oorspronkelijke_bouwdepot")).equals((this.getValueBefore("./bedrag_oorspronkelijke_bouwdepot"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_oorspronkelijke_bouwdepot = com.orchestranetworks.schema.Path.parse("./bedrag_oorspronkelijke_bouwdepot");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_oorspronkelijke_bouwdepot()
	{
		return path_Bedrag_oorspronkelijke_bouwdepot;
	}

   /* Get and Set methods for attribute:  
    * aanvraag_volgnummer_argenta (Standard)
    *
    */

	public String get_Aanvraag_volgnummer_argenta()
	{
		return this.getString("./aanvraag_volgnummer_argenta");
	}

	public String get_Aanvraag_volgnummer_argenta_After()
	{
		return (String)this.getValueAfter("./aanvraag_volgnummer_argenta");
	}

	public String get_Aanvraag_volgnummer_argenta_Before()
	{
		return (String)this.getValueBefore("./aanvraag_volgnummer_argenta");
	}

	public void set_Aanvraag_volgnummer_argenta(final String value)
	{
		this.setObject(value, "./aanvraag_volgnummer_argenta", String.class);
	}

	public boolean is_Aanvraag_volgnummer_argenta_Changed()
	{
	    return (((String)this.getValueAfter("./aanvraag_volgnummer_argenta")) != null && 
	        !((String)this.getValueAfter("./aanvraag_volgnummer_argenta")).equals((this.getValueBefore("./aanvraag_volgnummer_argenta"))));
	}

	private static final com.orchestranetworks.schema.Path path_Aanvraag_volgnummer_argenta = com.orchestranetworks.schema.Path.parse("./aanvraag_volgnummer_argenta");
	public static final com.orchestranetworks.schema.Path getPath_Aanvraag_volgnummer_argenta()
	{
		return path_Aanvraag_volgnummer_argenta;
	}

   /* Get and Set methods for attribute:  
    * soort_advies (Standard)
    *
    */

	public String get_Soort_advies()
	{
		return this.getString("./soort_advies");
	}

	public String get_Soort_advies_After()
	{
		return (String)this.getValueAfter("./soort_advies");
	}

	public String get_Soort_advies_Before()
	{
		return (String)this.getValueBefore("./soort_advies");
	}

	public void set_Soort_advies(final String value)
	{
		this.setObject(value, "./soort_advies", String.class);
	}

	public boolean is_Soort_advies_Changed()
	{
	    return (((String)this.getValueAfter("./soort_advies")) != null && 
	        !((String)this.getValueAfter("./soort_advies")).equals((this.getValueBefore("./soort_advies"))));
	}

	private static final com.orchestranetworks.schema.Path path_Soort_advies = com.orchestranetworks.schema.Path.parse("./soort_advies");
	public static final com.orchestranetworks.schema.Path getPath_Soort_advies()
	{
		return path_Soort_advies;
	}

   /* Get and Set methods for attribute:  
    * aantal_dagen_in_achterstand (Standard)
    *
    */

	public String get_Aantal_dagen_in_achterstand()
	{
		return this.getString("./aantal_dagen_in_achterstand");
	}

	public String get_Aantal_dagen_in_achterstand_After()
	{
		return (String)this.getValueAfter("./aantal_dagen_in_achterstand");
	}

	public String get_Aantal_dagen_in_achterstand_Before()
	{
		return (String)this.getValueBefore("./aantal_dagen_in_achterstand");
	}

	public void set_Aantal_dagen_in_achterstand(final String value)
	{
		this.setObject(value, "./aantal_dagen_in_achterstand", String.class);
	}

	public boolean is_Aantal_dagen_in_achterstand_Changed()
	{
	    return (((String)this.getValueAfter("./aantal_dagen_in_achterstand")) != null && 
	        !((String)this.getValueAfter("./aantal_dagen_in_achterstand")).equals((this.getValueBefore("./aantal_dagen_in_achterstand"))));
	}

	private static final com.orchestranetworks.schema.Path path_Aantal_dagen_in_achterstand = com.orchestranetworks.schema.Path.parse("./aantal_dagen_in_achterstand");
	public static final com.orchestranetworks.schema.Path getPath_Aantal_dagen_in_achterstand()
	{
		return path_Aantal_dagen_in_achterstand;
	}

   /* Get and Set methods for attribute:  
    * reden_achterstand_omschrijving (Standard)
    *
    */

	public String get_Reden_achterstand_omschrijving()
	{
		return this.getString("./reden_achterstand_omschrijving");
	}

	public String get_Reden_achterstand_omschrijving_After()
	{
		return (String)this.getValueAfter("./reden_achterstand_omschrijving");
	}

	public String get_Reden_achterstand_omschrijving_Before()
	{
		return (String)this.getValueBefore("./reden_achterstand_omschrijving");
	}

	public void set_Reden_achterstand_omschrijving(final String value)
	{
		this.setObject(value, "./reden_achterstand_omschrijving", String.class);
	}

	public boolean is_Reden_achterstand_omschrijving_Changed()
	{
	    return (((String)this.getValueAfter("./reden_achterstand_omschrijving")) != null && 
	        !((String)this.getValueAfter("./reden_achterstand_omschrijving")).equals((this.getValueBefore("./reden_achterstand_omschrijving"))));
	}

	private static final com.orchestranetworks.schema.Path path_Reden_achterstand_omschrijving = com.orchestranetworks.schema.Path.parse("./reden_achterstand_omschrijving");
	public static final com.orchestranetworks.schema.Path getPath_Reden_achterstand_omschrijving()
	{
		return path_Reden_achterstand_omschrijving;
	}

   /* Get and Set methods for attribute:  
    * profiel_omschrijving (Standard)
    *
    */

	public String get_Profiel_omschrijving()
	{
		return this.getString("./profiel_omschrijving");
	}

	public String get_Profiel_omschrijving_After()
	{
		return (String)this.getValueAfter("./profiel_omschrijving");
	}

	public String get_Profiel_omschrijving_Before()
	{
		return (String)this.getValueBefore("./profiel_omschrijving");
	}

	public void set_Profiel_omschrijving(final String value)
	{
		this.setObject(value, "./profiel_omschrijving", String.class);
	}

	public boolean is_Profiel_omschrijving_Changed()
	{
	    return (((String)this.getValueAfter("./profiel_omschrijving")) != null && 
	        !((String)this.getValueAfter("./profiel_omschrijving")).equals((this.getValueBefore("./profiel_omschrijving"))));
	}

	private static final com.orchestranetworks.schema.Path path_Profiel_omschrijving = com.orchestranetworks.schema.Path.parse("./profiel_omschrijving");
	public static final com.orchestranetworks.schema.Path getPath_Profiel_omschrijving()
	{
		return path_Profiel_omschrijving;
	}

   /* Get and Set methods for attribute:  
    * dreigende_achterstand_door_corona (Standard)
    *
    */

	public String get_Dreigende_achterstand_door_corona()
	{
		return this.getString("./dreigende_achterstand_door_corona");
	}

	public String get_Dreigende_achterstand_door_corona_After()
	{
		return (String)this.getValueAfter("./dreigende_achterstand_door_corona");
	}

	public String get_Dreigende_achterstand_door_corona_Before()
	{
		return (String)this.getValueBefore("./dreigende_achterstand_door_corona");
	}

	public void set_Dreigende_achterstand_door_corona(final String value)
	{
		this.setObject(value, "./dreigende_achterstand_door_corona", String.class);
	}

	public boolean is_Dreigende_achterstand_door_corona_Changed()
	{
	    return (((String)this.getValueAfter("./dreigende_achterstand_door_corona")) != null && 
	        !((String)this.getValueAfter("./dreigende_achterstand_door_corona")).equals((this.getValueBefore("./dreigende_achterstand_door_corona"))));
	}

	private static final com.orchestranetworks.schema.Path path_Dreigende_achterstand_door_corona = com.orchestranetworks.schema.Path.parse("./dreigende_achterstand_door_corona");
	public static final com.orchestranetworks.schema.Path getPath_Dreigende_achterstand_door_corona()
	{
		return path_Dreigende_achterstand_door_corona;
	}

}

