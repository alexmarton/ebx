package nl.vqd.demo.ebx.mdm.argenta.service.declaration;

import com.orchestranetworks.schema.types.dataspace.DataspaceSet;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.declaration.ActivationContextOnTableView;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import com.orchestranetworks.userservice.declaration.UserServicePropertiesDefinitionContext;
import com.orchestranetworks.userservice.declaration.WebComponentDeclarationContext;
import nl.vqd.demo.ebx.mdm.argenta.service.ImportDataService;
import nl.vqd.demo.ebx.mdm.commons.datamodel.ContractenLening;

public class ImportDataDeclaration implements UserServiceDeclaration.OnTableView {

    public static final ServiceKey SERVICE_KEY = ServiceKey.forName("ImportData");

    @Override
    public ServiceKey getServiceKey() {
        return ImportDataDeclaration.SERVICE_KEY;
    }

    @Override
    public UserService<TableViewEntitySelection> createUserService() {
        return new ImportDataService();
    }

    @Override
    public void defineActivation(ActivationContextOnTableView context) {
        context.includeAllDatasets();
        context.includeAllDataspaces(DataspaceSet.DataspaceType.BRANCH);
        context.includeSchemaNodesMatching(ContractenLening.getTablePath());

    }

    @Override
    public void defineProperties(UserServicePropertiesDefinitionContext userServicePropertiesDefinitionContext) {
        userServicePropertiesDefinitionContext.setDescription("Degevens die moeten worden geïmporteerd");
        userServicePropertiesDefinitionContext.setLabel("Leningen Importeren");

    }

    @Override
    public void declareWebComponent(WebComponentDeclarationContext webComponentDeclarationContext) {
        //webComponentDeclarationContext.setAvailableAsWorkflowUserTask(false);
        //webComponentDeclarationContext.setAvailableAsPerspectiveAction(false);

    }
}
