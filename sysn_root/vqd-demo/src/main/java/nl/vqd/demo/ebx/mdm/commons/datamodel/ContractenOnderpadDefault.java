package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Contracten Onderpand" record from the data model "Argenta Demo"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Thu Nov 19 14:33:38 CET 2020
*/

public class ContractenOnderpadDefault extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/ContractenOnderpadDefault";

	public ContractenOnderpadDefault(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public ContractenOnderpadDefault(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public ContractenOnderpadDefault(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public ContractenOnderpadDefault(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public ContractenOnderpadDefault(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public ContractenOnderpadDefault(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public ContractenOnderpadDefault(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public ContractenOnderpadDefault(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * rapportagedatum (Standard)
    *
    */

	public String get_Rapportagedatum()
	{
		return this.getString("./rapportagedatum");
	}

	public String get_Rapportagedatum_After()
	{
		return (String)this.getValueAfter("./rapportagedatum");
	}

	public String get_Rapportagedatum_Before()
	{
		return (String)this.getValueBefore("./rapportagedatum");
	}

	public void set_Rapportagedatum(final String value)
	{
		this.setObject(value, "./rapportagedatum", String.class);
	}

	public boolean is_Rapportagedatum_Changed()
	{
	    return (((String)this.getValueAfter("./rapportagedatum")) != null && 
	        !((String)this.getValueAfter("./rapportagedatum")).equals((this.getValueBefore("./rapportagedatum"))));
	}

	private static final com.orchestranetworks.schema.Path path_Rapportagedatum = com.orchestranetworks.schema.Path.parse("./rapportagedatum");
	public static final com.orchestranetworks.schema.Path getPath_Rapportagedatum()
	{
		return path_Rapportagedatum;
	}

   /* Get and Set methods for attribute:  
    * lening_contract_nummer (Standard)
    *
    */

	public String get_Lening_contract_nummer()
	{
		return this.getString("./lening_contract_nummer");
	}

	public String get_Lening_contract_nummer_After()
	{
		return (String)this.getValueAfter("./lening_contract_nummer");
	}

	public String get_Lening_contract_nummer_Before()
	{
		return (String)this.getValueBefore("./lening_contract_nummer");
	}

	public void set_Lening_contract_nummer(final String value)
	{
		this.setObject(value, "./lening_contract_nummer", String.class);
	}

	public boolean is_Lening_contract_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./lening_contract_nummer")) != null && 
	        !((String)this.getValueAfter("./lening_contract_nummer")).equals((this.getValueBefore("./lening_contract_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Lening_contract_nummer = com.orchestranetworks.schema.Path.parse("./lening_contract_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Lening_contract_nummer()
	{
		return path_Lening_contract_nummer;
	}

   /* Get and Set methods for attribute:  
    * contract_procesversie (Standard)
    *
    */

	public String get_Contract_procesversie()
	{
		return this.getString("./contract_procesversie");
	}

	public String get_Contract_procesversie_After()
	{
		return (String)this.getValueAfter("./contract_procesversie");
	}

	public String get_Contract_procesversie_Before()
	{
		return (String)this.getValueBefore("./contract_procesversie");
	}

	public void set_Contract_procesversie(final String value)
	{
		this.setObject(value, "./contract_procesversie", String.class);
	}

	public boolean is_Contract_procesversie_Changed()
	{
	    return (((String)this.getValueAfter("./contract_procesversie")) != null && 
	        !((String)this.getValueAfter("./contract_procesversie")).equals((this.getValueBefore("./contract_procesversie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Contract_procesversie = com.orchestranetworks.schema.Path.parse("./contract_procesversie");
	public static final com.orchestranetworks.schema.Path getPath_Contract_procesversie()
	{
		return path_Contract_procesversie;
	}

   /* Get and Set methods for attribute:  
    * onderpand_omschrijving_status (Standard)
    *
    */

	public String get_Onderpand_omschrijving_status()
	{
		return this.getString("./onderpand_omschrijving_status");
	}

	public String get_Onderpand_omschrijving_status_After()
	{
		return (String)this.getValueAfter("./onderpand_omschrijving_status");
	}

	public String get_Onderpand_omschrijving_status_Before()
	{
		return (String)this.getValueBefore("./onderpand_omschrijving_status");
	}

	public void set_Onderpand_omschrijving_status(final String value)
	{
		this.setObject(value, "./onderpand_omschrijving_status", String.class);
	}

	public boolean is_Onderpand_omschrijving_status_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_omschrijving_status")) != null && 
	        !((String)this.getValueAfter("./onderpand_omschrijving_status")).equals((this.getValueBefore("./onderpand_omschrijving_status"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_omschrijving_status = com.orchestranetworks.schema.Path.parse("./onderpand_omschrijving_status");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_omschrijving_status()
	{
		return path_Onderpand_omschrijving_status;
	}

   /* Get and Set methods for attribute:  
    * onderpand_omschrijving_soort (Standard)
    *
    */

	public String get_Onderpand_omschrijving_soort()
	{
		return this.getString("./onderpand_omschrijving_soort");
	}

	public String get_Onderpand_omschrijving_soort_After()
	{
		return (String)this.getValueAfter("./onderpand_omschrijving_soort");
	}

	public String get_Onderpand_omschrijving_soort_Before()
	{
		return (String)this.getValueBefore("./onderpand_omschrijving_soort");
	}

	public void set_Onderpand_omschrijving_soort(final String value)
	{
		this.setObject(value, "./onderpand_omschrijving_soort", String.class);
	}

	public boolean is_Onderpand_omschrijving_soort_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_omschrijving_soort")) != null && 
	        !((String)this.getValueAfter("./onderpand_omschrijving_soort")).equals((this.getValueBefore("./onderpand_omschrijving_soort"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_omschrijving_soort = com.orchestranetworks.schema.Path.parse("./onderpand_omschrijving_soort");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_omschrijving_soort()
	{
		return path_Onderpand_omschrijving_soort;
	}

   /* Get and Set methods for attribute:  
    * omschrijving_soort_bouw (Standard)
    *
    */

	public String get_Omschrijving_soort_bouw()
	{
		return this.getString("./omschrijving_soort_bouw");
	}

	public String get_Omschrijving_soort_bouw_After()
	{
		return (String)this.getValueAfter("./omschrijving_soort_bouw");
	}

	public String get_Omschrijving_soort_bouw_Before()
	{
		return (String)this.getValueBefore("./omschrijving_soort_bouw");
	}

	public void set_Omschrijving_soort_bouw(final String value)
	{
		this.setObject(value, "./omschrijving_soort_bouw", String.class);
	}

	public boolean is_Omschrijving_soort_bouw_Changed()
	{
	    return (((String)this.getValueAfter("./omschrijving_soort_bouw")) != null && 
	        !((String)this.getValueAfter("./omschrijving_soort_bouw")).equals((this.getValueBefore("./omschrijving_soort_bouw"))));
	}

	private static final com.orchestranetworks.schema.Path path_Omschrijving_soort_bouw = com.orchestranetworks.schema.Path.parse("./omschrijving_soort_bouw");
	public static final com.orchestranetworks.schema.Path getPath_Omschrijving_soort_bouw()
	{
		return path_Omschrijving_soort_bouw;
	}

   /* Get and Set methods for attribute:  
    * onderpand_bouwjaar (Standard)
    *
    */

	public String get_Onderpand_bouwjaar()
	{
		return this.getString("./onderpand_bouwjaar");
	}

	public String get_Onderpand_bouwjaar_After()
	{
		return (String)this.getValueAfter("./onderpand_bouwjaar");
	}

	public String get_Onderpand_bouwjaar_Before()
	{
		return (String)this.getValueBefore("./onderpand_bouwjaar");
	}

	public void set_Onderpand_bouwjaar(final String value)
	{
		this.setObject(value, "./onderpand_bouwjaar", String.class);
	}

	public boolean is_Onderpand_bouwjaar_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_bouwjaar")) != null && 
	        !((String)this.getValueAfter("./onderpand_bouwjaar")).equals((this.getValueBefore("./onderpand_bouwjaar"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_bouwjaar = com.orchestranetworks.schema.Path.parse("./onderpand_bouwjaar");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_bouwjaar()
	{
		return path_Onderpand_bouwjaar;
	}

   /* Get and Set methods for attribute:  
    * bedrag_koopsom (Standard)
    *
    */

	public String get_Bedrag_koopsom()
	{
		return this.getString("./bedrag_koopsom");
	}

	public String get_Bedrag_koopsom_After()
	{
		return (String)this.getValueAfter("./bedrag_koopsom");
	}

	public String get_Bedrag_koopsom_Before()
	{
		return (String)this.getValueBefore("./bedrag_koopsom");
	}

	public void set_Bedrag_koopsom(final String value)
	{
		this.setObject(value, "./bedrag_koopsom", String.class);
	}

	public boolean is_Bedrag_koopsom_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_koopsom")) != null && 
	        !((String)this.getValueAfter("./bedrag_koopsom")).equals((this.getValueBefore("./bedrag_koopsom"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_koopsom = com.orchestranetworks.schema.Path.parse("./bedrag_koopsom");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_koopsom()
	{
		return path_Bedrag_koopsom;
	}

   /* Get and Set methods for attribute:  
    * onderpand_datum_oorspronkelijke_taxatie (Standard)
    *
    */

	public String get_Onderpand_datum_oorspronkelijke_taxatie()
	{
		return this.getString("./onderpand_datum_oorspronkelijke_taxatie");
	}

	public String get_Onderpand_datum_oorspronkelijke_taxatie_After()
	{
		return (String)this.getValueAfter("./onderpand_datum_oorspronkelijke_taxatie");
	}

	public String get_Onderpand_datum_oorspronkelijke_taxatie_Before()
	{
		return (String)this.getValueBefore("./onderpand_datum_oorspronkelijke_taxatie");
	}

	public void set_Onderpand_datum_oorspronkelijke_taxatie(final String value)
	{
		this.setObject(value, "./onderpand_datum_oorspronkelijke_taxatie", String.class);
	}

	public boolean is_Onderpand_datum_oorspronkelijke_taxatie_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_datum_oorspronkelijke_taxatie")) != null && 
	        !((String)this.getValueAfter("./onderpand_datum_oorspronkelijke_taxatie")).equals((this.getValueBefore("./onderpand_datum_oorspronkelijke_taxatie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_datum_oorspronkelijke_taxatie = com.orchestranetworks.schema.Path.parse("./onderpand_datum_oorspronkelijke_taxatie");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_datum_oorspronkelijke_taxatie()
	{
		return path_Onderpand_datum_oorspronkelijke_taxatie;
	}

   /* Get and Set methods for attribute:  
    * onderpand_oorspronkelijk_taxatiekantoor (Standard)
    *
    */

	public String get_Onderpand_oorspronkelijk_taxatiekantoor()
	{
		return this.getString("./onderpand_oorspronkelijk_taxatiekantoor");
	}

	public String get_Onderpand_oorspronkelijk_taxatiekantoor_After()
	{
		return (String)this.getValueAfter("./onderpand_oorspronkelijk_taxatiekantoor");
	}

	public String get_Onderpand_oorspronkelijk_taxatiekantoor_Before()
	{
		return (String)this.getValueBefore("./onderpand_oorspronkelijk_taxatiekantoor");
	}

	public void set_Onderpand_oorspronkelijk_taxatiekantoor(final String value)
	{
		this.setObject(value, "./onderpand_oorspronkelijk_taxatiekantoor", String.class);
	}

	public boolean is_Onderpand_oorspronkelijk_taxatiekantoor_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_oorspronkelijk_taxatiekantoor")) != null && 
	        !((String)this.getValueAfter("./onderpand_oorspronkelijk_taxatiekantoor")).equals((this.getValueBefore("./onderpand_oorspronkelijk_taxatiekantoor"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_oorspronkelijk_taxatiekantoor = com.orchestranetworks.schema.Path.parse("./onderpand_oorspronkelijk_taxatiekantoor");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_oorspronkelijk_taxatiekantoor()
	{
		return path_Onderpand_oorspronkelijk_taxatiekantoor;
	}

   /* Get and Set methods for attribute:  
    * bedrag_oorspronkelijke_executiewaarde (Standard)
    *
    */

	public String get_Bedrag_oorspronkelijke_executiewaarde()
	{
		return this.getString("./bedrag_oorspronkelijke_executiewaarde");
	}

	public String get_Bedrag_oorspronkelijke_executiewaarde_After()
	{
		return (String)this.getValueAfter("./bedrag_oorspronkelijke_executiewaarde");
	}

	public String get_Bedrag_oorspronkelijke_executiewaarde_Before()
	{
		return (String)this.getValueBefore("./bedrag_oorspronkelijke_executiewaarde");
	}

	public void set_Bedrag_oorspronkelijke_executiewaarde(final String value)
	{
		this.setObject(value, "./bedrag_oorspronkelijke_executiewaarde", String.class);
	}

	public boolean is_Bedrag_oorspronkelijke_executiewaarde_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_oorspronkelijke_executiewaarde")) != null && 
	        !((String)this.getValueAfter("./bedrag_oorspronkelijke_executiewaarde")).equals((this.getValueBefore("./bedrag_oorspronkelijke_executiewaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_oorspronkelijke_executiewaarde = com.orchestranetworks.schema.Path.parse("./bedrag_oorspronkelijke_executiewaarde");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_oorspronkelijke_executiewaarde()
	{
		return path_Bedrag_oorspronkelijke_executiewaarde;
	}

   /* Get and Set methods for attribute:  
    * bedrag_oorspronkelijke_marktwaarde (Standard)
    *
    */

	public String get_Bedrag_oorspronkelijke_marktwaarde()
	{
		return this.getString("./bedrag_oorspronkelijke_marktwaarde");
	}

	public String get_Bedrag_oorspronkelijke_marktwaarde_After()
	{
		return (String)this.getValueAfter("./bedrag_oorspronkelijke_marktwaarde");
	}

	public String get_Bedrag_oorspronkelijke_marktwaarde_Before()
	{
		return (String)this.getValueBefore("./bedrag_oorspronkelijke_marktwaarde");
	}

	public void set_Bedrag_oorspronkelijke_marktwaarde(final String value)
	{
		this.setObject(value, "./bedrag_oorspronkelijke_marktwaarde", String.class);
	}

	public boolean is_Bedrag_oorspronkelijke_marktwaarde_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_oorspronkelijke_marktwaarde")) != null && 
	        !((String)this.getValueAfter("./bedrag_oorspronkelijke_marktwaarde")).equals((this.getValueBefore("./bedrag_oorspronkelijke_marktwaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_oorspronkelijke_marktwaarde = com.orchestranetworks.schema.Path.parse("./bedrag_oorspronkelijke_marktwaarde");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_oorspronkelijke_marktwaarde()
	{
		return path_Bedrag_oorspronkelijke_marktwaarde;
	}

   /* Get and Set methods for attribute:  
    * datum_taxatie (Standard)
    *
    */

	public String get_Datum_taxatie()
	{
		return this.getString("./datum_taxatie");
	}

	public String get_Datum_taxatie_After()
	{
		return (String)this.getValueAfter("./datum_taxatie");
	}

	public String get_Datum_taxatie_Before()
	{
		return (String)this.getValueBefore("./datum_taxatie");
	}

	public void set_Datum_taxatie(final String value)
	{
		this.setObject(value, "./datum_taxatie", String.class);
	}

	public boolean is_Datum_taxatie_Changed()
	{
	    return (((String)this.getValueAfter("./datum_taxatie")) != null && 
	        !((String)this.getValueAfter("./datum_taxatie")).equals((this.getValueBefore("./datum_taxatie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_taxatie = com.orchestranetworks.schema.Path.parse("./datum_taxatie");
	public static final com.orchestranetworks.schema.Path getPath_Datum_taxatie()
	{
		return path_Datum_taxatie;
	}

   /* Get and Set methods for attribute:  
    * taxateur_naam (Standard)
    *
    */

	public String get_Taxateur_naam()
	{
		return this.getString("./taxateur_naam");
	}

	public String get_Taxateur_naam_After()
	{
		return (String)this.getValueAfter("./taxateur_naam");
	}

	public String get_Taxateur_naam_Before()
	{
		return (String)this.getValueBefore("./taxateur_naam");
	}

	public void set_Taxateur_naam(final String value)
	{
		this.setObject(value, "./taxateur_naam", String.class);
	}

	public boolean is_Taxateur_naam_Changed()
	{
	    return (((String)this.getValueAfter("./taxateur_naam")) != null && 
	        !((String)this.getValueAfter("./taxateur_naam")).equals((this.getValueBefore("./taxateur_naam"))));
	}

	private static final com.orchestranetworks.schema.Path path_Taxateur_naam = com.orchestranetworks.schema.Path.parse("./taxateur_naam");
	public static final com.orchestranetworks.schema.Path getPath_Taxateur_naam()
	{
		return path_Taxateur_naam;
	}

   /* Get and Set methods for attribute:  
    * bedrag_huidige_executiewaarde (Standard)
    *
    */

	public String get_Bedrag_huidige_executiewaarde()
	{
		return this.getString("./bedrag_huidige_executiewaarde");
	}

	public String get_Bedrag_huidige_executiewaarde_After()
	{
		return (String)this.getValueAfter("./bedrag_huidige_executiewaarde");
	}

	public String get_Bedrag_huidige_executiewaarde_Before()
	{
		return (String)this.getValueBefore("./bedrag_huidige_executiewaarde");
	}

	public void set_Bedrag_huidige_executiewaarde(final String value)
	{
		this.setObject(value, "./bedrag_huidige_executiewaarde", String.class);
	}

	public boolean is_Bedrag_huidige_executiewaarde_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_huidige_executiewaarde")) != null && 
	        !((String)this.getValueAfter("./bedrag_huidige_executiewaarde")).equals((this.getValueBefore("./bedrag_huidige_executiewaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_huidige_executiewaarde = com.orchestranetworks.schema.Path.parse("./bedrag_huidige_executiewaarde");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_huidige_executiewaarde()
	{
		return path_Bedrag_huidige_executiewaarde;
	}

   /* Get and Set methods for attribute:  
    * bedrag_huidige_marktwaarde (Standard)
    *
    */

	public String get_Bedrag_huidige_marktwaarde()
	{
		return this.getString("./bedrag_huidige_marktwaarde");
	}

	public String get_Bedrag_huidige_marktwaarde_After()
	{
		return (String)this.getValueAfter("./bedrag_huidige_marktwaarde");
	}

	public String get_Bedrag_huidige_marktwaarde_Before()
	{
		return (String)this.getValueBefore("./bedrag_huidige_marktwaarde");
	}

	public void set_Bedrag_huidige_marktwaarde(final String value)
	{
		this.setObject(value, "./bedrag_huidige_marktwaarde", String.class);
	}

	public boolean is_Bedrag_huidige_marktwaarde_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_huidige_marktwaarde")) != null && 
	        !((String)this.getValueAfter("./bedrag_huidige_marktwaarde")).equals((this.getValueBefore("./bedrag_huidige_marktwaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_huidige_marktwaarde = com.orchestranetworks.schema.Path.parse("./bedrag_huidige_marktwaarde");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_huidige_marktwaarde()
	{
		return path_Bedrag_huidige_marktwaarde;
	}

   /* Get and Set methods for attribute:  
    * bedrag_executiewaarde_na_verbouwing (Standard)
    *
    */

	public String get_Bedrag_executiewaarde_na_verbouwing()
	{
		return this.getString("./bedrag_executiewaarde_na_verbouwing");
	}

	public String get_Bedrag_executiewaarde_na_verbouwing_After()
	{
		return (String)this.getValueAfter("./bedrag_executiewaarde_na_verbouwing");
	}

	public String get_Bedrag_executiewaarde_na_verbouwing_Before()
	{
		return (String)this.getValueBefore("./bedrag_executiewaarde_na_verbouwing");
	}

	public void set_Bedrag_executiewaarde_na_verbouwing(final String value)
	{
		this.setObject(value, "./bedrag_executiewaarde_na_verbouwing", String.class);
	}

	public boolean is_Bedrag_executiewaarde_na_verbouwing_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_executiewaarde_na_verbouwing")) != null && 
	        !((String)this.getValueAfter("./bedrag_executiewaarde_na_verbouwing")).equals((this.getValueBefore("./bedrag_executiewaarde_na_verbouwing"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_executiewaarde_na_verbouwing = com.orchestranetworks.schema.Path.parse("./bedrag_executiewaarde_na_verbouwing");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_executiewaarde_na_verbouwing()
	{
		return path_Bedrag_executiewaarde_na_verbouwing;
	}

   /* Get and Set methods for attribute:  
    * bedrag_marktwaarde_na_verbouwing (Standard)
    *
    */

	public String get_Bedrag_marktwaarde_na_verbouwing()
	{
		return this.getString("./bedrag_marktwaarde_na_verbouwing");
	}

	public String get_Bedrag_marktwaarde_na_verbouwing_After()
	{
		return (String)this.getValueAfter("./bedrag_marktwaarde_na_verbouwing");
	}

	public String get_Bedrag_marktwaarde_na_verbouwing_Before()
	{
		return (String)this.getValueBefore("./bedrag_marktwaarde_na_verbouwing");
	}

	public void set_Bedrag_marktwaarde_na_verbouwing(final String value)
	{
		this.setObject(value, "./bedrag_marktwaarde_na_verbouwing", String.class);
	}

	public boolean is_Bedrag_marktwaarde_na_verbouwing_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_marktwaarde_na_verbouwing")) != null && 
	        !((String)this.getValueAfter("./bedrag_marktwaarde_na_verbouwing")).equals((this.getValueBefore("./bedrag_marktwaarde_na_verbouwing"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_marktwaarde_na_verbouwing = com.orchestranetworks.schema.Path.parse("./bedrag_marktwaarde_na_verbouwing");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_marktwaarde_na_verbouwing()
	{
		return path_Bedrag_marktwaarde_na_verbouwing;
	}

   /* Get and Set methods for attribute:  
    * bedrag_geindexeerde_executiewaarde (Standard)
    *
    */

	public String get_Bedrag_geindexeerde_executiewaarde()
	{
		return this.getString("./bedrag_geindexeerde_executiewaarde");
	}

	public String get_Bedrag_geindexeerde_executiewaarde_After()
	{
		return (String)this.getValueAfter("./bedrag_geindexeerde_executiewaarde");
	}

	public String get_Bedrag_geindexeerde_executiewaarde_Before()
	{
		return (String)this.getValueBefore("./bedrag_geindexeerde_executiewaarde");
	}

	public void set_Bedrag_geindexeerde_executiewaarde(final String value)
	{
		this.setObject(value, "./bedrag_geindexeerde_executiewaarde", String.class);
	}

	public boolean is_Bedrag_geindexeerde_executiewaarde_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_geindexeerde_executiewaarde")) != null && 
	        !((String)this.getValueAfter("./bedrag_geindexeerde_executiewaarde")).equals((this.getValueBefore("./bedrag_geindexeerde_executiewaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_geindexeerde_executiewaarde = com.orchestranetworks.schema.Path.parse("./bedrag_geindexeerde_executiewaarde");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_geindexeerde_executiewaarde()
	{
		return path_Bedrag_geindexeerde_executiewaarde;
	}

   /* Get and Set methods for attribute:  
    * bedrag_geindexeerde_marktwaarde (Standard)
    *
    */

	public String get_Bedrag_geindexeerde_marktwaarde()
	{
		return this.getString("./bedrag_geindexeerde_marktwaarde");
	}

	public String get_Bedrag_geindexeerde_marktwaarde_After()
	{
		return (String)this.getValueAfter("./bedrag_geindexeerde_marktwaarde");
	}

	public String get_Bedrag_geindexeerde_marktwaarde_Before()
	{
		return (String)this.getValueBefore("./bedrag_geindexeerde_marktwaarde");
	}

	public void set_Bedrag_geindexeerde_marktwaarde(final String value)
	{
		this.setObject(value, "./bedrag_geindexeerde_marktwaarde", String.class);
	}

	public boolean is_Bedrag_geindexeerde_marktwaarde_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_geindexeerde_marktwaarde")) != null && 
	        !((String)this.getValueAfter("./bedrag_geindexeerde_marktwaarde")).equals((this.getValueBefore("./bedrag_geindexeerde_marktwaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_geindexeerde_marktwaarde = com.orchestranetworks.schema.Path.parse("./bedrag_geindexeerde_marktwaarde");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_geindexeerde_marktwaarde()
	{
		return path_Bedrag_geindexeerde_marktwaarde;
	}

   /* Get and Set methods for attribute:  
    * onderpand_indexwaarde (Standard)
    *
    */

	public String get_Onderpand_indexwaarde()
	{
		return this.getString("./onderpand_indexwaarde");
	}

	public String get_Onderpand_indexwaarde_After()
	{
		return (String)this.getValueAfter("./onderpand_indexwaarde");
	}

	public String get_Onderpand_indexwaarde_Before()
	{
		return (String)this.getValueBefore("./onderpand_indexwaarde");
	}

	public void set_Onderpand_indexwaarde(final String value)
	{
		this.setObject(value, "./onderpand_indexwaarde", String.class);
	}

	public boolean is_Onderpand_indexwaarde_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_indexwaarde")) != null && 
	        !((String)this.getValueAfter("./onderpand_indexwaarde")).equals((this.getValueBefore("./onderpand_indexwaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_indexwaarde = com.orchestranetworks.schema.Path.parse("./onderpand_indexwaarde");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_indexwaarde()
	{
		return path_Onderpand_indexwaarde;
	}

   /* Get and Set methods for attribute:  
    * onderpand_indexwaarde_soort (Standard)
    *
    */

	public String get_Onderpand_indexwaarde_soort()
	{
		return this.getString("./onderpand_indexwaarde_soort");
	}

	public String get_Onderpand_indexwaarde_soort_After()
	{
		return (String)this.getValueAfter("./onderpand_indexwaarde_soort");
	}

	public String get_Onderpand_indexwaarde_soort_Before()
	{
		return (String)this.getValueBefore("./onderpand_indexwaarde_soort");
	}

	public void set_Onderpand_indexwaarde_soort(final String value)
	{
		this.setObject(value, "./onderpand_indexwaarde_soort", String.class);
	}

	public boolean is_Onderpand_indexwaarde_soort_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_indexwaarde_soort")) != null && 
	        !((String)this.getValueAfter("./onderpand_indexwaarde_soort")).equals((this.getValueBefore("./onderpand_indexwaarde_soort"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_indexwaarde_soort = com.orchestranetworks.schema.Path.parse("./onderpand_indexwaarde_soort");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_indexwaarde_soort()
	{
		return path_Onderpand_indexwaarde_soort;
	}

   /* Get and Set methods for attribute:  
    * onderpand_regiosoort (Standard)
    *
    */

	public String get_Onderpand_regiosoort()
	{
		return this.getString("./onderpand_regiosoort");
	}

	public String get_Onderpand_regiosoort_After()
	{
		return (String)this.getValueAfter("./onderpand_regiosoort");
	}

	public String get_Onderpand_regiosoort_Before()
	{
		return (String)this.getValueBefore("./onderpand_regiosoort");
	}

	public void set_Onderpand_regiosoort(final String value)
	{
		this.setObject(value, "./onderpand_regiosoort", String.class);
	}

	public boolean is_Onderpand_regiosoort_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_regiosoort")) != null && 
	        !((String)this.getValueAfter("./onderpand_regiosoort")).equals((this.getValueBefore("./onderpand_regiosoort"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_regiosoort = com.orchestranetworks.schema.Path.parse("./onderpand_regiosoort");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_regiosoort()
	{
		return path_Onderpand_regiosoort;
	}

   /* Get and Set methods for attribute:  
    * onderpand_regio (Standard)
    *
    */

	public String get_Onderpand_regio()
	{
		return this.getString("./onderpand_regio");
	}

	public String get_Onderpand_regio_After()
	{
		return (String)this.getValueAfter("./onderpand_regio");
	}

	public String get_Onderpand_regio_Before()
	{
		return (String)this.getValueBefore("./onderpand_regio");
	}

	public void set_Onderpand_regio(final String value)
	{
		this.setObject(value, "./onderpand_regio", String.class);
	}

	public boolean is_Onderpand_regio_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_regio")) != null && 
	        !((String)this.getValueAfter("./onderpand_regio")).equals((this.getValueBefore("./onderpand_regio"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_regio = com.orchestranetworks.schema.Path.parse("./onderpand_regio");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_regio()
	{
		return path_Onderpand_regio;
	}

   /* Get and Set methods for attribute:  
    * onderpand_straat (Standard)
    *
    */

	public String get_Onderpand_straat()
	{
		return this.getString("./onderpand_straat");
	}

	public String get_Onderpand_straat_After()
	{
		return (String)this.getValueAfter("./onderpand_straat");
	}

	public String get_Onderpand_straat_Before()
	{
		return (String)this.getValueBefore("./onderpand_straat");
	}

	public void set_Onderpand_straat(final String value)
	{
		this.setObject(value, "./onderpand_straat", String.class);
	}

	public boolean is_Onderpand_straat_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_straat")) != null && 
	        !((String)this.getValueAfter("./onderpand_straat")).equals((this.getValueBefore("./onderpand_straat"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_straat = com.orchestranetworks.schema.Path.parse("./onderpand_straat");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_straat()
	{
		return path_Onderpand_straat;
	}

   /* Get and Set methods for attribute:  
    * onderpand_huisnummer (Standard)
    *
    */

	public String get_Onderpand_huisnummer()
	{
		return this.getString("./onderpand_huisnummer");
	}

	public String get_Onderpand_huisnummer_After()
	{
		return (String)this.getValueAfter("./onderpand_huisnummer");
	}

	public String get_Onderpand_huisnummer_Before()
	{
		return (String)this.getValueBefore("./onderpand_huisnummer");
	}

	public void set_Onderpand_huisnummer(final String value)
	{
		this.setObject(value, "./onderpand_huisnummer", String.class);
	}

	public boolean is_Onderpand_huisnummer_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_huisnummer")) != null && 
	        !((String)this.getValueAfter("./onderpand_huisnummer")).equals((this.getValueBefore("./onderpand_huisnummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_huisnummer = com.orchestranetworks.schema.Path.parse("./onderpand_huisnummer");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_huisnummer()
	{
		return path_Onderpand_huisnummer;
	}

   /* Get and Set methods for attribute:  
    * onderpand_woonplaats (Standard)
    *
    */

	public String get_Onderpand_woonplaats()
	{
		return this.getString("./onderpand_woonplaats");
	}

	public String get_Onderpand_woonplaats_After()
	{
		return (String)this.getValueAfter("./onderpand_woonplaats");
	}

	public String get_Onderpand_woonplaats_Before()
	{
		return (String)this.getValueBefore("./onderpand_woonplaats");
	}

	public void set_Onderpand_woonplaats(final String value)
	{
		this.setObject(value, "./onderpand_woonplaats", String.class);
	}

	public boolean is_Onderpand_woonplaats_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_woonplaats")) != null && 
	        !((String)this.getValueAfter("./onderpand_woonplaats")).equals((this.getValueBefore("./onderpand_woonplaats"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_woonplaats = com.orchestranetworks.schema.Path.parse("./onderpand_woonplaats");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_woonplaats()
	{
		return path_Onderpand_woonplaats;
	}

   /* Get and Set methods for attribute:  
    * onderpand_postcode (Standard)
    *
    */

	public String get_Onderpand_postcode()
	{
		return this.getString("./onderpand_postcode");
	}

	public String get_Onderpand_postcode_After()
	{
		return (String)this.getValueAfter("./onderpand_postcode");
	}

	public String get_Onderpand_postcode_Before()
	{
		return (String)this.getValueBefore("./onderpand_postcode");
	}

	public void set_Onderpand_postcode(final String value)
	{
		this.setObject(value, "./onderpand_postcode", String.class);
	}

	public boolean is_Onderpand_postcode_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_postcode")) != null && 
	        !((String)this.getValueAfter("./onderpand_postcode")).equals((this.getValueBefore("./onderpand_postcode"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_postcode = com.orchestranetworks.schema.Path.parse("./onderpand_postcode");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_postcode()
	{
		return path_Onderpand_postcode;
	}

   /* Get and Set methods for attribute:  
    * onderpand_provincie (Standard)
    *
    */

	public String get_Onderpand_provincie()
	{
		return this.getString("./onderpand_provincie");
	}

	public String get_Onderpand_provincie_After()
	{
		return (String)this.getValueAfter("./onderpand_provincie");
	}

	public String get_Onderpand_provincie_Before()
	{
		return (String)this.getValueBefore("./onderpand_provincie");
	}

	public void set_Onderpand_provincie(final String value)
	{
		this.setObject(value, "./onderpand_provincie", String.class);
	}

	public boolean is_Onderpand_provincie_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_provincie")) != null && 
	        !((String)this.getValueAfter("./onderpand_provincie")).equals((this.getValueBefore("./onderpand_provincie"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_provincie = com.orchestranetworks.schema.Path.parse("./onderpand_provincie");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_provincie()
	{
		return path_Onderpand_provincie;
	}

   /* Get and Set methods for attribute:  
    * onderpand_land (Standard)
    *
    */

	public String get_Onderpand_land()
	{
		return this.getString("./onderpand_land");
	}

	public String get_Onderpand_land_After()
	{
		return (String)this.getValueAfter("./onderpand_land");
	}

	public String get_Onderpand_land_Before()
	{
		return (String)this.getValueBefore("./onderpand_land");
	}

	public void set_Onderpand_land(final String value)
	{
		this.setObject(value, "./onderpand_land", String.class);
	}

	public boolean is_Onderpand_land_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_land")) != null && 
	        !((String)this.getValueAfter("./onderpand_land")).equals((this.getValueBefore("./onderpand_land"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_land = com.orchestranetworks.schema.Path.parse("./onderpand_land");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_land()
	{
		return path_Onderpand_land;
	}

   /* Get and Set methods for attribute:  
    * onderpand_kadastraal_nummer (Standard)
    *
    */

	public String get_Onderpand_kadastraal_nummer()
	{
		return this.getString("./onderpand_kadastraal_nummer");
	}

	public String get_Onderpand_kadastraal_nummer_After()
	{
		return (String)this.getValueAfter("./onderpand_kadastraal_nummer");
	}

	public String get_Onderpand_kadastraal_nummer_Before()
	{
		return (String)this.getValueBefore("./onderpand_kadastraal_nummer");
	}

	public void set_Onderpand_kadastraal_nummer(final String value)
	{
		this.setObject(value, "./onderpand_kadastraal_nummer", String.class);
	}

	public boolean is_Onderpand_kadastraal_nummer_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_kadastraal_nummer")) != null && 
	        !((String)this.getValueAfter("./onderpand_kadastraal_nummer")).equals((this.getValueBefore("./onderpand_kadastraal_nummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_kadastraal_nummer = com.orchestranetworks.schema.Path.parse("./onderpand_kadastraal_nummer");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_kadastraal_nummer()
	{
		return path_Onderpand_kadastraal_nummer;
	}

   /* Get and Set methods for attribute:  
    * onderpand_bouwplannummer (Standard)
    *
    */

	public String get_Onderpand_bouwplannummer()
	{
		return this.getString("./onderpand_bouwplannummer");
	}

	public String get_Onderpand_bouwplannummer_After()
	{
		return (String)this.getValueAfter("./onderpand_bouwplannummer");
	}

	public String get_Onderpand_bouwplannummer_Before()
	{
		return (String)this.getValueBefore("./onderpand_bouwplannummer");
	}

	public void set_Onderpand_bouwplannummer(final String value)
	{
		this.setObject(value, "./onderpand_bouwplannummer", String.class);
	}

	public boolean is_Onderpand_bouwplannummer_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_bouwplannummer")) != null && 
	        !((String)this.getValueAfter("./onderpand_bouwplannummer")).equals((this.getValueBefore("./onderpand_bouwplannummer"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_bouwplannummer = com.orchestranetworks.schema.Path.parse("./onderpand_bouwplannummer");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_bouwplannummer()
	{
		return path_Onderpand_bouwplannummer;
	}

   /* Get and Set methods for attribute:  
    * onderpand_bouwplan (Standard)
    *
    */

	public String get_Onderpand_bouwplan()
	{
		return this.getString("./onderpand_bouwplan");
	}

	public String get_Onderpand_bouwplan_After()
	{
		return (String)this.getValueAfter("./onderpand_bouwplan");
	}

	public String get_Onderpand_bouwplan_Before()
	{
		return (String)this.getValueBefore("./onderpand_bouwplan");
	}

	public void set_Onderpand_bouwplan(final String value)
	{
		this.setObject(value, "./onderpand_bouwplan", String.class);
	}

	public boolean is_Onderpand_bouwplan_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_bouwplan")) != null && 
	        !((String)this.getValueAfter("./onderpand_bouwplan")).equals((this.getValueBefore("./onderpand_bouwplan"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_bouwplan = com.orchestranetworks.schema.Path.parse("./onderpand_bouwplan");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_bouwplan()
	{
		return path_Onderpand_bouwplan;
	}

   /* Get and Set methods for attribute:  
    * bedrag_toetsing_NHG (Standard)
    *
    */

	public String get_Bedrag_toetsing_NHG()
	{
		return this.getString("./bedrag_toetsing_NHG");
	}

	public String get_Bedrag_toetsing_NHG_After()
	{
		return (String)this.getValueAfter("./bedrag_toetsing_NHG");
	}

	public String get_Bedrag_toetsing_NHG_Before()
	{
		return (String)this.getValueBefore("./bedrag_toetsing_NHG");
	}

	public void set_Bedrag_toetsing_NHG(final String value)
	{
		this.setObject(value, "./bedrag_toetsing_NHG", String.class);
	}

	public boolean is_Bedrag_toetsing_NHG_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_toetsing_NHG")) != null && 
	        !((String)this.getValueAfter("./bedrag_toetsing_NHG")).equals((this.getValueBefore("./bedrag_toetsing_NHG"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_toetsing_NHG = com.orchestranetworks.schema.Path.parse("./bedrag_toetsing_NHG");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_toetsing_NHG()
	{
		return path_Bedrag_toetsing_NHG;
	}

   /* Get and Set methods for attribute:  
    * onderpand_status_perfectmelding (Standard)
    *
    */

	public String get_Onderpand_status_perfectmelding()
	{
		return this.getString("./onderpand_status_perfectmelding");
	}

	public String get_Onderpand_status_perfectmelding_After()
	{
		return (String)this.getValueAfter("./onderpand_status_perfectmelding");
	}

	public String get_Onderpand_status_perfectmelding_Before()
	{
		return (String)this.getValueBefore("./onderpand_status_perfectmelding");
	}

	public void set_Onderpand_status_perfectmelding(final String value)
	{
		this.setObject(value, "./onderpand_status_perfectmelding", String.class);
	}

	public boolean is_Onderpand_status_perfectmelding_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_status_perfectmelding")) != null && 
	        !((String)this.getValueAfter("./onderpand_status_perfectmelding")).equals((this.getValueBefore("./onderpand_status_perfectmelding"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_status_perfectmelding = com.orchestranetworks.schema.Path.parse("./onderpand_status_perfectmelding");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_status_perfectmelding()
	{
		return path_Onderpand_status_perfectmelding;
	}

   /* Get and Set methods for attribute:  
    * onderpand_nummer_zekerheid (Standard)
    *
    */

	public String get_Onderpand_nummer_zekerheid()
	{
		return this.getString("./onderpand_nummer_zekerheid");
	}

	public String get_Onderpand_nummer_zekerheid_After()
	{
		return (String)this.getValueAfter("./onderpand_nummer_zekerheid");
	}

	public String get_Onderpand_nummer_zekerheid_Before()
	{
		return (String)this.getValueBefore("./onderpand_nummer_zekerheid");
	}

	public void set_Onderpand_nummer_zekerheid(final String value)
	{
		this.setObject(value, "./onderpand_nummer_zekerheid", String.class);
	}

	public boolean is_Onderpand_nummer_zekerheid_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_nummer_zekerheid")) != null && 
	        !((String)this.getValueAfter("./onderpand_nummer_zekerheid")).equals((this.getValueBefore("./onderpand_nummer_zekerheid"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_nummer_zekerheid = com.orchestranetworks.schema.Path.parse("./onderpand_nummer_zekerheid");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_nummer_zekerheid()
	{
		return path_Onderpand_nummer_zekerheid;
	}

   /* Get and Set methods for attribute:  
    * bedrag_executiewaarde_winkel (Standard)
    *
    */

	public String get_Bedrag_executiewaarde_winkel()
	{
		return this.getString("./bedrag_executiewaarde_winkel");
	}

	public String get_Bedrag_executiewaarde_winkel_After()
	{
		return (String)this.getValueAfter("./bedrag_executiewaarde_winkel");
	}

	public String get_Bedrag_executiewaarde_winkel_Before()
	{
		return (String)this.getValueBefore("./bedrag_executiewaarde_winkel");
	}

	public void set_Bedrag_executiewaarde_winkel(final String value)
	{
		this.setObject(value, "./bedrag_executiewaarde_winkel", String.class);
	}

	public boolean is_Bedrag_executiewaarde_winkel_Changed()
	{
	    return (((String)this.getValueAfter("./bedrag_executiewaarde_winkel")) != null && 
	        !((String)this.getValueAfter("./bedrag_executiewaarde_winkel")).equals((this.getValueBefore("./bedrag_executiewaarde_winkel"))));
	}

	private static final com.orchestranetworks.schema.Path path_Bedrag_executiewaarde_winkel = com.orchestranetworks.schema.Path.parse("./bedrag_executiewaarde_winkel");
	public static final com.orchestranetworks.schema.Path getPath_Bedrag_executiewaarde_winkel()
	{
		return path_Bedrag_executiewaarde_winkel;
	}

   /* Get and Set methods for attribute:  
    * onderpand_indicator_erfpacht (Standard)
    *
    */

	public String get_Onderpand_indicator_erfpacht()
	{
		return this.getString("./onderpand_indicator_erfpacht");
	}

	public String get_Onderpand_indicator_erfpacht_After()
	{
		return (String)this.getValueAfter("./onderpand_indicator_erfpacht");
	}

	public String get_Onderpand_indicator_erfpacht_Before()
	{
		return (String)this.getValueBefore("./onderpand_indicator_erfpacht");
	}

	public void set_Onderpand_indicator_erfpacht(final String value)
	{
		this.setObject(value, "./onderpand_indicator_erfpacht", String.class);
	}

	public boolean is_Onderpand_indicator_erfpacht_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_indicator_erfpacht")) != null && 
	        !((String)this.getValueAfter("./onderpand_indicator_erfpacht")).equals((this.getValueBefore("./onderpand_indicator_erfpacht"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_indicator_erfpacht = com.orchestranetworks.schema.Path.parse("./onderpand_indicator_erfpacht");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_indicator_erfpacht()
	{
		return path_Onderpand_indicator_erfpacht;
	}

   /* Get and Set methods for attribute:  
    * onderpand_type_waardering (Standard)
    *
    */

	public String get_Onderpand_type_waardering()
	{
		return this.getString("./onderpand_type_waardering");
	}

	public String get_Onderpand_type_waardering_After()
	{
		return (String)this.getValueAfter("./onderpand_type_waardering");
	}

	public String get_Onderpand_type_waardering_Before()
	{
		return (String)this.getValueBefore("./onderpand_type_waardering");
	}

	public void set_Onderpand_type_waardering(final String value)
	{
		this.setObject(value, "./onderpand_type_waardering", String.class);
	}

	public boolean is_Onderpand_type_waardering_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_type_waardering")) != null && 
	        !((String)this.getValueAfter("./onderpand_type_waardering")).equals((this.getValueBefore("./onderpand_type_waardering"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_type_waardering = com.orchestranetworks.schema.Path.parse("./onderpand_type_waardering");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_type_waardering()
	{
		return path_Onderpand_type_waardering;
	}

   /* Get and Set methods for attribute:  
    * onderpand_indicator_verhuur (Standard)
    *
    */

	public String get_Onderpand_indicator_verhuur()
	{
		return this.getString("./onderpand_indicator_verhuur");
	}

	public String get_Onderpand_indicator_verhuur_After()
	{
		return (String)this.getValueAfter("./onderpand_indicator_verhuur");
	}

	public String get_Onderpand_indicator_verhuur_Before()
	{
		return (String)this.getValueBefore("./onderpand_indicator_verhuur");
	}

	public void set_Onderpand_indicator_verhuur(final String value)
	{
		this.setObject(value, "./onderpand_indicator_verhuur", String.class);
	}

	public boolean is_Onderpand_indicator_verhuur_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_indicator_verhuur")) != null && 
	        !((String)this.getValueAfter("./onderpand_indicator_verhuur")).equals((this.getValueBefore("./onderpand_indicator_verhuur"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_indicator_verhuur = com.orchestranetworks.schema.Path.parse("./onderpand_indicator_verhuur");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_indicator_verhuur()
	{
		return path_Onderpand_indicator_verhuur;
	}

   /* Get and Set methods for attribute:  
    * valutasoort (Standard)
    *
    */

	public String get_Valutasoort()
	{
		return this.getString("./valutasoort");
	}

	public String get_Valutasoort_After()
	{
		return (String)this.getValueAfter("./valutasoort");
	}

	public String get_Valutasoort_Before()
	{
		return (String)this.getValueBefore("./valutasoort");
	}

	public void set_Valutasoort(final String value)
	{
		this.setObject(value, "./valutasoort", String.class);
	}

	public boolean is_Valutasoort_Changed()
	{
	    return (((String)this.getValueAfter("./valutasoort")) != null && 
	        !((String)this.getValueAfter("./valutasoort")).equals((this.getValueBefore("./valutasoort"))));
	}

	private static final com.orchestranetworks.schema.Path path_Valutasoort = com.orchestranetworks.schema.Path.parse("./valutasoort");
	public static final com.orchestranetworks.schema.Path getPath_Valutasoort()
	{
		return path_Valutasoort;
	}

   /* Get and Set methods for attribute:  
    * onderpand_indicator_overige_bezittingen (Standard)
    *
    */

	public String get_Onderpand_indicator_overige_bezittingen()
	{
		return this.getString("./onderpand_indicator_overige_bezittingen");
	}

	public String get_Onderpand_indicator_overige_bezittingen_After()
	{
		return (String)this.getValueAfter("./onderpand_indicator_overige_bezittingen");
	}

	public String get_Onderpand_indicator_overige_bezittingen_Before()
	{
		return (String)this.getValueBefore("./onderpand_indicator_overige_bezittingen");
	}

	public void set_Onderpand_indicator_overige_bezittingen(final String value)
	{
		this.setObject(value, "./onderpand_indicator_overige_bezittingen", String.class);
	}

	public boolean is_Onderpand_indicator_overige_bezittingen_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_indicator_overige_bezittingen")) != null && 
	        !((String)this.getValueAfter("./onderpand_indicator_overige_bezittingen")).equals((this.getValueBefore("./onderpand_indicator_overige_bezittingen"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_indicator_overige_bezittingen = com.orchestranetworks.schema.Path.parse("./onderpand_indicator_overige_bezittingen");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_indicator_overige_bezittingen()
	{
		return path_Onderpand_indicator_overige_bezittingen;
	}

   /* Get and Set methods for attribute:  
    * onderpand_reken_marktwaarde (Standard)
    *
    */

	public String get_Onderpand_reken_marktwaarde()
	{
		return this.getString("./onderpand_reken_marktwaarde");
	}

	public String get_Onderpand_reken_marktwaarde_After()
	{
		return (String)this.getValueAfter("./onderpand_reken_marktwaarde");
	}

	public String get_Onderpand_reken_marktwaarde_Before()
	{
		return (String)this.getValueBefore("./onderpand_reken_marktwaarde");
	}

	public void set_Onderpand_reken_marktwaarde(final String value)
	{
		this.setObject(value, "./onderpand_reken_marktwaarde", String.class);
	}

	public boolean is_Onderpand_reken_marktwaarde_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_reken_marktwaarde")) != null && 
	        !((String)this.getValueAfter("./onderpand_reken_marktwaarde")).equals((this.getValueBefore("./onderpand_reken_marktwaarde"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_reken_marktwaarde = com.orchestranetworks.schema.Path.parse("./onderpand_reken_marktwaarde");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_reken_marktwaarde()
	{
		return path_Onderpand_reken_marktwaarde;
	}

   /* Get and Set methods for attribute:  
    * onderpand_huisnummer_toevoeging (Standard)
    *
    */

	public String get_Onderpand_huisnummer_toevoeging()
	{
		return this.getString("./onderpand_huisnummer_toevoeging");
	}

	public String get_Onderpand_huisnummer_toevoeging_After()
	{
		return (String)this.getValueAfter("./onderpand_huisnummer_toevoeging");
	}

	public String get_Onderpand_huisnummer_toevoeging_Before()
	{
		return (String)this.getValueBefore("./onderpand_huisnummer_toevoeging");
	}

	public void set_Onderpand_huisnummer_toevoeging(final String value)
	{
		this.setObject(value, "./onderpand_huisnummer_toevoeging", String.class);
	}

	public boolean is_Onderpand_huisnummer_toevoeging_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_huisnummer_toevoeging")) != null && 
	        !((String)this.getValueAfter("./onderpand_huisnummer_toevoeging")).equals((this.getValueBefore("./onderpand_huisnummer_toevoeging"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_huisnummer_toevoeging = com.orchestranetworks.schema.Path.parse("./onderpand_huisnummer_toevoeging");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_huisnummer_toevoeging()
	{
		return path_Onderpand_huisnummer_toevoeging;
	}

   /* Get and Set methods for attribute:  
    * onderpand_indicator_aanvraag (Standard)
    *
    */

	public String get_Onderpand_indicator_aanvraag()
	{
		return this.getString("./onderpand_indicator_aanvraag");
	}

	public String get_Onderpand_indicator_aanvraag_After()
	{
		return (String)this.getValueAfter("./onderpand_indicator_aanvraag");
	}

	public String get_Onderpand_indicator_aanvraag_Before()
	{
		return (String)this.getValueBefore("./onderpand_indicator_aanvraag");
	}

	public void set_Onderpand_indicator_aanvraag(final String value)
	{
		this.setObject(value, "./onderpand_indicator_aanvraag", String.class);
	}

	public boolean is_Onderpand_indicator_aanvraag_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_indicator_aanvraag")) != null && 
	        !((String)this.getValueAfter("./onderpand_indicator_aanvraag")).equals((this.getValueBefore("./onderpand_indicator_aanvraag"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_indicator_aanvraag = com.orchestranetworks.schema.Path.parse("./onderpand_indicator_aanvraag");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_indicator_aanvraag()
	{
		return path_Onderpand_indicator_aanvraag;
	}

   /* Get and Set methods for attribute:  
    * onderpand_betrouwbaarheid_waardebepaling (Standard)
    *
    */

	public String get_Onderpand_betrouwbaarheid_waardebepaling()
	{
		return this.getString("./onderpand_betrouwbaarheid_waardebepaling");
	}

	public String get_Onderpand_betrouwbaarheid_waardebepaling_After()
	{
		return (String)this.getValueAfter("./onderpand_betrouwbaarheid_waardebepaling");
	}

	public String get_Onderpand_betrouwbaarheid_waardebepaling_Before()
	{
		return (String)this.getValueBefore("./onderpand_betrouwbaarheid_waardebepaling");
	}

	public void set_Onderpand_betrouwbaarheid_waardebepaling(final String value)
	{
		this.setObject(value, "./onderpand_betrouwbaarheid_waardebepaling", String.class);
	}

	public boolean is_Onderpand_betrouwbaarheid_waardebepaling_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_betrouwbaarheid_waardebepaling")) != null && 
	        !((String)this.getValueAfter("./onderpand_betrouwbaarheid_waardebepaling")).equals((this.getValueBefore("./onderpand_betrouwbaarheid_waardebepaling"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_betrouwbaarheid_waardebepaling = com.orchestranetworks.schema.Path.parse("./onderpand_betrouwbaarheid_waardebepaling");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_betrouwbaarheid_waardebepaling()
	{
		return path_Onderpand_betrouwbaarheid_waardebepaling;
	}

   /* Get and Set methods for attribute:  
    * onderpand_indicator_verkocht (Standard)
    *
    */

	public String get_Onderpand_indicator_verkocht()
	{
		return this.getString("./onderpand_indicator_verkocht");
	}

	public String get_Onderpand_indicator_verkocht_After()
	{
		return (String)this.getValueAfter("./onderpand_indicator_verkocht");
	}

	public String get_Onderpand_indicator_verkocht_Before()
	{
		return (String)this.getValueBefore("./onderpand_indicator_verkocht");
	}

	public void set_Onderpand_indicator_verkocht(final String value)
	{
		this.setObject(value, "./onderpand_indicator_verkocht", String.class);
	}

	public boolean is_Onderpand_indicator_verkocht_Changed()
	{
	    return (((String)this.getValueAfter("./onderpand_indicator_verkocht")) != null && 
	        !((String)this.getValueAfter("./onderpand_indicator_verkocht")).equals((this.getValueBefore("./onderpand_indicator_verkocht"))));
	}

	private static final com.orchestranetworks.schema.Path path_Onderpand_indicator_verkocht = com.orchestranetworks.schema.Path.parse("./onderpand_indicator_verkocht");
	public static final com.orchestranetworks.schema.Path getPath_Onderpand_indicator_verkocht()
	{
		return path_Onderpand_indicator_verkocht;
	}

   /* Get and Set methods for attribute:  
    * type_verhuur (Standard)
    *
    */

	public String get_Type_verhuur()
	{
		return this.getString("./type_verhuur");
	}

	public String get_Type_verhuur_After()
	{
		return (String)this.getValueAfter("./type_verhuur");
	}

	public String get_Type_verhuur_Before()
	{
		return (String)this.getValueBefore("./type_verhuur");
	}

	public void set_Type_verhuur(final String value)
	{
		this.setObject(value, "./type_verhuur", String.class);
	}

	public boolean is_Type_verhuur_Changed()
	{
	    return (((String)this.getValueAfter("./type_verhuur")) != null && 
	        !((String)this.getValueAfter("./type_verhuur")).equals((this.getValueBefore("./type_verhuur"))));
	}

	private static final com.orchestranetworks.schema.Path path_Type_verhuur = com.orchestranetworks.schema.Path.parse("./type_verhuur");
	public static final com.orchestranetworks.schema.Path getPath_Type_verhuur()
	{
		return path_Type_verhuur;
	}

   /* Get and Set methods for attribute:  
    * reden_verhuur_omschrijving (Standard)
    *
    */

	public String get_Reden_verhuur_omschrijving()
	{
		return this.getString("./reden_verhuur_omschrijving");
	}

	public String get_Reden_verhuur_omschrijving_After()
	{
		return (String)this.getValueAfter("./reden_verhuur_omschrijving");
	}

	public String get_Reden_verhuur_omschrijving_Before()
	{
		return (String)this.getValueBefore("./reden_verhuur_omschrijving");
	}

	public void set_Reden_verhuur_omschrijving(final String value)
	{
		this.setObject(value, "./reden_verhuur_omschrijving", String.class);
	}

	public boolean is_Reden_verhuur_omschrijving_Changed()
	{
	    return (((String)this.getValueAfter("./reden_verhuur_omschrijving")) != null && 
	        !((String)this.getValueAfter("./reden_verhuur_omschrijving")).equals((this.getValueBefore("./reden_verhuur_omschrijving"))));
	}

	private static final com.orchestranetworks.schema.Path path_Reden_verhuur_omschrijving = com.orchestranetworks.schema.Path.parse("./reden_verhuur_omschrijving");
	public static final com.orchestranetworks.schema.Path getPath_Reden_verhuur_omschrijving()
	{
		return path_Reden_verhuur_omschrijving;
	}

   /* Get and Set methods for attribute:  
    * datum_begin_verhuur (Standard)
    *
    */

	public String get_Datum_begin_verhuur()
	{
		return this.getString("./datum_begin_verhuur");
	}

	public String get_Datum_begin_verhuur_After()
	{
		return (String)this.getValueAfter("./datum_begin_verhuur");
	}

	public String get_Datum_begin_verhuur_Before()
	{
		return (String)this.getValueBefore("./datum_begin_verhuur");
	}

	public void set_Datum_begin_verhuur(final String value)
	{
		this.setObject(value, "./datum_begin_verhuur", String.class);
	}

	public boolean is_Datum_begin_verhuur_Changed()
	{
	    return (((String)this.getValueAfter("./datum_begin_verhuur")) != null && 
	        !((String)this.getValueAfter("./datum_begin_verhuur")).equals((this.getValueBefore("./datum_begin_verhuur"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_begin_verhuur = com.orchestranetworks.schema.Path.parse("./datum_begin_verhuur");
	public static final com.orchestranetworks.schema.Path getPath_Datum_begin_verhuur()
	{
		return path_Datum_begin_verhuur;
	}

   /* Get and Set methods for attribute:  
    * datum_einde_verhuur (Standard)
    *
    */

	public String get_Datum_einde_verhuur()
	{
		return this.getString("./datum_einde_verhuur");
	}

	public String get_Datum_einde_verhuur_After()
	{
		return (String)this.getValueAfter("./datum_einde_verhuur");
	}

	public String get_Datum_einde_verhuur_Before()
	{
		return (String)this.getValueBefore("./datum_einde_verhuur");
	}

	public void set_Datum_einde_verhuur(final String value)
	{
		this.setObject(value, "./datum_einde_verhuur", String.class);
	}

	public boolean is_Datum_einde_verhuur_Changed()
	{
	    return (((String)this.getValueAfter("./datum_einde_verhuur")) != null && 
	        !((String)this.getValueAfter("./datum_einde_verhuur")).equals((this.getValueBefore("./datum_einde_verhuur"))));
	}

	private static final com.orchestranetworks.schema.Path path_Datum_einde_verhuur = com.orchestranetworks.schema.Path.parse("./datum_einde_verhuur");
	public static final com.orchestranetworks.schema.Path getPath_Datum_einde_verhuur()
	{
		return path_Datum_einde_verhuur;
	}

   /* Get and Set methods for attribute:  
    * extra_column (Standard)
    *
    */

	public String get_Extra_column()
	{
		return this.getString("./extra_column");
	}

	public String get_Extra_column_After()
	{
		return (String)this.getValueAfter("./extra_column");
	}

	public String get_Extra_column_Before()
	{
		return (String)this.getValueBefore("./extra_column");
	}

	public void set_Extra_column(final String value)
	{
		this.setObject(value, "./extra_column", String.class);
	}

	public boolean is_Extra_column_Changed()
	{
	    return (((String)this.getValueAfter("./extra_column")) != null && 
	        !((String)this.getValueAfter("./extra_column")).equals((this.getValueBefore("./extra_column"))));
	}

	private static final com.orchestranetworks.schema.Path path_Extra_column = com.orchestranetworks.schema.Path.parse("./extra_column");
	public static final com.orchestranetworks.schema.Path getPath_Extra_column()
	{
		return path_Extra_column;
	}

}

