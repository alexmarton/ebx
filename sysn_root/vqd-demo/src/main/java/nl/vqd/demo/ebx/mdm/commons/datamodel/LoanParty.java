package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Loan Party" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class LoanParty extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/LoanParty";

	public LoanParty(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public LoanParty(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public LoanParty(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public LoanParty(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public LoanParty(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public LoanParty(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public LoanParty(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public LoanParty(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Party (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Party_FK()
	{
		String ret = this.getString("./party");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Party_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./party");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Party_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./party");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Party_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./party", String.class);
	}

	public void set_Party(final nl.vqd.demo.ebx.mdm.commons.datamodel.Party value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./party", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Party_Record()
	{
		return super.getFK("./party");
	}

	public boolean is_Party_Changed()
	{
	    return (((String)this.getValueAfter("./party")) != null && 
	        !((String)this.getValueAfter("./party")).equals((this.getValueBefore("./party"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Party get_Party()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./party");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Party(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Party = com.orchestranetworks.schema.Path.parse("./party");
	public static final com.orchestranetworks.schema.Path getPath_Party()
	{
		return path_Party;
	}

   /* Get and Set methods for attribute:  
    * Loan (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_FK()
	{
		String ret = this.getString("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Loan_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./loan", String.class);
	}

	public void set_Loan(final nl.vqd.demo.ebx.mdm.commons.datamodel.Loan value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./loan", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Loan_Record()
	{
		return super.getFK("./loan");
	}

	public boolean is_Loan_Changed()
	{
	    return (((String)this.getValueAfter("./loan")) != null && 
	        !((String)this.getValueAfter("./loan")).equals((this.getValueBefore("./loan"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Loan get_Loan()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./loan");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Loan(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Loan = com.orchestranetworks.schema.Path.parse("./loan");
	public static final com.orchestranetworks.schema.Path getPath_Loan()
	{
		return path_Loan;
	}

}

