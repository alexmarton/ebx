package nl.vqd.demo.ebx.mdm.argenta.service;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.base.text.UserMessage;
import com.onwbp.com.google.gson.JsonArray;
import com.onwbp.com.google.gson.JsonElement;
import com.onwbp.com.google.gson.JsonObject;
import com.onwbp.com.google.gson.JsonParser;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.RecordEntitySelection;
import com.orchestranetworks.userservice.*;
import nl.vqd.demo.ebx.mdm.argenta.service.product.message.Messages;
import nl.vqd.demo.ebx.mdm.commons.datamodel.Contract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class NotarisDataService implements UserService<RecordEntitySelection> {

    private String notarisNaam = null;

    @Override
    public void setupObjectContext(UserServiceSetupObjectContext<RecordEntitySelection> userServiceSetupObjectContext, UserServiceObjectContextBuilder userServiceObjectContextBuilder) {

        Adaptation dataSet = userServiceSetupObjectContext.getEntitySelection().getRecord();
        Contract contract = new Contract(dataSet);
        this.notarisNaam = contract.get_Notaris();
        System.out.println("Contract: " + this.notarisNaam);

    }

    @Override
    public void setupDisplay(UserServiceSetupDisplayContext<RecordEntitySelection> context, UserServiceDisplayConfigurator config) {
        config.setContent(this::writeContent);

        {
            final UserMessage closeMessage = Messages.get("Close");
            final UIButtonSpecNavigation closeButtonSpec = config.newCloseButton();

            closeButtonSpec.setLabel(closeMessage);
            closeButtonSpec.setDefaultButton(true);
            config.setRightButtons(closeButtonSpec);
        }

    }

    @Override
    public void validate(UserServiceValidateContext<RecordEntitySelection> userServiceValidateContext) {

    }

    @Override
    public UserServiceEventOutcome processEventOutcome(UserServiceProcessEventOutcomeContext<RecordEntitySelection> userServiceProcessEventOutcomeContext, UserServiceEventOutcome userServiceEventOutcome) {
        return userServiceEventOutcome;
    }

    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {

        final UIFormLabelSpec labelSpec = new UIFormLabelSpec(UserMessage.createInfo("Notaris Naam: " + this.notarisNaam));
        final UIFormLabelSpec jsonArray = new UIFormLabelSpec(UserMessage.createInfo(drawJsonOnTable(requestService(this.notarisNaam).get(0))));

        writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
        writer.addUILabel(labelSpec);
        writer.add("</div>");

        writer.add("<table><tr><th colspan='2'><u>Details</u></th></tr>");
        writer.addUILabel(jsonArray);
        writer.add("</table>");


    }

    public static JsonArray requestService(String name) {

        //Pre config
        String staticUrl = "https://registernotariaat.nl/registernotariaat/find/" + name;
        JsonArray jsonArray = new JsonArray();

        try {
            URL url = new URL(staticUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoInput(true);
            connection.setDoOutput(true);


            int status = connection.getResponseCode();
            if (status == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                jsonArray = new JsonParser().parse(String.valueOf(content)).getAsJsonArray();
                in.close();
                return jsonArray;
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
        return jsonArray;

    }

    private String drawJsonOnTable(JsonElement element) {
        StringBuilder rowDraw = new StringBuilder();
        JsonObject jsonObject = element.getAsJsonObject();

        ArrayList<String> strings = new ArrayList<>(
                Arrays.asList("titelVoor", "voorletters", "achternaam","geslacht", "geboorteplaats", "geboorteland", "lidId", "debiteurnummerKantoor", "typeLid"));


        for (String s : strings) {
            rowDraw.append("<tr><td><strong>"+s+ "</strong></td><td> "+jsonObject.get(s).getAsString()+"</td></tr>");
        }

        rowDraw.append("<tr><td><strong>datumStart</strong></td><td>"+jsonObject.getAsJsonArray("loopbaanstappen").get(0).getAsJsonObject().get("datumStart").getAsString()+"</td></tr>");
        rowDraw.append("<tr><td><strong>kantoorNaam</strong></td><td>"+jsonObject.getAsJsonArray("loopbaanstappen").get(0).getAsJsonObject().get("kantoorNaam").getAsString()+"</td></tr>");
        rowDraw.append("<tr><td><strong>kantoorVestigingsplaats</strong></td><td>"+jsonObject.getAsJsonArray("loopbaanstappen").get(0).getAsJsonObject().get("kantoorVestigingsplaats").getAsString()+"</td></tr>");

        return rowDraw.toString();
    }


}
