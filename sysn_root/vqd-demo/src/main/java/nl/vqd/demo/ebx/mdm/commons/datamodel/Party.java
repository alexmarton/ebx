package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Party" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class Party extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/party";

	public Party(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Party(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Party(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Party(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Party(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Party(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Party(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Party(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * name (Standard)
    *
    */

	public String get_Name()
	{
		return this.getString("./name");
	}

	public String get_Name_After()
	{
		return (String)this.getValueAfter("./name");
	}

	public String get_Name_Before()
	{
		return (String)this.getValueBefore("./name");
	}

	public void set_Name(final String value)
	{
		this.setObject(value, "./name", String.class);
	}

	public boolean is_Name_Changed()
	{
	    return (((String)this.getValueAfter("./name")) != null && 
	        !((String)this.getValueAfter("./name")).equals((this.getValueBefore("./name"))));
	}

	private static final com.orchestranetworks.schema.Path path_Name = com.orchestranetworks.schema.Path.parse("./name");
	public static final com.orchestranetworks.schema.Path getPath_Name()
	{
		return path_Name;
	}

   /* Get and Set methods for attribute:  
    * partyRole (Standard)
    *
    */

	public String get_PartyRole()
	{
		return this.getString("./partyRole");
	}

	public String get_PartyRole_After()
	{
		return (String)this.getValueAfter("./partyRole");
	}

	public String get_PartyRole_Before()
	{
		return (String)this.getValueBefore("./partyRole");
	}

	public void set_PartyRole(final String value)
	{
		this.setObject(value, "./partyRole", String.class);
	}

	public boolean is_PartyRole_Changed()
	{
	    return (((String)this.getValueAfter("./partyRole")) != null && 
	        !((String)this.getValueAfter("./partyRole")).equals((this.getValueBefore("./partyRole"))));
	}

	private static final com.orchestranetworks.schema.Path path_PartyRole = com.orchestranetworks.schema.Path.parse("./partyRole");
	public static final com.orchestranetworks.schema.Path getPath_PartyRole()
	{
		return path_PartyRole;
	}

   /* Get and Set methods for attribute:  
    * startDatePartyRole (Standard)
    *
    */

	public java.util.Date get_StartDatePartyRole()
	{
		return this.getDate("./startDatePartyRole");
	}

	public java.util.Date get_StartDatePartyRole_After()
	{
		return (java.util.Date)this.getValueAfter("./startDatePartyRole");
	}

	public java.util.Date get_StartDatePartyRole_Before()
	{
		return (java.util.Date)this.getValueBefore("./startDatePartyRole");
	}

	public void set_StartDatePartyRole(final java.util.Date value)
	{
		this.setObject(value, "./startDatePartyRole", java.util.Date.class);
	}

	public boolean is_StartDatePartyRole_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./startDatePartyRole")) != null && 
	        !((java.util.Date)this.getValueAfter("./startDatePartyRole")).equals((this.getValueBefore("./startDatePartyRole"))));
	}

	private static final com.orchestranetworks.schema.Path path_StartDatePartyRole = com.orchestranetworks.schema.Path.parse("./startDatePartyRole");
	public static final com.orchestranetworks.schema.Path getPath_StartDatePartyRole()
	{
		return path_StartDatePartyRole;
	}

   /* Get and Set methods for attribute:  
    * endDataPartyRole (Standard)
    *
    */

	public java.util.Date get_EndDataPartyRole()
	{
		return this.getDate("./endDataPartyRole");
	}

	public java.util.Date get_EndDataPartyRole_After()
	{
		return (java.util.Date)this.getValueAfter("./endDataPartyRole");
	}

	public java.util.Date get_EndDataPartyRole_Before()
	{
		return (java.util.Date)this.getValueBefore("./endDataPartyRole");
	}

	public void set_EndDataPartyRole(final java.util.Date value)
	{
		this.setObject(value, "./endDataPartyRole", java.util.Date.class);
	}

	public boolean is_EndDataPartyRole_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./endDataPartyRole")) != null && 
	        !((java.util.Date)this.getValueAfter("./endDataPartyRole")).equals((this.getValueBefore("./endDataPartyRole"))));
	}

	private static final com.orchestranetworks.schema.Path path_EndDataPartyRole = com.orchestranetworks.schema.Path.parse("./endDataPartyRole");
	public static final com.orchestranetworks.schema.Path getPath_EndDataPartyRole()
	{
		return path_EndDataPartyRole;
	}

   /* Get and Set methods for attribute:  
    * statusActive (Standard boolean)
    *
    */
    
	/**
	 * 
	 * @Deprecated use the is_Status() method instead
	 */
	@Deprecated 
	public Boolean get_Status()
	{
		return this.getBoolean("./status");
	}

	public Boolean get_Status_After()
	{
		return (Boolean)this.getValueAfter("./status");
	}

	public Boolean get_Status_Before()
	{
		return (Boolean)this.getValueBefore("./status");
	}

	public void set_Status(final Boolean value)
	{
		this.setObject(value, "./status", Boolean.class);
	}

	public boolean is_Status() {
		return this.getBoolean("./status");
	}

	public boolean is_Status_Changed()
	{
	    return (((Boolean)this.getValueAfter("./status")) != null && 
	        !((Boolean)this.getValueAfter("./status")).equals((this.getValueBefore("./status"))));
	}

	private static final com.orchestranetworks.schema.Path path_Status = com.orchestranetworks.schema.Path.parse("./status");
	public static final com.orchestranetworks.schema.Path getPath_Status()
	{
		return path_Status;
	}

   /* Get and Set methods for attribute:  
    * financialOwner (Standard)
    *
    */

	public String get_FinancialOwner()
	{
		return this.getString("./financialOwner");
	}

	public String get_FinancialOwner_After()
	{
		return (String)this.getValueAfter("./financialOwner");
	}

	public String get_FinancialOwner_Before()
	{
		return (String)this.getValueBefore("./financialOwner");
	}

	public void set_FinancialOwner(final String value)
	{
		this.setObject(value, "./financialOwner", String.class);
	}

	public boolean is_FinancialOwner_Changed()
	{
	    return (((String)this.getValueAfter("./financialOwner")) != null && 
	        !((String)this.getValueAfter("./financialOwner")).equals((this.getValueBefore("./financialOwner"))));
	}

	private static final com.orchestranetworks.schema.Path path_FinancialOwner = com.orchestranetworks.schema.Path.parse("./financialOwner");
	public static final com.orchestranetworks.schema.Path getPath_FinancialOwner()
	{
		return path_FinancialOwner;
	}

   /* Get and Set methods for attribute:  
    * legalOwner (Standard)
    *
    */

	public String get_LegalOwner()
	{
		return this.getString("./legalOwner");
	}

	public String get_LegalOwner_After()
	{
		return (String)this.getValueAfter("./legalOwner");
	}

	public String get_LegalOwner_Before()
	{
		return (String)this.getValueBefore("./legalOwner");
	}

	public void set_LegalOwner(final String value)
	{
		this.setObject(value, "./legalOwner", String.class);
	}

	public boolean is_LegalOwner_Changed()
	{
	    return (((String)this.getValueAfter("./legalOwner")) != null && 
	        !((String)this.getValueAfter("./legalOwner")).equals((this.getValueBefore("./legalOwner"))));
	}

	private static final com.orchestranetworks.schema.Path path_LegalOwner = com.orchestranetworks.schema.Path.parse("./legalOwner");
	public static final com.orchestranetworks.schema.Path getPath_LegalOwner()
	{
		return path_LegalOwner;
	}

   /* Get and Set methods for attribute:  
    * customerSupportTeam (Standard)
    *
    */

	public String get_CustomerSupportTeam()
	{
		return this.getString("./customerSupportTeam");
	}

	public String get_CustomerSupportTeam_After()
	{
		return (String)this.getValueAfter("./customerSupportTeam");
	}

	public String get_CustomerSupportTeam_Before()
	{
		return (String)this.getValueBefore("./customerSupportTeam");
	}

	public void set_CustomerSupportTeam(final String value)
	{
		this.setObject(value, "./customerSupportTeam", String.class);
	}

	public boolean is_CustomerSupportTeam_Changed()
	{
	    return (((String)this.getValueAfter("./customerSupportTeam")) != null && 
	        !((String)this.getValueAfter("./customerSupportTeam")).equals((this.getValueBefore("./customerSupportTeam"))));
	}

	private static final com.orchestranetworks.schema.Path path_CustomerSupportTeam = com.orchestranetworks.schema.Path.parse("./customerSupportTeam");
	public static final com.orchestranetworks.schema.Path getPath_CustomerSupportTeam()
	{
		return path_CustomerSupportTeam;
	}

   /* Get and Set methods for attribute:  
    * notary (Standard)
    *
    */

	public String get_Notary()
	{
		return this.getString("./notary");
	}

	public String get_Notary_After()
	{
		return (String)this.getValueAfter("./notary");
	}

	public String get_Notary_Before()
	{
		return (String)this.getValueBefore("./notary");
	}

	public void set_Notary(final String value)
	{
		this.setObject(value, "./notary", String.class);
	}

	public boolean is_Notary_Changed()
	{
	    return (((String)this.getValueAfter("./notary")) != null && 
	        !((String)this.getValueAfter("./notary")).equals((this.getValueBefore("./notary"))));
	}

	private static final com.orchestranetworks.schema.Path path_Notary = com.orchestranetworks.schema.Path.parse("./notary");
	public static final com.orchestranetworks.schema.Path getPath_Notary()
	{
		return path_Notary;
	}

   /* Get and Set methods for attribute:  
    * Event (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Event_FK()
	{
		String ret = this.getString("./event");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Event_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./event");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Event_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./event");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Event_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./event", String.class);
	}

	public void set_Event(final nl.vqd.demo.ebx.mdm.commons.datamodel.Event value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./event", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Event_Record()
	{
		return super.getFK("./event");
	}

	public boolean is_Event_Changed()
	{
	    return (((String)this.getValueAfter("./event")) != null && 
	        !((String)this.getValueAfter("./event")).equals((this.getValueBefore("./event"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Event get_Event()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./event");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Event(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Event = com.orchestranetworks.schema.Path.parse("./event");
	public static final com.orchestranetworks.schema.Path getPath_Event()
	{
		return path_Event;
	}

   /* Party (Association)
    *
    */

	public java.util.List<com.onwbp.adaptation.Adaptation> get_Party(){
		return super.getAssociation("./party");
	}

	private static final com.orchestranetworks.schema.Path path_Party = com.orchestranetworks.schema.Path.parse("./party");
	public static final com.orchestranetworks.schema.Path getPath_Party()
	{
		return path_Party;
	}

	private java.util.List<nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty> list_Party = new java.util.ArrayList<>();
	
	public void add_Party(nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty frameworkTable) {
		list_Party.add(frameworkTable);
	}
	
	public void add_Party(java.util.List<nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty> frameworkTableList) {
		list_Party.addAll(frameworkTableList);
	}

	public void clear_Party() {
		list_Party.clear();
	}
	
	public boolean has_Party() {
		return !list_Party.isEmpty();
	}
	
	public java.util.List<nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty> getList_Party(){
		return list_Party;
	}


   /* Get and Set methods for attribute:  
    * street (Standard)
    *
    */

	public String get_Street()
	{
		return this.getString("./address/street");
	}

	public String get_Street_After()
	{
		return (String)this.getValueAfter("./address/street");
	}

	public String get_Street_Before()
	{
		return (String)this.getValueBefore("./address/street");
	}

	public void set_Street(final String value)
	{
		this.setObject(value, "./address/street", String.class);
	}

	public boolean is_Street_Changed()
	{
	    return (((String)this.getValueAfter("./address/street")) != null && 
	        !((String)this.getValueAfter("./address/street")).equals((this.getValueBefore("./address/street"))));
	}

	private static final com.orchestranetworks.schema.Path path_Street = com.orchestranetworks.schema.Path.parse("./address/street");
	public static final com.orchestranetworks.schema.Path getPath_Street()
	{
		return path_Street;
	}

   /* Get and Set methods for attribute:  
    * city (Standard)
    *
    */

	public String get_City()
	{
		return this.getString("./address/city");
	}

	public String get_City_After()
	{
		return (String)this.getValueAfter("./address/city");
	}

	public String get_City_Before()
	{
		return (String)this.getValueBefore("./address/city");
	}

	public void set_City(final String value)
	{
		this.setObject(value, "./address/city", String.class);
	}

	public boolean is_City_Changed()
	{
	    return (((String)this.getValueAfter("./address/city")) != null && 
	        !((String)this.getValueAfter("./address/city")).equals((this.getValueBefore("./address/city"))));
	}

	private static final com.orchestranetworks.schema.Path path_City = com.orchestranetworks.schema.Path.parse("./address/city");
	public static final com.orchestranetworks.schema.Path getPath_City()
	{
		return path_City;
	}

   /* Get and Set methods for attribute:  
    * postCode (Standard)
    *
    */

	public String get_PostCode()
	{
		return this.getString("./address/postCode");
	}

	public String get_PostCode_After()
	{
		return (String)this.getValueAfter("./address/postCode");
	}

	public String get_PostCode_Before()
	{
		return (String)this.getValueBefore("./address/postCode");
	}

	public void set_PostCode(final String value)
	{
		this.setObject(value, "./address/postCode", String.class);
	}

	public boolean is_PostCode_Changed()
	{
	    return (((String)this.getValueAfter("./address/postCode")) != null && 
	        !((String)this.getValueAfter("./address/postCode")).equals((this.getValueBefore("./address/postCode"))));
	}

	private static final com.orchestranetworks.schema.Path path_PostCode = com.orchestranetworks.schema.Path.parse("./address/postCode");
	public static final com.orchestranetworks.schema.Path getPath_PostCode()
	{
		return path_PostCode;
	}

   /* Get and Set methods for attribute:  
    * country (Standard)
    *
    */

	public String get_Country()
	{
		return this.getString("./address/country");
	}

	public String get_Country_After()
	{
		return (String)this.getValueAfter("./address/country");
	}

	public String get_Country_Before()
	{
		return (String)this.getValueBefore("./address/country");
	}

	public void set_Country(final String value)
	{
		this.setObject(value, "./address/country", String.class);
	}

	public boolean is_Country_Changed()
	{
	    return (((String)this.getValueAfter("./address/country")) != null && 
	        !((String)this.getValueAfter("./address/country")).equals((this.getValueBefore("./address/country"))));
	}

	private static final com.orchestranetworks.schema.Path path_Country = com.orchestranetworks.schema.Path.parse("./address/country");
	public static final com.orchestranetworks.schema.Path getPath_Country()
	{
		return path_Country;
	}

   /* Group attribute template:  
    * address (Group)
    *
    */

	private static final com.orchestranetworks.schema.Path pathGroup_Address = com.orchestranetworks.schema.Path.parse("./address");
	public static final com.orchestranetworks.schema.Path getGroupPath_Address()
	{
		return pathGroup_Address;
	}

   /* Association has Childs Code
    *
    */

	@Override
	public boolean hasChilds() {
		if (!list_Party.isEmpty()) {
			return true;
		}
		return false;
	}

   /* Association Create Parent and Child's Code
    *
    */

	@Override
	public com.onwbp.adaptation.Adaptation createParentAndChilds(com.onwbp.adaptation.AdaptationHome dataSpace, com.orchestranetworks.service.Session session, com.ebx5.frameworkrt.util.TransactionParameters transactionParameters, com.orchestranetworks.service.ProcedureContext procedureContext) throws com.orchestranetworks.service.OperationException {
		final CreateChilds procedure = new CreateChilds();
		procedure.parent = this;
		procedure.attributes = this.getAttributes();
		procedure.table = this.getTable();
		procedure.transactionParameters = transactionParameters;
		if (this.getProcedureContext() != null)
		{
			procedure.setProcedureContext(this.getProcedureContext());
			this.setRecord(procedure.createRecord());
		}
		else
		{
			com.orchestranetworks.service.ProgrammaticService ps = com.orchestranetworks.service.ProgrammaticService.createForSession(session, dataSpace);
			com.orchestranetworks.service.ProcedureResult pResult = ps.execute(procedure);
			if (!pResult.hasFailed())
			{
				this.setRecord(procedure.record);
			}
			else
			{
				com.orchestranetworks.service.OperationException oe = pResult.getException();
				if (oe != null) {
					throw oe; 
				}
				throw pResult.getConstraintViolationException();
			}
		}
		return procedure.record;
	}

	private class CreateChilds implements com.orchestranetworks.service.Procedure {
		private com.ebx5.frameworkrt.util.TransactionParameters transactionParameters = new com.ebx5.frameworkrt.util.TransactionParameters(); 
		private java.util.Map<com.orchestranetworks.schema.Path, Object> attributes = null;
		private com.orchestranetworks.service.ProcedureContext procedureContext = null;
		private com.onwbp.adaptation.Adaptation record = null;
		private com.onwbp.adaptation.AdaptationTable table = null;
		private Party parent = null;

		@Override
		public void execute(com.orchestranetworks.service.ProcedureContext context) throws Exception {
			this.record = this.createRecord(context);
		}

		public void setProcedureContext(final com.orchestranetworks.service.ProcedureContext procedureContext)
		{
			this.procedureContext = procedureContext;
		}

		public com.onwbp.adaptation.Adaptation createRecord() throws com.orchestranetworks.service.OperationException
		{
			this.record = this.createRecord(this.procedureContext);
			return this.record;
		}

		private com.onwbp.adaptation.Adaptation createRecord(final com.orchestranetworks.service.ProcedureContext context) throws com.orchestranetworks.service.OperationException
		{
			this.record = context.doCreateOccurrence(prepareCreate(context), this.table);
			
			Party newParty = new Party(this.record);

			for (nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty child : this.parent.list_Party) {
				
				child.set_Party(newParty);
				this.table = child.getTable();
				this.attributes = child.getAttributes();
				
				context.doCreateOccurrence(prepareCreate(context), this.table);
			}
			list_Party.clear();
			return this.record;
		}

		private com.orchestranetworks.service.ValueContextForUpdate prepareCreate(final com.orchestranetworks.service.ProcedureContext context) {
			
			com.orchestranetworks.service.ValueContextForUpdate contextForNewOccurrence = context.getContextForNewOccurrence(this.table);

			if (this.attributes == null)
			{
				return null;
			}

			for (java.util.Map.Entry<com.orchestranetworks.schema.Path, Object> entry : this.attributes.entrySet())
			{
				Object object = entry.getValue();
				com.orchestranetworks.schema.Path path = entry.getKey();
				if (transactionParameters.isAllPrivileges()) {
					contextForNewOccurrence.setPrivilegeForNode(path);
				}
				contextForNewOccurrence.setValue(object, path);
			}
			if (transactionParameters.getExecutionInfo() != null)
			{
				context.setExecutionInformation(transactionParameters.getExecutionInfo());
			}
			context.setCommitThreshold(transactionParameters.getCommitThreshold());
			context.setAllPrivileges(transactionParameters.isAllPrivileges());
			context.setTriggerActivation(transactionParameters.isTriggersActive());
			context.setHistoryActivation(transactionParameters.isHistoryActivated());
			context.setDatabaseHistoryActivation(transactionParameters.isDataBaseHistoryActivated());
			if (transactionParameters.isDisableDataSetValidation()) {
				context.disableDatasetValidation(transactionParameters.getDataSet());
			}
			
			return contextForNewOccurrence;
		}
	}

}

