package nl.vqd.demo.ebx.mdm.argenta.service.product.documentation;

import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import nl.vqd.demo.ebx.mdm.argenta.service.product.message.Messages;


import java.util.Locale;

public class DocumentationConstraint  implements Constraint<Object> {
    @Override
    public void checkOccurrence(Object o, ValueContextForValidation valueContextForValidation) throws InvalidSchemaException {

    }

    @Override
    public void setup(ConstraintContext constraintContext) {

    }

    @Override
    public String toUserDocumentation(Locale locale, ValueContext valueContext) throws InvalidSchemaException {
        final String message = Messages.get(locale, "DocumentationValueForType");
        return message;
    }
}
