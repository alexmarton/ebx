package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "DexDagexportContractenDeellening" record from the data model "Argenta Demo"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Thu Nov 19 14:33:38 CET 2020
*/

public class DexDagexportContractenDeellening extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/DexDagexportContractenDeellening";

	public DexDagexportContractenDeellening(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public DexDagexportContractenDeellening(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public DexDagexportContractenDeellening(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public DexDagexportContractenDeellening(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public DexDagexportContractenDeellening(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public DexDagexportContractenDeellening(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public DexDagexportContractenDeellening(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public DexDagexportContractenDeellening(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Funder (Standard)
    *
    */

	public String get_Funder()
	{
		return this.getString("./funder");
	}

	public String get_Funder_After()
	{
		return (String)this.getValueAfter("./funder");
	}

	public String get_Funder_Before()
	{
		return (String)this.getValueBefore("./funder");
	}

	public void set_Funder(final String value)
	{
		this.setObject(value, "./funder", String.class);
	}

	public boolean is_Funder_Changed()
	{
	    return (((String)this.getValueAfter("./funder")) != null && 
	        !((String)this.getValueAfter("./funder")).equals((this.getValueBefore("./funder"))));
	}

	private static final com.orchestranetworks.schema.Path path_Funder = com.orchestranetworks.schema.Path.parse("./funder");
	public static final com.orchestranetworks.schema.Path getPath_Funder()
	{
		return path_Funder;
	}

   /* Get and Set methods for attribute:  
    * Depersonaliseren (Standard)
    *
    */

	public String get_Depersonaliseren()
	{
		return this.getString("./depersonaliseren");
	}

	public String get_Depersonaliseren_After()
	{
		return (String)this.getValueAfter("./depersonaliseren");
	}

	public String get_Depersonaliseren_Before()
	{
		return (String)this.getValueBefore("./depersonaliseren");
	}

	public void set_Depersonaliseren(final String value)
	{
		this.setObject(value, "./depersonaliseren", String.class);
	}

	public boolean is_Depersonaliseren_Changed()
	{
	    return (((String)this.getValueAfter("./depersonaliseren")) != null && 
	        !((String)this.getValueAfter("./depersonaliseren")).equals((this.getValueBefore("./depersonaliseren"))));
	}

	private static final com.orchestranetworks.schema.Path path_Depersonaliseren = com.orchestranetworks.schema.Path.parse("./depersonaliseren");
	public static final com.orchestranetworks.schema.Path getPath_Depersonaliseren()
	{
		return path_Depersonaliseren;
	}

   /* Get and Set methods for attribute:  
    * Column Name (Standard)
    *
    */

	public String get_ColumnName()
	{
		return this.getString("./columnName");
	}

	public String get_ColumnName_After()
	{
		return (String)this.getValueAfter("./columnName");
	}

	public String get_ColumnName_Before()
	{
		return (String)this.getValueBefore("./columnName");
	}

	public void set_ColumnName(final String value)
	{
		this.setObject(value, "./columnName", String.class);
	}

	public boolean is_ColumnName_Changed()
	{
	    return (((String)this.getValueAfter("./columnName")) != null && 
	        !((String)this.getValueAfter("./columnName")).equals((this.getValueBefore("./columnName"))));
	}

	private static final com.orchestranetworks.schema.Path path_ColumnName = com.orchestranetworks.schema.Path.parse("./columnName");
	public static final com.orchestranetworks.schema.Path getPath_ColumnName()
	{
		return path_ColumnName;
	}

   /* Get and Set methods for attribute:  
    * Active (Standard)
    *
    */

	public String get_Active()
	{
		return this.getString("./active");
	}

	public String get_Active_After()
	{
		return (String)this.getValueAfter("./active");
	}

	public String get_Active_Before()
	{
		return (String)this.getValueBefore("./active");
	}

	public void set_Active(final String value)
	{
		this.setObject(value, "./active", String.class);
	}

	public boolean is_Active_Changed()
	{
	    return (((String)this.getValueAfter("./active")) != null && 
	        !((String)this.getValueAfter("./active")).equals((this.getValueBefore("./active"))));
	}

	private static final com.orchestranetworks.schema.Path path_Active = com.orchestranetworks.schema.Path.parse("./active");
	public static final com.orchestranetworks.schema.Path getPath_Active()
	{
		return path_Active;
	}

   /* Get and Set methods for attribute:  
    * Description (Dutch) (Standard)
    *
    */

	public String get_DescriptionDutch()
	{
		return this.getString("./descriptionDutch");
	}

	public String get_DescriptionDutch_After()
	{
		return (String)this.getValueAfter("./descriptionDutch");
	}

	public String get_DescriptionDutch_Before()
	{
		return (String)this.getValueBefore("./descriptionDutch");
	}

	public void set_DescriptionDutch(final String value)
	{
		this.setObject(value, "./descriptionDutch", String.class);
	}

	public boolean is_DescriptionDutch_Changed()
	{
	    return (((String)this.getValueAfter("./descriptionDutch")) != null && 
	        !((String)this.getValueAfter("./descriptionDutch")).equals((this.getValueBefore("./descriptionDutch"))));
	}

	private static final com.orchestranetworks.schema.Path path_DescriptionDutch = com.orchestranetworks.schema.Path.parse("./descriptionDutch");
	public static final com.orchestranetworks.schema.Path getPath_DescriptionDutch()
	{
		return path_DescriptionDutch;
	}

   /* Get and Set methods for attribute:  
    * Description (English) (Standard)
    *
    */

	public String get_DescriptionEnglish()
	{
		return this.getString("./descriptionEnglish");
	}

	public String get_DescriptionEnglish_After()
	{
		return (String)this.getValueAfter("./descriptionEnglish");
	}

	public String get_DescriptionEnglish_Before()
	{
		return (String)this.getValueBefore("./descriptionEnglish");
	}

	public void set_DescriptionEnglish(final String value)
	{
		this.setObject(value, "./descriptionEnglish", String.class);
	}

	public boolean is_DescriptionEnglish_Changed()
	{
	    return (((String)this.getValueAfter("./descriptionEnglish")) != null && 
	        !((String)this.getValueAfter("./descriptionEnglish")).equals((this.getValueBefore("./descriptionEnglish"))));
	}

	private static final com.orchestranetworks.schema.Path path_DescriptionEnglish = com.orchestranetworks.schema.Path.parse("./descriptionEnglish");
	public static final com.orchestranetworks.schema.Path getPath_DescriptionEnglish()
	{
		return path_DescriptionEnglish;
	}

   /* Get and Set methods for attribute:  
    * Example Values (Standard)
    *
    */

	public String get_ExampleValues()
	{
		return this.getString("./exampleValues");
	}

	public String get_ExampleValues_After()
	{
		return (String)this.getValueAfter("./exampleValues");
	}

	public String get_ExampleValues_Before()
	{
		return (String)this.getValueBefore("./exampleValues");
	}

	public void set_ExampleValues(final String value)
	{
		this.setObject(value, "./exampleValues", String.class);
	}

	public boolean is_ExampleValues_Changed()
	{
	    return (((String)this.getValueAfter("./exampleValues")) != null && 
	        !((String)this.getValueAfter("./exampleValues")).equals((this.getValueBefore("./exampleValues"))));
	}

	private static final com.orchestranetworks.schema.Path path_ExampleValues = com.orchestranetworks.schema.Path.parse("./exampleValues");
	public static final com.orchestranetworks.schema.Path getPath_ExampleValues()
	{
		return path_ExampleValues;
	}

   /* Get and Set methods for attribute:  
    * Waarde in bestand (Standard)
    *
    */

	public String get_WaardeInBestand()
	{
		return this.getString("./waardeInBestand");
	}

	public String get_WaardeInBestand_After()
	{
		return (String)this.getValueAfter("./waardeInBestand");
	}

	public String get_WaardeInBestand_Before()
	{
		return (String)this.getValueBefore("./waardeInBestand");
	}

	public void set_WaardeInBestand(final String value)
	{
		this.setObject(value, "./waardeInBestand", String.class);
	}

	public boolean is_WaardeInBestand_Changed()
	{
	    return (((String)this.getValueAfter("./waardeInBestand")) != null && 
	        !((String)this.getValueAfter("./waardeInBestand")).equals((this.getValueBefore("./waardeInBestand"))));
	}

	private static final com.orchestranetworks.schema.Path path_WaardeInBestand = com.orchestranetworks.schema.Path.parse("./waardeInBestand");
	public static final com.orchestranetworks.schema.Path getPath_WaardeInBestand()
	{
		return path_WaardeInBestand;
	}

   /* Get and Set methods for attribute:  
    * Opmerkingen (Standard)
    *
    */

	public String get_Opmerkingen()
	{
		return this.getString("./opmerkingen");
	}

	public String get_Opmerkingen_After()
	{
		return (String)this.getValueAfter("./opmerkingen");
	}

	public String get_Opmerkingen_Before()
	{
		return (String)this.getValueBefore("./opmerkingen");
	}

	public void set_Opmerkingen(final String value)
	{
		this.setObject(value, "./opmerkingen", String.class);
	}

	public boolean is_Opmerkingen_Changed()
	{
	    return (((String)this.getValueAfter("./opmerkingen")) != null && 
	        !((String)this.getValueAfter("./opmerkingen")).equals((this.getValueBefore("./opmerkingen"))));
	}

	private static final com.orchestranetworks.schema.Path path_Opmerkingen = com.orchestranetworks.schema.Path.parse("./opmerkingen");
	public static final com.orchestranetworks.schema.Path getPath_Opmerkingen()
	{
		return path_Opmerkingen;
	}

}

