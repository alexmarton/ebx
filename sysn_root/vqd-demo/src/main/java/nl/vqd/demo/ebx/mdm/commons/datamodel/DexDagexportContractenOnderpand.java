package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "DexDagexportContractenOnderpand" record from the data model "Argenta Demo"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Thu Nov 19 14:33:38 CET 2020
*/

public class DexDagexportContractenOnderpand extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/DexDagexportContractenOnderpand";

	public DexDagexportContractenOnderpand(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public DexDagexportContractenOnderpand(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public DexDagexportContractenOnderpand(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public DexDagexportContractenOnderpand(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public DexDagexportContractenOnderpand(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public DexDagexportContractenOnderpand(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public DexDagexportContractenOnderpand(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public DexDagexportContractenOnderpand(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Funder (Standard)
    *
    */

	public String get_Funder()
	{
		return this.getString("./Funder");
	}

	public String get_Funder_After()
	{
		return (String)this.getValueAfter("./Funder");
	}

	public String get_Funder_Before()
	{
		return (String)this.getValueBefore("./Funder");
	}

	public void set_Funder(final String value)
	{
		this.setObject(value, "./Funder", String.class);
	}

	public boolean is_Funder_Changed()
	{
	    return (((String)this.getValueAfter("./Funder")) != null && 
	        !((String)this.getValueAfter("./Funder")).equals((this.getValueBefore("./Funder"))));
	}

	private static final com.orchestranetworks.schema.Path path_Funder = com.orchestranetworks.schema.Path.parse("./Funder");
	public static final com.orchestranetworks.schema.Path getPath_Funder()
	{
		return path_Funder;
	}

   /* Get and Set methods for attribute:  
    * Depersonaliseren (Standard)
    *
    */

	public String get_Depersonaliseren()
	{
		return this.getString("./Depersonaliseren");
	}

	public String get_Depersonaliseren_After()
	{
		return (String)this.getValueAfter("./Depersonaliseren");
	}

	public String get_Depersonaliseren_Before()
	{
		return (String)this.getValueBefore("./Depersonaliseren");
	}

	public void set_Depersonaliseren(final String value)
	{
		this.setObject(value, "./Depersonaliseren", String.class);
	}

	public boolean is_Depersonaliseren_Changed()
	{
	    return (((String)this.getValueAfter("./Depersonaliseren")) != null && 
	        !((String)this.getValueAfter("./Depersonaliseren")).equals((this.getValueBefore("./Depersonaliseren"))));
	}

	private static final com.orchestranetworks.schema.Path path_Depersonaliseren = com.orchestranetworks.schema.Path.parse("./Depersonaliseren");
	public static final com.orchestranetworks.schema.Path getPath_Depersonaliseren()
	{
		return path_Depersonaliseren;
	}

   /* Get and Set methods for attribute:  
    * Column Name (Standard)
    *
    */

	public String get_ColumnName()
	{
		return this.getString("./ColumnName");
	}

	public String get_ColumnName_After()
	{
		return (String)this.getValueAfter("./ColumnName");
	}

	public String get_ColumnName_Before()
	{
		return (String)this.getValueBefore("./ColumnName");
	}

	public void set_ColumnName(final String value)
	{
		this.setObject(value, "./ColumnName", String.class);
	}

	public boolean is_ColumnName_Changed()
	{
	    return (((String)this.getValueAfter("./ColumnName")) != null && 
	        !((String)this.getValueAfter("./ColumnName")).equals((this.getValueBefore("./ColumnName"))));
	}

	private static final com.orchestranetworks.schema.Path path_ColumnName = com.orchestranetworks.schema.Path.parse("./ColumnName");
	public static final com.orchestranetworks.schema.Path getPath_ColumnName()
	{
		return path_ColumnName;
	}

   /* Get and Set methods for attribute:  
    * Active (Standard)
    *
    */

	public String get_Active()
	{
		return this.getString("./active");
	}

	public String get_Active_After()
	{
		return (String)this.getValueAfter("./active");
	}

	public String get_Active_Before()
	{
		return (String)this.getValueBefore("./active");
	}

	public void set_Active(final String value)
	{
		this.setObject(value, "./active", String.class);
	}

	public boolean is_Active_Changed()
	{
	    return (((String)this.getValueAfter("./active")) != null && 
	        !((String)this.getValueAfter("./active")).equals((this.getValueBefore("./active"))));
	}

	private static final com.orchestranetworks.schema.Path path_Active = com.orchestranetworks.schema.Path.parse("./active");
	public static final com.orchestranetworks.schema.Path getPath_Active()
	{
		return path_Active;
	}

   /* Get and Set methods for attribute:  
    * Omschrijving (Nederlands) (Standard)
    *
    */

	public String get_DescriptionDutch()
	{
		return this.getString("./DescriptionDutch");
	}

	public String get_DescriptionDutch_After()
	{
		return (String)this.getValueAfter("./DescriptionDutch");
	}

	public String get_DescriptionDutch_Before()
	{
		return (String)this.getValueBefore("./DescriptionDutch");
	}

	public void set_DescriptionDutch(final String value)
	{
		this.setObject(value, "./DescriptionDutch", String.class);
	}

	public boolean is_DescriptionDutch_Changed()
	{
	    return (((String)this.getValueAfter("./DescriptionDutch")) != null && 
	        !((String)this.getValueAfter("./DescriptionDutch")).equals((this.getValueBefore("./DescriptionDutch"))));
	}

	private static final com.orchestranetworks.schema.Path path_DescriptionDutch = com.orchestranetworks.schema.Path.parse("./DescriptionDutch");
	public static final com.orchestranetworks.schema.Path getPath_DescriptionDutch()
	{
		return path_DescriptionDutch;
	}

   /* Get and Set methods for attribute:  
    * Description (English) (Standard)
    *
    */

	public String get_DescriptionEnglish()
	{
		return this.getString("./DescriptionEnglish");
	}

	public String get_DescriptionEnglish_After()
	{
		return (String)this.getValueAfter("./DescriptionEnglish");
	}

	public String get_DescriptionEnglish_Before()
	{
		return (String)this.getValueBefore("./DescriptionEnglish");
	}

	public void set_DescriptionEnglish(final String value)
	{
		this.setObject(value, "./DescriptionEnglish", String.class);
	}

	public boolean is_DescriptionEnglish_Changed()
	{
	    return (((String)this.getValueAfter("./DescriptionEnglish")) != null && 
	        !((String)this.getValueAfter("./DescriptionEnglish")).equals((this.getValueBefore("./DescriptionEnglish"))));
	}

	private static final com.orchestranetworks.schema.Path path_DescriptionEnglish = com.orchestranetworks.schema.Path.parse("./DescriptionEnglish");
	public static final com.orchestranetworks.schema.Path getPath_DescriptionEnglish()
	{
		return path_DescriptionEnglish;
	}

   /* Get and Set methods for attribute:  
    * Example Values (Standard)
    *
    */

	public String get_ExampleValues()
	{
		return this.getString("./exampleValues");
	}

	public String get_ExampleValues_After()
	{
		return (String)this.getValueAfter("./exampleValues");
	}

	public String get_ExampleValues_Before()
	{
		return (String)this.getValueBefore("./exampleValues");
	}

	public void set_ExampleValues(final String value)
	{
		this.setObject(value, "./exampleValues", String.class);
	}

	public boolean is_ExampleValues_Changed()
	{
	    return (((String)this.getValueAfter("./exampleValues")) != null && 
	        !((String)this.getValueAfter("./exampleValues")).equals((this.getValueBefore("./exampleValues"))));
	}

	private static final com.orchestranetworks.schema.Path path_ExampleValues = com.orchestranetworks.schema.Path.parse("./exampleValues");
	public static final com.orchestranetworks.schema.Path getPath_ExampleValues()
	{
		return path_ExampleValues;
	}

   /* Get and Set methods for attribute:  
    * Voorbeeld data (Standard)
    *
    */

	public String get_VoorbeeldData()
	{
		return this.getString("./voorbeeldData");
	}

	public String get_VoorbeeldData_After()
	{
		return (String)this.getValueAfter("./voorbeeldData");
	}

	public String get_VoorbeeldData_Before()
	{
		return (String)this.getValueBefore("./voorbeeldData");
	}

	public void set_VoorbeeldData(final String value)
	{
		this.setObject(value, "./voorbeeldData", String.class);
	}

	public boolean is_VoorbeeldData_Changed()
	{
	    return (((String)this.getValueAfter("./voorbeeldData")) != null && 
	        !((String)this.getValueAfter("./voorbeeldData")).equals((this.getValueBefore("./voorbeeldData"))));
	}

	private static final com.orchestranetworks.schema.Path path_VoorbeeldData = com.orchestranetworks.schema.Path.parse("./voorbeeldData");
	public static final com.orchestranetworks.schema.Path getPath_VoorbeeldData()
	{
		return path_VoorbeeldData;
	}

   /* Get and Set methods for attribute:  
    * Opmerking (Standard)
    *
    */

	public String get_Opmerking()
	{
		return this.getString("./Opmerking");
	}

	public String get_Opmerking_After()
	{
		return (String)this.getValueAfter("./Opmerking");
	}

	public String get_Opmerking_Before()
	{
		return (String)this.getValueBefore("./Opmerking");
	}

	public void set_Opmerking(final String value)
	{
		this.setObject(value, "./Opmerking", String.class);
	}

	public boolean is_Opmerking_Changed()
	{
	    return (((String)this.getValueAfter("./Opmerking")) != null && 
	        !((String)this.getValueAfter("./Opmerking")).equals((this.getValueBefore("./Opmerking"))));
	}

	private static final com.orchestranetworks.schema.Path path_Opmerking = com.orchestranetworks.schema.Path.parse("./Opmerking");
	public static final com.orchestranetworks.schema.Path getPath_Opmerking()
	{
		return path_Opmerking;
	}

}

