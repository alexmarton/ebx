package nl.vqd.demo.ebx.mdm.argenta.service.product.display;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.UIButtonSpec;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;
import nl.vqd.demo.ebx.mdm.argenta.service.product.message.Messages;
import nl.vqd.demo.ebx.mdm.argenta.service.product.filter.FilterPaths;


public class DisplayFilterStep implements DisplayStep {

    @Override
    public void setupDisplay(final UserServiceSetupDisplayContext<TableViewEntitySelection> context,
                             final UserServiceDisplayConfigurator config) {
        config.setContent(this::writeContent);

        {
            final UserMessage cancelMessage = Messages.get("Cancel");
            final UIButtonSpec cancelButtonSpec = config.newCancelButton();
            cancelButtonSpec.setLabel(cancelMessage);
            config.setLeftButtons(cancelButtonSpec);
        }

        {
            final UserMessage nextMessage = Messages.get("Next");
            final UIButtonSpecNavigation nextButtonSpec = config.newNextButton(this::onNextPressed);

            nextButtonSpec.setLabel(nextMessage);
            nextButtonSpec.setDefaultButton(true);
            config.setRightButtons(nextButtonSpec);
        }
    }

    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
        writer.setCurrentObject(FilterPaths._objectKey);

        {
            final UserMessage message = Messages.get("TheRowsMatchingTheseCriteriaWillBeUpdate");
            final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

            writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
            writer.addUILabel(labelSpec);
            writer.add("</div>");
        }

        writer.startTableFormRow();
        writer.addFormRow(FilterPaths._startDate);
        writer.addFormRow(FilterPaths._endDate);
        writer.endTableFormRow();
    }

    private UserServiceEventOutcome onNextPressed(final UserServiceEventContext context) {
        return EventOutcome.DISPLAY_CONFIRMATION;
    }
}
