package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Loan" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class Loan extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/loan";

	public Loan(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Loan(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Loan(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Loan(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Loan(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Loan(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Loan(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Loan(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * id (Standard)
    *
    */

	public Integer get_Id()
	{
		return this.getInteger("./id");
	}

	public Integer get_Id_After()
	{
		return (Integer)this.getValueAfter("./id");
	}

	public Integer get_Id_Before()
	{
		return (Integer)this.getValueBefore("./id");
	}

	public void set_Id(final Integer value)
	{
		this.setObject(value, "./id", Integer.class);
	}

	public boolean is_Id_Changed()
	{
	    return (((Integer)this.getValueAfter("./id")) != null && 
	        !((Integer)this.getValueAfter("./id")).equals((this.getValueBefore("./id"))));
	}

	private static final com.orchestranetworks.schema.Path path_Id = com.orchestranetworks.schema.Path.parse("./id");
	public static final com.orchestranetworks.schema.Path getPath_Id()
	{
		return path_Id;
	}

   /* Get and Set methods for attribute:  
    * loanNumber (Standard)
    *
    */

	public Integer get_LoanNumber()
	{
		return this.getInteger("./loanNumber");
	}

	public Integer get_LoanNumber_After()
	{
		return (Integer)this.getValueAfter("./loanNumber");
	}

	public Integer get_LoanNumber_Before()
	{
		return (Integer)this.getValueBefore("./loanNumber");
	}

	public void set_LoanNumber(final Integer value)
	{
		this.setObject(value, "./loanNumber", Integer.class);
	}

	public boolean is_LoanNumber_Changed()
	{
	    return (((Integer)this.getValueAfter("./loanNumber")) != null && 
	        !((Integer)this.getValueAfter("./loanNumber")).equals((this.getValueBefore("./loanNumber"))));
	}

	private static final com.orchestranetworks.schema.Path path_LoanNumber = com.orchestranetworks.schema.Path.parse("./loanNumber");
	public static final com.orchestranetworks.schema.Path getPath_LoanNumber()
	{
		return path_LoanNumber;
	}

   /* Get and Set methods for attribute:  
    * lastQuionDate (Standard)
    *
    */

	public java.util.Date get_LastQuionDate()
	{
		return this.getDate("./lastQuionDate");
	}

	public java.util.Date get_LastQuionDate_After()
	{
		return (java.util.Date)this.getValueAfter("./lastQuionDate");
	}

	public java.util.Date get_LastQuionDate_Before()
	{
		return (java.util.Date)this.getValueBefore("./lastQuionDate");
	}

	public void set_LastQuionDate(final java.util.Date value)
	{
		this.setObject(value, "./lastQuionDate", java.util.Date.class);
	}

	public boolean is_LastQuionDate_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./lastQuionDate")) != null && 
	        !((java.util.Date)this.getValueAfter("./lastQuionDate")).equals((this.getValueBefore("./lastQuionDate"))));
	}

	private static final com.orchestranetworks.schema.Path path_LastQuionDate = com.orchestranetworks.schema.Path.parse("./lastQuionDate");
	public static final com.orchestranetworks.schema.Path getPath_LastQuionDate()
	{
		return path_LastQuionDate;
	}

   /* Get and Set methods for attribute:  
    * notaryPassingDate (Standard)
    *
    */

	public java.util.Date get_NotaryPassingDate()
	{
		return this.getDate("./notaryPassingDate");
	}

	public java.util.Date get_NotaryPassingDate_After()
	{
		return (java.util.Date)this.getValueAfter("./notaryPassingDate");
	}

	public java.util.Date get_NotaryPassingDate_Before()
	{
		return (java.util.Date)this.getValueBefore("./notaryPassingDate");
	}

	public void set_NotaryPassingDate(final java.util.Date value)
	{
		this.setObject(value, "./notaryPassingDate", java.util.Date.class);
	}

	public boolean is_NotaryPassingDate_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./notaryPassingDate")) != null && 
	        !((java.util.Date)this.getValueAfter("./notaryPassingDate")).equals((this.getValueBefore("./notaryPassingDate"))));
	}

	private static final com.orchestranetworks.schema.Path path_NotaryPassingDate = com.orchestranetworks.schema.Path.parse("./notaryPassingDate");
	public static final com.orchestranetworks.schema.Path getPath_NotaryPassingDate()
	{
		return path_NotaryPassingDate;
	}

   /* Get and Set methods for attribute:  
    * proformaRedemptionDate (Standard)
    *
    */

	public java.util.Date get_ProformaRedemptionDate()
	{
		return this.getDate("./proformaRedemptionDate");
	}

	public java.util.Date get_ProformaRedemptionDate_After()
	{
		return (java.util.Date)this.getValueAfter("./proformaRedemptionDate");
	}

	public java.util.Date get_ProformaRedemptionDate_Before()
	{
		return (java.util.Date)this.getValueBefore("./proformaRedemptionDate");
	}

	public void set_ProformaRedemptionDate(final java.util.Date value)
	{
		this.setObject(value, "./proformaRedemptionDate", java.util.Date.class);
	}

	public boolean is_ProformaRedemptionDate_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./proformaRedemptionDate")) != null && 
	        !((java.util.Date)this.getValueAfter("./proformaRedemptionDate")).equals((this.getValueBefore("./proformaRedemptionDate"))));
	}

	private static final com.orchestranetworks.schema.Path path_ProformaRedemptionDate = com.orchestranetworks.schema.Path.parse("./proformaRedemptionDate");
	public static final com.orchestranetworks.schema.Path getPath_ProformaRedemptionDate()
	{
		return path_ProformaRedemptionDate;
	}

   /* Get and Set methods for attribute:  
    * actualRedemptionDate (Standard)
    *
    */

	public java.util.Date get_ActualRedemptionDate()
	{
		return this.getDate("./actualRedemptionDate");
	}

	public java.util.Date get_ActualRedemptionDate_After()
	{
		return (java.util.Date)this.getValueAfter("./actualRedemptionDate");
	}

	public java.util.Date get_ActualRedemptionDate_Before()
	{
		return (java.util.Date)this.getValueBefore("./actualRedemptionDate");
	}

	public void set_ActualRedemptionDate(final java.util.Date value)
	{
		this.setObject(value, "./actualRedemptionDate", java.util.Date.class);
	}

	public boolean is_ActualRedemptionDate_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./actualRedemptionDate")) != null && 
	        !((java.util.Date)this.getValueAfter("./actualRedemptionDate")).equals((this.getValueBefore("./actualRedemptionDate"))));
	}

	private static final com.orchestranetworks.schema.Path path_ActualRedemptionDate = com.orchestranetworks.schema.Path.parse("./actualRedemptionDate");
	public static final com.orchestranetworks.schema.Path getPath_ActualRedemptionDate()
	{
		return path_ActualRedemptionDate;
	}

   /* Get and Set methods for attribute:  
    * expectedRedemptionDate (Standard)
    *
    */

	public java.util.Date get_ExpectedRedemptionDate()
	{
		return this.getDate("./expectedRedemptionDate");
	}

	public java.util.Date get_ExpectedRedemptionDate_After()
	{
		return (java.util.Date)this.getValueAfter("./expectedRedemptionDate");
	}

	public java.util.Date get_ExpectedRedemptionDate_Before()
	{
		return (java.util.Date)this.getValueBefore("./expectedRedemptionDate");
	}

	public void set_ExpectedRedemptionDate(final java.util.Date value)
	{
		this.setObject(value, "./expectedRedemptionDate", java.util.Date.class);
	}

	public boolean is_ExpectedRedemptionDate_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./expectedRedemptionDate")) != null && 
	        !((java.util.Date)this.getValueAfter("./expectedRedemptionDate")).equals((this.getValueBefore("./expectedRedemptionDate"))));
	}

	private static final com.orchestranetworks.schema.Path path_ExpectedRedemptionDate = com.orchestranetworks.schema.Path.parse("./expectedRedemptionDate");
	public static final com.orchestranetworks.schema.Path getPath_ExpectedRedemptionDate()
	{
		return path_ExpectedRedemptionDate;
	}

   /* Get and Set methods for attribute:  
    * completeRedemptionReason (Standard)
    *
    */

	public String get_CompleteRedemptionReason()
	{
		return this.getString("./completeRedemptionReason");
	}

	public String get_CompleteRedemptionReason_After()
	{
		return (String)this.getValueAfter("./completeRedemptionReason");
	}

	public String get_CompleteRedemptionReason_Before()
	{
		return (String)this.getValueBefore("./completeRedemptionReason");
	}

	public void set_CompleteRedemptionReason(final String value)
	{
		this.setObject(value, "./completeRedemptionReason", String.class);
	}

	public boolean is_CompleteRedemptionReason_Changed()
	{
	    return (((String)this.getValueAfter("./completeRedemptionReason")) != null && 
	        !((String)this.getValueAfter("./completeRedemptionReason")).equals((this.getValueBefore("./completeRedemptionReason"))));
	}

	private static final com.orchestranetworks.schema.Path path_CompleteRedemptionReason = com.orchestranetworks.schema.Path.parse("./completeRedemptionReason");
	public static final com.orchestranetworks.schema.Path getPath_CompleteRedemptionReason()
	{
		return path_CompleteRedemptionReason;
	}

   /* Get and Set methods for attribute:  
    * loanReason (Standard)
    *
    */

	public String get_LoanReason()
	{
		return this.getString("./loanReason");
	}

	public String get_LoanReason_After()
	{
		return (String)this.getValueAfter("./loanReason");
	}

	public String get_LoanReason_Before()
	{
		return (String)this.getValueBefore("./loanReason");
	}

	public void set_LoanReason(final String value)
	{
		this.setObject(value, "./loanReason", String.class);
	}

	public boolean is_LoanReason_Changed()
	{
	    return (((String)this.getValueAfter("./loanReason")) != null && 
	        !((String)this.getValueAfter("./loanReason")).equals((this.getValueBefore("./loanReason"))));
	}

	private static final com.orchestranetworks.schema.Path path_LoanReason = com.orchestranetworks.schema.Path.parse("./loanReason");
	public static final com.orchestranetworks.schema.Path getPath_LoanReason()
	{
		return path_LoanReason;
	}

   /* Get and Set methods for attribute:  
    * registredMortgageRank (Standard)
    *
    */

	public Integer get_RegistredMortgageRank()
	{
		return this.getInteger("./registredMortgageRank");
	}

	public Integer get_RegistredMortgageRank_After()
	{
		return (Integer)this.getValueAfter("./registredMortgageRank");
	}

	public Integer get_RegistredMortgageRank_Before()
	{
		return (Integer)this.getValueBefore("./registredMortgageRank");
	}

	public void set_RegistredMortgageRank(final Integer value)
	{
		this.setObject(value, "./registredMortgageRank", Integer.class);
	}

	public boolean is_RegistredMortgageRank_Changed()
	{
	    return (((Integer)this.getValueAfter("./registredMortgageRank")) != null && 
	        !((Integer)this.getValueAfter("./registredMortgageRank")).equals((this.getValueBefore("./registredMortgageRank"))));
	}

	private static final com.orchestranetworks.schema.Path path_RegistredMortgageRank = com.orchestranetworks.schema.Path.parse("./registredMortgageRank");
	public static final com.orchestranetworks.schema.Path getPath_RegistredMortgageRank()
	{
		return path_RegistredMortgageRank;
	}

   /* Get and Set methods for attribute:  
    * preceedingMortgageAmount (Standard)
    *
    */

	public Integer get_PreceedingMortgageAmount()
	{
		return this.getInteger("./preceedingMortgageAmount");
	}

	public Integer get_PreceedingMortgageAmount_After()
	{
		return (Integer)this.getValueAfter("./preceedingMortgageAmount");
	}

	public Integer get_PreceedingMortgageAmount_Before()
	{
		return (Integer)this.getValueBefore("./preceedingMortgageAmount");
	}

	public void set_PreceedingMortgageAmount(final Integer value)
	{
		this.setObject(value, "./preceedingMortgageAmount", Integer.class);
	}

	public boolean is_PreceedingMortgageAmount_Changed()
	{
	    return (((Integer)this.getValueAfter("./preceedingMortgageAmount")) != null && 
	        !((Integer)this.getValueAfter("./preceedingMortgageAmount")).equals((this.getValueBefore("./preceedingMortgageAmount"))));
	}

	private static final com.orchestranetworks.schema.Path path_PreceedingMortgageAmount = com.orchestranetworks.schema.Path.parse("./preceedingMortgageAmount");
	public static final com.orchestranetworks.schema.Path getPath_PreceedingMortgageAmount()
	{
		return path_PreceedingMortgageAmount;
	}

   /* Get and Set methods for attribute:  
    * penaltyFreeRepaymentDescription (Standard)
    *
    */

	public String get_PenaltyFreeRepaymentDescription()
	{
		return this.getString("./penaltyFreeRepaymentDescription");
	}

	public String get_PenaltyFreeRepaymentDescription_After()
	{
		return (String)this.getValueAfter("./penaltyFreeRepaymentDescription");
	}

	public String get_PenaltyFreeRepaymentDescription_Before()
	{
		return (String)this.getValueBefore("./penaltyFreeRepaymentDescription");
	}

	public void set_PenaltyFreeRepaymentDescription(final String value)
	{
		this.setObject(value, "./penaltyFreeRepaymentDescription", String.class);
	}

	public boolean is_PenaltyFreeRepaymentDescription_Changed()
	{
	    return (((String)this.getValueAfter("./penaltyFreeRepaymentDescription")) != null && 
	        !((String)this.getValueAfter("./penaltyFreeRepaymentDescription")).equals((this.getValueBefore("./penaltyFreeRepaymentDescription"))));
	}

	private static final com.orchestranetworks.schema.Path path_PenaltyFreeRepaymentDescription = com.orchestranetworks.schema.Path.parse("./penaltyFreeRepaymentDescription");
	public static final com.orchestranetworks.schema.Path getPath_PenaltyFreeRepaymentDescription()
	{
		return path_PenaltyFreeRepaymentDescription;
	}

   /* Get and Set methods for attribute:  
    * acceptanceConditionVersion (Standard)
    *
    */

	public String get_AcceptanceConditionVersion()
	{
		return this.getString("./acceptanceConditionVersion");
	}

	public String get_AcceptanceConditionVersion_After()
	{
		return (String)this.getValueAfter("./acceptanceConditionVersion");
	}

	public String get_AcceptanceConditionVersion_Before()
	{
		return (String)this.getValueBefore("./acceptanceConditionVersion");
	}

	public void set_AcceptanceConditionVersion(final String value)
	{
		this.setObject(value, "./acceptanceConditionVersion", String.class);
	}

	public boolean is_AcceptanceConditionVersion_Changed()
	{
	    return (((String)this.getValueAfter("./acceptanceConditionVersion")) != null && 
	        !((String)this.getValueAfter("./acceptanceConditionVersion")).equals((this.getValueBefore("./acceptanceConditionVersion"))));
	}

	private static final com.orchestranetworks.schema.Path path_AcceptanceConditionVersion = com.orchestranetworks.schema.Path.parse("./acceptanceConditionVersion");
	public static final com.orchestranetworks.schema.Path getPath_AcceptanceConditionVersion()
	{
		return path_AcceptanceConditionVersion;
	}

   /* Get and Set methods for attribute:  
    * Notary (Standard)
    *
    */

	public String get_Notary()
	{
		return this.getString("./Notary");
	}

	public String get_Notary_After()
	{
		return (String)this.getValueAfter("./Notary");
	}

	public String get_Notary_Before()
	{
		return (String)this.getValueBefore("./Notary");
	}

	public void set_Notary(final String value)
	{
		this.setObject(value, "./Notary", String.class);
	}

	public boolean is_Notary_Changed()
	{
	    return (((String)this.getValueAfter("./Notary")) != null && 
	        !((String)this.getValueAfter("./Notary")).equals((this.getValueBefore("./Notary"))));
	}

	private static final com.orchestranetworks.schema.Path path_Notary = com.orchestranetworks.schema.Path.parse("./Notary");
	public static final com.orchestranetworks.schema.Path getPath_Notary()
	{
		return path_Notary;
	}

   /* Get and Set methods for attribute:  
    * customerSupportTeam (Standard)
    *
    */

	public String get_CustomerSupportTeam()
	{
		return this.getString("./customerSupportTeam");
	}

	public String get_CustomerSupportTeam_After()
	{
		return (String)this.getValueAfter("./customerSupportTeam");
	}

	public String get_CustomerSupportTeam_Before()
	{
		return (String)this.getValueBefore("./customerSupportTeam");
	}

	public void set_CustomerSupportTeam(final String value)
	{
		this.setObject(value, "./customerSupportTeam", String.class);
	}

	public boolean is_CustomerSupportTeam_Changed()
	{
	    return (((String)this.getValueAfter("./customerSupportTeam")) != null && 
	        !((String)this.getValueAfter("./customerSupportTeam")).equals((this.getValueBefore("./customerSupportTeam"))));
	}

	private static final com.orchestranetworks.schema.Path path_CustomerSupportTeam = com.orchestranetworks.schema.Path.parse("./customerSupportTeam");
	public static final com.orchestranetworks.schema.Path getPath_CustomerSupportTeam()
	{
		return path_CustomerSupportTeam;
	}

   /* Get and Set methods for attribute:  
    * contractNumber (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_ContractNumber_FK()
	{
		String ret = this.getString("./contractNumber");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_ContractNumber_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./contractNumber");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_ContractNumber_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./contractNumber");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_ContractNumber_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./contractNumber", String.class);
	}

	public void set_ContractNumber(final nl.vqd.demo.ebx.mdm.commons.datamodel.Contract value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./contractNumber", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_ContractNumber_Record()
	{
		return super.getFK("./contractNumber");
	}

	public boolean is_ContractNumber_Changed()
	{
	    return (((String)this.getValueAfter("./contractNumber")) != null && 
	        !((String)this.getValueAfter("./contractNumber")).equals((this.getValueBefore("./contractNumber"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Contract get_ContractNumber()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./contractNumber");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Contract(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_ContractNumber = com.orchestranetworks.schema.Path.parse("./contractNumber");
	public static final com.orchestranetworks.schema.Path getPath_ContractNumber()
	{
		return path_ContractNumber;
	}

   /* Get and Set methods for attribute:  
    * Deellening (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_LoanComponent_FK()
	{
		String ret = this.getString("./loanComponent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_LoanComponent_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./loanComponent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_LoanComponent_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./loanComponent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_LoanComponent_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./loanComponent", String.class);
	}

	public void set_LoanComponent(final nl.vqd.demo.ebx.mdm.commons.datamodel.LoanComponent value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./loanComponent", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_LoanComponent_Record()
	{
		return super.getFK("./loanComponent");
	}

	public boolean is_LoanComponent_Changed()
	{
	    return (((String)this.getValueAfter("./loanComponent")) != null && 
	        !((String)this.getValueAfter("./loanComponent")).equals((this.getValueBefore("./loanComponent"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.LoanComponent get_LoanComponent()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./loanComponent");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.LoanComponent(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_LoanComponent = com.orchestranetworks.schema.Path.parse("./loanComponent");
	public static final com.orchestranetworks.schema.Path getPath_LoanComponent()
	{
		return path_LoanComponent;
	}

   /* Loan (Association)
    *
    */

	public java.util.List<com.onwbp.adaptation.Adaptation> get_Loan(){
		return super.getAssociation("./loan");
	}

	private static final com.orchestranetworks.schema.Path path_Loan = com.orchestranetworks.schema.Path.parse("./loan");
	public static final com.orchestranetworks.schema.Path getPath_Loan()
	{
		return path_Loan;
	}

	private java.util.List<nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty> list_Loan = new java.util.ArrayList<>();
	
	public void add_Loan(nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty frameworkTable) {
		list_Loan.add(frameworkTable);
	}
	
	public void add_Loan(java.util.List<nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty> frameworkTableList) {
		list_Loan.addAll(frameworkTableList);
	}

	public void clear_Loan() {
		list_Loan.clear();
	}
	
	public boolean has_Loan() {
		return !list_Loan.isEmpty();
	}
	
	public java.util.List<nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty> getList_Loan(){
		return list_Loan;
	}


   /* Association has Childs Code
    *
    */

	@Override
	public boolean hasChilds() {
		if (!list_Loan.isEmpty()) {
			return true;
		}
		return false;
	}

   /* Association Create Parent and Child's Code
    *
    */

	@Override
	public com.onwbp.adaptation.Adaptation createParentAndChilds(com.onwbp.adaptation.AdaptationHome dataSpace, com.orchestranetworks.service.Session session, com.ebx5.frameworkrt.util.TransactionParameters transactionParameters, com.orchestranetworks.service.ProcedureContext procedureContext) throws com.orchestranetworks.service.OperationException {
		final CreateChilds procedure = new CreateChilds();
		procedure.parent = this;
		procedure.attributes = this.getAttributes();
		procedure.table = this.getTable();
		procedure.transactionParameters = transactionParameters;
		if (this.getProcedureContext() != null)
		{
			procedure.setProcedureContext(this.getProcedureContext());
			this.setRecord(procedure.createRecord());
		}
		else
		{
			com.orchestranetworks.service.ProgrammaticService ps = com.orchestranetworks.service.ProgrammaticService.createForSession(session, dataSpace);
			com.orchestranetworks.service.ProcedureResult pResult = ps.execute(procedure);
			if (!pResult.hasFailed())
			{
				this.setRecord(procedure.record);
			}
			else
			{
				com.orchestranetworks.service.OperationException oe = pResult.getException();
				if (oe != null) {
					throw oe; 
				}
				throw pResult.getConstraintViolationException();
			}
		}
		return procedure.record;
	}

	private class CreateChilds implements com.orchestranetworks.service.Procedure {
		private com.ebx5.frameworkrt.util.TransactionParameters transactionParameters = new com.ebx5.frameworkrt.util.TransactionParameters(); 
		private java.util.Map<com.orchestranetworks.schema.Path, Object> attributes = null;
		private com.orchestranetworks.service.ProcedureContext procedureContext = null;
		private com.onwbp.adaptation.Adaptation record = null;
		private com.onwbp.adaptation.AdaptationTable table = null;
		private Loan parent = null;

		@Override
		public void execute(com.orchestranetworks.service.ProcedureContext context) throws Exception {
			this.record = this.createRecord(context);
		}

		public void setProcedureContext(final com.orchestranetworks.service.ProcedureContext procedureContext)
		{
			this.procedureContext = procedureContext;
		}

		public com.onwbp.adaptation.Adaptation createRecord() throws com.orchestranetworks.service.OperationException
		{
			this.record = this.createRecord(this.procedureContext);
			return this.record;
		}

		private com.onwbp.adaptation.Adaptation createRecord(final com.orchestranetworks.service.ProcedureContext context) throws com.orchestranetworks.service.OperationException
		{
			this.record = context.doCreateOccurrence(prepareCreate(context), this.table);
			
			Loan newLoan = new Loan(this.record);

			for (nl.vqd.demo.ebx.mdm.commons.datamodel.LoanParty child : this.parent.list_Loan) {
				
				child.set_Loan(newLoan);
				this.table = child.getTable();
				this.attributes = child.getAttributes();
				
				context.doCreateOccurrence(prepareCreate(context), this.table);
			}
			list_Loan.clear();
			return this.record;
		}

		private com.orchestranetworks.service.ValueContextForUpdate prepareCreate(final com.orchestranetworks.service.ProcedureContext context) {
			
			com.orchestranetworks.service.ValueContextForUpdate contextForNewOccurrence = context.getContextForNewOccurrence(this.table);

			if (this.attributes == null)
			{
				return null;
			}

			for (java.util.Map.Entry<com.orchestranetworks.schema.Path, Object> entry : this.attributes.entrySet())
			{
				Object object = entry.getValue();
				com.orchestranetworks.schema.Path path = entry.getKey();
				if (transactionParameters.isAllPrivileges()) {
					contextForNewOccurrence.setPrivilegeForNode(path);
				}
				contextForNewOccurrence.setValue(object, path);
			}
			if (transactionParameters.getExecutionInfo() != null)
			{
				context.setExecutionInformation(transactionParameters.getExecutionInfo());
			}
			context.setCommitThreshold(transactionParameters.getCommitThreshold());
			context.setAllPrivileges(transactionParameters.isAllPrivileges());
			context.setTriggerActivation(transactionParameters.isTriggersActive());
			context.setHistoryActivation(transactionParameters.isHistoryActivated());
			context.setDatabaseHistoryActivation(transactionParameters.isDataBaseHistoryActivated());
			if (transactionParameters.isDisableDataSetValidation()) {
				context.disableDatasetValidation(transactionParameters.getDataSet());
			}
			
			return contextForNewOccurrence;
		}
	}

}

