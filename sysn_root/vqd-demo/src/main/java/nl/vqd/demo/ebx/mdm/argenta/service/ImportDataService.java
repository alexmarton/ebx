package nl.vqd.demo.ebx.mdm.argenta.service;

import com.ebx5.commons.util.RepositoryUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.schema.dynamic.BeanDefinition;
import com.orchestranetworks.schema.dynamic.BeanElement;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;
import nl.vqd.demo.ebx.mdm.argenta.datamodel.ArgentaPaths;
import nl.vqd.demo.ebx.mdm.argenta.service.product.display.DisplayConfirmationStep;
import nl.vqd.demo.ebx.mdm.argenta.service.product.display.DisplayFilterStep;
import nl.vqd.demo.ebx.mdm.argenta.service.product.display.DisplayResultStep;
import nl.vqd.demo.ebx.mdm.argenta.service.product.display.DisplayStep;
import nl.vqd.demo.ebx.mdm.argenta.service.product.filter.FilterConstraint;
import nl.vqd.demo.ebx.mdm.argenta.service.product.filter.FilterPaths;
import nl.vqd.demo.ebx.mdm.argenta.service.product.message.Messages;
import nl.vqd.demo.ebx.mdm.commons.datamodel.Contract;
import nl.vqd.demo.ebx.mdm.commons.datamodel.ContractenLening;
import nl.vqd.demo.ebx.mdm.commons.datamodel.Loan;
import nl.vqd.demo.ebx.mdm.commons.datamodel.Transaction;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ImportDataService implements UserService<TableViewEntitySelection> {

    public final static ObjectKey _objectKey = ObjectKey.forName("ImportDataService");
    private static final Format DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private DisplayStep currentStep;

    public ImportDataService() {
        this.currentStep = new DisplayFilterStep();
    }

    private int size = 0;
    private List<Adaptation> result;
    List<ContractenLening> contractenLenings;


    @Override
    public void setupObjectContext(UserServiceSetupObjectContext<TableViewEntitySelection> userServiceSetupObjectContext, UserServiceObjectContextBuilder userServiceObjectContextBuilder) {

//        Adaptation dataSet = userServiceSetupObjectContext.getEntitySelection().getDataset();
//        ContractenLening contractenLeningSelect = new ContractenLening(dataSet);
//        result = contractenLeningSelect.selectRecordList();
//        this.size = result.size();

        final BeanDefinition definition = userServiceObjectContextBuilder.createBeanDefinition();

        {
            final BeanElement element = definition.createElement(FilterPaths._startDate, SchemaTypeName.XS_DATE);
            final UserMessage message = Messages.get("StartDate");

            element.setLabel(message);
            element.setMinOccurs(1);
            element.setMaxOccurs(1);
            element.setDefaultValue(new Date());
            element.addFacetConstraint(FilterConstraint.class);
        }

        {
            final BeanElement element = definition.createElement(FilterPaths._endDate, SchemaTypeName.XS_DATE);
            final UserMessage message = Messages.get("EndDate");

            element.setLabel(message);
            element.setMinOccurs(1);
            element.setMaxOccurs(1);
            element.setDefaultValue(new Date());
            element.addFacetConstraint(FilterConstraint.class);
        }

        userServiceObjectContextBuilder.registerBean(FilterPaths._objectKey, definition);

    }

    @Override
    public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> context, UserServiceDisplayConfigurator configurator) {

        this.currentStep.setupDisplay(context, configurator);
//        configurator.setContent(this::writeContent);
//        final UIButtonSpec closeButtonSpec = configurator.newCloseButton();
//        configurator.setLeftButtons(closeButtonSpec);
    }

    @Override
    public void validate(UserServiceValidateContext<TableViewEntitySelection> userServiceValidateContext) {

    }

    @Override
    public UserServiceEventOutcome processEventOutcome(UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context, UserServiceEventOutcome eventOutcome) {

//        System.out.println("We are on UserServiceEventOutcome processEventOutcome");
        if (!(eventOutcome instanceof DisplayStep.EventOutcome)) {
            return eventOutcome;
        }

        switch ((DisplayStep.EventOutcome) eventOutcome) {


            case DISPLAY_FILTER:
                this.currentStep = new DisplayFilterStep();
                return null;

            case DISPLAY_CONFIRMATION:
                //Confirm
                final int rowCounts = createRequestResult(context).getSize();
                this.currentStep = new DisplayConfirmationStep(rowCounts);
                return null;

            case DISPLAY_RESULT:
                //Update

                final ProcedureResult result = null;
                this.currentStep = new DisplayResultStep(updateData(context));
                return null;

            default:
                return null;
        }


    }


    private static RequestResult createRequestResult(
            final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {

        final TableViewEntitySelection selection = context.getEntitySelection();

        final AdaptationTable rows = selection.getTable();

        final String rapportagedatumPath = ArgentaPaths._ContractenLening._Rapportagedatum.format();

        final ValueContextForValidation valueContext = context.getValueContext(FilterPaths._objectKey);
        final String startDate = DATE_FORMAT.format(valueContext.getValue(FilterPaths._startDate));
        final String endDate = DATE_FORMAT.format(valueContext.getValue(FilterPaths._endDate));


        final String lening_contract_nummer = ArgentaPaths._ContractenLening._Lening_contract_nummer.format();
        final String contract_procesversie = ArgentaPaths._ContractenLening._Contract_procesversie.format();
        final String whitelabel_code = ArgentaPaths._ContractenLening._Whitelabel_code.format();


        final String condition = "date-greater-than(" + rapportagedatumPath + ",'" + startDate
                + "') and date-less-than(" + rapportagedatumPath + ",'" + endDate + "')";


        return rows.createRequestResult(condition);
    }


    private static boolean updateData(
            final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {

//        final RequestResult result = createRequestResult(context);
        final RequestResult result = context.getEntitySelection().getAllRecords().execute();

        if (result.isEmpty()) return false;

        for (Adaptation record; (record = result.nextAdaptation()) != null; ) {
            updateSelectedTable(record, context);
        }
        return true;
    }


    private static void updateSelectedTable(Adaptation record,
                                            final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {

        final AdaptationTable adaptationTable = context.getEntitySelection().getTable();
        final Adaptation adaptation = adaptationTable.getContainerAdaptation();
        final AdaptationHome adaptationHome = adaptation.getHome();

        //Switch dataSetName
        final Adaptation businessModelDataSet = RepositoryUtils.getDataSet(adaptationHome, "BusinessModel");

        final Contract contract = new Contract(businessModelDataSet);
        final Loan loan = new Loan(businessModelDataSet);
        final Transaction transaction = new Transaction(businessModelDataSet);

        //One table
        contract.set_ProcessVersion(record.get(ArgentaPaths._ContractenLening._Lening_contract_nummer).toString());
        contract.set_ContractNumber(record.get(ArgentaPaths._ContractenLening._Contract_procesversie).toString());
        contract.set_Notaris(record.get(ArgentaPaths._ContractenLening._Naam_notaris).toString());

        // Second table
        loan.set_LoanNumber(Integer.parseInt(record.get(ArgentaPaths._ContractenLening._Lening_nummer).toString()));

        // Third Table
        transaction.set_Account(record.get(ArgentaPaths._ContractenLening._Funder_naam).toString());





        try {
            final Adaptation storeAdaptation1 = contract.create(context.getSession(), adaptationHome);
            final Adaptation storeAdaptation2 = loan.create(context.getSession(), adaptationHome);
            final Adaptation storeAdaptation3 = transaction.create(context.getSession(), adaptationHome);
//            final ValueContext valueContext = storeAdaptation.createValueContext();
//            final SchemaNode schemaNode = valueContext.getNode(Path.SELF);
//            final SessionPermissions sessionPermissions = context.getSession().getPermissions();
//            sessionPermissions.getNodeAccessPermission(schemaNode, adaptation);
//            schemaNode.createNewOccurrence();

        } catch (OperationException ep) {
            ep.printStackTrace();
        }

    }


//    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
//        writer.setCurrentObject(ImportDataService._objectKey);
//        final UIFormLabelSpec arrayList = new UIFormLabelSpec("Array " + Arrays.toString(this.result.toArray()) + " results");
//        final UIFormLabelSpec labelSpec = new UIFormLabelSpec("Number of items " + this.size + " to be imported");
//
//
//        for (Adaptation contractenLening : result) {
//
//            System.out.println(contractenLening.getContainerTable());
//
//            writer.add("<div>");
//            writer.addUILabel(new UIFormLabelSpec("contractenLening " + contractenLening.toString()));
//            writer.add("</div>");
//
//        }
//
//        writer.add("<div>");
//        writer.addUILabel(labelSpec);
//        writer.add("</div>");
//        writer.add("<div>");
//        writer.addUILabel(arrayList);
//        writer.add("</div>");
//    }

}
