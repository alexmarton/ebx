package nl.vqd.demo.ebx.mdm.argenta.extension;

import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import nl.vqd.demo.ebx.mdm.argenta.service.declaration.ImportDataDeclaration;
import nl.vqd.demo.ebx.mdm.argenta.service.declaration.NotarisDataDeclaration;

public class ArgentaExtensions implements SchemaExtensions {

    @Override
    public void defineExtensions(SchemaExtensionsContext argentaContext) {

        final ImportDataDeclaration.OnTableView importDataDeclaration = new ImportDataDeclaration();
        final NotarisDataDeclaration.OnRecord record = new NotarisDataDeclaration();

        argentaContext.registerUserService(importDataDeclaration);
        argentaContext.registerUserService(record);

    }
}
