package nl.vqd.demo.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Object" record from the data model "Business Model"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Nov 20 11:43:27 CET 2020
*/

public class Object extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/object";

	public Object(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Object(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Object(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Object(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Object(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Object(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Object(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Object(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	

	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Place Property (Standard)
    *
    */

	public String get_PlaceProperty()
	{
		return this.getString("./placeProperty");
	}

	public String get_PlaceProperty_After()
	{
		return (String)this.getValueAfter("./placeProperty");
	}

	public String get_PlaceProperty_Before()
	{
		return (String)this.getValueBefore("./placeProperty");
	}

	public void set_PlaceProperty(final String value)
	{
		this.setObject(value, "./placeProperty", String.class);
	}

	public boolean is_PlaceProperty_Changed()
	{
	    return (((String)this.getValueAfter("./placeProperty")) != null && 
	        !((String)this.getValueAfter("./placeProperty")).equals((this.getValueBefore("./placeProperty"))));
	}

	private static final com.orchestranetworks.schema.Path path_PlaceProperty = com.orchestranetworks.schema.Path.parse("./placeProperty");
	public static final com.orchestranetworks.schema.Path getPath_PlaceProperty()
	{
		return path_PlaceProperty;
	}

   /* Get and Set methods for attribute:  
    * Street (Standard)
    *
    */

	public String get_Street()
	{
		return this.getString("./street");
	}

	public String get_Street_After()
	{
		return (String)this.getValueAfter("./street");
	}

	public String get_Street_Before()
	{
		return (String)this.getValueBefore("./street");
	}

	public void set_Street(final String value)
	{
		this.setObject(value, "./street", String.class);
	}

	public boolean is_Street_Changed()
	{
	    return (((String)this.getValueAfter("./street")) != null && 
	        !((String)this.getValueAfter("./street")).equals((this.getValueBefore("./street"))));
	}

	private static final com.orchestranetworks.schema.Path path_Street = com.orchestranetworks.schema.Path.parse("./street");
	public static final com.orchestranetworks.schema.Path getPath_Street()
	{
		return path_Street;
	}

   /* Get and Set methods for attribute:  
    * House Number (Standard)
    *
    */

	public Integer get_HouseNumber()
	{
		return this.getInteger("./houseNumber");
	}

	public Integer get_HouseNumber_After()
	{
		return (Integer)this.getValueAfter("./houseNumber");
	}

	public Integer get_HouseNumber_Before()
	{
		return (Integer)this.getValueBefore("./houseNumber");
	}

	public void set_HouseNumber(final Integer value)
	{
		this.setObject(value, "./houseNumber", Integer.class);
	}

	public boolean is_HouseNumber_Changed()
	{
	    return (((Integer)this.getValueAfter("./houseNumber")) != null && 
	        !((Integer)this.getValueAfter("./houseNumber")).equals((this.getValueBefore("./houseNumber"))));
	}

	private static final com.orchestranetworks.schema.Path path_HouseNumber = com.orchestranetworks.schema.Path.parse("./houseNumber");
	public static final com.orchestranetworks.schema.Path getPath_HouseNumber()
	{
		return path_HouseNumber;
	}

   /* Get and Set methods for attribute:  
    * Market Value (Standard)
    *
    */

	public Integer get_MarketValue()
	{
		return this.getInteger("./marketValue");
	}

	public Integer get_MarketValue_After()
	{
		return (Integer)this.getValueAfter("./marketValue");
	}

	public Integer get_MarketValue_Before()
	{
		return (Integer)this.getValueBefore("./marketValue");
	}

	public void set_MarketValue(final Integer value)
	{
		this.setObject(value, "./marketValue", Integer.class);
	}

	public boolean is_MarketValue_Changed()
	{
	    return (((Integer)this.getValueAfter("./marketValue")) != null && 
	        !((Integer)this.getValueAfter("./marketValue")).equals((this.getValueBefore("./marketValue"))));
	}

	private static final com.orchestranetworks.schema.Path path_MarketValue = com.orchestranetworks.schema.Path.parse("./marketValue");
	public static final com.orchestranetworks.schema.Path getPath_MarketValue()
	{
		return path_MarketValue;
	}

   /* Get and Set methods for attribute:  
    * Cadastre (Standard)
    *
    */

	public String get_Cadastre()
	{
		return this.getString("./cadastre");
	}

	public String get_Cadastre_After()
	{
		return (String)this.getValueAfter("./cadastre");
	}

	public String get_Cadastre_Before()
	{
		return (String)this.getValueBefore("./cadastre");
	}

	public void set_Cadastre(final String value)
	{
		this.setObject(value, "./cadastre", String.class);
	}

	public boolean is_Cadastre_Changed()
	{
	    return (((String)this.getValueAfter("./cadastre")) != null && 
	        !((String)this.getValueAfter("./cadastre")).equals((this.getValueBefore("./cadastre"))));
	}

	private static final com.orchestranetworks.schema.Path path_Cadastre = com.orchestranetworks.schema.Path.parse("./cadastre");
	public static final com.orchestranetworks.schema.Path getPath_Cadastre()
	{
		return path_Cadastre;
	}

   /* Get and Set methods for attribute:  
    * Status (Standard)
    *
    */

	public String get_Status()
	{
		return this.getString("./status");
	}

	public String get_Status_After()
	{
		return (String)this.getValueAfter("./status");
	}

	public String get_Status_Before()
	{
		return (String)this.getValueBefore("./status");
	}

	public void set_Status(final String value)
	{
		this.setObject(value, "./status", String.class);
	}

	public boolean is_Status_Changed()
	{
	    return (((String)this.getValueAfter("./status")) != null && 
	        !((String)this.getValueAfter("./status")).equals((this.getValueBefore("./status"))));
	}

	private static final com.orchestranetworks.schema.Path path_Status = com.orchestranetworks.schema.Path.parse("./status");
	public static final com.orchestranetworks.schema.Path getPath_Status()
	{
		return path_Status;
	}

   /* Get and Set methods for attribute:  
    * Loan (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_FK()
	{
		String ret = this.getString("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Loan_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./loan");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Loan_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./loan", String.class);
	}

	public void set_Loan(final nl.vqd.demo.ebx.mdm.commons.datamodel.Loan value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./loan", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Loan_Record()
	{
		return super.getFK("./loan");
	}

	public boolean is_Loan_Changed()
	{
	    return (((String)this.getValueAfter("./loan")) != null && 
	        !((String)this.getValueAfter("./loan")).equals((this.getValueBefore("./loan"))));
	}

	public nl.vqd.demo.ebx.mdm.commons.datamodel.Loan get_Loan()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./loan");
		return record != null ? new nl.vqd.demo.ebx.mdm.commons.datamodel.Loan(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Loan = com.orchestranetworks.schema.Path.parse("./loan");
	public static final com.orchestranetworks.schema.Path getPath_Loan()
	{
		return path_Loan;
	}

}

