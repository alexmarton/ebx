package nl.systemation.training.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Items" record from the data model "My Store Data Model Development"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Wed Apr 15 13:50:34 CEST 2020
*/

public class Item extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/Item";

	public Item(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Item(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Item(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Item(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Item(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Item(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Item(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Item(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Identifier (Standard)
    *
    */

	public Integer get_Identifier()
	{
		return this.getInteger("./identifier");
	}

	public Integer get_Identifier_After()
	{
		return (Integer)this.getValueAfter("./identifier");
	}

	public Integer get_Identifier_Before()
	{
		return (Integer)this.getValueBefore("./identifier");
	}

	public void set_Identifier(final Integer value)
	{
		this.setObject(value, "./identifier", Integer.class);
	}

	public boolean is_Identifier_Changed()
	{
	    return (((Integer)this.getValueAfter("./identifier")) != null && 
	        !((Integer)this.getValueAfter("./identifier")).equals((this.getValueBefore("./identifier"))));
	}

	private static final com.orchestranetworks.schema.Path path_Identifier = com.orchestranetworks.schema.Path.parse("./identifier");
	public static final com.orchestranetworks.schema.Path getPath_Identifier()
	{
		return path_Identifier;
	}

   /* Get and Set methods for attribute:  
    * Category (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Category_FK()
	{
		String ret = this.getString("./category");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Category_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./category");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Category_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./category");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Category_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./category", String.class);
	}

	public void set_Category(final nl.systemation.training.ebx.mdm.commons.datamodel.Category value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./category", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Category_Record()
	{
		return super.getFK("./category");
	}

	public boolean is_Category_Changed()
	{
	    return (((String)this.getValueAfter("./category")) != null && 
	        !((String)this.getValueAfter("./category")).equals((this.getValueBefore("./category"))));
	}

	public nl.systemation.training.ebx.mdm.commons.datamodel.Category get_Category()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./category");
		return record != null ? new nl.systemation.training.ebx.mdm.commons.datamodel.Category(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Category = com.orchestranetworks.schema.Path.parse("./category");
	public static final com.orchestranetworks.schema.Path getPath_Category()
	{
		return path_Category;
	}

   /* Get and Set methods for attribute:  
    * Brand (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Brand_FK()
	{
		String ret = this.getString("./brand");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Brand_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./brand");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Brand_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./brand");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Brand_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./brand", String.class);
	}

	public void set_Brand(final nl.systemation.training.ebx.mdm.commons.datamodel.Brand value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./brand", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Brand_Record()
	{
		return super.getFK("./brand");
	}

	public boolean is_Brand_Changed()
	{
	    return (((String)this.getValueAfter("./brand")) != null && 
	        !((String)this.getValueAfter("./brand")).equals((this.getValueBefore("./brand"))));
	}

	public nl.systemation.training.ebx.mdm.commons.datamodel.Brand get_Brand()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./brand");
		return record != null ? new nl.systemation.training.ebx.mdm.commons.datamodel.Brand(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Brand = com.orchestranetworks.schema.Path.parse("./brand");
	public static final com.orchestranetworks.schema.Path getPath_Brand()
	{
		return path_Brand;
	}

   /* Inventories (Association)
    *
    */

	public java.util.List<com.onwbp.adaptation.Adaptation> get_Inventories(){
		return super.getAssociation("./inventories");
	}

	private static final com.orchestranetworks.schema.Path path_Inventories = com.orchestranetworks.schema.Path.parse("./inventories");
	public static final com.orchestranetworks.schema.Path getPath_Inventories()
	{
		return path_Inventories;
	}

	private java.util.List<nl.systemation.training.ebx.mdm.commons.datamodel.Inventory> list_Inventories = new java.util.ArrayList<>();
	
	public void add_Inventories(nl.systemation.training.ebx.mdm.commons.datamodel.Inventory frameworkTable) {
		list_Inventories.add(frameworkTable);
	}
	
	public void add_Inventories(java.util.List<nl.systemation.training.ebx.mdm.commons.datamodel.Inventory> frameworkTableList) {
		list_Inventories.addAll(frameworkTableList);
	}

	public void clear_Inventories() {
		list_Inventories.clear();
	}
	
	public boolean has_Inventories() {
		return !list_Inventories.isEmpty();
	}
	
	public java.util.List<nl.systemation.training.ebx.mdm.commons.datamodel.Inventory> getList_Inventories(){
		return list_Inventories;
	}


   /* Get and Set methods for attribute:  
    * Name (Standard)
    *
    */

	public String get_Name()
	{
		return this.getString("./name");
	}

	public String get_Name_After()
	{
		return (String)this.getValueAfter("./name");
	}

	public String get_Name_Before()
	{
		return (String)this.getValueBefore("./name");
	}

	public void set_Name(final String value)
	{
		this.setObject(value, "./name", String.class);
	}

	public boolean is_Name_Changed()
	{
	    return (((String)this.getValueAfter("./name")) != null && 
	        !((String)this.getValueAfter("./name")).equals((this.getValueBefore("./name"))));
	}

	private static final com.orchestranetworks.schema.Path path_Name = com.orchestranetworks.schema.Path.parse("./name");
	public static final com.orchestranetworks.schema.Path getPath_Name()
	{
		return path_Name;
	}

   /* Get and Set methods for attribute:  
    * Available (Standard boolean)
    *
    */
    
	/**
	 * 
	 * @Deprecated use the is_Available() method instead
	 */
	@Deprecated 
	public Boolean get_Available()
	{
		return this.getBoolean("./available");
	}

	public Boolean get_Available_After()
	{
		return (Boolean)this.getValueAfter("./available");
	}

	public Boolean get_Available_Before()
	{
		return (Boolean)this.getValueBefore("./available");
	}

	public void set_Available(final Boolean value)
	{
		this.setObject(value, "./available", Boolean.class);
	}

	public boolean is_Available() {
		return this.getBoolean("./available");
	}

	public boolean is_Available_Changed()
	{
	    return (((Boolean)this.getValueAfter("./available")) != null && 
	        !((Boolean)this.getValueAfter("./available")).equals((this.getValueBefore("./available"))));
	}

	private static final com.orchestranetworks.schema.Path path_Available = com.orchestranetworks.schema.Path.parse("./available");
	public static final com.orchestranetworks.schema.Path getPath_Available()
	{
		return path_Available;
	}

   /* Get and Set methods for attribute:  
    * Default price (Standard)
    *
    */

	public java.math.BigDecimal get_DefaultPrice()
	{
		return this.getBigDecimal("./defaultPrice");
	}

	public java.math.BigDecimal get_DefaultPrice_After()
	{
		return (java.math.BigDecimal)this.getValueAfter("./defaultPrice");
	}

	public java.math.BigDecimal get_DefaultPrice_Before()
	{
		return (java.math.BigDecimal)this.getValueBefore("./defaultPrice");
	}

	public void set_DefaultPrice(final java.math.BigDecimal value)
	{
		this.setObject(value, "./defaultPrice", java.math.BigDecimal.class);
	}

	public boolean is_DefaultPrice_Changed()
	{
	    return (((java.math.BigDecimal)this.getValueAfter("./defaultPrice")) != null && 
	        !((java.math.BigDecimal)this.getValueAfter("./defaultPrice")).equals((this.getValueBefore("./defaultPrice"))));
	}

	private static final com.orchestranetworks.schema.Path path_DefaultPrice = com.orchestranetworks.schema.Path.parse("./defaultPrice");
	public static final com.orchestranetworks.schema.Path getPath_DefaultPrice()
	{
		return path_DefaultPrice;
	}

   /* Association has Childs Code
    *
    */

	@Override
	public boolean hasChilds() {
		if (!list_Inventories.isEmpty()) {
			return true;
		}
		return false;
	}

   /* Association Create Parent and Child's Code
    *
    */

	@Override
	public com.onwbp.adaptation.Adaptation createParentAndChilds(com.onwbp.adaptation.AdaptationHome dataSpace, com.orchestranetworks.service.Session session, com.ebx5.frameworkrt.util.TransactionParameters transactionParameters, com.orchestranetworks.service.ProcedureContext procedureContext) throws com.orchestranetworks.service.OperationException {
		final CreateChilds procedure = new CreateChilds();
		procedure.parent = this;
		procedure.attributes = this.getAttributes();
		procedure.table = this.getTable();
		procedure.transactionParameters = transactionParameters;
		if (this.getProcedureContext() != null)
		{
			procedure.setProcedureContext(this.getProcedureContext());
			this.setRecord(procedure.createRecord());
		}
		else
		{
			com.orchestranetworks.service.ProgrammaticService ps = com.orchestranetworks.service.ProgrammaticService.createForSession(session, dataSpace);
			com.orchestranetworks.service.ProcedureResult pResult = ps.execute(procedure);
			if (!pResult.hasFailed())
			{
				this.setRecord(procedure.record);
			}
			else
			{
				com.orchestranetworks.service.OperationException oe = pResult.getException();
				if (oe != null) {
					throw oe; 
				}
				throw pResult.getConstraintViolationException();
			}
		}
		return procedure.record;
	}

	private class CreateChilds implements com.orchestranetworks.service.Procedure {
		private com.ebx5.frameworkrt.util.TransactionParameters transactionParameters = new com.ebx5.frameworkrt.util.TransactionParameters(); 
		private java.util.Map<com.orchestranetworks.schema.Path, Object> attributes = null;
		private com.orchestranetworks.service.ProcedureContext procedureContext = null;
		private com.onwbp.adaptation.Adaptation record = null;
		private com.onwbp.adaptation.AdaptationTable table = null;
		private Item parent = null;

		@Override
		public void execute(com.orchestranetworks.service.ProcedureContext context) throws Exception {
			this.record = this.createRecord(context);
		}

		public void setProcedureContext(final com.orchestranetworks.service.ProcedureContext procedureContext)
		{
			this.procedureContext = procedureContext;
		}

		public com.onwbp.adaptation.Adaptation createRecord() throws com.orchestranetworks.service.OperationException
		{
			this.record = this.createRecord(this.procedureContext);
			return this.record;
		}

		private com.onwbp.adaptation.Adaptation createRecord(final com.orchestranetworks.service.ProcedureContext context) throws com.orchestranetworks.service.OperationException
		{
			this.record = context.doCreateOccurrence(prepareCreate(context), this.table);
			
			Item newItem = new Item(this.record);

			for (nl.systemation.training.ebx.mdm.commons.datamodel.Inventory child : this.parent.list_Inventories) {
				
				child.set_Item(newItem);
				this.table = child.getTable();
				this.attributes = child.getAttributes();
				
				context.doCreateOccurrence(prepareCreate(context), this.table);
			}
			list_Inventories.clear();
			return this.record;
		}

		private com.orchestranetworks.service.ValueContextForUpdate prepareCreate(final com.orchestranetworks.service.ProcedureContext context) {
			
			com.orchestranetworks.service.ValueContextForUpdate contextForNewOccurrence = context.getContextForNewOccurrence(this.table);

			if (this.attributes == null)
			{
				return null;
			}

			for (java.util.Map.Entry<com.orchestranetworks.schema.Path, Object> entry : this.attributes.entrySet())
			{
				Object object = entry.getValue();
				com.orchestranetworks.schema.Path path = entry.getKey();
				if (transactionParameters.isAllPrivileges()) {
					contextForNewOccurrence.setPrivilegeForNode(path);
				}
				contextForNewOccurrence.setValue(object, path);
			}
			if (transactionParameters.getExecutionInfo() != null)
			{
				context.setExecutionInformation(transactionParameters.getExecutionInfo());
			}
			context.setCommitThreshold(transactionParameters.getCommitThreshold());
			context.setAllPrivileges(transactionParameters.isAllPrivileges());
			context.setTriggerActivation(transactionParameters.isTriggersActive());
			context.setHistoryActivation(transactionParameters.isHistoryActivated());
			context.setDatabaseHistoryActivation(transactionParameters.isDataBaseHistoryActivated());
			if (transactionParameters.isDisableDataSetValidation()) {
				context.disableDatasetValidation(transactionParameters.getDataSet());
			}
			
			return contextForNewOccurrence;
		}
	}

}

