package nl.systemation.training.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Inventories" record from the data model "My Store Data Model Development"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Wed Apr 15 13:50:34 CEST 2020
*/

public class Inventory extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/Inventory";

	public Inventory(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Inventory(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Inventory(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Inventory(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Inventory(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Inventory(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Inventory(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Inventory(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Item (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Item_FK()
	{
		String ret = this.getString("./item");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Item_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./item");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Item_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./item");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Item_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./item", String.class);
	}

	public void set_Item(final nl.systemation.training.ebx.mdm.commons.datamodel.Item value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./item", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Item_Record()
	{
		return super.getFK("./item");
	}

	public boolean is_Item_Changed()
	{
	    return (((String)this.getValueAfter("./item")) != null && 
	        !((String)this.getValueAfter("./item")).equals((this.getValueBefore("./item"))));
	}

	public nl.systemation.training.ebx.mdm.commons.datamodel.Item get_Item()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./item");
		return record != null ? new nl.systemation.training.ebx.mdm.commons.datamodel.Item(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Item = com.orchestranetworks.schema.Path.parse("./item");
	public static final com.orchestranetworks.schema.Path getPath_Item()
	{
		return path_Item;
	}

   /* Get and Set methods for attribute:  
    * Store (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Store_FK()
	{
		String ret = this.getString("./store");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Store_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./store");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Store_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./store");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Store_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./store", String.class);
	}

	public void set_Store(final nl.systemation.training.ebx.mdm.commons.datamodel.Store value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./store", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Store_Record()
	{
		return super.getFK("./store");
	}

	public boolean is_Store_Changed()
	{
	    return (((String)this.getValueAfter("./store")) != null && 
	        !((String)this.getValueAfter("./store")).equals((this.getValueBefore("./store"))));
	}

	public nl.systemation.training.ebx.mdm.commons.datamodel.Store get_Store()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./store");
		return record != null ? new nl.systemation.training.ebx.mdm.commons.datamodel.Store(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Store = com.orchestranetworks.schema.Path.parse("./store");
	public static final com.orchestranetworks.schema.Path getPath_Store()
	{
		return path_Store;
	}

   /* Get and Set methods for attribute:  
    * Stock (Standard)
    *
    */

	public Integer get_Stock()
	{
		return this.getInteger("./stock");
	}

	public Integer get_Stock_After()
	{
		return (Integer)this.getValueAfter("./stock");
	}

	public Integer get_Stock_Before()
	{
		return (Integer)this.getValueBefore("./stock");
	}

	public void set_Stock(final Integer value)
	{
		this.setObject(value, "./stock", Integer.class);
	}

	public boolean is_Stock_Changed()
	{
	    return (((Integer)this.getValueAfter("./stock")) != null && 
	        !((Integer)this.getValueAfter("./stock")).equals((this.getValueBefore("./stock"))));
	}

	private static final com.orchestranetworks.schema.Path path_Stock = com.orchestranetworks.schema.Path.parse("./stock");
	public static final com.orchestranetworks.schema.Path getPath_Stock()
	{
		return path_Stock;
	}

   /* Get and Set methods for attribute:  
    * Price (Standard)
    *
    */

	public java.math.BigDecimal get_Price()
	{
		return this.getBigDecimal("./price");
	}

	public java.math.BigDecimal get_Price_After()
	{
		return (java.math.BigDecimal)this.getValueAfter("./price");
	}

	public java.math.BigDecimal get_Price_Before()
	{
		return (java.math.BigDecimal)this.getValueBefore("./price");
	}

	public void set_Price(final java.math.BigDecimal value)
	{
		this.setObject(value, "./price", java.math.BigDecimal.class);
	}

	public boolean is_Price_Changed()
	{
	    return (((java.math.BigDecimal)this.getValueAfter("./price")) != null && 
	        !((java.math.BigDecimal)this.getValueAfter("./price")).equals((this.getValueBefore("./price"))));
	}

	private static final com.orchestranetworks.schema.Path path_Price = com.orchestranetworks.schema.Path.parse("./price");
	public static final com.orchestranetworks.schema.Path getPath_Price()
	{
		return path_Price;
	}

	public void set_Price_Inherited() // Inherited Attributes only: true
	{
		this.setObject(com.onwbp.adaptation.AdaptationValue.INHERIT_VALUE, "./price");
	}

   /* Get and Set methods for attribute:  
    * Modified (Standard)
    *
    */

	public java.util.Date get_Modified()
	{
		return this.getDate("./modified");
	}

	public java.util.Date get_Modified_After()
	{
		return (java.util.Date)this.getValueAfter("./modified");
	}

	public java.util.Date get_Modified_Before()
	{
		return (java.util.Date)this.getValueBefore("./modified");
	}

	public void set_Modified(final java.util.Date value)
	{
		this.setObject(value, "./modified", java.util.Date.class);
	}

	public boolean is_Modified_Changed()
	{
	    return (((java.util.Date)this.getValueAfter("./modified")) != null && 
	        !((java.util.Date)this.getValueAfter("./modified")).equals((this.getValueBefore("./modified"))));
	}

	private static final com.orchestranetworks.schema.Path path_Modified = com.orchestranetworks.schema.Path.parse("./modified");
	public static final com.orchestranetworks.schema.Path getPath_Modified()
	{
		return path_Modified;
	}

}

