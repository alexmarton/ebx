package nl.systemation.training.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Categories" record from the data model "My Store Data Model Development"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Wed Apr 15 13:50:33 CEST 2020
*/

public class Category extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/Category";

	public Category(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Category(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Category(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Category(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Category(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Category(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Category(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Category(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Identifier (Standard)
    *
    */

	public Integer get_Identifier()
	{
		return this.getInteger("./identifier");
	}

	public Integer get_Identifier_After()
	{
		return (Integer)this.getValueAfter("./identifier");
	}

	public Integer get_Identifier_Before()
	{
		return (Integer)this.getValueBefore("./identifier");
	}

	public void set_Identifier(final Integer value)
	{
		this.setObject(value, "./identifier", Integer.class);
	}

	public boolean is_Identifier_Changed()
	{
	    return (((Integer)this.getValueAfter("./identifier")) != null && 
	        !((Integer)this.getValueAfter("./identifier")).equals((this.getValueBefore("./identifier"))));
	}

	private static final com.orchestranetworks.schema.Path path_Identifier = com.orchestranetworks.schema.Path.parse("./identifier");
	public static final com.orchestranetworks.schema.Path getPath_Identifier()
	{
		return path_Identifier;
	}

   /* Get and Set methods for attribute:  
    * Parent (FK)
    */

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Parent_FK()
	{
		String ret = this.getString("./parent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Parent_After()
	{
		String ret = (java.lang.String)this.getValueAfter("./parent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey get_Parent_Before()
	{
		String ret = (java.lang.String)this.getValueBefore("./parent");
		return ret != null ? new com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey(ret) : null;
	}

	public void set_Parent_FK(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey value)
	{
		this.setObject(value, "./parent", String.class);
	}

	public void set_Parent(final nl.systemation.training.ebx.mdm.commons.datamodel.Category value)
	{
		this.setObject((value != null && value.getPK() != null)? value.getPK().toString() : null, "./parent", String.class);
	}
	
	public com.onwbp.adaptation.Adaptation get_Parent_Record()
	{
		return super.getFK("./parent");
	}

	public boolean is_Parent_Changed()
	{
	    return (((String)this.getValueAfter("./parent")) != null && 
	        !((String)this.getValueAfter("./parent")).equals((this.getValueBefore("./parent"))));
	}

	public nl.systemation.training.ebx.mdm.commons.datamodel.Category get_Parent()
	{
		com.onwbp.adaptation.Adaptation record = super.getFK("./parent");
		return record != null ? new nl.systemation.training.ebx.mdm.commons.datamodel.Category(record) : null;
	}
	
	private static final com.orchestranetworks.schema.Path path_Parent = com.orchestranetworks.schema.Path.parse("./parent");
	public static final com.orchestranetworks.schema.Path getPath_Parent()
	{
		return path_Parent;
	}

   /* Extra method because this is a Parent relation
    */
	public java.util.List<com.onwbp.adaptation.Adaptation> get_Parent_ParentList()
	{
		return super.getParents("./parent");
	}

   /* Get and Set methods for attribute:  
    * Name (Standard)
    *
    */

	public String get_Name()
	{
		return this.getString("./name");
	}

	public String get_Name_After()
	{
		return (String)this.getValueAfter("./name");
	}

	public String get_Name_Before()
	{
		return (String)this.getValueBefore("./name");
	}

	public void set_Name(final String value)
	{
		this.setObject(value, "./name", String.class);
	}

	public boolean is_Name_Changed()
	{
	    return (((String)this.getValueAfter("./name")) != null && 
	        !((String)this.getValueAfter("./name")).equals((this.getValueBefore("./name"))));
	}

	private static final com.orchestranetworks.schema.Path path_Name = com.orchestranetworks.schema.Path.parse("./name");
	public static final com.orchestranetworks.schema.Path getPath_Name()
	{
		return path_Name;
	}

   /* Get and Set methods for attribute:  
    * Comment (Standard)
    *
    */

	public String get_Comment()
	{
		return this.getString("./comment");
	}

	public String get_Comment_After()
	{
		return (String)this.getValueAfter("./comment");
	}

	public String get_Comment_Before()
	{
		return (String)this.getValueBefore("./comment");
	}

	public void set_Comment(final String value)
	{
		this.setObject(value, "./comment", String.class);
	}

	public boolean is_Comment_Changed()
	{
	    return (((String)this.getValueAfter("./comment")) != null && 
	        !((String)this.getValueAfter("./comment")).equals((this.getValueBefore("./comment"))));
	}

	private static final com.orchestranetworks.schema.Path path_Comment = com.orchestranetworks.schema.Path.parse("./comment");
	public static final com.orchestranetworks.schema.Path getPath_Comment()
	{
		return path_Comment;
	}

}

