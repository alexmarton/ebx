package nl.systemation.training.ebx.mdm.commons.datamodel ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class represents a "Stores" record from the data model "My Store Data Model Development"
 * Is provides an easy to use interface to access the contents of a record
 *  
 * @author Systemation
 * @version 1.14
 * @date Wed Apr 15 13:50:33 CEST 2020
*/

public class Store extends com.ebx5.frameworkrt.datamodelgenerator.FrameworkTable
{
	public final static String TABLE_PATH = "/root/Store";

	public Store(final com.onwbp.adaptation.Adaptation dataSetOrRecord)
	{
		super(dataSetOrRecord);
	}

	public Store(final com.onwbp.adaptation.Adaptation record, final com.orchestranetworks.service.ProcedureContext procedureContext)
	{
		super(record, procedureContext);
	}

	public Store(final com.onwbp.adaptation.Adaptation dataSetOrRecord, final com.orchestranetworks.instance.ValueContext valueContext, final com.orchestranetworks.schema.trigger.ValueChanges changes)
	{
		super(dataSetOrRecord, valueContext, changes);
	}

	public Store(final com.ebx5.frameworkrt.datamodelgenerator.FrameworkKey foreignKey, com.onwbp.adaptation.Adaptation dataSet)
	{
		super(com.onwbp.adaptation.PrimaryKey.parseString(foreignKey.toString()), dataSet);
	}

	public Store(final com.onwbp.adaptation.PrimaryKey pk, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(pk, dataSet);
	}

	public Store(final String predicate, final com.onwbp.adaptation.Adaptation dataSet)
	{
		super(predicate, dataSet);
	}

	public Store(com.onwbp.adaptation.Adaptation dataSet, String xpathRecord) {
		super(dataSet, xpathRecord);
	}
	
	public Store(final com.orchestranetworks.instance.ValueContext valueContext)
	{
		super(valueContext);
	}

	@Override
	protected com.onwbp.adaptation.AdaptationTable getTable(final com.onwbp.adaptation.Adaptation dataSet)
	{
		return dataSet.getTable(getTablePath());
	}
	
	public static com.orchestranetworks.schema.Path getTablePath()
	{
		return com.orchestranetworks.schema.Path.parse(TABLE_PATH);
	}
	
	@Override
	public boolean equals(Object object) {
		return super.equals(object);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	

   /* Get and Set methods for attribute:  
    * Identifier (Standard)
    *
    */

	public Integer get_Identifier()
	{
		return this.getInteger("./identifier");
	}

	public Integer get_Identifier_After()
	{
		return (Integer)this.getValueAfter("./identifier");
	}

	public Integer get_Identifier_Before()
	{
		return (Integer)this.getValueBefore("./identifier");
	}

	public void set_Identifier(final Integer value)
	{
		this.setObject(value, "./identifier", Integer.class);
	}

	public boolean is_Identifier_Changed()
	{
	    return (((Integer)this.getValueAfter("./identifier")) != null && 
	        !((Integer)this.getValueAfter("./identifier")).equals((this.getValueBefore("./identifier"))));
	}

	private static final com.orchestranetworks.schema.Path path_Identifier = com.orchestranetworks.schema.Path.parse("./identifier");
	public static final com.orchestranetworks.schema.Path getPath_Identifier()
	{
		return path_Identifier;
	}

   /* Inventories (Association)
    *
    */

	public java.util.List<com.onwbp.adaptation.Adaptation> get_Inventories(){
		return super.getAssociation("./inventories");
	}

	private static final com.orchestranetworks.schema.Path path_Inventories = com.orchestranetworks.schema.Path.parse("./inventories");
	public static final com.orchestranetworks.schema.Path getPath_Inventories()
	{
		return path_Inventories;
	}

	private java.util.List<nl.systemation.training.ebx.mdm.commons.datamodel.Inventory> list_Inventories = new java.util.ArrayList<>();
	
	public void add_Inventories(nl.systemation.training.ebx.mdm.commons.datamodel.Inventory frameworkTable) {
		list_Inventories.add(frameworkTable);
	}
	
	public void add_Inventories(java.util.List<nl.systemation.training.ebx.mdm.commons.datamodel.Inventory> frameworkTableList) {
		list_Inventories.addAll(frameworkTableList);
	}

	public void clear_Inventories() {
		list_Inventories.clear();
	}
	
	public boolean has_Inventories() {
		return !list_Inventories.isEmpty();
	}
	
	public java.util.List<nl.systemation.training.ebx.mdm.commons.datamodel.Inventory> getList_Inventories(){
		return list_Inventories;
	}


   /* Get and Set methods for attribute:  
    * Name (Standard)
    *
    */

	public String get_Name()
	{
		return this.getString("./name");
	}

	public String get_Name_After()
	{
		return (String)this.getValueAfter("./name");
	}

	public String get_Name_Before()
	{
		return (String)this.getValueBefore("./name");
	}

	public void set_Name(final String value)
	{
		this.setObject(value, "./name", String.class);
	}

	public boolean is_Name_Changed()
	{
	    return (((String)this.getValueAfter("./name")) != null && 
	        !((String)this.getValueAfter("./name")).equals((this.getValueBefore("./name"))));
	}

	private static final com.orchestranetworks.schema.Path path_Name = com.orchestranetworks.schema.Path.parse("./name");
	public static final com.orchestranetworks.schema.Path getPath_Name()
	{
		return path_Name;
	}
	/*
	 * Enum Type Type, attribute: Type
	 *
	 */
	public enum Type
	{
		SUPERMARKET("Supermarket", "Supermarket"),
		HYPERMARKET("Hypermarket", "Hypermarket"),
		;
		private String label;
		private String value;

		private Type(final String value, final String label)
		{
			this.setValue(value);
			this.setLabel(label);
		}

		public String getLabel()
		{
			return this.label;
		}

		public String getValue()
		{
			return this.value;
		}

		private void setLabel(final String label)
		{
			this.label = label;
		}

		private void setValue(final String value)
		{
			this.value = value;
		}
	}

	public void set_Type(final Type value)
	{
		this.setObject(value.getValue(), "./type", String.class);
	}
	/*
	 * Enum method Type, attribute: Type value Supermarket
	 *
	 */

	public boolean is_Type_enum_Supermarket () {
		return get_Type().equals(Type.SUPERMARKET.getValue());
	}
	/*
	 * Enum method Type, attribute: Type value Hypermarket
	 *
	 */

	public boolean is_Type_enum_Hypermarket () {
		return get_Type().equals(Type.HYPERMARKET.getValue());
	}

   /* Get and Set methods for attribute:  
    * Type (Standard)
    *
    */

	public String get_Type()
	{
		return this.getString("./type");
	}

	public String get_Type_After()
	{
		return (String)this.getValueAfter("./type");
	}

	public String get_Type_Before()
	{
		return (String)this.getValueBefore("./type");
	}

	public void set_Type(final String value)
	{
		this.setObject(value, "./type", String.class);
	}

	public boolean is_Type_Changed()
	{
	    return (((String)this.getValueAfter("./type")) != null && 
	        !((String)this.getValueAfter("./type")).equals((this.getValueBefore("./type"))));
	}

	private static final com.orchestranetworks.schema.Path path_Type = com.orchestranetworks.schema.Path.parse("./type");
	public static final com.orchestranetworks.schema.Path getPath_Type()
	{
		return path_Type;
	}

   /* Get and Set methods for attribute:  
    * Street (Standard)
    *
    */

	public String get_Street()
	{
		return this.getString("./address/street");
	}

	public String get_Street_After()
	{
		return (String)this.getValueAfter("./address/street");
	}

	public String get_Street_Before()
	{
		return (String)this.getValueBefore("./address/street");
	}

	public void set_Street(final String value)
	{
		this.setObject(value, "./address/street", String.class);
	}

	public boolean is_Street_Changed()
	{
	    return (((String)this.getValueAfter("./address/street")) != null && 
	        !((String)this.getValueAfter("./address/street")).equals((this.getValueBefore("./address/street"))));
	}

	private static final com.orchestranetworks.schema.Path path_Street = com.orchestranetworks.schema.Path.parse("./address/street");
	public static final com.orchestranetworks.schema.Path getPath_Street()
	{
		return path_Street;
	}

   /* Get and Set methods for attribute:  
    * City (Standard)
    *
    */

	public String get_City()
	{
		return this.getString("./address/city");
	}

	public String get_City_After()
	{
		return (String)this.getValueAfter("./address/city");
	}

	public String get_City_Before()
	{
		return (String)this.getValueBefore("./address/city");
	}

	public void set_City(final String value)
	{
		this.setObject(value, "./address/city", String.class);
	}

	public boolean is_City_Changed()
	{
	    return (((String)this.getValueAfter("./address/city")) != null && 
	        !((String)this.getValueAfter("./address/city")).equals((this.getValueBefore("./address/city"))));
	}

	private static final com.orchestranetworks.schema.Path path_City = com.orchestranetworks.schema.Path.parse("./address/city");
	public static final com.orchestranetworks.schema.Path getPath_City()
	{
		return path_City;
	}

   /* Get and Set methods for attribute:  
    * State (Standard)
    *
    */

	public String get_State()
	{
		return this.getString("./address/state");
	}

	public String get_State_After()
	{
		return (String)this.getValueAfter("./address/state");
	}

	public String get_State_Before()
	{
		return (String)this.getValueBefore("./address/state");
	}

	public void set_State(final String value)
	{
		this.setObject(value, "./address/state", String.class);
	}

	public boolean is_State_Changed()
	{
	    return (((String)this.getValueAfter("./address/state")) != null && 
	        !((String)this.getValueAfter("./address/state")).equals((this.getValueBefore("./address/state"))));
	}

	private static final com.orchestranetworks.schema.Path path_State = com.orchestranetworks.schema.Path.parse("./address/state");
	public static final com.orchestranetworks.schema.Path getPath_State()
	{
		return path_State;
	}

   /* Get and Set methods for attribute:  
    * Postcode (Standard)
    *
    */

	public String get_Postcode()
	{
		return this.getString("./address/postcode");
	}

	public String get_Postcode_After()
	{
		return (String)this.getValueAfter("./address/postcode");
	}

	public String get_Postcode_Before()
	{
		return (String)this.getValueBefore("./address/postcode");
	}

	public void set_Postcode(final String value)
	{
		this.setObject(value, "./address/postcode", String.class);
	}

	public boolean is_Postcode_Changed()
	{
	    return (((String)this.getValueAfter("./address/postcode")) != null && 
	        !((String)this.getValueAfter("./address/postcode")).equals((this.getValueBefore("./address/postcode"))));
	}

	private static final com.orchestranetworks.schema.Path path_Postcode = com.orchestranetworks.schema.Path.parse("./address/postcode");
	public static final com.orchestranetworks.schema.Path getPath_Postcode()
	{
		return path_Postcode;
	}

   /* Get and Set methods for attribute:  
    * Country (Standard)
    *
    */

	public String get_Country()
	{
		return this.getString("./address/country");
	}

	public String get_Country_After()
	{
		return (String)this.getValueAfter("./address/country");
	}

	public String get_Country_Before()
	{
		return (String)this.getValueBefore("./address/country");
	}

	public void set_Country(final String value)
	{
		this.setObject(value, "./address/country", String.class);
	}

	public boolean is_Country_Changed()
	{
	    return (((String)this.getValueAfter("./address/country")) != null && 
	        !((String)this.getValueAfter("./address/country")).equals((this.getValueBefore("./address/country"))));
	}

	private static final com.orchestranetworks.schema.Path path_Country = com.orchestranetworks.schema.Path.parse("./address/country");
	public static final com.orchestranetworks.schema.Path getPath_Country()
	{
		return path_Country;
	}

   /* Group attribute template:  
    * Address (Group)
    *
    */

	private static final com.orchestranetworks.schema.Path pathGroup_Address = com.orchestranetworks.schema.Path.parse("./address");
	public static final com.orchestranetworks.schema.Path getGroupPath_Address()
	{
		return pathGroup_Address;
	}

   /* Association has Childs Code
    *
    */

	@Override
	public boolean hasChilds() {
		if (!list_Inventories.isEmpty()) {
			return true;
		}
		return false;
	}

   /* Association Create Parent and Child's Code
    *
    */

	@Override
	public com.onwbp.adaptation.Adaptation createParentAndChilds(com.onwbp.adaptation.AdaptationHome dataSpace, com.orchestranetworks.service.Session session, com.ebx5.frameworkrt.util.TransactionParameters transactionParameters, com.orchestranetworks.service.ProcedureContext procedureContext) throws com.orchestranetworks.service.OperationException {
		final CreateChilds procedure = new CreateChilds();
		procedure.parent = this;
		procedure.attributes = this.getAttributes();
		procedure.table = this.getTable();
		procedure.transactionParameters = transactionParameters;
		if (this.getProcedureContext() != null)
		{
			procedure.setProcedureContext(this.getProcedureContext());
			this.setRecord(procedure.createRecord());
		}
		else
		{
			com.orchestranetworks.service.ProgrammaticService ps = com.orchestranetworks.service.ProgrammaticService.createForSession(session, dataSpace);
			com.orchestranetworks.service.ProcedureResult pResult = ps.execute(procedure);
			if (!pResult.hasFailed())
			{
				this.setRecord(procedure.record);
			}
			else
			{
				com.orchestranetworks.service.OperationException oe = pResult.getException();
				if (oe != null) {
					throw oe; 
				}
				throw pResult.getConstraintViolationException();
			}
		}
		return procedure.record;
	}

	private class CreateChilds implements com.orchestranetworks.service.Procedure {
		private com.ebx5.frameworkrt.util.TransactionParameters transactionParameters = new com.ebx5.frameworkrt.util.TransactionParameters(); 
		private java.util.Map<com.orchestranetworks.schema.Path, Object> attributes = null;
		private com.orchestranetworks.service.ProcedureContext procedureContext = null;
		private com.onwbp.adaptation.Adaptation record = null;
		private com.onwbp.adaptation.AdaptationTable table = null;
		private Store parent = null;

		@Override
		public void execute(com.orchestranetworks.service.ProcedureContext context) throws Exception {
			this.record = this.createRecord(context);
		}

		public void setProcedureContext(final com.orchestranetworks.service.ProcedureContext procedureContext)
		{
			this.procedureContext = procedureContext;
		}

		public com.onwbp.adaptation.Adaptation createRecord() throws com.orchestranetworks.service.OperationException
		{
			this.record = this.createRecord(this.procedureContext);
			return this.record;
		}

		private com.onwbp.adaptation.Adaptation createRecord(final com.orchestranetworks.service.ProcedureContext context) throws com.orchestranetworks.service.OperationException
		{
			this.record = context.doCreateOccurrence(prepareCreate(context), this.table);
			
			Store newStore = new Store(this.record);

			for (nl.systemation.training.ebx.mdm.commons.datamodel.Inventory child : this.parent.list_Inventories) {
				
				child.set_Store(newStore);
				this.table = child.getTable();
				this.attributes = child.getAttributes();
				
				context.doCreateOccurrence(prepareCreate(context), this.table);
			}
			list_Inventories.clear();
			return this.record;
		}

		private com.orchestranetworks.service.ValueContextForUpdate prepareCreate(final com.orchestranetworks.service.ProcedureContext context) {
			
			com.orchestranetworks.service.ValueContextForUpdate contextForNewOccurrence = context.getContextForNewOccurrence(this.table);

			if (this.attributes == null)
			{
				return null;
			}

			for (java.util.Map.Entry<com.orchestranetworks.schema.Path, Object> entry : this.attributes.entrySet())
			{
				Object object = entry.getValue();
				com.orchestranetworks.schema.Path path = entry.getKey();
				if (transactionParameters.isAllPrivileges()) {
					contextForNewOccurrence.setPrivilegeForNode(path);
				}
				contextForNewOccurrence.setValue(object, path);
			}
			if (transactionParameters.getExecutionInfo() != null)
			{
				context.setExecutionInformation(transactionParameters.getExecutionInfo());
			}
			context.setCommitThreshold(transactionParameters.getCommitThreshold());
			context.setAllPrivileges(transactionParameters.isAllPrivileges());
			context.setTriggerActivation(transactionParameters.isTriggersActive());
			context.setHistoryActivation(transactionParameters.isHistoryActivated());
			context.setDatabaseHistoryActivation(transactionParameters.isDataBaseHistoryActivated());
			if (transactionParameters.isDisableDataSetValidation()) {
				context.disableDatasetValidation(transactionParameters.getDataSet());
			}
			
			return contextForNewOccurrence;
		}
	}

}

