package nl.systemation.training.ebx.mdm.commons.directory;
//package nl.minjenv.ebx.rdm.commons.directory;

import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.service.directory.Directory;
import com.orchestranetworks.service.directory.DirectoryFactory;

/**
 * trainingDirectoryFactory is a factory class to hook in the 
 * custom EpoDirectory as the default Directory handler 
 * for EBX. 
 * This class is referenced in ebx.properties
 * 
 * @author Leen Buizert
 *
 */
public class EbxDirectoryFactory extends DirectoryFactory
{
	@Override
	public Directory createDirectory(AdaptationHome home) throws Exception
	{
		return EbxDirectory.getInstance(home);
	}
}
