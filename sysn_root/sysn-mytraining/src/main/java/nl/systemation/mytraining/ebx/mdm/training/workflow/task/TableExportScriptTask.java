package nl.systemation.mytraining.ebx.mdm.training.workflow.task;

import com.ebx5.commons.procedure.ExportXMLProcedure;
import com.ebx5.commons.util.RepositoryUtils;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

import java.io.File;

public class TableExportScriptTask extends ScriptTaskBean {

    private String dataSpaceName;
    private String dataSetName;
    private String tablePath;
    private String filePath;

    public String getDataSpaceName() {
        return dataSpaceName;
    }

    public String getDataSetName() {
        return dataSetName;
    }

    public String getTablePath() {
        return tablePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setDataSpaceName(String dataSpaceName) {
        this.dataSpaceName = dataSpaceName;
    }

    public void setDataSetName(String dataSetName) {
        this.dataSetName = dataSetName;
    }

    public void setTablePath(String tablePath) {
        this.tablePath = tablePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void executeScript(ScriptTaskBeanContext context) throws OperationException {
        final AdaptationHome dataSpace = RepositoryUtils.getDataSpace(this.dataSpaceName);
        final Adaptation dataSet = RepositoryUtils.getDataSet(dataSpace, this.dataSetName);
        final AdaptationTable table = dataSet.getTable(Path.parse(this.tablePath));
        final File file = new File(this.filePath);
        final ExportSpec exportSpec = createExportSpec(table, file);
        exportToFile(context, dataSpace, exportSpec);

    }

    protected ExportSpec createExportSpec(final AdaptationTable table, final File file) {
        final ExportSpec exportSpec = new ExportSpec();
        final Request request = table.createRequest();

        exportSpec.setRequest(request);
        exportSpec.setCheckAccessRules(false);
        exportSpec.setContentIndented(true);
        exportSpec.setIncludesTechnicalData(false);
        exportSpec.setDestinationFile(file);

        return exportSpec;
    }

    private static void exportToFile(final ScriptTaskBeanContext context, final AdaptationHome dataSpace, final ExportSpec exportSpec) throws OperationException {
        final Session session = context.getSession();
        final ProgrammaticService service = ProgrammaticService.createForSession(session, dataSpace);
        ExportXMLProcedure exportXml = new ExportXMLProcedure();
        exportXml.setSpec(exportSpec);

        final ProcedureResult result = service.execute(exportXml);
        if (result.hasFailed()) {
            throw result.getException();
        }
    }
}
