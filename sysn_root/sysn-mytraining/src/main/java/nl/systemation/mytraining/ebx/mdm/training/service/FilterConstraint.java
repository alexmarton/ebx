package nl.systemation.mytraining.ebx.mdm.training.service;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;

import java.util.Date;
import java.util.Locale;

public class FilterConstraint implements Constraint<Object> {
    @Override
    public void checkOccurrence(Object o, ValueContextForValidation context) throws InvalidSchemaException {
        final Date startDate = (Date)  context.getValue(Path.PARENT.add(PurgeInventoryService._startDate));
        final Date endDate = (Date)  context.getValue(Path.PARENT.add(PurgeInventoryService._endDate));

        if((startDate != null) && (endDate != null) && startDate.compareTo(endDate) <= 0){
            return;
        }
        context.addMessage(UserMessage.createError("End date must be after start date."));
    }

    @Override
    public void setup(ConstraintContext constraintContext) {

    }

    @Override
    public String toUserDocumentation(Locale locale, ValueContext valueContext) throws InvalidSchemaException {
        return "Filter Constraint";
    }
}
