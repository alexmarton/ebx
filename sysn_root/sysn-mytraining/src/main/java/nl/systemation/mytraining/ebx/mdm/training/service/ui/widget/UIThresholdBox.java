package nl.systemation.mytraining.ebx.mdm.training.service.ui.widget;

import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.form.widget.*;

import java.math.BigDecimal;

public class UIThresholdBox extends UISimpleCustomWidget {

    private static final String TABLE_STYLE = "background-color:red; background-repeat: no-repeat; background-position: left center; padding-left:20px;";

    private final BigDecimal threshold;

    public UIThresholdBox(final WidgetFactoryContext widgetFactoryContext, final BigDecimal threshold) {
        super(widgetFactoryContext);
        this.threshold = threshold;
    }

    @Override
    public void write(WidgetWriter widgetWriter, WidgetDisplayContext widgetDisplayContext) {

        final ValueContext valueContext = widgetDisplayContext.getValueContext();
        final Object value = valueContext.getValue(Path.SELF);

        if(value != null && this.threshold.compareTo(new BigDecimal(value.toString())) >= 0){
            if(widgetDisplayContext.isDisplayedInTable()){
                widgetWriter.add("<span").addSafeAttribute("style", TABLE_STYLE).add(">");
                widgetWriter.addWidget(Path.SELF);
                widgetWriter.add("</span>");

            }else{
                UITextBox box = widgetWriter.newTextBox(Path.SELF);
                box.setForegroundColor("#FF0000");
                widgetWriter.addWidget(box);
            }
        }else{
            widgetWriter.addWidget(Path.SELF);
        }


    }
}
