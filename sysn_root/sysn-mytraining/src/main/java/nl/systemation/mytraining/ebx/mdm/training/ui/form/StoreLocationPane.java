package nl.systemation.mytraining.ebx.mdm.training.ui.form;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ui.form.UIFormContext;
import com.orchestranetworks.ui.form.UIFormPane;
import com.orchestranetworks.ui.form.UIFormPaneWriter;
import nl.systemation.training.ebx.mdm.commons.datamodel.Store;

import java.net.URLEncoder;
import java.text.MessageFormat;

public class StoreLocationPane implements UIFormPane {
    public static final String ADDRESS_STYLE = "margin:10px";

    public static final String MAP_STYLE = "width:500px; height:375px; margin:10px";

    public static final String MAP_URL = "https://maps.google.com/maps?q={0}+{1}+{2}+{3}+{4}&output=embed";

    public static final String URL_ENCODING = "UTF-8";

    @Override
    public void writePane(UIFormPaneWriter writer, UIFormContext context) {

        writer.add("<div").addSafeAttribute("style", ADDRESS_STYLE).add(">");
        writer.startTableFormRow();
        writer.addFormRow(Store.getPath_Street());
        writer.addFormRow(Store.getPath_City());
        writer.addFormRow(Store.getPath_State());
        writer.addFormRow(Store.getPath_Postcode());
        writer.addFormRow(Store.getPath_Country());
        writer.endTableFormRow();
        writer.add("</div>");

        final Adaptation storeRecord = context.getCurrentRecord();

        if (storeRecord == null) {
            return;
        }

        Store store = new Store(storeRecord);

        final String street = store.get_Street();
        final String city = store.get_City();
        final String state = store.get_State();
        final String postcode = store.get_Postcode();
        final String country = store.get_Country();

        final String mapURL = MessageFormat.format(MAP_URL, encode(street), encode(city), encode(state),
                encode(postcode), encode(country));

        writer.add("<iframe").addSafeAttribute("style", MAP_STYLE).addSafeAttribute("src", mapURL).add(">");
        writer.add("</iframe>");
    }

    private static String encode(final String string) {
        try {
            return URLEncoder.encode(string, URL_ENCODING);
        } catch (final Exception exception) {
            return "";
        }
    }
}
