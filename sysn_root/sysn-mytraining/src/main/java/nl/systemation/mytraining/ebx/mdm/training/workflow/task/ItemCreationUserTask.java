package nl.systemation.mytraining.ebx.mdm.training.workflow.task;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.UserTaskWorkItemCompletionContext;

public class ItemCreationUserTask extends UserTask {
    private static final String STEWARD = "steward";

    @Override
    public void handleWorkItemCompletion(UserTaskWorkItemCompletionContext context) throws OperationException {
        final WorkItem workItem = context.getCompletedWorkItem();

        final UserReference userReference = workItem.getUserReference();

        final String userIdentifier = userReference.getUserId();

        context.setVariableString(STEWARD, userIdentifier);

        super.handleWorkItemCompletion(context);
    }
}
