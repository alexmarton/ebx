package nl.systemation.mytraining.ebx.mdm.training.service.declaration;

import com.orchestranetworks.schema.types.dataspace.DataspaceSet.DataspaceType;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;
import com.orchestranetworks.userservice.declaration.ActivationContextOnTableView;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import com.orchestranetworks.userservice.declaration.UserServicePropertiesDefinitionContext;
import com.orchestranetworks.userservice.declaration.WebComponentDeclarationContext;

import nl.systemation.mytraining.ebx.mdm.training.service.PurgeInventoryService;
import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;

/***
 * <p>
 * <UserService TableViewEntitySelection Single Page>
 * <p>
 * 
 * @author alexm
 */
public class PurgeInventoryDeclaration implements UserServiceDeclaration.OnTableView {

	public static final ServiceKey SERVICE_KEY = ServiceKey.forName("PurgeInventories");

    @Override
    public ServiceKey getServiceKey() {
        return PurgeInventoryDeclaration.SERVICE_KEY;
    }

    @Override
    public UserService<TableViewEntitySelection> createUserService() {
//        return new ServiceNameToCall();
    	return new PurgeInventoryService();
    }

    @Override
    public void defineActivation(ActivationContextOnTableView context) {
        context.includeAllDatasets();
        context.includeAllDataspaces(DataspaceType.BRANCH);
        context.includeSchemaNodesMatching(Inventory.getTablePath());
        // limit selection to 1 record
        //context.limitRecordSelection(1);


    }

    @Override
    public void defineProperties(UserServicePropertiesDefinitionContext context) {
        context.setDescription("Description");
        context.setLabel("Purge Inventory");

    }

    @Override
    public void declareWebComponent(WebComponentDeclarationContext context) {
        context.setAvailableAsWorkflowUserTask(false);
        context.setAvailableAsPerspectiveAction(false);

    }
}