package nl.systemation.mytraining.ebx.mdm.training.service.ui.widget;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.form.widget.UIWidgetFactory;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetFactorySetupContext;

import java.math.BigDecimal;

public class UIThresholdBoxFactory implements UIWidgetFactory<UIThresholdBox> {

    private BigDecimal threshold;

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    @Override
    public UIThresholdBox newInstance(WidgetFactoryContext widgetFactoryContext) {
        return new UIThresholdBox(widgetFactoryContext, this.threshold);
    }

    @Override
    public void setup(WidgetFactorySetupContext widgetFactorySetupContext) {

        if(this.threshold == null){
            final UserMessage message = UserMessage.createError("Threshold is Mandatory");
            widgetFactorySetupContext.addMessage(message);
        }
    }
}
