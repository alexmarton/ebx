package nl.systemation.mytraining.ebx.mdm.training.trigger;

import java.util.Date;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChanges;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;

/***
 * <trigger description>
 * 
 * @author alexm
 */

public class InventoryTrigger extends TableTrigger {

	/**
	 * Describe handle new context
	 */
	@Override
	public void handleNewContext(final NewTransientOccurrenceContext context) {
		//super.handleNewContext(context);
		final ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		// code to create default values for new records

		// no explicit update required because table is based on value context for update 
		//AframeworkTable frameworkTable = new AframeworkTable(vc);
		//frameworkTable.set_Field(value);
	}

	/**
	 * Describe before create code
	 */
	@Override
	public void handleBeforeCreate(final BeforeCreateOccurrenceContext context) throws OperationException {
		final Session session = context.getSession();
		final AdaptationHome dataSpace = context.getAdaptationHome();
		final Adaptation dataSet = context.getOccurrenceContext().getAdaptationInstance();
		final ValueContext vc = context.getOccurrenceContext();

	}

	/**
	 * Describe after create code
	 */
	@Override
	public void handleAfterCreate(final AfterCreateOccurrenceContext context) throws OperationException {
		final Session session = context.getSession();
		final AdaptationHome dataSpace = context.getAdaptationHome();
		final Adaptation dataSet = context.getOccurrenceContext().getAdaptationInstance();
		final Adaptation record = context.getAdaptationOccurrence();
		final ProcedureContext procedureContext = context.getProcedureContext();
	}

	/**
	 * Describe before delete code
	 */
	@Override
	public void handleBeforeDelete(final BeforeDeleteOccurrenceContext context) {
		final AdaptationHome dataSpace = context.getAdaptationHome();
		final Adaptation dataSet = context.getOccurrenceContext().getAdaptationInstance();
		final Session session = context.getSession();
		final Adaptation record = context.getAdaptationOccurrence();
	}

	/**
	 * Describe after delete code
	 */
	@Override
	public void handleAfterDelete(final AfterDeleteOccurrenceContext context) {
		final AdaptationHome dataSpace = context.getAdaptationHome();
		final Adaptation dataSet = context.getOccurrenceContext().getAdaptationInstance();
		final Session session = context.getSession();
		final ProcedureContext procedureContext = context.getProcedureContext();
		// do not implement standard cascading or restricted deletes here, 
		// use the commonDeleteTrigger or data model features in stead

	}


	/**
	 * Describe after modify code
	 */
	@Override
	public void handleAfterModify(final AfterModifyOccurrenceContext context) throws OperationException {
		final Adaptation record = context.getAdaptationOccurrence();
		final AdaptationHome dataSpace = context.getAdaptationHome();
		final Adaptation dataSet = context.getOccurrenceContext().getAdaptationInstance();
		final ProcedureContext procedureContext = context.getProcedureContext();
		final ValueChanges valueChanges = context.getChanges();
		final ValueContext valueContext = context.getOccurrenceContext();
		final Session session = context.getSession();
		
		/*
		
		Inventory inventory = new Inventory(record);
		inventory.set_Modified(new Date());
		inventory.setProcedureContext(procedureContext);
		inventory.update(session, dataSpace);

*/
		// AframeworkTable frameworkTableSelect = new AframeworkTable(dataSet, valueContext, valueChanges);
		// check if a field is changed
		// if (frameworkTableSelect.is_Field_Changed())
		// {
		//    write some code to act on the modification of a single field
		// }
	}

	/**
	 * Describe setup code
	 */
	@Override
	public void setup(final TriggerSetupContext context) {
		// setup code, only executed once
	}
}
