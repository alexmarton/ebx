package nl.systemation.mytraining.ebx.mdm.training.ui.form;

import com.orchestranetworks.ui.form.UIFormContext;
import com.orchestranetworks.ui.form.UIFormPane;
import com.orchestranetworks.ui.form.UIFormPaneWriter;
import nl.systemation.training.ebx.mdm.commons.datamodel.Store;

public class StoreInventoriesPane implements UIFormPane {
    @Override
    public void writePane(UIFormPaneWriter uiFormPaneWriter, UIFormContext uiFormContext) {
        uiFormPaneWriter.addWidget(Store.getPath_Inventories());
    }
}
