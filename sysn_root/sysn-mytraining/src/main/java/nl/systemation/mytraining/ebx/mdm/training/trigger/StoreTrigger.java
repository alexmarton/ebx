package nl.systemation.mytraining.ebx.mdm.training.trigger;



import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;

import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;

import nl.systemation.training.ebx.mdm.commons.datamodel.Store;

/***
 * <trigger description>
 * 
 * @author alexm
 */

public class StoreTrigger extends TableTrigger {


	/**
	 * Describe after create code
	 */
	@Override
	public void handleAfterCreate(final AfterCreateOccurrenceContext context) throws OperationException {
		final Session session = context.getSession();
		final AdaptationHome dataSpace = context.getAdaptationHome();
		final Adaptation dataSet = context.getOccurrenceContext().getAdaptationInstance();
		final Adaptation record = context.getAdaptationOccurrence();
		final ProcedureContext procedureContext = context.getProcedureContext();
		
		Store store = new Store(record);
		
		if(store.get_Postcode() != null) {
			store.set_Postcode(store.get_Postcode().replaceAll("\\s+",""));
			store.setProcedureContext(procedureContext);
			store.update(session, dataSpace);
		}
		
	}


	/**
	 * Describe after modify code
	 */
	@Override
	public void handleAfterModify(final AfterModifyOccurrenceContext context) throws OperationException {
		final Adaptation record = context.getAdaptationOccurrence();
		final AdaptationHome dataSpace = context.getAdaptationHome();
		final ProcedureContext procedureContext = context.getProcedureContext();
		final Session session = context.getSession();
		
		
		Store store = new Store(record);	
		store.set_Postcode(store.get_Postcode().replaceAll("\\s+",""));
		store.setProcedureContext(procedureContext);
		store.update(session, dataSpace);

	}

	/**
	 * Describe setup code
	 */
	@Override
	public void setup(final TriggerSetupContext context) {
		// setup code, only executed once
	}

}
