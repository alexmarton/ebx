package nl.systemation.mytraining.ebx.mdm.training.extension;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;
import com.orchestranetworks.service.AccessRule;

import nl.systemation.mytraining.ebx.mdm.training.permision.InventoryAccessRule;
import nl.systemation.mytraining.ebx.mdm.training.service.declaration.CountItemsDeclaration;
import nl.systemation.mytraining.ebx.mdm.training.service.declaration.PurgeInventoryDeclaration;
import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;



public class MyTrainingExtensions implements SchemaExtensions {

	@Override
	public void defineExtensions(SchemaExtensionsContext aContext) {
		
		final Path path = Inventory.getTablePath().add(Inventory.getPath_Price());
		final AccessRule rule = new InventoryAccessRule();
		
		aContext.setAccessRuleOnNode(path, rule);
		aContext.registerUserService(new PurgeInventoryDeclaration());
		aContext.registerUserService(new CountItemsDeclaration());
		
	}
	

}
