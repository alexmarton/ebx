package nl.systemation.mytraining.ebx.mdm.training.constraint;

import java.util.List;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;
import nl.systemation.training.ebx.mdm.commons.datamodel.Item;

/***
 * <p>
 * <Constraint On Table With Record Level Check description>
 * <p>
 * Parameter description:
 * <ul>
 * <li>p1: description1
 * <li>p2: description2
 * </ul>
 * 
 * @author alexm
 */
public class ItemStockConstraint implements ConstraintOnTable {

	@Override
	public void checkTable(final ValueContextForValidationOnTable context) {
		Adaptation dataSet = context.getTable().getContainerAdaptation();
		AdaptationHome dataSpace = context.getTable().getContainerAdaptation().getHome();

		Item itemSelect = new Item(dataSet);

		for (Adaptation itemRecord : itemSelect.selectRecordList()){

		    Item item = new Item(itemRecord);
            Inventory inventorySelect = new Inventory(dataSet);
            inventorySelect.set_Item(item);
            List<Adaptation> inventoryRecords =  inventorySelect.selectRecordList();

            if(inventoryRecords.size() == 0){

                context.addMessage(UserMessage.createWarning("No Inventory found."));
            }
            int inventoryCount =0;
            for (Adaptation inventoryRecord : inventoryRecords){
                Inventory inventory = new Inventory(inventoryRecord);
                inventoryCount += inventory.get_Stock().intValue();
            }

            if(inventoryCount == 0){
                context.addMessage(UserMessage.createWarning("Inventory found, but empty"));
            }


        }

	}

	/***
	 * Describe the setup method
	 * @param context the method context
	 */
	@Override
	public void setup(final ConstraintContextOnTable context) {
		// implement add dependency to the context to force the business to be run when these events happen
		context.addDependencyToInsertDeleteAndModify(context.getSchemaNode());
		// set the block commit to a certain value 
//		context.setBlocksCommitToNever();
	}

	/***
	 * Describe the user documentation method
	 * @param valueContext the record value context
	 * @param locale the current locale
	 */
	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext valueContext)
			throws InvalidSchemaException {
		return "Check inventory Constraint";
	}


}
