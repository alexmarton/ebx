package nl.systemation.mytraining.ebx.mdm.training.constraint;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;


import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.InvalidSchemaException;

import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;


/***
 * <p>
 * <Constraint description>
 * <p>
 * Parameter description:
 * <ul>
 * <li>p1: description1
 * <li>p2: description2
 * </ul>
 * 
 * @author alexm
 */

public class InventoryPriceConstraint implements Constraint<Object> {

	private BigDecimal tolerance;

	public void setTolerance(final BigDecimal tolerance) {
		this.tolerance = tolerance;
	}
	
	public BigDecimal getTolerance() {
		return tolerance;
	}
	
	
	
	@Override
	public void checkOccurrence(final Object value, final ValueContextForValidation validationContext)	throws InvalidSchemaException {
		
		//Adaptation dataSet = validationContext.getAdaptationInstance();
		
		Inventory inventory = new Inventory(validationContext);
		
		if(inventory.get_Price() == null || inventory.get_Item().get_DefaultPrice() == null) {
			return;
		}
		
		final Double price = inventory.get_Price().doubleValue();
		final Double defaultPrice = inventory.get_Item().get_DefaultPrice().doubleValue();
		
		if(Math.abs(price - defaultPrice) > (this.tolerance.doubleValue() * defaultPrice)) {
			final NumberFormat format = NumberFormat.getInstance();
			
			final UserMessage message = UserMessage.createError("The price must not differ from the default price more than"+ 
			format.format(this.tolerance.doubleValue() * 100) +"%");
			
			validationContext.addMessage(message);
					
		}	

	}


	@Override
	public void setup(final ConstraintContext context) {
		if ((getTolerance() == null) || (getTolerance().doubleValue() < 0)) {
			context.addMessage(UserMessage.createError("Tolerance parameter must be a positive decimal"));
		}
	}

	/***
	 * Describe the user documentation method
	 * @param valueContext the record value context
	 * @param locale the current locale
	 */
	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext valueContext)
			throws InvalidSchemaException {
		return "Tolerance parameter must be a positive decimal";
	}
}