package nl.systemation.mytraining.ebx.mdm.training.workflow.task;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.CreationWorkItemSpec;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.UserTaskCreationContext;

public class ItemCorrectionUserTask extends UserTask {

    private static final String STEWARD = "steward";

    @Override
    public void handleCreate(UserTaskCreationContext context) throws OperationException {
        final String userIdentifier = context.getVariableString(STEWARD);

        final UserReference userReference = UserReference.forUser(userIdentifier);

        final CreationWorkItemSpec creationWorkItemSpec = CreationWorkItemSpec.forAllocation(userReference);

        context.createWorkItem(creationWorkItemSpec);
    }
}
