package nl.systemation.mytraining.ebx.mdm.training.constraint;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;

import nl.systemation.training.ebx.mdm.commons.datamodel.Category;

/***
 * <p>
 * <Constraint On Table With Record Level Check description>
 * <p>
 * Parameter description:
 * <ul>
 * <li>p1: description1
 * <li>p2: description2
 * </ul>
 * 
 * @author alexm
 */
public class CategoryTableConstraint implements ConstraintOnTable {


	/***
	 * Describe table record
	 * @param context the method context
	 */
	@Override
	public void checkTable(final ValueContextForValidationOnTable context) {
		
		Adaptation dataSet = context.getTable().getContainerAdaptation();
		
		List<Adaptation> dupRecords = new ArrayList<>();
		Category categorySelect = new Category(dataSet);
		
		for (Adaptation categoryRecord : categorySelect.selectRecordList()) {			
			Category category = new Category(categoryRecord);
			
			Category categorySelectDuplicate = new Category(dataSet);
			categorySelectDuplicate.set_Parent(category.get_Parent());
			categorySelectDuplicate.set_Name(category.get_Name());
			
			List<Adaptation> dupRecordsFnd = categorySelectDuplicate.selectRecordList();
			
			if(categorySelectDuplicate.selectRecordList().size() > 1 && !dupRecords.contains(categoryRecord)) {
				context.addMessage(dupRecordsFnd, UserMessage.createError("Category can not have the same name with the same parent"));
				dupRecords.addAll(dupRecordsFnd);
			}
			
		}

	}

	/***
	 * Describe the setup method
	 * @param context the method context
	 */
	@Override
	public void setup(final ConstraintContextOnTable context) {
		// implement add dependency to the context to force the business to be run when these events happen
		context.addDependencyToInsertDeleteAndModify(context.getSchemaNode());
		// set the block commit to a certain value 
//		context.setBlocksCommitToNever();
	}

	/***
	 * Describe the user documentation method
	 * @param valueContext the record value context
	 * @param locale the current locale
	 */
	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext valueContext)
			throws InvalidSchemaException {
		return "Check for uniqueness";
	}

	/**
	 * Best practice to implement the actual record check in a single method.
	 * Call this method from the check table and check record methods
	 * 
	 * @param record the record to check
	 * @param dataSet the dataSet for this record
	 * 
	 */
	//	private UserMessage checkRecord(final FrameworkTableObject frameworkTableObject, final Adaptation dataSet) {
	//		// implement the record check here
	//		UserMessage userMessage = UserMessage.createError("<error message>");
	//		return userMessage;
	//	}
}
