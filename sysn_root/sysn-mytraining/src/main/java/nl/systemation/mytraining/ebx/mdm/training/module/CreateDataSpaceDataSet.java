package nl.systemation.mytraining.ebx.mdm.training.module ;

/** 
 * WARNING: Any manual changes to this class may be overwritten by the Framework Data Model Generation process. 
 * DO NOT MODIFY THIS CLASS.
 
 * <P>This class generates data spaces and data sets on initial module startup
 *  
 * @author Systemation
 * @version 1.14
 * @date Fri Apr 17 14:42:20 CEST 2020
*/

public class CreateDataSpaceDataSet {

	private CreateDataSpaceDataSet() {
		// do nothing
	}

	public static void createDataSpaceDataSet(com.orchestranetworks.module.ModuleContextOnRepositoryStartup context) throws com.orchestranetworks.service.OperationException {
		com.orchestranetworks.instance.Repository repository = context.getRepository();
		com.orchestranetworks.service.Session systemSession = context.createSystemUserSession("Create data spaces and data sets");

   /* Generate the data space and data set creation line for: 
    * Data space: MyStoreDataModel-Development
    * Data set: MyStoreDataModel-Development
    * Relational: false
    */
		com.ebx5.frameworkrt.util.ModuleRegistrationUtils.initiateDataSpaceDataSet(repository, systemSession, "MyStoreDataModel-Development", new String[] {"en-US",}, new String []{"My Store Data Model Development",}, "MyStoreDataModel-Development", new String[] {"en-US",} , new String []{"My Store Data Model Development",}, "/WEB-INF/ebx/schemas/MyStoreDataModel-Development.xsd", context.getModuleName(), false);
	}
}

