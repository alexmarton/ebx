package nl.systemation.mytraining.ebx.mdm.training.service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.schema.dynamic.BeanDefinition;
import com.orchestranetworks.schema.dynamic.BeanElement;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;
import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;


public class PurgeInventoryService implements UserService<TableViewEntitySelection> {

	public final static Path _startDate = Path.parse("startDate");
	public final static Path _endDate = Path.parse("endDate");
	public final static ObjectKey _objectKey = ObjectKey.forName("PurgeInventoryFilterService");

	private static final Format DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");


	private DisplayStep currentStep;

	public PurgeInventoryService() {

		this.currentStep = new PurgeInventoryFilterService();
	}
	

	@Override
	public UserServiceEventOutcome processEventOutcome(
			final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> aContext,
			final UserServiceEventOutcome anEventOutcome) {

		if (!(anEventOutcome instanceof DisplayStep.EventOutcome)) {
			return anEventOutcome;
		}

		switch ((DisplayStep.EventOutcome) anEventOutcome) {

			case DISPLAY_FILTER:
				this.currentStep = new PurgeInventoryFilterService();
				return null;

			case DISPLAY_CONFIRMATION:
				final int inventoriesCount = createRequestResult(aContext).getSize();
				this.currentStep = new PurgeInventoryDisplayConfirmation(inventoriesCount);
				return null;

			case DISPLAY_RESULT:
				final ProcedureResult result = purgeInventories(aContext);
				this.currentStep = new DisplayPurgeResultStep(result);
				return null;

			default:
				return null;
		}


	}

	@Override
	public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> aContext,
			UserServiceDisplayConfigurator aConfigurator) {
		this.currentStep.setupDisplay(aContext,aConfigurator);

	}

	@Override
	public void setupObjectContext(UserServiceSetupObjectContext<TableViewEntitySelection> aContext,
			UserServiceObjectContextBuilder aBuilder) {

		if (!aContext.isInitialDisplay()) {
			return;
		}

		final BeanDefinition definition = aBuilder.createBeanDefinition();

		{
			final BeanElement element = definition.createElement(_startDate, SchemaTypeName.XS_DATETIME);
			final UserMessage message = UserMessage.createInfo("Start Date");

			element.setLabel(message);
			element.setMinOccurs(1);
			element.setMaxOccurs(1);
			element.setDefaultValue(new Date());
			element.addFacetConstraint(FilterConstraint.class);
		}

		{
			final BeanElement element = definition.createElement(_endDate, SchemaTypeName.XS_DATETIME);
			final UserMessage message = UserMessage.createInfo("End Date");

			element.setLabel(message);
			element.setMinOccurs(1);
			element.setMaxOccurs(1);
			element.setDefaultValue(new Date());
			element.addFacetConstraint(FilterConstraint.class);
		}

		aBuilder.registerBean(_objectKey, definition);

	}

	@Override
	public void validate(UserServiceValidateContext<TableViewEntitySelection> aContext) {
		// Nothing to do

	}

	private static RequestResult createRequestResult(
			final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {

		final String stockPath = Inventory.getPath_Stock().format();
		final String modifiedPath = Inventory.getPath_Modified().format();
		final ValueContextForValidation valueContext = context.getValueContext(_objectKey);
		final String startDate = DATE_FORMAT.format(valueContext.getValue(_startDate));
		final String endDate = DATE_FORMAT.format(valueContext.getValue(_endDate));

		final String condition = stockPath + "='0' and date-greater-than(" + modifiedPath + ",'" + startDate
				+ "') and date-less-than(" + modifiedPath + ",'" + endDate + "')";
		Inventory inventory = new Inventory(context.getEntitySelection().getDataset());
		inventory.setPredicate(condition);

		return inventory.selectRecordRequestResult();
	}

	private static ProcedureResult purgeInventories(
			final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {
		final RequestResult result = createRequestResult(context);

		final Procedure procedure = new Procedure() {
			public void execute(final ProcedureContext procedureContext) throws Exception {
				try {
					for (Adaptation inventory; (inventory = result.nextAdaptation()) != null;) {
						final AdaptationName inventoryName = inventory.getAdaptationName();

						procedureContext.doDelete(inventoryName, false);
					}
				} finally {
					result.close();
				}
			}
		};

		final UserServiceTransaction transaction = context.createTransaction();
		transaction.add(procedure);

		return transaction.execute();
	}

}
