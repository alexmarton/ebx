package nl.systemation.mytraining.ebx.mdm.training.service;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.UIButtonSpec;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;

public class CountItemsDisplayView implements DisplayStep {

    private final int inventoriesCount;

    public CountItemsDisplayView(int inventoriesCount) {
        this.inventoriesCount = inventoriesCount;
    }
    @Override
    public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> context, UserServiceDisplayConfigurator config) {
        config.setContent(this::writeContent);

        {
            final UIButtonSpec cancelButtonSpec = config.newCancelButton();
            config.setLeftButtons(cancelButtonSpec);
        }

    }

    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
        final UserMessage message = UserMessage.createInfo("You have "+ this.inventoriesCount+" items in your database: ");
        final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

        writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
        writer.addUILabel(labelSpec);
        writer.add("</div>");
    }


}
