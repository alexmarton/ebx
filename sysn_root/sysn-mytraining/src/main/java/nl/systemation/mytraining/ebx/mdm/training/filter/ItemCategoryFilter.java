package nl.systemation.mytraining.ebx.mdm.training.filter;

import java.util.Locale;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.TableRefFilter;
import com.orchestranetworks.schema.TableRefFilterContext;

import nl.systemation.training.ebx.mdm.commons.datamodel.Category;

/***
 * <p>
 * <TableRefFilter description>
 * <p>
 * Parameter description:
 * <ul>
 * <li>p1: description1
 * <li>p2: description2
 * </ul>
 * 
 * @author alexm
 * 
 */ 

public class ItemCategoryFilter implements TableRefFilter {
	
	

	@Override
	public void setup(TableRefFilterContext context) {
		final SchemaNode itemCategoryNode = context.getSchemaNode();
		final SchemaNode categorieNode = itemCategoryNode.getNode(Category.getTablePath());
		context.addDependencyToInsertDeleteAndModify(categorieNode);
		
	}

	/***
	 * Describe the user documentation method
	 * @param valueContext the record value context
	 * @param locale the current locale
	 */
	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext valueContext)
			throws InvalidSchemaException {
		return "The category must be a leaf category";
	}
	
	@Override
	public boolean accept(final Adaptation record, final ValueContext valueContext) {
		// AdaptationHome dataSpace = valueContext.getHome();
		Adaptation dataSet = valueContext.getAdaptationInstance();
		
		// create the framework object Category from the adaptation record
		Category category = new Category(record);
		
		// create a selection object based on this data set
		Category categorySelection = new Category(dataSet);
		
		// set the current category as parent
		categorySelection.set_Parent(category);
		
		// return true if selection is false, false when this category is used as a parent
		return !categorySelection.selectFirstRecord();
	}
}
