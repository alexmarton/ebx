package nl.systemation.mytraining.ebx.mdm.training.constraint;

import java.math.BigDecimal;
import java.util.Locale;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.InvalidSchemaException;

import nl.systemation.training.ebx.mdm.commons.datamodel.Item;

/***
 * <p>
 * <Constraint description>
 * <p>
 * Parameter description:
 * <ul>
 * <li>p1: description1
 * <li>p2: description2
 * </ul>
 * 
 * @author alexm
 */

public class ItemPriceConstraint implements Constraint<Object> {

	private BigDecimal tolerance;

	public void setTolerance(final BigDecimal tolerance) {
		this.tolerance = tolerance;
	}
	
	public BigDecimal getTolerance() {
		return tolerance;
	}
	
	
	@Override
	public void checkOccurrence(final Object value, final ValueContextForValidation validationContext)
			throws InvalidSchemaException {
		

		Item item = new Item(validationContext);
		
		if(item.get_DefaultPrice() == null) {
			return;
		}
		
		final Double defaultPrice = item.get_DefaultPrice().doubleValue();
		
		if(defaultPrice < 0) {			
			final UserMessage message = UserMessage.createError("The price must be positive");			
			validationContext.addMessage(message);
					
		}	
		
		if(defaultPrice > getTolerance().doubleValue()) {
			
			final UserMessage message = UserMessage.createError("The price must not bigger than 10.000");			
			validationContext.addMessage(message);
					
		}	
	}

	/***
	 * Describe the setup method
	 * @param context the method context
	 */
	@Override
	public void setup(final ConstraintContext context) {
		if ((getTolerance() == null) || (getTolerance().doubleValue() < 0)) {
			context.addMessage(UserMessage.createError("A value should be added"));
		}
	}

	/***
	 * Describe the user documentation method
	 * @param valueContext the record value context
	 * @param locale the current locale
	 */
	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext valueContext)
			throws InvalidSchemaException {
		return "<descript the enum here>";
	}
}