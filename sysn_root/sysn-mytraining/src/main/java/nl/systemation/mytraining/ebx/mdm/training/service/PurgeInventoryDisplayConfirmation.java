package nl.systemation.mytraining.ebx.mdm.training.service;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;

public class PurgeInventoryDisplayConfirmation implements DisplayStep {
    private final int inventoriesCount;
    
    public PurgeInventoryDisplayConfirmation(int inventoriesCount) {
        this.inventoriesCount = inventoriesCount;
    }

    @Override
    public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> context, UserServiceDisplayConfigurator config) {
    	
        config.setContent(this::writeContent);

        {
            final UIButtonSpecNavigation backButtonSpec = config.newBackButton(this::onBackPressed);
            final UIButtonSpecNavigation cancelButtonSpec = config.newCancelButton();

            backButtonSpec.setDefaultButton(this.inventoriesCount == 0);
            config.setLeftButtons(backButtonSpec, cancelButtonSpec);
        }

        {
            final UserMessage message = UserMessage.createInfo("Purge");
            final UIButtonSpecNavigation purgeButtonSpec = config.newActionButton(message, this::onPurgePressed);

            purgeButtonSpec.setDefaultButton(this.inventoriesCount > 0);
            purgeButtonSpec.setDisabled(this.inventoriesCount == 0);
            config.setRightButtons(purgeButtonSpec);
        }
    }

    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
        final UserMessage message = UserMessage.createInfo("This number of empty Inventories will be purged: "+ this.inventoriesCount);
        final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

        writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
        writer.addUILabel(labelSpec);
        writer.add("</div>");
    }

    private UserServiceEventOutcome onBackPressed(final UserServiceEventContext context) {
        return EventOutcome.DISPLAY_FILTER;
    }

    private UserServiceEventOutcome onPurgePressed(final UserServiceEventContext context) {
        return EventOutcome.DISPLAY_RESULT;
    }
}
