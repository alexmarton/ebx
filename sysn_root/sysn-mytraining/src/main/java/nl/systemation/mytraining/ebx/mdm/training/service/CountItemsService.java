package nl.systemation.mytraining.ebx.mdm.training.service;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.UIButtonSpec;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;
import nl.systemation.training.ebx.mdm.commons.datamodel.Item;

import java.util.List;

public class CountItemsService implements UserService<TableViewEntitySelection> {


    public final static ObjectKey _objectKey = ObjectKey.forName("CountItemsService");

    private int size = 0;

    @Override
    public void setupObjectContext(UserServiceSetupObjectContext<TableViewEntitySelection> userServiceSetupObjectContext, UserServiceObjectContextBuilder userServiceObjectContextBuilder) {

         Adaptation dataSet = userServiceSetupObjectContext.getEntitySelection().getDataset();
         Item itemSelect = new Item(dataSet);
         List<Adaptation> result = itemSelect.selectRecordList();
         this.size = result.size();
    }

    @Override
    public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> aContext, UserServiceDisplayConfigurator aConfigurator) {
        aConfigurator.setContent(this::writeContent);
//        this.currentStep.setupDisplay(aContext,aConfigurator);
        //add the close button
        final UIButtonSpec closeButtonSpec = aConfigurator.newCloseButton();
        aConfigurator.setRightButtons(closeButtonSpec);
    }

    @Override
    public void validate(UserServiceValidateContext<TableViewEntitySelection> userServiceValidateContext) {

    }

    @Override
    public UserServiceEventOutcome processEventOutcome(UserServiceProcessEventOutcomeContext<TableViewEntitySelection> aContext,
                                                       UserServiceEventOutcome anEventOutcome) {
        return anEventOutcome;

    }

    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
        writer.setCurrentObject(PurgeInventoryService._objectKey);
        final UIFormLabelSpec labelSpec = new UIFormLabelSpec(UserMessage.createInfo("Number of items: " + this.size));

        writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
        writer.addUILabel(labelSpec);
        writer.add("</div>");
    }


    private static RequestResult createRequestResult(
            final UserServiceProcessEventOutcomeContext<TableViewEntitySelection> context) {
        Item items = new Item(context.getEntitySelection().getDataset());
        return items.selectRecordRequestResult();
    }
}
