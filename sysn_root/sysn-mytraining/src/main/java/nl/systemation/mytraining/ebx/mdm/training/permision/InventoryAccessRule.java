package nl.systemation.mytraining.ebx.mdm.training.permision;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.Session;

import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;

/***
 * <p>
 * <Access Rule description>
 * <p>
 * Parameter description:
 * <ul>
 * <li>p1: description1
 * <li>p2: description2
 * </ul>
 * 
 * Make sure you also define your access rules on the applicable data model path in your SchemaExtensions class in the method defineExtensions.
 * For example:
 * <pre>
 * context.setAccessRuleOnNodee(certainNode, new InventoryAccessRule());
 * </pre>
 * 
 * 
 * @author alexm
 */

public class InventoryAccessRule implements AccessRule {

	private static final int STOCK_THRESHOLD = 0;
	
	
	@Override
	public AccessPermission getPermission(final Adaptation record, final Session session, final SchemaNode node) {
		
		if (record.isSchemaInstance()) {
			return AccessPermission.getReadWrite();
		}
		
		Adaptation dataSet = record.getContainerTable().getContainerAdaptation();

		// administrators role are allowed to ignore this rule
		if (session.isUserInRole(Profile.ADMINISTRATOR)) {
			return AccessPermission.getReadWrite();
		}
	
		Inventory inventory = new Inventory(record);

		AccessPermission accessRights = AccessPermission.getReadWrite();

		int stockLevel = inventory.get_Stock();

		// add default access rights when no access rights are provided
		// if the value is 0 then any profile diferent from Administrator do not see the price.
		if (stockLevel <= STOCK_THRESHOLD) {
			accessRights = AccessPermission.getHidden();
		} 
		return accessRights;
	}
}
