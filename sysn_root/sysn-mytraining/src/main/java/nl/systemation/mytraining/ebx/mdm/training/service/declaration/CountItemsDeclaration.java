package nl.systemation.mytraining.ebx.mdm.training.service.declaration;

import com.orchestranetworks.schema.types.dataspace.DataspaceSet;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.declaration.ActivationContextOnTableView;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import com.orchestranetworks.userservice.declaration.UserServicePropertiesDefinitionContext;
import com.orchestranetworks.userservice.declaration.WebComponentDeclarationContext;
import nl.systemation.mytraining.ebx.mdm.training.service.CountItemsService;
import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;
import nl.systemation.training.ebx.mdm.commons.datamodel.Item;

public class CountItemsDeclaration implements UserServiceDeclaration.OnTableView {

    public static final ServiceKey SERVICE_KEY = ServiceKey.forName("CountItems");

    @Override
    public ServiceKey getServiceKey() {
        return CountItemsDeclaration.SERVICE_KEY;
    }

    @Override
    public UserService<TableViewEntitySelection> createUserService() {
        return new CountItemsService();
    }

    @Override
    public void defineActivation(ActivationContextOnTableView context) {
        context.includeAllDatasets();
        context.includeAllDataspaces(DataspaceSet.DataspaceType.BRANCH);
        context.includeSchemaNodesMatching(Item.getTablePath());
    }

    @Override
    public void defineProperties(UserServicePropertiesDefinitionContext context) {
        context.setDescription("Description");
        context.setLabel("Count Items");
    }

    @Override
    public void declareWebComponent(WebComponentDeclarationContext context) {
        context.setAvailableAsWorkflowUserTask(false);
        context.setAvailableAsPerspectiveAction(false);
    }
}
