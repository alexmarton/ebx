package nl.systemation.mytraining.ebx.mdm.training.ui.form;


import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.form.*;

import java.util.Locale;

public class StoreForm extends UIForm {

    @Override
    public void defineHeader(final UIFormHeader header, final UIFormContext context) {

        if (context.isCreatingRecord()) {
            header.setTitle(new UIFormLabelSpec("New Store"));
        } else {
            final Adaptation store = context.getCurrentRecord();
            final Locale locale = context.getLocale();
            final String storeLabel = store.getLabel(locale);

            header.setTitle(new UIFormLabelSpec("Store " + storeLabel));
        }
        header.removePagingPane();
    }

    @Override
    public void defineBody(final UIFormBody body, final UIFormContext context) {
        final UIFormPaneWithTabs tabs = new UIFormPaneWithTabs();

        tabs.addTab("Main", new StoreMainPane());
        tabs.addTab("Location", new StoreLocationPane());
        tabs.addTab("Inventories", new StoreInventoriesPane());
        body.setContent(tabs);
    }

    @Override
    public void defineBottomBar(final UIFormBottomBar bottomBar, final UIFormContext context) {
        super.defineBottomBar(bottomBar, context);
    }
}
