package nl.systemation.mytraining.ebx.mdm.training.function;

import java.math.BigDecimal;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

import nl.systemation.training.ebx.mdm.commons.datamodel.Inventory;


public class InventoryTotalFunction implements ValueFunction {

	@Override
	public void setup(final ValueFunctionContext context) {

	}

	@Override
	public Object getValue(final Adaptation inventoryRecord) {
		Inventory inventory = new Inventory(inventoryRecord);
		
		final Integer stock =  inventory.get_Stock();
		final BigDecimal price =  inventory.get_Price();
		if ((stock == null) || (price == null)) {
			return null;
		}
		return price.multiply(new BigDecimal(stock));
		
	}

}
