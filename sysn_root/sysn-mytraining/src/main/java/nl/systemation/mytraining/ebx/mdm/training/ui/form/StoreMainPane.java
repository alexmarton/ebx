package nl.systemation.mytraining.ebx.mdm.training.ui.form;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.form.UIFormContext;
import com.orchestranetworks.ui.form.UIFormPane;
import com.orchestranetworks.ui.form.UIFormPaneWriter;
import com.orchestranetworks.ui.form.widget.UIRadioButtonGroup;
import nl.systemation.training.ebx.mdm.commons.datamodel.Store;

public class StoreMainPane implements UIFormPane {
    public static final String CELL_STYLE="width:50%; padding:5px; vertical-align:top;";

    @Override
    public void writePane(final UIFormPaneWriter writer, final UIFormContext context) {
        writer.add("<table>");
        writer.add("<tr>");
        writer.add("<td").addSafeAttribute("style", CELL_STYLE).add(">");
        writer.startBorder(true, UserMessage.createInfo("Identifiers"));
        writer.startTableFormRow();
        writer.addFormRow(Store.getPath_Identifier());
        writer.addFormRow(Store.getPath_Name());
        writer.endTableFormRow();
        writer.endBorder();
        writer.add("</td>");

        final UIRadioButtonGroup radioButtonGroup = writer.newRadioButtonGroup(Store.getPath_Type());

        writer.add("<td").addSafeAttribute("style", CELL_STYLE).add(">");
        writer.startBorder(true, UserMessage.createInfo("Styles"));
        writer.addWidget(radioButtonGroup);
        writer.endBorder();
        writer.add("</td>");
        writer.add("</tr>");
        writer.add("</table>");

    }
}
