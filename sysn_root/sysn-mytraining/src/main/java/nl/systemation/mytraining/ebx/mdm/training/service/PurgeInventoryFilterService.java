package nl.systemation.mytraining.ebx.mdm.training.service;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.UIButtonSpec;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.UIFormLabelSpec;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.*;


public class PurgeInventoryFilterService implements DisplayStep {
    @Override
    public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> context, UserServiceDisplayConfigurator config) {
        config.setContent(this::writeContent);

        {
            final UIButtonSpec cancelButtonSpec = config.newCancelButton();

            config.setLeftButtons(cancelButtonSpec);
        }

        {
            final UIButtonSpecNavigation nextButtonSpec = config.newNextButton(this::onNextPressed);

            nextButtonSpec.setDefaultButton(true);
            config.setRightButtons(nextButtonSpec);
        }

    }

    private void writeContent(final UserServicePaneContext context, final UserServicePaneWriter writer) {
        writer.setCurrentObject(PurgeInventoryService._objectKey);

        {
            final UserMessage message = UserMessage.createInfo("This service removes inventory matching these criteria");
            final UIFormLabelSpec labelSpec = new UIFormLabelSpec(message);

            writer.add("<div").addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT).add(">");
            writer.addUILabel(labelSpec);
            writer.add("</div>");
        }

        writer.startTableFormRow();
        writer.addFormRow(PurgeInventoryService._startDate);
        writer.addFormRow(PurgeInventoryService._endDate);
        writer.endTableFormRow();
    }

    private UserServiceEventOutcome onNextPressed(final UserServiceEventContext context) {
        return EventOutcome.DISPLAY_CONFIRMATION;
    }
}
