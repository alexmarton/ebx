package nl.systemation.training.ebx.mdm.commonmodule.extension;

import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;


/*
 * SchemaExtensions are used to register access rules and user services
 * 
 * This class is run at startup, so restart EBX when this class is changed before the changes are executed.
 * 
 * You can only have 1 access rule for the same entity
 */


// rename this class to the data model that you want to use 
public class SampleExtensions implements SchemaExtensions {

	public void defineExtensions(final SchemaExtensionsContext aContext)
	{
		
		RegisterUserService.registerUserService(aContext);
	}

}

