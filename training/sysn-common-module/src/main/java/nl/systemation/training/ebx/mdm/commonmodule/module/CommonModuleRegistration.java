package nl.systemation.training.ebx.mdm.commonmodule.module;

import com.ebx5.commons.service.validationoverview.declaration.ValidationOverviewDataSetDeclaration;
import com.ebx5.commons.service.validationoverview.declaration.ValidationOverviewDataTableDeclaration;
import com.ebx5.commons.service.showlogfiles.ShowLogfilesDeclaration;
import com.ebx5.frameworkrt.service.declaration.ImportEbxAssetsDeclaration;
import com.orchestranetworks.module.ModuleContextOnRepositoryStartup;
import com.orchestranetworks.module.ModuleRegistrationServlet;
import com.orchestranetworks.module.ModuleServiceRegistrationContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

import  nl.systemation.training.ebx.mdm.commons.util.Constant;

public class CommonModuleRegistration extends ModuleRegistrationServlet {

	private static final long serialVersionUID = -1595820418565507933L;
	public static LoggingCategory log = null;
	
	@Override
	public void handleRepositoryStartup(ModuleContextOnRepositoryStartup context) throws OperationException {

		super.handleRepositoryStartup(context);
		log = context.getLoggingCategory();
		
		//CommonModuleRegistration.log.info("Common Module create data space and data set");

		//CreateDataSpaceDataSet.createDataSpaceDataSet(context);

		CommonModuleRegistration.log.info("Common Module module started");
	}
	
	@Override
	public void handleServiceRegistration(ModuleServiceRegistrationContext context) {

		super.handleServiceRegistration(context);
		
		context.registerUserService(new ImportEbxAssetsDeclaration(Constant.COMMON_MODULE));
		context.registerUserService(new ShowLogfilesDeclaration(Constant.COMMON_MODULE));

		context.registerUserService(new ValidationOverviewDataSetDeclaration(Constant.COMMON_MODULE, false));
		context.registerUserService(new ValidationOverviewDataTableDeclaration(Constant.COMMON_MODULE, false));

		//context.registerUserService(new SecurePropertyServiceDeclaration(Constant.COMMON_MODULE, Constant.PROCESS, Constant.PROCESS, Constant.MASTER_PW_LOCATION));
		//context.registerUserService(new UserReportServiceDeclaration(Constant.COMMON_MODULE));

		//context.registerUserService(new SampleServiceDeclaration(Constant.COMMON_MODULE));
	}
}