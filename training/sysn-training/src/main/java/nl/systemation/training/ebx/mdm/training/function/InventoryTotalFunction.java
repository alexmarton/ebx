package nl.systemation.training.ebx.mdm.training.function;

import java.math.BigDecimal;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import nl.systemation.training.ebx.mdm.training.Paths;

public class InventoryTotalFunction implements ValueFunction {

	@Override
	public void setup(final ValueFunctionContext context) {
		// Nothing to do
	}

	@Override
	public Object getValue(final Adaptation inventory) {
		final Integer stock = (Integer) inventory.get(Paths._Inventory._Stock);

		final BigDecimal price = (BigDecimal) inventory.get(Paths._Inventory._Price);

		if ((stock == null) || (price == null)) {
			return null;
		}

		final BigDecimal total = price.multiply(new BigDecimal(stock));

		return total;
	}
}
