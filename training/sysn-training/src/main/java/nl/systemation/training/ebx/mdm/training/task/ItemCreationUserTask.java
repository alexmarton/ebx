package nl.systemation.training.ebx.mdm.training.task;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.workflow.UserTask;
import com.orchestranetworks.workflow.UserTaskWorkItemCompletionContext;

public class ItemCreationUserTask extends UserTask {
	public static final String STEWARD = "steward";

	@Override
	public void handleWorkItemCompletion(final UserTaskWorkItemCompletionContext context) throws OperationException {
		final WorkItem workItem = context.getCompletedWorkItem();

		final UserReference userReference = workItem.getUserReference();

		final String userIdentifier = userReference.getUserId();

		context.setVariableString(STEWARD, userIdentifier);

		super.handleWorkItemCompletion(context);
	}
}
