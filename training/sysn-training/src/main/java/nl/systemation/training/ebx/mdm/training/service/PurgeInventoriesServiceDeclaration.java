package nl.systemation.training.ebx.mdm.training.service;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ServiceKey;
import nl.systemation.training.ebx.mdm.training.Paths;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.declaration.ActivationContextOnTableView;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import com.orchestranetworks.userservice.declaration.UserServicePropertiesDefinitionContext;
import com.orchestranetworks.userservice.declaration.WebComponentDeclarationContext;

public class PurgeInventoriesServiceDeclaration implements UserServiceDeclaration.OnTableView {
	public static final ServiceKey SERVICE_KEY = ServiceKey.forName("PurgeInventories");

	@Override
	public ServiceKey getServiceKey() {
		return SERVICE_KEY;
	}

	@Override
	public UserService<TableViewEntitySelection> createUserService() {
		return new PurgeInventoriesService();
	}

	@Override
	public void defineProperties(final UserServicePropertiesDefinitionContext context) {
		final UserMessage message = Messages.get("PurgeInventories");

		context.setLabel(message);
	}

	@Override
	public void defineActivation(final ActivationContextOnTableView context) {
		final Path path = Paths._Inventory.getPathInSchema();

		context.includeSchemaNodesMatching(path);
	}

	@Override
	public void declareWebComponent(final WebComponentDeclarationContext context) {
		// Nothing to do
	}
}
