package nl.systemation.training.ebx.mdm.training.permissions;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import nl.systemation.training.ebx.mdm.training.Paths;

public class InventoryPriceAccessRule implements AccessRule {
	public static final int INVENTORY_STOCK_THRESHOLD = 0;

	@Override
	public AccessPermission getPermission(final Adaptation adaptation, final Session session, final SchemaNode node) {
		if (adaptation.isSchemaInstance()) {
			return AccessPermission.getReadWrite();
		}

		if (session.isUserInRole(Role.ADMINISTRATOR)) {
			return AccessPermission.getReadWrite();
		}

		final Integer inventoryStock = (Integer) adaptation.get(Paths._Inventory._Stock);

		if (inventoryStock != null) {
			if (inventoryStock > INVENTORY_STOCK_THRESHOLD) {
				return AccessPermission.getReadWrite();
			}
		}

		return AccessPermission.getHidden();
	}
}