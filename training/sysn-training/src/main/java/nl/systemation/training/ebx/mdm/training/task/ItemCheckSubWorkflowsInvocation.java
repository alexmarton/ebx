package nl.systemation.training.ebx.mdm.training.task;

import java.math.BigDecimal;
import java.util.UUID;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.XPathExpressionHelper;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.OperationException;
import nl.systemation.training.ebx.mdm.training.Paths;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.SubWorkflowsCompletionContext;
import com.orchestranetworks.workflow.SubWorkflowsCreationContext;
import com.orchestranetworks.workflow.SubWorkflowsInvocationBean;

public class ItemCheckSubWorkflowsInvocation extends SubWorkflowsInvocationBean {
	public static final AdaptationName STORE_DATA_OWNER_SUBWORKFLOW_MODEL_NAME = AdaptationName
			.forName("StoreDataOwnerSubworkflowModel");

	public static final AdaptationName STORE_DATA_VALIDATOR_SUBWORKFLOW_MODEL_NAME = AdaptationName
			.forName("StoreDataValidatorSubworkflowModel");

	@Override
	public void handleCreateSubWorkflows(final SubWorkflowsCreationContext context) throws OperationException {
		final String dataSpaceName = context.getVariableString("dataSpace");
		final String dataSetName = context.getVariableString("dataSet");
		final String itemPath = context.getVariableString("item");
		final String itemPriceThresholdString = context.getVariableString("itemPriceThreshold");

		final Repository repository = context.getRepository();
		final AdaptationHome dataSpace = toDataSpace(repository, dataSpaceName);
		final Adaptation dataSet = toDataSet(dataSpace, dataSetName);
		final Adaptation item = toItem(dataSet, itemPath);

		final BigDecimal itemPriceThreshold = toItemPriceThreshold(itemPriceThresholdString);
		final BigDecimal itemPrice = (BigDecimal) item.get(Paths._Item._DefaultPrice);

		final boolean doubleChecking = (itemPriceThreshold == null) || (itemPriceThreshold.compareTo(itemPrice) <= 0);

		handleCreateSubWorkflows(context, dataSpaceName, dataSetName, itemPath, doubleChecking);
	}

	@Override
	public void handleCompleteAllSubWorkflows(final SubWorkflowsCompletionContext context) throws OperationException {
		// Nothing to do
	}

	private void handleCreateSubWorkflows(final SubWorkflowsCreationContext context, final String dataSpaceName,
			final String dataSetName, final String itemPath, final boolean doubleChecking) throws OperationException {
		{
			final ProcessLauncher launcher = context.registerSubWorkflow(STORE_DATA_OWNER_SUBWORKFLOW_MODEL_NAME,
					UUID.randomUUID().toString());

			launcher.setInputParameter("dataSpace", dataSpaceName);
			launcher.setInputParameter("dataSet", dataSetName);
			launcher.setInputParameter("item", itemPath);
		}

		if (doubleChecking) {
			final ProcessLauncher launcher = context.registerSubWorkflow(STORE_DATA_VALIDATOR_SUBWORKFLOW_MODEL_NAME,
					UUID.randomUUID().toString());

			launcher.setInputParameter("dataSpace", dataSpaceName);
			launcher.setInputParameter("dataSet", dataSetName);
			launcher.setInputParameter("item", itemPath);
		}

		context.launchSubWorkflows();
	}

	private static AdaptationHome toDataSpace(final Repository repository, final String dataSpaceName)
			throws OperationException {
		try {
			final HomeKey dataSpaceKey = HomeKey.forBranchName(dataSpaceName);

			final AdaptationHome dataSpace = repository.lookupHome(dataSpaceKey);

			if (dataSpace == null) {
				throw new IllegalArgumentException();
			}

			return dataSpace;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedDataSpaceNameIsIncorrect{0}",
					dataSpaceName);

			throw OperationException.createError(message);
		}
	}

	private static Adaptation toDataSet(final AdaptationHome dataSpace, final String dataSetName)
			throws OperationException {
		try {
			final AdaptationName dataSetKey = AdaptationName.forName(dataSetName);

			final Adaptation dataSet = dataSpace.findAdaptationOrNull(dataSetKey);

			if (dataSet == null) {
				throw new IllegalArgumentException();
			}

			return dataSet;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedDataSetNameIsIncorrect{0}",
					dataSetName);

			throw OperationException.createError(message);
		}
	}

	private static Adaptation toItem(final Adaptation dataSet, final String itemPath) throws OperationException {
		try {
			final Adaptation item = XPathExpressionHelper.lookupFirstRecordMatchingXPath(dataSet, itemPath);

			if (item == null) {
				throw new IllegalArgumentException();
			}

			return item;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedItemPathIsIncorrect{0}", itemPath);

			throw OperationException.createError(message);
		}
	}

	private static BigDecimal toItemPriceThreshold(final String itemPriceThresholdString) throws OperationException {
		try {
			if ((itemPriceThresholdString == null) || (itemPriceThresholdString.trim().isEmpty())) {
				return null;
			}

			return new BigDecimal(itemPriceThresholdString);
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedItemPriceThresholdIsIncorrect{0}",
					itemPriceThresholdString);

			throw OperationException.createError(message);
		}
	}
}
