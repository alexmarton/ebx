package nl.systemation.training.ebx.mdm.training.widget;

import java.math.BigDecimal;

import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.schema.SchemaTypeName;
import com.orchestranetworks.ui.form.widget.UIWidgetFactory;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetFactorySetupContext;

public class UIThresholdBoxFactory implements UIWidgetFactory<UIThresholdBox> {
	private BigDecimal threshold;

	private String image;

	public final void setThreshold(final BigDecimal threshold) {
		this.threshold = threshold;
	}

	public void setImage(final String image) {
		this.image = image;
	}

	@Override
	public void setup(final WidgetFactorySetupContext context) {
		{
			final SchemaNode schemaNode = context.getSchemaNode();

			final SchemaTypeName schemaTypeName = schemaNode.getXsTypeName();

			if ((!schemaTypeName.equals(SchemaTypeName.XS_DECIMAL))
					&& (!schemaTypeName.equals(SchemaTypeName.XS_INT))) {
				final UserMessage message = Messages.get(Severity.ERROR, "TheAttributeMustHaveANumberType");

				context.addMessage(message);
			}
		}
		
		if (this.threshold == null) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheThresholdIsMandatory");

			context.addMessage(message);
		}

		if (this.image == null) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheImageIsMandatory");

			context.addMessage(message);
		}
	}

	@Override
	public UIThresholdBox newInstance(final WidgetFactoryContext context) {
		return new UIThresholdBox(context, this.threshold, this.image);
	}
}
