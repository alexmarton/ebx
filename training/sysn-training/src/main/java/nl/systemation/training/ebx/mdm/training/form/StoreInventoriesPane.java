package nl.systemation.training.ebx.mdm.training.form;

import nl.systemation.training.ebx.mdm.training.Paths;
import com.orchestranetworks.ui.form.UIFormContext;
import com.orchestranetworks.ui.form.UIFormPane;
import com.orchestranetworks.ui.form.UIFormPaneWriter;

public class StoreInventoriesPane implements UIFormPane {

	@Override
	public void writePane(final UIFormPaneWriter writer, final UIFormContext context) {
		writer.addWidget(Paths._Store._Inventories);
	}
}
