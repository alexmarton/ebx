package nl.systemation.training.ebx.mdm.training.form;

import java.net.URLEncoder;
import java.text.MessageFormat;

import com.onwbp.adaptation.Adaptation;
import nl.systemation.training.ebx.mdm.training.Paths;
import com.orchestranetworks.ui.form.UIFormContext;
import com.orchestranetworks.ui.form.UIFormPane;
import com.orchestranetworks.ui.form.UIFormPaneWriter;

public class StoreLocationPane implements UIFormPane {
	public static final String ADDRESS_STYLE = "margin:10px";

	public static final String MAP_STYLE = "width:500px; height:375px; margin:10px";

	public static final String MAP_URL = "https://maps.google.com/maps?q={0}+{1}+{2}+{3}+{4}&output=embed";

	public static final String URL_ENCODING = "UTF-8";

	@Override
	public void writePane(final UIFormPaneWriter writer, final UIFormContext context) {
		writer.add("<div").addSafeAttribute("style", ADDRESS_STYLE).add(">");
		writer.startTableFormRow();
		writer.addFormRow(Paths._Store._Address_Street);
		writer.addFormRow(Paths._Store._Address_City);
		writer.addFormRow(Paths._Store._Address_State);
		writer.addFormRow(Paths._Store._Address_Postcode);
		writer.addFormRow(Paths._Store._Address_Country);
		writer.endTableFormRow();
		writer.add("</div>");

		final Adaptation store = context.getCurrentRecord();

		if (store == null) {
			return;
		}

		final String street = store.getString(Paths._Store._Address_Street);
		final String city = store.getString(Paths._Store._Address_City);
		final String state = store.getString(Paths._Store._Address_State);
		final String postcode = store.getString(Paths._Store._Address_Postcode);
		final String country = store.getString(Paths._Store._Address_Country);

		final String mapURL = MessageFormat.format(MAP_URL, encode(street), encode(city), encode(state),
				encode(postcode), encode(country));

		writer.add("<iframe").addSafeAttribute("style", MAP_STYLE).addSafeAttribute("src", mapURL).add(">");
		writer.add("</iframe>");
	}

	private static String encode(final String string) {
		try {
			return URLEncoder.encode(string, URL_ENCODING);
		} catch (final Exception exception) {
			return "";
		}
	}
}
