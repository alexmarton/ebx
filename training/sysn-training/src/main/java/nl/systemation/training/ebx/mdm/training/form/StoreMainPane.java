package nl.systemation.training.ebx.mdm.training.form;

import com.onwbp.base.text.UserMessage;
import nl.systemation.training.ebx.mdm.training.Paths;
import com.orchestranetworks.ui.form.UIFormContext;
import com.orchestranetworks.ui.form.UIFormPane;
import com.orchestranetworks.ui.form.UIFormPaneWriter;
import com.orchestranetworks.ui.form.widget.UIRadioButtonGroup;

public class StoreMainPane implements UIFormPane {
	public static final String CELL_STYLE = "width:50%; padding:5px; vertical-align:top";

	@Override
	public void writePane(final UIFormPaneWriter writer, final UIFormContext context) {
		writer.add("<table>");
		writer.add("<tr>");

		{
			final UserMessage message = Messages.get("Identifiers");

			writer.add("<td").addSafeAttribute("style", CELL_STYLE).add(">");
			writer.startBorder(true, message);
			writer.startTableFormRow();
			writer.addFormRow(Paths._Store._Identifier);
			writer.addFormRow(Paths._Store._Name);
			writer.endTableFormRow();
			writer.endBorder();
			writer.add("</td>");
		}

		{
			final UserMessage message = Messages.get("Type");

			final UIRadioButtonGroup radioButtonGroup = writer.newRadioButtonGroup(Paths._Store._Type);

			radioButtonGroup.setColumnsNumber(1);

			writer.add("<td").addSafeAttribute("style", CELL_STYLE).add(">");
			writer.startBorder(true, message);
			writer.addWidget(radioButtonGroup);
			writer.endBorder();
			writer.add("</td>");
		}

		writer.add("</tr>");
		writer.add("</table>");
	}
}
