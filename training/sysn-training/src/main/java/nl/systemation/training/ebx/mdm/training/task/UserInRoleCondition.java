package nl.systemation.training.ebx.mdm.training.task;

import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class UserInRoleCondition extends ConditionBean {
	private String userName;

	private String roleName;

	public final void setUserName(final String userName) {
		this.userName = userName;
	}

	public final void setRoleName(final String roleName) {
		this.roleName = roleName;
	}

	@Override
	public boolean evaluateCondition(final ConditionBeanContext context) throws OperationException {
		final UserReference user = toUser(this.userName);

		final Role role = toRole(this.roleName);

		final Repository repository = Repository.getDefault();

		final DirectoryHandler directoryHandler = DirectoryHandler.getInstance(repository);

		final boolean result = directoryHandler.isUserInRole(user, role);

		return result;
	}

	private static UserReference toUser(final String userName) throws OperationException {
		try {
			final UserReference userReference = UserReference.forUser(userName);

			return userReference;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedUserNameIsIncorrect{0}", userName);

			throw OperationException.createError(message);
		}
	}

	private static Role toRole(final String roleName) throws OperationException {
		try {
			final Role role = UserReference.forSpecificRole(roleName);

			return role;
		} catch (final Exception exception) {
			final UserMessage message = Messages.get(Severity.ERROR, "TheProvidedRoleNameIsIncorrect{0}", roleName);

			throw OperationException.createError(message);
		}
	}
}
