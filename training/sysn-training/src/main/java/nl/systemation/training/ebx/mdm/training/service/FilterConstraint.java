package nl.systemation.training.ebx.mdm.training.service;

import java.util.Date;
import java.util.Locale;

import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;

public class FilterConstraint implements Constraint<Object> {

	@Override
	public void setup(final ConstraintContext context) {
		// Nothing to do
	}

	@Override
	public String toUserDocumentation(final Locale locale, final ValueContext context) throws InvalidSchemaException {
		final String message = Messages.get(locale, "TheStartDateAndTheEndDateMustBeOrdered");

		return message;
	}

	@Override
	public void checkOccurrence(final Object value, final ValueContextForValidation context)
			throws InvalidSchemaException {
		final Date startDate = (Date) context.getValue(Path.PARENT.add(FilterPaths._startDate));

		final Date endDate = (Date) context.getValue(Path.PARENT.add(FilterPaths._endDate));

		if ((startDate == null) || (endDate == null)) {
			return;
		}

		if (startDate.compareTo(endDate) <= 0) {
			return;
		}

		{
			final UserMessage message = Messages.get(Severity.ERROR, "TheStartDateAndTheEndDateMustBeOrdered");

			context.addMessage(message);
		}
	}
}
