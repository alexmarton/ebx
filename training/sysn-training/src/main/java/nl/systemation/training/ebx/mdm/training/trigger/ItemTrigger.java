package nl.systemation.training.ebx.mdm.training.trigger;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import nl.systemation.training.ebx.mdm.training.Paths;

public class ItemTrigger extends TableTrigger {

	@Override
	public void setup(final TriggerSetupContext context) {
		// Nothing to do
	}

	@Override
	public void handleAfterDelete(final AfterDeleteOccurrenceContext occurrenceContext) throws OperationException {
		final RequestResult collectedInventories = collectInventories(occurrenceContext);

		final ProcedureContext procedureContext = occurrenceContext.getProcedureContext();

		deleteInventories(procedureContext, collectedInventories);
	}

	protected static RequestResult collectInventories(final AfterDeleteOccurrenceContext occurrenceContext) {
		final ValueContext valueContext = occurrenceContext.getOccurrenceContext();

		final int itemIdentifier = (Integer) valueContext.getValue(Paths._Item._Identifier);

		final String condition = Paths._Inventory._Item.format() + " = '" + itemIdentifier + "'";

		final Adaptation dataSet = valueContext.getAdaptationInstance();

		final AdaptationTable inventories = dataSet.getTable(Paths._Inventory.getPathInSchema());

		final RequestResult collectedInventories = inventories.createRequestResult(condition);

		return collectedInventories;
	}

	protected static void deleteInventories(final ProcedureContext procedureContext,
			final RequestResult collectedInventories) throws OperationException {
		try {
			for (Adaptation inventory; (inventory = collectedInventories.nextAdaptation()) != null;) {
				final AdaptationName inventoryName = inventory.getAdaptationName();

				procedureContext.doDelete(inventoryName, false);
			}
		} finally {
			collectedInventories.close();
		}
	}
}
