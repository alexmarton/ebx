package nl.systemation.training.ebx.mdm.training.widget;

import java.math.BigDecimal;
import java.text.MessageFormat;

import com.onwbp.base.misc.StringUtils;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.form.widget.UISimpleCustomWidget;
import com.orchestranetworks.ui.form.widget.WidgetDisplayContext;
import com.orchestranetworks.ui.form.widget.WidgetFactoryContext;
import com.orchestranetworks.ui.form.widget.WidgetWriter;

public class UIThresholdBox extends UISimpleCustomWidget {
	private static final String TABLE_STYLE = "background-image: url(''{0}''); background-repeat: no-repeat; background-position: left center; padding-left: 20px;";

	private static final String FORM_STYLE = "background-image: url(''{0}''); background-repeat: no-repeat; background-position: right center; padding-right: 20px;";

	private final BigDecimal threshold;

	private final String image;

	UIThresholdBox(final WidgetFactoryContext context, final BigDecimal threshold, final String image) {
		super(context);

		this.threshold = threshold;
		this.image = image;
	}

	@Override
	public void write(final WidgetWriter writer, final WidgetDisplayContext context) {
		final String style = getStyle(context);

		writer.add("<span").addSafeAttribute("style", style).add(">");
		writer.addWidget(Path.SELF);
		writer.add("</span>");
	}

	private String getStyle(final WidgetDisplayContext context) {
		final ValueContext valueContext = context.getValueContext();

		final Object value = valueContext.getValue(Path.SELF);

		if (value == null) {
			return StringUtils.EMPTY_STRING;
		}

		if (this.threshold.compareTo(new BigDecimal(value.toString())) < 0) {
			return StringUtils.EMPTY_STRING;
		}

		if (context.isDisplayedInTable()) {
			return MessageFormat.format(TABLE_STYLE, this.image);
		}

		if (context.isDisplayedInForm()) {
			return MessageFormat.format(FORM_STYLE, this.image);
		}

		return StringUtils.EMPTY_STRING;
	}
}
