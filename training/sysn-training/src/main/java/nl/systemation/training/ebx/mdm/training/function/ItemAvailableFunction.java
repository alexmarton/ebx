package nl.systemation.training.ebx.mdm.training.function;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;
import nl.systemation.training.ebx.mdm.training.Paths;

public class ItemAvailableFunction implements ValueFunction {

	public void setup(final ValueFunctionContext context) {
		// Nothing to do
	}

	public Object getValue(final Adaptation item) {
		final int itemIdentifier = item.get_int(Paths._Item._Identifier);

		final String condition = Paths._Inventory._Item.format() + " = " + "'" + itemIdentifier + "'" + " and "
				+ Paths._Inventory._Stock.format() + " > 0";
		final AdaptationTable items = item.getContainerTable();

		final Adaptation dataSet = items.getContainerAdaptation();

		final AdaptationTable inventories = dataSet.getTable(Paths._Inventory.getPathInSchema());

		final RequestResult result = inventories.createRequestResult(condition);

		return !result.isEmpty();
	}
}
