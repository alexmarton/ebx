package nl.systemation.training.ebx.mdm.training.module;

import com.orchestranetworks.module.ModuleContextOnRepositoryStartup;
import com.orchestranetworks.module.ModuleRegistrationServlet;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;

public class RegistrationServlet extends ModuleRegistrationServlet {

	private static final long serialVersionUID = -1595820418555507933L;
	public static LoggingCategory log = null;
	
	@Override
	public void handleRepositoryStartup(ModuleContextOnRepositoryStartup context) throws OperationException {

		super.handleRepositoryStartup(context);
		log = context.getLoggingCategory();
		
		RegistrationServlet.log.info("trn-ebx-training module started");
	}
	
	
}

