package nl.systemation.training.ebx.mdm.commons.util;

import com.orchestranetworks.service.Profile;
import com.orchestranetworks.service.Role;

/**
 *  Constant Class, this class contains all project constants like module names, fiel names, roles, workflow names, etc  
 *
 * @author Leen Buizert, Edwin van Asch
 */

public final class Constant {

	private Constant() {
		// do nothing
	}

	public static final String EBX_HOME = "ebx.home";
	public static final String EBX_PROPERTIES = "ebx.properties";
	public static final String COMMON_MODULE = "common-module";

	public static enum Roles {
		SAMPLE_ROLE("SAMPLE_ROLE");

		private final String role;

		private Roles(final String pRole) {
			this.role = pRole;
		}

		public Profile getProfile() {
			return Profile.forSpecificRole(this.role);
		}

		public Role getRole() {
			return Profile.forSpecificRole(this.role);
		}
	}

}
