package nl.systemation.training.ebx.mdm.commons.directory;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Element;

import com.ebx5.commons.util.Property;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.AuthenticationException;
import com.orchestranetworks.service.directory.DirectoryDefault;
import nl.systemation.training.ebx.mdm.commons.util.Constant;

/**
 * Class EbxDirectory extends the DirectoryDefault class
 * which is shipped with EBX. The class is extended because
 * users are authenticated using Active Directory/Kerberos tickets.
 * For authorizations the standard directory roles in EBX are still used
 * This class is instantiated from EbxDirectoryFactory
 *  
 * @author Leen Buizert
 *
 */
public class EbxDirectory extends DirectoryDefault
{

	private static boolean directoryDebug = false;
	private static EbxDirectory directoryInstance;
	private static Repository repository;
	
	private static final LoggingCategory kernelLog = LoggingCategory.getKernel();

	private static String ebxHome = null;
	
	private EbxDirectory(AdaptationHome home)
	{
		super(home);
		
		// set the debug logging on/off based on property in epo.properties
		directoryDebug = "Y".equalsIgnoreCase(Property.getProperty("directoryDebugEnabled")) ? true : false;
				
		EbxDirectory.repository = home.getRepository();
		
		EbxDirectory.ebxHome = System.getProperty(Constant.EBX_HOME);
		logMessage("Using ebxHome as:" + EbxDirectory.ebxHome + " and repository: " + EbxDirectory.repository.getDeclaration().getRepositoryLabel());
		
	}		
	
	public static EbxDirectory getInstance(AdaptationHome home)
	{
		if (directoryInstance == null)
		{
			directoryInstance = new EbxDirectory(home);
			logMessage("Using EbxDirectory");
		}
		return directoryInstance;
	}

	@Override
	public UserReference authenticateUserFromLoginPassword(String login, String password) {
		
		return super.authenticateUserFromLoginPassword(login, password);
	}

	@Override
	public UserReference authenticateUserFromHttpRequest(HttpServletRequest request) throws AuthenticationException {
		
		return super.authenticateUserFromHttpRequest(request);
	}

	@Override
	protected UserReference authenticateUserFromLoginPassword(String login, String password, HttpServletRequest request) {

		return super.authenticateUserFromLoginPassword(login, password, request);
	}

	@Override
	public UserReference authenticateUserFromSOAPHeader(Element header) {

		return super.authenticateUserFromSOAPHeader(header);
	}

	@Override
	public UserReference authenticateUserFromArray(Object[] args) {

		logMessage("authenticateUserFromArray: " + args.length);

		try {
		
			if (args != null && args.length > 0 && args[0] != null) {
				
				UserReference userReference = (UserReference) UserReference.forUser(args[0].toString());
				
				if (isUserDefined(userReference)) {
					return userReference;
				}
			}
			
			return null;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	private static void logMessage(String message) {

		if (directoryDebug) {
			getKernelLog().debug(message);
		}
	}

	public static LoggingCategory getKernelLog() {
		return kernelLog;
	}
}	


